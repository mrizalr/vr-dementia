﻿using Photon.Pun;
using UnityEngine;
using SandboxVRSeason2.Framework.Multiplayer;

[RequireComponent(typeof(Renderer))]
[RequireComponent(typeof(PhotonView))]

public class ChangeColor : MonoBehaviour
{
    private PhotonView photonView;

    Sandbox_BroadcastMethods broadcast;

    private void Start()
    {
        broadcast = GetComponent<Sandbox_BroadcastMethods>();

        this.photonView = this.GetComponent<PhotonView>();
        if (this.photonView.IsMine)
        {
            Color random = Random.ColorHSV();
            this.photonView.RPC(nameof(broadcast.ChangeColour), RpcTarget.AllBuffered, this.GetComponent<Renderer>(), new Vector3(random.r, random.g, random.b));
        }
    }
}
