using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Research
{
    [CreateAssetMenu(menuName = "Singleton/GameSettings")]
    public class GameSettings : SingletonScriptableObject<GameSettings>
    {
        [SerializeField]
        private string _gameVersion = "0.0.0";
        public string GameVersion { get { return _gameVersion; } }
        [SerializeField]
        private string _nickName = "Punfish";
        public string NickName
        {
            get
            {
                int value = Random.Range(0, 999);
                return _nickName + value.ToString();
            }
        }
    }
}