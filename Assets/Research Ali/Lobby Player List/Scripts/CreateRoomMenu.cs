using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

namespace Research
{
    public class CreateRoomMenu : MonoBehaviourPunCallbacks
    {
        [SerializeField]
        private TMPro.TextMeshProUGUI _roomNameTMP;

        private RoomsCanvases _roomsCanvases;

        public void FirstInitialize(RoomsCanvases canvases)
        {
            _roomsCanvases = canvases;
        }

        public void OnClick_CreateRoom()
        {
            if (!PhotonNetwork.IsConnected)
                return;

            RoomOptions options = new RoomOptions();
            options.BroadcastPropsChangeToAll = true;
            options.PublishUserId = true;
            options.MaxPlayers = 4;
            PhotonNetwork.JoinOrCreateRoom(_roomNameTMP.text, options, null);
        }

        public override void OnCreatedRoom()
        {
            print("Room created successfully");
            _roomsCanvases.CurrentRoomCanvas.Show();
        }

        public override void OnCreateRoomFailed(short returnCode, string message)
        {
            print("Cannot create room: " + message);
        }
    }
}