using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

namespace Research
{
    public class TestConnect : MonoBehaviourPunCallbacks
    {
        private void Start()
        {
            print("Connecting");
            PhotonNetwork.SendRate = 20;
            PhotonNetwork.SerializationRate = 5; //10
            PhotonNetwork.AutomaticallySyncScene = true;
            PhotonNetwork.NickName = MasterManager.GameSettings.NickName;
            PhotonNetwork.GameVersion = MasterManager.GameSettings.GameVersion;
            PhotonNetwork.ConnectUsingSettings();

        }

        public override void OnConnectedToMaster()
        {
            print("Connected");
            print(PhotonNetwork.LocalPlayer.NickName);

            if(!PhotonNetwork.InLobby)
                PhotonNetwork.JoinLobby();
        }

        public override void OnDisconnected(DisconnectCause cause)
        {
            print("Disconnected by " + cause.ToString());
        }
    }
}