using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Research
{
    public class CurrentRoomCanvas : MonoBehaviour
    {
        [SerializeField]
        private PlayerListingMenu _playerListingMenu;

        [SerializeField]
        private LeaveRoomMenu _leaveRoomMenu;
        public LeaveRoomMenu LeaveRoomMenu { get { return _leaveRoomMenu; } }

        private RoomsCanvases _roomsCanvases;

        public void FirstInitialize(RoomsCanvases canvases)
        {
            _roomsCanvases = canvases;
            _playerListingMenu.FirstInitialize(canvases);
            LeaveRoomMenu.FirstInitialize(canvases);
        }

        public void Show()
        {
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}