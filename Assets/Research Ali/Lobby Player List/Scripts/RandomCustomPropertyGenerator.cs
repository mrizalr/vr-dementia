using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

namespace Research
{
    public class RandomCustomPropertyGenerator : MonoBehaviour
    {
        [SerializeField]
        private TMPro.TextMeshProUGUI _textTMP;

        private ExitGames.Client.Photon.Hashtable _myCustomProperties = new ExitGames.Client.Photon.Hashtable();

        private void SetCustomNumber()
        {
            System.Random rnd = new System.Random();
            int result = rnd.Next(0, 99);

            _textTMP.text = result.ToString();

            _myCustomProperties["RandomNumber"] = result;
            //_myCustomProperties.Remove("RandomNumber");
            PhotonNetwork.SetPlayerCustomProperties(_myCustomProperties);
        }

        public void OnClick_Button()
        {
            SetCustomNumber();
        }
    }
}