using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Realtime;
using Photon.Pun;

namespace Research
{
    public class RoomListing : MonoBehaviour
    {
        [SerializeField]
        private TMPro.TextMeshProUGUI _textTMP;

        public RoomInfo RoomInfo { get; private set; }

        public void SetRoomInfo(RoomInfo roomInfo)
        {
            RoomInfo = roomInfo;
            _textTMP.text = roomInfo.MaxPlayers + ", " + roomInfo.Name;
        }

        public void OnClick_Button()
        {
            PhotonNetwork.JoinRoom(RoomInfo.Name);
        }
    }
}