using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Research
{
    public class InstantiateExample : MonoBehaviour
    {
        [SerializeField]
        private GameObject _prefab;

        private void Awake()
        {
            MasterManager.NetworkInstantiate(_prefab, new Vector3(Random.Range(-3f, 3f), Random.Range(-2f, 2f), transform.position.z), Quaternion.identity);
        }
    }
}