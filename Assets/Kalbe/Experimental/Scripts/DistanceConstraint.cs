using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DistanceConstraint : MonoBehaviour
{
    public float maxDistance;
    public Transform trackedTransform;

    public UnityEvent OnExceedLimit;

    [FoldoutGroup("Experimental")] public float minimumDistanceRatio;

    Vector3 direction;
    Vector3 trackedSelfPosition;
    Vector3 trackedTargetPosition;

    [Button]
    void Debugging()
    {
        trackedTransform.position = transform.position + direction.normalized * maxDistance * minimumDistanceRatio;
    }

    public void FixedUpdate()
    {
        if(trackedSelfPosition != transform.position)
        {
            trackedSelfPosition = transform.position;
            return;
        }

        if(trackedTargetPosition != trackedTransform.position)
        {
            direction = trackedTransform.position - transform.position;
            if (Vector3.Distance(trackedSelfPosition, trackedTransform.position) >= maxDistance)
            {
                Debug.Log("HUBLA");

                OnExceedLimit?.Invoke();

                trackedTransform.position = transform.position + direction.normalized * maxDistance * minimumDistanceRatio;
            }
        }
    }
}
