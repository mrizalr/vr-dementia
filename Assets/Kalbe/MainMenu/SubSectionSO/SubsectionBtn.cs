using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SubsectionBtn : MonoBehaviour
{
    public TextMeshProUGUI Number, Title, Status;
    public Image LockImage, Background;
}
