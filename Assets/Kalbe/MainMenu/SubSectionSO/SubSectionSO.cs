using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum Status
{
    Tutorial,
    Locked,
    Finish
}

[CreateAssetMenu(menuName = "MainMenu/SubSection")]
public class SubSectionSO : ScriptableObject
{
    public string Title, Description;
    public Sprite Image;
    public Status status;
    public GameObject SubsectionPrefabs;
}
