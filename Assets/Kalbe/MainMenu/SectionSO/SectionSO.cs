using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "MainMenu/Section")]
public class SectionSO : ScriptableObject
{
    public string SectionName, SectionDescription;
    public Sprite SectionImage;
    public List<SubSectionSO> SubsectionData;
}
