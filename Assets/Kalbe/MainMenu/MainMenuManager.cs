using SandboxVRSeason2.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Tictech.LoadManager;
using TMPro;
using System.Linq;

public class MainMenuManager : MonoBehaviour
{
    public GameObject SelectedSubSectionPrefabs;

    public Transform SectionPanel, SubsectionPanel;
    //public List<SectionSO> SectionData;
    public List<Sandbox_ScenarioData> ScenarioData;
    public Button SectionButton, SubsectionButton, ButtonMulai;
    public Image SectionImage;
    public TextMeshProUGUI SectionDescription;

    public Sprite ActiveButton, DeactiveButton;
    public Color32 SubsectionActiveColor, SubsectionDeactiveColor;

    private int activeSectionButtonIdx = -1, activeSubsectionButtonIdx = -1;
    [SerializeField] private List<Button> sectionButtonList = new List<Button>();
    [SerializeField] private List<Button> subsectionButtonList = new List<Button>();

    int selectedSectionId;
    int selectedSubSectionId;
    Sandbox_SubScenarioData targetSection;

    public GameObject panelFetchFailed;
    public TextMeshProUGUI errorMessageTMP;

    private void Awake()
    {
        GenerateSections();

        SectionButton.gameObject.SetActive(false);
        SubsectionButton.gameObject.SetActive(false);

        OnSectionButtonClick(0);
        ButtonMulai.GetComponent<SB_UIButton>().onDown.AddListener(delegate
        {
            print("Starting subsection " + SelectedSubSectionPrefabs.name);
            LoadScenario(true);
        });
    }

    public void RegenerateSection()
    {
        OnSectionButtonClick(0);
    }
    private void GenerateSections()
    {
        var i = 0;
        foreach (var section in ScenarioData)
        {
            var btn = Instantiate(SectionButton, SectionPanel);
            sectionButtonList.Add(btn);

            var idx = i++;
            btn.GetComponent<SB_UIButton>().onDown.AddListener(delegate { OnSectionButtonClick(idx); });

            var title = btn.transform.Find("Title").GetComponent<TextMeshProUGUI>();
            title.text = section.name;
        }
    }

    private void OnSectionButtonClick(int idx)
    {
        //foreach (var btn in sectionButtonList)
        //{
        //    var sbButton = btn.GetComponent<SB_UIButton>();
        //    sbButton.enabled = true;
        //}

        //sectionButtonList[idx].GetComponent<SB_UIButton>().enabled = false;
        print(Tictech.EnterpriseUniversity.EuRuntimeManager.Instance.User + "------user");
        if (Tictech.EnterpriseUniversity.EuRuntimeManager.Instance.User != null)
            SubsectionPanel.gameObject.SetActive(false);

        activeSubsectionButtonIdx = -1;
        subsectionButtonList.Clear();
        SelectedSubSectionPrefabs = null;
        CheckButtonMulai();

        if (activeSectionButtonIdx >= 0)
        {
            sectionButtonList[activeSectionButtonIdx].GetComponent<Image>().sprite = DeactiveButton;
        }

        activeSectionButtonIdx = idx;
        sectionButtonList[activeSectionButtonIdx].GetComponent<Image>().sprite = ActiveButton;
        SectionImage.sprite = ScenarioData[activeSectionButtonIdx].ScenarioSprite;
        SectionDescription.text = ScenarioData[activeSectionButtonIdx].ScenarioDescription;


        foreach (Transform ch in SubsectionPanel)
        {
            if (ch.name.Contains("Template")) continue;
            Destroy(ch.gameObject);
        }

        var i = 0;
        foreach (var sub in ScenarioData[idx].SubScenariolist)
        {
            var btn = Instantiate(SubsectionButton, SubsectionPanel);
            subsectionButtonList.Add(btn);

            var subsection = btn.GetComponent<SubsectionBtn>();

            var subsectionIdx = ++i;
            subsection.Title.text = sub.name;
            subsection.Number.text = subsectionIdx < 10 ? "0" + subsectionIdx.ToString() : subsectionIdx.ToString();
            subsection.Background.sprite = sub.SubScenarioSprite;
            subsection.LockImage.gameObject.SetActive(false);

            if (Tictech.EnterpriseUniversity.EuRuntimeManager.Instance.User == null)
                sub.status = Status.Finish;
            else
                sub.status = Status.Locked;

            switch (sub.status)
            {
                case Status.Tutorial:
                    subsection.Status.text = "Tutorial Simulasi";
                    break;
                case Status.Locked:
                    subsection.Status.text = "Terkunci";
                    subsection.LockImage.gameObject.SetActive(true);

                    btn.interactable = false;
                    btn.GetComponent<SB_UIButton>().enabled = false;
                    break;
                case Status.Finish:
                    subsection.Status.text = "Rekor Waktu 00:00";
                    break;
            }

            btn.name = "SubSectionButton";
            btn.gameObject.SetActive(true);
            btn.image.color = SubsectionDeactiveColor;

            var sbUiButton = btn.GetComponent<SB_UIButton>();
            sbUiButton.onDown.AddListener(delegate { OnSubsectionButtonClick(subsectionIdx - 1); });
        }

        if (Tictech.EnterpriseUniversity.EuRuntimeManager.Instance.User != null)
        {
            var firstSubsectionID = Tictech.EnterpriseUniversity.EuRuntimeManager.Instance.subScenarios.
                First(e => e.name == ScenarioData[idx].SubScenariolist[0].SubScenarioPrefab.name && e.scenario_id == idx + 1).id;

            Tictech.EnterpriseUniversity.EuRuntimeManager.Instance.GetTime(idx + 1, (times) =>
            {
                var listOfTimes = times.ToList();
                for (int i = 0; i <= subsectionButtonList.Count; i++)
                {
                    var btn = subsectionButtonList[i];
                    var subsection = btn.GetComponent<SubsectionBtn>();
                    subsection.LockImage.gameObject.SetActive(false);

                    btn.interactable = true;
                    btn.GetComponent<SB_UIButton>().enabled = true;

                    var first = listOfTimes.FirstOrDefault(e => e.sub_scenario_id == i + firstSubsectionID);
                    if (first == null)
                    {
                        subsection.Status.text = $"Rekor Waktu 00:00";
                        break;
                    }

                    var timeInt = int.Parse(first.time);
                    var minutes = timeInt / 60;
                    var minutesString = minutes < 10 ? "0" + minutes : minutes.ToString();
                    var seconds = timeInt % 60;
                    var secondsString = seconds < 10 ? "0" + seconds : seconds.ToString();

                    subsection.Status.text = $"Rekor Waktu {minutesString}:{secondsString}";
                }
                SubsectionPanel.gameObject.SetActive(true);
            }, (err) =>
            {
                errorMessageTMP.text = err;
                panelFetchFailed.SetActive(true);
            });
        }
    }

    private void OnSubsectionButtonClick(int subsectionIdx)
    {
        if(activeSubsectionButtonIdx >= 0)
        {
            subsectionButtonList[activeSubsectionButtonIdx].image.color = SubsectionDeactiveColor;
        }
        activeSubsectionButtonIdx = subsectionIdx;
        subsectionButtonList[activeSubsectionButtonIdx].image.color = SubsectionActiveColor;
        SelectedSubSectionPrefabs = ScenarioData[activeSectionButtonIdx].SubScenariolist[activeSubsectionButtonIdx].SubScenarioPrefab;
        targetSection = ScenarioData[activeSectionButtonIdx].SubScenariolist[activeSubsectionButtonIdx];
        CheckButtonMulai();
    }

    private void CheckButtonMulai()
    {
        ButtonMulai.interactable = SelectedSubSectionPrefabs != null;
        ButtonMulai.GetComponent<SB_UIButton>().enabled = SelectedSubSectionPrefabs != null;
    }

    void LoadScenario(bool isLinear, float timer = 9999)
    {
        RuntimeManager.LoadScenario(targetSection, isLinear, timer);
    }
}