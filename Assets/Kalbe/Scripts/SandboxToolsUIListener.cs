using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

public class SandboxToolsUIListener : MonoBehaviour
{
    public UnityEvent OnToolSelected;
    [HideInInspector] public string toolName;
    [HideInInspector] public int toolID;

    public bool stayActiveAfterInvoked = false;

    private void OnEnable()
    {
        SandboxToolsUIHandler.OnToolSelected += Checking;

        //LockAllToolSelection();
        //UnlockToolSelection();
    }

    private void OnDisable()
    {
        SandboxToolsUIHandler.OnToolSelected -= Checking;
    }

    void Checking(string id)
    {
        if (toolName == id && isActiveAndEnabled)
        {
            OnToolSelected?.Invoke();

            if (!stayActiveAfterInvoked)
            {
                gameObject.SetActive(false);
            }
        }
    }

    public void LockToolSelection()
    {
        SandboxToolsUIHandler.Instance.tools[toolID].lockSelection = true;
    }

    public void LockAllToolSelection()
    {
        SandboxToolsUIHandler.LockAll();
    }

    public void UnlockToolSelection()
    {
        SandboxToolsUIHandler.Instance.tools[toolID].lockSelection = false;
    }

    public void UnlockAllToolSelection()
    {
        SandboxToolsUIHandler.UnlockAll();
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(SandboxToolsUIListener)), CanEditMultipleObjects]
public class SandboxToolsUIListenerEditor : Editor
{
    SandboxToolsUIListener dst;

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        dst.toolID = EditorGUILayout.Popup("Tool Name", dst.toolID, SandboxToolsUIHandler.Instance.ToolNames);
        dst.toolName = SandboxToolsUIHandler.Instance.tools[dst.toolID].name;

        serializedObject.ApplyModifiedProperties();
    }

    private void OnEnable()
    {
        dst = (SandboxToolsUIListener)target;

        EditorUtility.SetDirty(dst);
    }
}
#endif