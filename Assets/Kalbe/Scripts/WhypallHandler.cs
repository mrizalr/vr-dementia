using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WhypallHandler : MonoBehaviour
{
    [SerializeField] private GameObject wypall;
    private Color32 cleanColor = new Color32(255,255,255,255);
    private Color32 dirtyColor = new Color32(108,93,93,255);

    private MeshRenderer meshRenderer;
    [SerializeField] private float dirtyPercentage;
    private void Start()
    {
        meshRenderer = wypall.GetComponent<MeshRenderer>();
    }

    public void AddDirtyValue(float value)
    {
        dirtyPercentage += value;
        meshRenderer.material.color = Color.Lerp(cleanColor, dirtyColor, dirtyPercentage/100);
    }

    public void Reset()
    {
        dirtyPercentage = 0;
        meshRenderer.material.color = cleanColor;
    }
}
