using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformModifier : MonoBehaviour
{
    public float delay;

    public void ApplyPositionToWithDelay(Transform target)
    {
        StartCoroutine(DelayApplyPositionTo(target));
    }

    public void ApplyRotationToWithDelay(Transform target)
    {
        StartCoroutine(DelayApplyRotationTo(target));
    }

    public void ApplyPositionTo(Transform target)
    {
        transform.position = target.position;
    }

    public void ApplyRotationTo(Transform target)
    {
        transform.eulerAngles = target.eulerAngles;
    }

    public IEnumerator DelayApplyPositionTo(Transform target)
    {
        yield return new WaitForSeconds(delay);
        ApplyPositionTo(target);
    }

    public IEnumerator DelayApplyRotationTo(Transform target)
    {
        yield return new WaitForSeconds(delay);
        ApplyRotationTo(target);
    }
}
