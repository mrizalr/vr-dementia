using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
using Sirenix.OdinInspector;
using DG.Tweening;
using UnityEditor;
using SandboxVRSeason2.Framework;


[ExecuteInEditMode]
public class CleaningTriggerMeshManager : MonoBehaviour
{
    public CleaningTriggerGroupController[] allTriggerGroups;
    public List<GameObject> allTriggers;

    [Button]
    private void EnableTriggerMeshAndApplyToPrefab()
    {
        StartCoroutine(Iterate(true, true));
    }
    [Button]
    private void DisableTriggerMeshAndApplyToPrefab()
    {
        StartCoroutine(Iterate(false, true));
    }
    public void EnableTriggerMesh()
    {
        StartCoroutine(Iterate(true, false));
    }
    public void DisableTriggerMesh()
    {
        StartCoroutine(Iterate(false, false));
    }

    public IEnumerator Iterate(bool enable, bool saveToPrefab)
    {
        for (int i = 0; i < allTriggers.Count; i++)
        {
            allTriggers[i].GetComponent<MeshRenderer>().enabled = enable;

            if (i % 50 == 0)
            {
                yield return null;
            }
        }
        #if UNITY_EDITOR
        if (saveToPrefab)
        {
            GameObject prefabRoot = transform.root.gameObject;
            PrefabUtility.ApplyPrefabInstance(prefabRoot, InteractionMode.UserAction);
        }
        #endif
        Debug.Log("mesh triggers iteration finished");
    }

}
