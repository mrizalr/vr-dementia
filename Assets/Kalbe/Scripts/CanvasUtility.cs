using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CanvasUtility : MonoBehaviour
{
    public Transform playerCamTransform;
    public float distanceFromPlayer;

    public UnityEvent OnCanvasShow;

    public virtual void Show()
    {
        Vector3 playerForward = playerCamTransform.forward;
        playerForward.y = 0f;
        playerForward = playerForward.normalized;

        transform.position = playerCamTransform.position + playerForward * distanceFromPlayer;
        transform.LookAt(playerCamTransform, Vector3.up);

        OnCanvasShow?.Invoke();
    }
}
