using DG.Tweening;
using HurricaneVR.Framework.Core.Player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialCanvasHandler : MonoBehaviour
{
    public float spawnOffset;

    Canvas canvasMain;
    Transform canvasCallTargetPos;
    Transform playerTransform;

    private void Start()
    {
        canvasMain = GetComponent<Canvas>();
        canvasCallTargetPos = GameObject.FindGameObjectsWithTag("UITargetPos")[0].transform;
        playerTransform = FindObjectOfType<HVRPlayerController>().transform;

        StartCoroutine(CallCanvasCoroutine());
    }

    public void CallCanvas()
    {
        StartCoroutine(CallCanvasCoroutine());
    }

    private IEnumerator CallCanvasCoroutine()
    {
        if (canvasMain)
        {
            transform.GetChild(0).gameObject.SetActive(true);
            Debug.Log("main - " + Camera.main);
            Sequence seq = DOTween.Sequence();
            seq.Append(canvasMain.transform.DOMove(canvasCallTargetPos.position - canvasCallTargetPos.forward * spawnOffset, 0.4f));
            yield return new WaitForSeconds(0.4f);
            seq.Insert(0, canvasMain.transform.DOLookAt(playerTransform.position, 0.4f, AxisConstraint.Y));
            yield return new WaitForSeconds(0.4f);
        }
    }
}
