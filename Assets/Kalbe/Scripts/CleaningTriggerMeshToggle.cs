using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class CleaningTriggerMeshToggle : MonoBehaviour
{
    public Sprite enableSprite;
    public Sprite disableSprite;
    private CleaningTriggerMeshManager manager;
    private bool on = false;

    // Start is called before the first frame update
    void Start()
    {
        manager = GameObject.FindObjectOfType<CleaningTriggerMeshManager>();
    }

    public void Toggle()
    {
        if (on)
        {
            manager.DisableTriggerMesh();
        }
        else
        {
            manager.EnableTriggerMesh();
        }

        GetComponent<Image>().sprite = on ? enableSprite : disableSprite;

        on = !on;
    }
}
