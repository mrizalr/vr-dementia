using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class SandboxToolsOverrider : MonoBehaviour
{
    public string toolName;
    public GameObject toolOverridePrefab;

    private SandboxToolsUIHandler _sandboxToolsUIHandler;
    private void Start()
    {
        if (toolName == "")
        {
            Debug.LogError($"tool name should not be empty : check {gameObject.name}");
            Debug.Break();

            return;
        }
        _sandboxToolsUIHandler = FindObjectOfType<SandboxToolsUIHandler>();
    }

    public void OverrideTools()
    {
        _sandboxToolsUIHandler.OverrideTool(toolName, toolOverridePrefab);
    }

    public void ResetOverrideTools()
    {
        _sandboxToolsUIHandler.OverrideTool(toolName, null);
    }
}
