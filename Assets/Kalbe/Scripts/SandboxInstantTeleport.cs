using HurricaneVR.Framework.Core.Player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SandboxInstantTeleport : MonoBehaviour
{
    public float teleportDuration;
    public Transform destinationTransform;

    float fadeDuration = 2f;
    Transform VRPlayerTransform;

    private void Start()
    {
        VRPlayerTransform = FindObjectOfType<HVRPlayerController>().transform.parent;
    }

    public void Teleport()
    {
        StartCoroutine(Teleporting());
    }

    IEnumerator Teleporting()
    {
        float delta = 1f / (60f * fadeDuration);

        //while(true)
        //{


        //    yield return null;
        //}

        VRPlayerTransform.position = destinationTransform.position;
        VRPlayerTransform.rotation = destinationTransform.rotation;

        yield return new WaitForSeconds(teleportDuration);

        //while(true)
        //{


        //    yield return null;
        //}

        yield break;
    }
}
