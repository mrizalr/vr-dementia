using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

public class SandboxToolsAction : MonoBehaviour
{
    [ReadOnly] public int toolID;

    [SerializeField] private UnityEvent OnToolTriggeredOn;
    [SerializeField] private UnityEvent OnToolTriggeredOff;

    public void OnGrab()
    {
        SandboxToolsUIHandler.Instance.tools[toolID].OnToolGrabbed?.Invoke();
    }

    public void ToolTriggerOn()
    {
        OnToolTriggeredOn?.Invoke();
    }

    public void ToolTriggerOff()
    {
        OnToolTriggeredOff?.Invoke();
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(SandboxToolsAction)), CanEditMultipleObjects]
public class SandboxToolsActionEditor : Editor
{
    SandboxToolsAction dst;

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        base.OnInspectorGUI();

        dst.toolID = EditorGUILayout.Popup("Tool Name", dst.toolID, SandboxToolsUIHandler.Instance.ToolNames);

        serializedObject.ApplyModifiedProperties();
    }

    private void OnEnable()
    {
        dst = (SandboxToolsAction)target;

        EditorUtility.SetDirty(dst);
    }
}
#endif