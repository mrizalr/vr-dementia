using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SetWelcomeMessage : MonoBehaviour
{
    private string username;

    public void SetMessage()
    {
        StartCoroutine(GetUsername());
    }

    private IEnumerator GetUsername()
    {
        while (Tictech.EnterpriseUniversity.EuRuntimeManager.Instance.User == null)
        {
            yield return new WaitForEndOfFrame();
        }

        username = Tictech.EnterpriseUniversity.EuRuntimeManager.Instance.User.first_name + " " + Tictech.EnterpriseUniversity.EuRuntimeManager.Instance.User.last_name;

        GetComponent<TextMeshProUGUI>().text = $"Selamat datang <#004D57>{username}<#FFFFFF>, kamu sudah berhasil login dan siap untuk bermain";
    }
}
