using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kalbe
{
    public class SoundEffectManager : MonoBehaviour
    {
        [System.Serializable]
        public struct SoundEffect
        {
            public string name;
            public AudioClip audioClip;
            public bool playOnAwake;
            public bool looping;
        }

        [SerializeField] private List<SoundEffect> soundEffects;

        private static Dictionary<string, AudioSource> audioMap = new Dictionary<string, AudioSource>();

        private void Start()
        {
            for (int i = 0; i < soundEffects.Count; i++)
            {
                var audioGO = new GameObject($"audioSource {soundEffects[i].name}");
                audioGO.transform.parent = transform;

                var _as = audioGO.AddComponent<AudioSource>();
                _as.clip = soundEffects[i].audioClip;
                _as.loop = soundEffects[i].looping;

                if (soundEffects[i].playOnAwake)
                {
                    _as.Play();
                }

                audioMap[soundEffects[i].name] = _as;
            }

            FindObjectOfType<HurricaneVR.Framework.Core.Player.HVRTeleporter>().PositionUpdate.AddListener(delegate { Play("teleport"); });
        }

        public static void Play(string audioName)
        {
            if (!audioMap.ContainsKey(audioName) || audioMap[audioName] == null) return;
            audioMap[audioName].Play();
        }

    }
}
