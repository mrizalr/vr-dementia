using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SandboxVRSeason2.Framework;

public class SampahSpawner : MonoBehaviour
{
    private GameObject objectAsli, objectSampah;
    public void PrepareSampah(Collider collider)
    {
        objectAsli = collider.gameObject;
        OnHandReleased(objectAsli.GetComponent<SB_Item>());
    }
    public void OnHandReleased(SB_Item item)
    {
        item.onHandReleased.AddListener(() => { SpawnSampah(); });
    }
    public void SpawnSampah()
    {
        objectSampah = Instantiate(objectAsli, objectAsli.transform.position, objectAsli.transform.rotation);
        objectSampah.name = "sampah";

        var components = objectSampah.GetComponents(typeof(Component));
        foreach (var component in components)
        {
            if (!(component is MeshFilter) && !(component is MeshRenderer))
            {
                Destroy(component);
            }
        }

        objectAsli.gameObject.SetActive(false);

        objectAsli.GetComponent<SB_Item>().onHandReleased.RemoveListener(() => { SpawnSampah(); });
    }
}
