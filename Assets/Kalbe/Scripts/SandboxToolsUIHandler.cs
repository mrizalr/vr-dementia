using SandboxVRSeason2.Framework;
using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SandboxToolsUIHandler : CanvasUtility
{
    public List<GameObject> allToolsOnScene; //untuk limit jumlah semua tools di scene
    public int limitAmountToolsOnScene = 2; //untuk limit jumlah semua tools di scene
    static SandboxToolsUIHandler instance;

    public static SandboxToolsUIHandler Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<SandboxToolsUIHandler>(true);
            }
            return instance;
        }
    }

    public static Action<string> OnToolSelected;

    public Transform spawnTransform;
    public Transform toolParent;
    public TMP_Text pageDisplay;
    public GameObject UI;
    [SerializeField] public List<ToolProperties> tools;
    public List<GameObject> pages;

    [ReadOnly] public int pageIndex = 0;

    public string[] ToolNames
    {
        get
        {
            string[] result;

            if (tools.Count > 0)
            {
                result = new string[tools.Count];
                for (int i = 0; i < result.Length; i++)
                {
                    result[i] = tools[i].name;
                }
            }
            else
            {
                result = new string[] { "<NONE>" };
            }

            return result;
        }
    }

    public void Start()
    {
        pageIndex = 0;
        Hide();

        if (limitAmountToolsOnScene <= 0) limitAmountToolsOnScene = 2; //assign value limit awal
    }

    public override void Show()
    {
        base.Show();

        pageIndex = 0;
        MovePage(0);

        UI.SetActive(true);
    }

    public void Hide()
    {
        pages[pageIndex].SetActive(false);

        UI.SetActive(false);
    }

    public void MovePage(int increment)
    {
        foreach (var p in pages)
        {
            p.SetActive(false);
        }

        Debug.Log(pageIndex);
        pageIndex += increment;
        if (pageIndex >= pages.Count)
        {
            pageIndex = 0;
        }
        else if (pageIndex < 0)
        {
            pageIndex = pages.Count - 1;
        }
        pageDisplay.text = $"{pageIndex + 1}";
        Debug.Log(pageIndex);

        pages[pageIndex].SetActive(true);
    }

    public static void SpawnTool(int id)
    {
        if (id < 0) return;
        if (Instance.tools[id] == null) return;

        GameObject tmp;

        tmp = Instance.tools[id].GetTool();

        if (tmp != null)
        {
            tmp.transform.parent = Instance.toolParent;
            tmp.transform.position = Instance.spawnTransform.position;
            tmp.transform.rotation = Quaternion.identity;
        }

        OnToolSelected?.Invoke(Instance.tools[id].name);

        //- masukkan tool yg akan dispawn ke list
        //- jika lebih dari limit, list index ke 0 (tool paling awal di spawn) akan di destroy
        if (tmp != null)
        {
            if (tmp.GetComponent<DontDestroyThisTool>()) return;

            Instance.allToolsOnScene.Add(tmp);
            if (Instance.allToolsOnScene.Count > Instance.limitAmountToolsOnScene)
            {
                Destroy(Instance.allToolsOnScene[0]);
                Instance.allToolsOnScene.Remove(Instance.allToolsOnScene[0]);
            }
        }
    }



    public static void LockAll()
    {
        foreach (var t in Instance.tools)
        {
            t.lockSelection = true;
        }
    }

    public static void UnlockAll()
    {
        foreach (var t in Instance.tools)
        {
            t.lockSelection = false;
        }
    }

    public void OverrideTool(string toolName, GameObject toolPrefab)
    {
        foreach (var t in tools)
        {
            if (t.name == toolName)
            {
                t.overrideToolPrefab = toolPrefab;
                //OverrideActiveTool(t);
            }
        }
    }

    public void OverrideActiveTool(ToolProperties t)
    {
        if (!t.IsToolActive()) return;

        var currentTool = t.GetActiveTool();
        var overrideTool = t.SetToolPrefab();

        overrideTool.transform.position = currentTool.transform.position;
        overrideTool.transform.eulerAngles = currentTool.transform.eulerAngles;
        overrideTool.transform.parent = currentTool.transform.parent;
        t.SetPool(overrideTool);

        Destroy(currentTool);

    }
}

[System.Serializable]
public class ToolProperties
{
    [ReadOnly] public bool lockSelection;
    public string name;
    public int maxNumber;
    public GameObject toolPrefab;
    public bool alwaysCreateNew;
    public SandboxToolsAction currentActive;
    public Action OnToolGrabbed;
    public Action OnToolTriggeredOn;

    [HideInInspector] public GameObject overrideToolPrefab;
    public GameObject SetToolPrefab()
    {
        return MonoBehaviour.Instantiate(overrideToolPrefab != null ? overrideToolPrefab : toolPrefab);
    }

    int lastIndex = 0;
    List<GameObject> pool = new List<GameObject>();

    public bool IsToolActive()
    {
        return pool.Count > 0;
    }

    public GameObject GetActiveTool()
    {
        return pool[lastIndex];
    }

    public void SetPool(GameObject obj)
    {
        pool[lastIndex] = obj;
    }

    public GameObject GetTool()
    {
        GameObject result = null;

        if (lockSelection) return result;

        if (pool.Count >= maxNumber)
        {
            if (++lastIndex >= pool.Count)
            {
                lastIndex = 0;
            }

            if (alwaysCreateNew)
            {
                var items = pool[lastIndex].GetComponentsInChildren<SB_Item>();
                foreach (var i in items)
                {
                    i.ItemUnSocketed(null, null);
                }

                GameObject.Destroy(pool[lastIndex]);
                pool[lastIndex] = SetToolPrefab();
            }

            GameObject.Destroy(pool[lastIndex]);
            pool[lastIndex] = SetToolPrefab();
            result = pool[lastIndex];
        }
        else
        {
            if (toolPrefab != null)
            {
                result = SetToolPrefab();
                pool.Add(result);
            }
        }

        if (result != null)
        {
            currentActive = result.GetComponent<SandboxToolsAction>();
            //currentActive.ToolTriggerOff();

            result.SetActive(true);
            return result;
        }
        else
        {
            return null;
        }
    }
}
