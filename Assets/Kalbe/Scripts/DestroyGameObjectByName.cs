using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyGameObjectByName : MonoBehaviour
{
    public void DestroyByName(string name)
    {
        Destroy(GameObject.Find(name));
    }
}
