using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlovesToolsHandler : MonoBehaviour
{
    private ChangeGloves changeGloves;
    private void Start()
    {
        changeGloves = FindObjectOfType<ChangeGloves>();
    }

    public void OnGlovesGrabbed()
    {
        if (!changeGloves.isUsable()) return;

        changeGloves.Change();
        gameObject.SetActive(false);
    }
}
