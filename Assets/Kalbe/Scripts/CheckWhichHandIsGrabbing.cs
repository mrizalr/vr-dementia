using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using HurricaneVR.Framework;
using HurricaneVR.Framework.Core.Grabbers;
using HurricaneVR.Framework.Shared;
using Sirenix.OdinInspector;

public class CheckWhichHandIsGrabbing : MonoBehaviour
{
    [ReadOnly] public HVRHandSide currentGrabbing;
    [ReadOnly] public bool isCurrentlyGrabbing;
    public UnityEvent onOtherHandHoverEnter;
    public UnityEvent onOtherHandHoverExit;

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<HVRHandGrabber>())
        {
            if (other.GetComponent<HVRHandGrabber>().HandSide != currentGrabbing)
            {
                onOtherHandHoverEnter?.Invoke();
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<HVRHandGrabber>())
        {
            if (other.GetComponent<HVRHandGrabber>().HandSide != currentGrabbing)
            {
                onOtherHandHoverExit?.Invoke();
            }
        }
    }

    public void SetCurrentGrabbing(HVRHandGrabber grabber)
    {
        currentGrabbing = grabber.HandSide;
        isCurrentlyGrabbing = true;
    }
    public void ResetCurrentGrabbing()
    {
        isCurrentlyGrabbing = false;
    }


}
