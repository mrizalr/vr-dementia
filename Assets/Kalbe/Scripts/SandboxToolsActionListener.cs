using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

public class SandboxToolsActionListener : MonoBehaviour
{
    [HideInInspector] public string toolName;
    [HideInInspector] public int toolID;

    public bool stayActiveAfterInvoked = false;

    public UnityEvent OnToolGrabbed;

    private void OnEnable()
    {
        if(OnToolGrabbed!=null)
        {
            SandboxToolsUIHandler.Instance.tools[toolID].OnToolGrabbed += OnGrab;
        }
    }

    private void OnDisable()
    {
        if (OnToolGrabbed != null)
        {
            SandboxToolsUIHandler.Instance.tools[toolID].OnToolGrabbed -= OnGrab;
        }
    }

    public void OnGrab()
    {
        if(isActiveAndEnabled)
        {
            OnToolGrabbed?.Invoke();

            if(!stayActiveAfterInvoked)
            {
                gameObject.SetActive(false);
            }
        }
    }

    public void ToolTriggerOn()
    {
        if(isActiveAndEnabled)
        {
            SandboxToolsUIHandler.Instance.tools[toolID].currentActive.ToolTriggerOn();

            if (!stayActiveAfterInvoked)
            {
                gameObject.SetActive(false);
            }
        }
    }

    public void ToolTriggerOff()
    {
        if (isActiveAndEnabled)
        {
            SandboxToolsUIHandler.Instance.tools[toolID].currentActive.ToolTriggerOff();

            if (!stayActiveAfterInvoked)
            {
                gameObject.SetActive(false);
            }
        }
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(SandboxToolsActionListener)), CanEditMultipleObjects]
public class SandboxToolsActionListenerEditor : Editor
{
    SandboxToolsActionListener dst;

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        dst.toolID = EditorGUILayout.Popup("Tool Name", dst.toolID, SandboxToolsUIHandler.Instance.ToolNames);
        dst.toolName = SandboxToolsUIHandler.Instance.tools[dst.toolID].name;

        serializedObject.ApplyModifiedProperties();
    }

    private void OnEnable()
    {
        dst = (SandboxToolsActionListener)target;

        EditorUtility.SetDirty(dst);
    }
}
#endif