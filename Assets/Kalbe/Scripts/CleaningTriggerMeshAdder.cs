using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
using Sirenix.OdinInspector;
using DG.Tweening;
using UnityEditor;
using SandboxVRSeason2.Framework;

[ExecuteInEditMode]
public class CleaningTriggerMeshAdder : MonoBehaviour
{
    public Material cleanTriggerMaterial;
    public GameObject cleaningTriggerMeshManagerPrefab;
    [ReadOnly] public CleaningTriggerGroupController[] allTriggerGroups;
    [ReadOnly] public List<GameObject> allTriggers;

    [Button]
    public void AddMesh()
    {
        //init
        System.Array.Clear(allTriggerGroups, 0, 0);
        allTriggers.Clear();

        //persiapan apply ke prefab
        GameObject essentialComponents = GameObject.Find("-----Essentials Components");
        //Object prefabParent = PrefabUtility.GetCorrespondingObjectFromSource(essentialComponents);
        //string path = AssetDatabase.GetAssetPath(prefabParent);
        GameObject prefabRoot = essentialComponents.transform.root.gameObject;

        //buat template sphere
        GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        MeshFilter sphereMesh = sphere.GetComponent<MeshFilter>();

        //ambil semua cleaning trigger group
        allTriggerGroups = GameObject.FindObjectsOfType<CleaningTriggerGroupController>(true);

        //ambil semua trigger
        for (int i = 0; i < allTriggerGroups.Length; i++)
        {
            for (int j = 0; j < allTriggerGroups[i].transform.childCount; j++)
            {
                allTriggers.Add(allTriggerGroups[i].transform.GetChild(j).gameObject);
            }
        }

        //tambahkan mesh pada setiap trigger
        foreach (GameObject trigger in allTriggers)
        {
            if (trigger.GetComponent<MeshFilter>() == null)
                trigger.AddComponent<MeshFilter>();
            if (trigger.GetComponent<MeshRenderer>() == null)
                trigger.AddComponent<MeshRenderer>();

            trigger.GetComponent<MeshFilter>().sharedMesh = sphereMesh.sharedMesh;
            trigger.GetComponent<MeshRenderer>().material = cleanTriggerMaterial;
            trigger.GetComponent<MeshRenderer>().enabled = false;

            trigger.transform.localScale = Vector3.Scale(trigger.transform.localScale, trigger.GetComponent<BoxCollider>().size);
            trigger.GetComponent<BoxCollider>().size = Vector3.one;
        }

        //hapus template sphere
        DestroyImmediate(sphere);

        //masukkan komponen mesh trigger manager
        GameObject manager = Instantiate(cleaningTriggerMeshManagerPrefab, essentialComponents.transform.position, essentialComponents.transform.rotation, essentialComponents.transform);
        manager.name = "CleaningTriggerMeshManager";

        //set referensi semua trigger ke manager
        var component = manager.GetComponent<CleaningTriggerMeshManager>();
        component.allTriggerGroups = allTriggerGroups;
        component.allTriggers = allTriggers;

        //apply perubahan
        #if UNITY_EDITOR
            PrefabUtility.ApplyPrefabInstance(prefabRoot, InteractionMode.UserAction);
        #endif
    }
}
