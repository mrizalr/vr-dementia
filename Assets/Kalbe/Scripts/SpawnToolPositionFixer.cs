using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
using Sirenix.OdinInspector;
using DG.Tweening;
using UnityEditor;
using SandboxVRSeason2.Framework;

[ExecuteInEditMode]
public class SpawnToolPositionFixer : MonoBehaviour
{
    [Button]
    public void Fix()
    {
        #if UNITY_EDITOR
            ParentConstraint spawnTransform = GameObject.Find("SpawnTransform").GetComponent<ParentConstraint>();
            Transform toolCanvas = GameObject.Find("ToolsCanvas").transform;
            GameObject template = new GameObject();
            GameObject anchor = Instantiate(template, toolCanvas.transform.position + new Vector3(0, 0, 0.25f), spawnTransform.transform.rotation, toolCanvas);
            anchor.name = "SpawnTranform anchor";
            DestroyImmediate(template);

            Object prefabParent = PrefabUtility.GetCorrespondingObjectFromSource(spawnTransform);
            string path = AssetDatabase.GetAssetPath(prefabParent);
            PrefabUtility.ApplyAddedGameObject(anchor, path, InteractionMode.UserAction);

            ConstraintSource cs = new ConstraintSource();
            cs.sourceTransform = anchor.transform;
            cs.weight = 1;

            Debug.Log("prepare");

            spawnTransform.locked = false;

            if (spawnTransform.sourceCount > 0)
            {
                for (int i = 0; i < spawnTransform.sourceCount; i++)
                {
                    spawnTransform.RemoveSource(i);
                }
            }

            Debug.Log("remove source");

            spawnTransform.AddSource(cs);

            Debug.Log("add source");
            //spawnTransform.transform.localPosition = new Vector3(0, 1.75f, 0.25f);
            //spawnTransform.translationAtRest = new Vector3(0, 1.75f, 0.25f);
            //spawnTransform.rotationAtRest = Vector3.zero;
            //spawnTransform.SetTranslationOffset(0, new Vector3(0, 1.75f, 0.25f));
            //spawnTransform.SetRotationOffset(0, Vector3.zero);

            Debug.Log("set offsets");

            spawnTransform.constraintActive = true;

            spawnTransform.locked = true;
            Debug.Log("lock");

            //PrefabUtility.ApplyObjectOverride(spawnTransform, path, InteractionMode.UserAction);
            GameObject prefabRoot = spawnTransform.transform.root.gameObject;
        
            PrefabUtility.ApplyPrefabInstance(prefabRoot, InteractionMode.UserAction);
        #endif
    }
}
