using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class HoldButtonHandler : MonoBehaviour
{
    private IndicatorBar indicatorBar;
    [SerializeField] private bool isHold;

    public UnityEvent OnFinishHold;

    private void Start()
    {
        indicatorBar = GetComponentInChildren<IndicatorBar>();
        isHold = false;
    }

    private void Update()
    {
        if (indicatorBar.GetValue() >= 100)
        {
            this.enabled = false;
            OnFinishHold.Invoke();
        }
        if (!isHold) return;

        indicatorBar.AddValIndicator(.1f);
    }



    public void SetIsHolding(bool value)
    {
        isHold = value;
    }
}
