using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Linq;
using TMPro;
using Sirenix.OdinInspector;
using SandboxVRSeason2.Framework;

public class QuizManager : MonoBehaviour
{
    [System.Serializable]
    public struct QuizAnswer
    {
        public string answerText;
        public bool isCorrect;
    }

    [System.Serializable]
    public struct QuizAnswerImage
    {
        public Sprite image;
        public bool isCorrect;
    }

    [System.Serializable]
    public class Quiz
    {
        public string question;
        public Sprite quizImage;
        public bool isImageAnswer;

        [HideIf("isImageAnswer")]
        public QuizAnswer[] answers = new QuizAnswer[4];

        [ShowIf("isImageAnswer")]
        public QuizAnswerImage[] answerImages = new QuizAnswerImage[4];
    }
    [SerializeField]
    string name;
    public List<Quiz> quizList;
    public int selectQuizCount;
    public TextMeshProUGUI questionTMP, quizCount;
    public Image questionImage;

    public QuizAnswerButton[] quizButton = new QuizAnswerButton[4];
    public QuizProgressBar progressBar;

    public List<Quiz> selectedQuiz;

    public int score = 0;
    private int currentQuestionIndex = 0;
    private bool allowClicked = true;

    public UnityEvent OnQuizCompleted;
    public UnityEvent OnNextQuiz;

    Sandbox_ScenarioManager.QuizResult quiz;

    private void Start()
    {
        SelectRandomQuiz();
        RandomingAnswer();

        SetQuiz();
    }

    private void SetQuiz()
    {
        quiz = new Sandbox_ScenarioManager.QuizResult();
        quiz.name = name;
        quiz.total = selectQuizCount;

        if (currentQuestionIndex == selectedQuiz.Count)
        {
            OnQuizCompleted.Invoke();
            return;
        }

        quizCount.text = currentQuestionIndex+1 + "/" + selectedQuiz.Count;
        progressBar.SetActiveBar(currentQuestionIndex);

        var isUsingImage = selectedQuiz[currentQuestionIndex].quizImage != null;
        questionTMP.text = selectedQuiz[currentQuestionIndex].question;

        questionTMP.transform.GetComponent<ContentSizeFitter>().verticalFit = isUsingImage ? ContentSizeFitter.FitMode.MinSize : ContentSizeFitter.FitMode.PreferredSize;
        LayoutRebuilder.ForceRebuildLayoutImmediate(questionTMP.transform.GetComponentInParent<RectTransform>());

        if (isUsingImage) questionImage.sprite = selectedQuiz[currentQuestionIndex].quizImage;
        questionImage.gameObject.SetActive(isUsingImage);
        questionImage.preserveAspect = true;

        for (int i = 0; i < quizButton.Length; i++)
        {
            quizButton[i].gameObject.SetActive(true);
            if (selectedQuiz[currentQuestionIndex].isImageAnswer)
            {
                var images = selectedQuiz[currentQuestionIndex].answerImages;
                if (i >= images.Length)
                {
                    quizButton[i].gameObject.SetActive(false);
                    return;
                }

                quizButton[i].SetImage(images[i].image);
                continue;
            }

            var answers = selectedQuiz[currentQuestionIndex].answers;
            if (i >= answers.Length)
            {
                quizButton[i].gameObject.SetActive(false);
                return;
            }
            quizButton[i].SetText(answers[i].answerText);
        }
    }

    public void SetScoreUI(TextMeshProUGUI tmp)
    {
        quiz.value = score;
        Sandbox_ScenarioManager.instance.quizList.Add(quiz);

        tmp.text = score + "/" + selectQuizCount;
    }

    private void SelectRandomQuiz()
    {
        List<int> randomIndex = new List<int>();
        List<int> indices = new List<int>();
        for (int i = 0; i < quizList.Count; i++)
        {
            indices.Add(i);
        }

        while (indices.Count > quizList.Count - selectQuizCount)
        {
            var rand = Random.Range(0, indices.Count);
            randomIndex.Add(indices[rand]);
            indices.RemoveAt(rand);
        }

        foreach (var idx in randomIndex)
        {
            selectedQuiz.Add(quizList[idx]);
        }
    }

    private void RandomingAnswer()
    {
        foreach (var question in selectedQuiz)
        {
            if (question.isImageAnswer)
            {
                var randomAnswer = new List<QuizAnswerImage>();
                var answerList = question.answerImages.ToList();
                while (answerList.Count > 0)
                {
                    var rand = Random.Range(0, answerList.Count);
                    randomAnswer.Add(answerList[rand]);
                    answerList.RemoveAt(rand);
                }
                question.answerImages = randomAnswer.ToArray();
            }
            else
            {
                var randomAnswer = new List<QuizAnswer>();
                var answerList = question.answers.ToList();
                while (answerList.Count > 0)
                {
                    var rand = Random.Range(0, answerList.Count);
                    randomAnswer.Add(answerList[rand]);
                    answerList.RemoveAt(rand);
                }
                question.answers = randomAnswer.ToArray();
            }
        }
    }

    public void SetNextQuestion(QuizAnswerButton answerButton)
    {
        if (!allowClicked) return;
        allowClicked = false;
        answerButton.OnClickedAnswer();
        StartCoroutine(DelayedSetNextQuestion(answerButton));
    }


    private IEnumerator DelayedSetNextQuestion(QuizAnswerButton answerButton)
    {
        yield return new WaitForSecondsRealtime(1f);
        int id = answerButton.GetID();
        bool isCorrectAnswer = selectedQuiz[currentQuestionIndex].isImageAnswer ? selectedQuiz[currentQuestionIndex].answerImages[id].isCorrect : selectedQuiz[currentQuestionIndex].answers[id].isCorrect;

        if (isCorrectAnswer) score++;
        currentQuestionIndex++;
        SetQuiz();
        answerButton.Reset();
        allowClicked = true;
        OnNextQuiz?.Invoke();
    }
}
