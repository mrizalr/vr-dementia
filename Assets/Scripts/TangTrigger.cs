using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TangTrigger : MonoBehaviour
{
    [SerializeField] private Transform segelPoint;
    [SerializeField] private string triggerName;

    private GameObject segel;
    public UnityEvent OnSealTakeOff;
    public UnityEvent OnFinished;
    private void OnTriggerEnter(Collider other)
    {
        if (other.name.Contains("OilDrumSegel"))
        {
            segel = other.gameObject;
            segel.transform.parent = transform.parent;
            segel.transform.position = segelPoint.position;
            OnSealTakeOff.Invoke();
        }
        else if (other.transform.name == triggerName)
        {
            Destroy(segel);
            if (other.transform.parent.name != "TempatSampahB3")
            {
                FindObjectOfType<SandboxVRSeason2.Framework.Sandbox_ScenarioManager>().CompleteScenario();
                return;
            }

            OnFinished.Invoke();
        }
    }
}
