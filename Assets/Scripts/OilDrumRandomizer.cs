using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using SandboxVRSeason2.Framework;
using System.Linq;

public class OilDrumRandomizer : MonoBehaviour
{
    [SerializeField] private Transform[] oilDrums;
    [SerializeField] private Sandbox_Section section;
    [SerializeField] private Transform valveHandler;
    [SerializeField] private Transform triggerArea;
    [SerializeField] private Transform oilPumpSocket;
    [SerializeField] private Transform oilPumpValve;
    [SerializeField] private Transform oilPumpHandleValve;
    [SerializeField] private Transform oilPump;
    [SerializeField] private Transform oilCan;

    private TangTrigger tangTrigger;
    private string randomOilDrumName;

    public UnityEvent OnMSDSOpen;

    private void Start()
    {
        tangTrigger = FindObjectOfType<TangTrigger>();
        RandomizeOil();

        var replaceStep = section.stepList.First(e => e.stepName.Contains("XXX"));
        replaceStep.stepName = replaceStep.stepName.Replace("XXX", randomOilDrumName);
    }

    private void RandomizeOil()
    {
        var randomIndex = Random.Range(0, oilDrums.Length);
        valveHandler.transform.position = oilDrums[randomIndex].Find("OilDrumCap").position;
        oilPumpSocket.position = oilDrums[randomIndex].Find("OilDrumCap").position;
        oilPumpValve.position = oilDrums[randomIndex].Find("OilDrumCap").position;
        oilPumpHandleValve.position = oilDrums[randomIndex].Find("OilDrumCap").position;
        triggerArea.position = oilDrums[randomIndex].Find("OilDrumBody").position;
        oilPump.position = oilDrums[randomIndex].Find("OilDrumBody").transform.GetChild(0).position;
        oilCan.position = oilDrums[randomIndex].Find("OilDrumBody").transform.GetChild(1).position;
        randomOilDrumName = oilDrums[randomIndex].name.Replace("OilDrum", "");

        oilDrums[randomIndex].GetComponentInChildren<DrumOilMSDS>().OnOpenUI.AddListener(delegate { OnMSDSOpen.Invoke(); });
        oilDrums[randomIndex].Find("OilDrumCap").gameObject.SetActive(false);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.N))
            RandomizeOil();
    }
}
