using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace SandboxVRSeason2.Framework
{
    [CreateAssetMenu(fileName = "New API Setting", menuName = "Sandbox/API Setting", order = 0)]
    public class APISetting : ScriptableObject
    {
        [Space]
        public string ldapUrl;
        public string baseUrl;
        public string loginHandle;
        public string scoreHandle;
        public string stepHandle;
    }
}

