using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockRigidbody : MonoBehaviour
{
    Rigidbody _rigidbody;

    private void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    public RigidbodyConstraints LockPosition(int lockAxis)
    {
        bool xLock, yLock, zLock;
        xLock = (lockAxis & 4) == 4;
        yLock = (lockAxis & 2) == 2;
        zLock = (lockAxis & 1) == 1;

        Debug.Log($"Position locks: {xLock}, {yLock}, {zLock}");

        return (RigidbodyConstraints)((xLock ? 2 : 0 )|(yLock ? 4 : 0)|(zLock ? 8 : 0));
    }

    public void LockSpecificRotation(int lockAxis)
    {
        bool xLock, yLock, zLock;
        xLock = (lockAxis & 4) == 4;
        yLock = (lockAxis & 2) == 2;
        zLock = (lockAxis & 1) == 1;

        Debug.Log($"Rotation locks: {xLock}, {yLock}, {zLock}");

        _rigidbody.constraints = (RigidbodyConstraints)((xLock ? 16 : 0) | (yLock ? 32 : 0) | (zLock ? 64 : 0));
    }

    public RigidbodyConstraints LockRotation(int lockAxis)
    {
        bool xLock, yLock, zLock;
        xLock = (lockAxis & 4) == 4;
        yLock = (lockAxis & 2) == 2;
        zLock = (lockAxis & 1) == 1;

        Debug.Log($"Rotation locks: {xLock}, {yLock}, {zLock}");

        return (RigidbodyConstraints)((xLock ? 16 : 0) | (yLock ? 32 : 0) | (zLock ? 64 : 0));
    }

    public void LockAll(bool _value)
    {
        _rigidbody.constraints = _value ? RigidbodyConstraints.FreezeAll : RigidbodyConstraints.None;
    }

    public void Lock(int locks)
    {
        int rotationLocks = (locks & 56) >> 3;
        int positionLocks = locks & 7;

        _rigidbody.constraints = LockPosition(positionLocks) | LockRotation(rotationLocks);
    }
}
