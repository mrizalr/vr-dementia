using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using DG.Tweening;

public class UITweeners : MonoBehaviour
{
    public Vector3 openedLocalPosition = Vector3.forward;
    public Vector3 closedLocalPosition = Vector3.zero;
    public float openedLocalScale = 2f;
    public float closedLocalScale = 1f;

    public UnityEvent onOpened;
    public UnityEvent onClosed;

    bool isOpen = false;
    RectTransform rect;

    public void Start()
    {
        rect = GetComponent<RectTransform>();
    }

    public void Reset()
    {
        isOpen = false;
        transform.localScale = closedLocalScale * Vector3.one;
        if (rect==null)
        {
            transform.localPosition = closedLocalPosition;
        }
        else
        {
            rect.anchoredPosition3D = closedLocalPosition;
        }
    }

    public void Open()
    {
        isOpen = false;
        ToggleScale();
    }

    public void Close()
    {
        isOpen = true;
        ToggleScale();
    }

    public void ToggleScale()
    {
        transform.DOScale(isOpen ? closedLocalScale : openedLocalScale, .5f);
        if (rect == null)
        {
            transform.DOMove(isOpen ? closedLocalPosition : openedLocalPosition, .5f);
        }
        else
        {
            rect.DOAnchorPos3D(isOpen ? closedLocalPosition : openedLocalPosition, .5f);
        }
        
        if(isOpen)
        {
            onClosed?.Invoke();
        }
        else
        {
            onOpened?.Invoke();
        }

        isOpen = !isOpen;
    }
}
