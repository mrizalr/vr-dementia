using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DelayShow : MonoBehaviour
{
    [SerializeField]
    float delayTime;
    [SerializeField]
    Vector3 currentScale;
    private void OnEnable() {
        currentScale = transform.localScale;
        print(currentScale);
        if(currentScale == Vector3.zero)
            print("wrong");
        transform.localScale = Vector3.zero;
        StartCoroutine(Wait());
    }
    // Start is called before the first frame update
    void Start()
    {
    }
    IEnumerator Wait(){
        yield return new WaitForSeconds(delayTime);
        transform.localScale = currentScale;
        currentScale = transform.localScale;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
