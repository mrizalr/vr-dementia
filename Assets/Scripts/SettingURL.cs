using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tictech.EnterpriseUniversity;
using TMPro;
using UnityEngine.UI;

public class SettingURL : MonoBehaviour
{
    [SerializeField]
    GameObject panel;
    [SerializeField]
    TMP_InputField LDAPInput;    
    [SerializeField]
    TMP_InputField serverInput;
    [SerializeField]
    TPMTagSelectable toggleLDAP;

    int targetClick = 3;
    int currentClick = 0;
    public void TryOpen(){
        if(currentClick >= targetClick){
            panel.SetActive(true);
        }else{
            currentClick++;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        serverInput.text = EuRuntimeManager.Instance.baseUrl;
        LDAPInput.text = EuRuntimeManager.Instance.LDAPUrl;

        if(!string.IsNullOrEmpty(PlayerPrefs.GetString("LDAPURL"))){
            LDAPInput.text = PlayerPrefs.GetString("LDAPURL");
        }
        if(!string.IsNullOrEmpty(PlayerPrefs.GetString("BASEURL"))){
            serverInput.text = PlayerPrefs.GetString("BASEURL");
        }
        if(PlayerPrefs.GetInt("USELDAP") != 0){
            toggleLDAP.value = PlayerPrefs.GetInt("USELDAP") == 2 ? true : false;
        }        
    }
    public void SaveURL(){
        EuRuntimeManager.Instance.baseUrl = serverInput.text;
        EuRuntimeManager.Instance.LDAPUrl = LDAPInput.text;
        EuRuntimeManager.Instance.useLDAP = toggleLDAP.value;
        PlayerPrefs.SetString("LDAPURL",LDAPInput.text);
        PlayerPrefs.SetString("BASEURL",serverInput.text);
        PlayerPrefs.SetInt("USELDAP",toggleLDAP.value ? 2 : 1);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
