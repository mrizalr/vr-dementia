using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;
public class HelperUI : MonoBehaviour
{
    const float UI_OPEN_SCALE = 0.0005f;
    float currentScale;

    public bool skipTextSetOnInit = false;

    [SerializeField, TextArea] private string helperText;
    private TextMeshProUGUI textTMP;

    private void Start()
    {
        currentScale = transform.localScale.x;
        transform.localScale = Vector3.zero;
        textTMP = GetComponentInChildren<TextMeshProUGUI>();
        if (!skipTextSetOnInit)
        {
            textTMP.text = helperText;
        }
    }
    
    public void OpenUI()
    {
        print("open ui "+transform.parent.name+"/"+currentScale);
        transform.DOScale(currentScale, .5f);
    }

    public void CloseUI()
    {
        transform.DOScale(Vector3.zero, .5f);
    }

    public void SetText(string text)
    {
        helperText = text;
        textTMP.text = helperText;
    }
}
