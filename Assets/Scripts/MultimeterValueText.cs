using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MultimeterValueText : MonoBehaviour
{
    private TextMeshProUGUI valueTMP;

    private void Start()
    {
        valueTMP = GetComponent<TextMeshProUGUI>();
    }

    public void GenerateValue()
    {
        StartCoroutine(GenerateValueCor());
    }

    public void ResetValue()
    {
        valueTMP.text = "0.0 V";
    }

    private IEnumerator GenerateValueCor()
    {
        for (int i = 0; i < 5; i++)
        {
            var random = Random.Range(0f, 1f);
            valueTMP.text = random.ToString("0.0") + " V";

            yield return new WaitForSeconds(.2f);
        }
    }
}
