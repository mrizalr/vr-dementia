using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuizProgressBar : MonoBehaviour
{
    [SerializeField] private Sprite deactiveImage, activeImage;
    [SerializeField] private GameObject barPrefabs;
    [SerializeField] private QuizManager quizManager;

    private Image[] bars;


    private void Awake()
    {
        quizManager = FindObjectOfType<QuizManager>();
        bars = new Image[quizManager.selectQuizCount];
        GenerateBar();
        barPrefabs.SetActive(false);
    }

    private void GenerateBar()
    {
        for (int i = 0; i < quizManager.selectQuizCount; i++)
        {
            var bar = Instantiate(barPrefabs, transform).GetComponent<Image>();
            bar.sprite = deactiveImage;
            bars[i] = bar;
        }
    }

    public void SetActiveBar(int index)
    {
        bars[index].sprite = activeImage;
    }
}
