using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GearboxManager : MonoBehaviour
{
    [SerializeField] private Animator animator;
    [SerializeField] private AudioSource audioSource;

    public UnityEvent OnActive, OnDeactive;
    bool isOn;

    public void ToggleOnOff()
    {
        animator.enabled = !animator.enabled;
        if (audioSource != null) audioSource.enabled = !audioSource.enabled;
        isOn = !isOn;

        if (isOn) OnActive?.Invoke();
        else OnDeactive?.Invoke();
    }
}
