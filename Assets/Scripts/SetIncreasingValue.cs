using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SetIncreasingValue : MonoBehaviour
{
    public TMP_InputField inputField;
    public float max;
    public float time;

    private Coroutine increaseCor;

    public void StopIncrease()
    {
        StopCoroutine(increaseCor);
    }

    public void StartIncrease()
    {
        increaseCor =  StartCoroutine(StartIncreasing());
    }

    private IEnumerator StartIncreasing()
    {
        var increaseGap = (max / time) / 10;
        var currentValue = 0;
        while (true)
        {
            currentValue += Mathf.CeilToInt(increaseGap);
            inputField.text = currentValue.ToString();
            yield return new WaitForSeconds(.1f);
        }
    }
}
