using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;
using SandboxVRSeason2.Framework;

public class MateriManager : MonoBehaviour
{
    [System.Serializable]
    public struct Materi
    {
        public string title;
        [TextArea]public string value;
    }

    public List<Materi> listMateri;
    public TextMeshProUGUI titleTMP, valueTMP;
    public SB_UIButton prevButton, nextButton;


    private int currentMateriIndex = 0;

    public UnityEvent OnMateriDone;

    private void Start()
    {
        SetMateri(currentMateriIndex);
    }

    private void SetMateri(int index)
    {
        titleTMP.text = listMateri[index].title;
        valueTMP.text = listMateri[index].value;

        prevButton.gameObject.SetActive(index != 0);
        nextButton.GetComponentInChildren<TextMeshProUGUI>().text = index == listMateri.Count-1 ? "Selesai" : "Selanjutnya";
    }

    public void NextMateri()
    {
        currentMateriIndex++;
        if(currentMateriIndex == listMateri.Count)
        {
            OnMateriDone.Invoke();
            return;
        }
        SetMateri(currentMateriIndex);
    }

    public void PrevMateri()
    {
        SetMateri(--currentMateriIndex);
    }
}
