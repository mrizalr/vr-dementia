using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;

public enum CheckType {Name, Collider}

[System.Serializable]
public class ColliderEvent : UnityEvent<Collision>
{

}

[System.Serializable]
public class TriggerEvent : UnityEvent<Collider>
{

}

public class IntersectionCallback : MonoBehaviour
{
    public CheckType checkBy;

    [ShowIf("checkBy", CheckType.Collider)] public Collider target;
    [ShowIf("checkBy", CheckType.Name)] public string targetName;

    //"Pass" variables will determine should a callback pass the required parameter or not to an event
    
    public bool passColliderEnterData;
    [HideIf("passColliderEnterData",false)]
    public UnityEvent onColliderEnter;
    [ShowIf("passColliderEnterData",true)]
    public ColliderEvent parameterizedOnColliderEnter;

    public bool passColliderStayData;
    [HideIf("passColliderStayData",false)]
    public UnityEvent onColliderStay;
    [ShowIf("passColliderStayData",true)]
    public ColliderEvent parameterizedOnColliderStay;

    public bool passColliderExitData;
    [HideIf("passColliderExitData",false)]
    public UnityEvent onColliderExit;
    [ShowIf("passColliderExitData",true)]
    public ColliderEvent parameterizedOnColliderExit;

    public bool passTriggerEnterData;
    [HideIf("passTriggerEnterData",false)]
    public UnityEvent onTriggerEnter;
    [ShowIf("passTriggerEnterData",true)]
    public TriggerEvent parameterizedOnTriggerEnter;

    public bool passTriggerStayData;
    [HideIf("passTriggerStayData",false)]
    public UnityEvent onTriggerStay;
    [ShowIf("passTriggerStayData",true)]
    public TriggerEvent parameterizedOnTriggerStay;

    public bool passTriggerExitData;
    [HideIf("passTriggerExitData",false)]
    public UnityEvent onTriggerExit;
    [ShowIf("passTriggerExitData",true)]
    public TriggerEvent parameterizedOnTriggerExit;

    public void OnCollisionEnter(Collision other)
    {
        if (!Check(other)) return;
        if (isActiveAndEnabled)
        {
            if (passColliderEnterData)
            {
                parameterizedOnColliderEnter?.Invoke(other);
            }
            else
            {
                onColliderEnter?.Invoke();
            }
        }
    }

    public void OnCollisionStay(Collision other)
    {
        if (!Check(other)) return;
        if (isActiveAndEnabled)
        {
            if (passColliderStayData)
            {
                parameterizedOnColliderStay?.Invoke(other);
            }
            else
            {
                onColliderStay?.Invoke();
            }
        }
    }

    public void OnCollisionExit(Collision other)
    {
        if (!Check(other)) return;
        if (isActiveAndEnabled)
        {
            if (passColliderExitData)
            {
                parameterizedOnColliderExit?.Invoke(other);
            }
            else
            {
                onColliderExit?.Invoke();
            }
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if (!Check(other)) return;
        if (isActiveAndEnabled)
        {
            if (passTriggerEnterData)
            {
                parameterizedOnTriggerEnter?.Invoke(other);
            }
            else
            {
                onTriggerEnter?.Invoke();
            }
        }
    }

    public void OnTriggerStay(Collider other)
    {
        if (!Check(other)) return;
        if (isActiveAndEnabled)
        {
            if (passTriggerStayData)
            {
                parameterizedOnTriggerStay?.Invoke(other);
            }
            else
            {
                onTriggerStay?.Invoke();
            }
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (!Check(other)) return;
        if (isActiveAndEnabled)
        {
            if (passTriggerExitData)
            {
                parameterizedOnTriggerExit?.Invoke(other);
            }
            else
            {
                onTriggerExit?.Invoke();
            }
        }
    }

    bool Check(Collider detectedCollider)
    {
        bool result = false;

        switch (checkBy)
        {
            case CheckType.Collider:
                result = target == detectedCollider;
                break;
            case CheckType.Name:
                result = detectedCollider.gameObject.name.Contains(targetName);
                break;
        }

        return result;
    }

    bool Check(Collision detectedCollision)
    {
        bool result = false;

        switch(checkBy)
        {
            case CheckType.Collider:
                result = target == detectedCollision.collider;
                break;
            case CheckType.Name:
                result = detectedCollision.gameObject.name.Contains(targetName);
                break;
        }

        return result;
    }
}