using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RegulatorPSIHandler : MonoBehaviour
{
    [SerializeField] private float minAngle, maxAngle, maxRotation, angle;

    private void Start()
    {
        var angle = transform.localEulerAngles;
        angle.y = minAngle;
        transform.localEulerAngles = angle; 
    }

    public void OnAngleChange(float angle, float delta, float percent)
    {
        this.angle = angle;
        var diff = maxAngle - minAngle;
        var newAngle = transform.localEulerAngles;
        newAngle.y =  minAngle + (angle / maxRotation) * diff;
        transform.localEulerAngles = newAngle;
    }
}
