using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Events;

public class InputHMI : MonoBehaviour
{
    TouchScreenKeyboard keyboard;
    [SerializeField]
    GameObject container;
    private TMP_InputField focusInput;
    public UnityEvent InputAction;
    public void OpenKeyboard(){
        keyboard = TouchScreenKeyboard.Open("",TouchScreenKeyboardType.Default, false, false,true,true);
    }
    public void OpenInput(TMP_InputField _input){
        focusInput = _input;
        container.SetActive(true);
    }
    public void InputText(){

    }
    public void InputText(string ch){
        focusInput.text += ch;
    }
    public void DeleteAll(){
        focusInput.text = "";
    }
    public void Enter(){
        container.SetActive(false);
        focusInput.onEndEdit.Invoke("");
        InputAction.Invoke();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
