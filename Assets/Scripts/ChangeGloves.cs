using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeGloves : MonoBehaviour
{
    [SerializeField]
    Transform leftHand;
    [SerializeField]
    Transform rightHand;
    public Material changeLeftHand;
    public Material changeRightHand;
    public Material defaultHand;

    private bool usable;

    public bool isUsable()
    {
        return usable;
    }

    public void SetUsable()
    {
        usable = true;
    }


    public void Change()
    {
        ChangeMaterialHand(leftHand);
        ChangeMaterialHand(rightHand);
    }

    public void Reset()
    {
        SkinnedMeshRenderer[] skins = leftHand.GetComponentsInChildren<SkinnedMeshRenderer>(true);
        for (int i = 0; i < skins.Length; i++)
        {
            skins[i].material = defaultHand;
        }

        skins = rightHand.GetComponentsInChildren<SkinnedMeshRenderer>(true);
        for (int i = 0; i < skins.Length; i++)
        {
            skins[i].material = defaultHand;
        }
        Invoke("Reset", 1);
    }

    void ChangeMaterialHand(Transform hand)
    {
        SkinnedMeshRenderer[] skins = hand.GetComponentsInChildren<SkinnedMeshRenderer>();
        for (int i = 0; i < skins.Length; i++)
        {
            skins[i].material = changeLeftHand;
        }
    }
}
