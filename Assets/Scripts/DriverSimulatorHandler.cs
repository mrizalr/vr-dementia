using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DriverSimulatorHandler : MonoBehaviour
{
    private Animator animator;
    private AnimatorOverrideController aoc;
    private ChainAnimation chain;
    private BearingRenderTexture rt;

    [SerializeField]
    bool startplay = false;
    [SerializeField]
    Animator newAnimatorChain;
    private bool isMachineActive;
    private bool isNormalRotateAnimation;

    [SerializeField] private GameObject audioGroup;
    [SerializeField] private AudioSource suaraMesin;
    [SerializeField] private RuntimeAnimatorController rotateAnimation;
    [SerializeField] private RuntimeAnimatorController rusakAnimation;
    [SerializeField] private float changeAnimationTime;

    public UnityEvent TriggerEnter;

    private void Start()
    {
        animator = GetComponent<Animator>();
        chain = GetComponentInChildren<ChainAnimation>();
        rt = FindObjectOfType<BearingRenderTexture>();

        animator.enabled = false;
        if(chain) chain.enabled = false;
        audioGroup.SetActive(false);
        
        isMachineActive = false;
        isNormalRotateAnimation = true;

        animator.runtimeAnimatorController = rotateAnimation;
        StartCoroutine(ChangeAnimation(changeAnimationTime));
        if (startplay)
        {
            ToggleOnOff();
        }
    }

    private IEnumerator ChangeAnimation(float timer)
    {

        yield return new WaitForSeconds(timer);
        StartCoroutine(ChangeAnimation(timer));
        if (!isMachineActive) yield break;

        audioGroup.SetActive(!isNormalRotateAnimation);
        animator.runtimeAnimatorController = isNormalRotateAnimation ? rotateAnimation : rusakAnimation;
        isNormalRotateAnimation = !isNormalRotateAnimation;
    }

    public void ToggleOnOff()
    {
        print(isMachineActive);
        isMachineActive = !isMachineActive;
        animator = GetComponent<Animator>();
        Debug.Log(animator.name);
        animator.enabled = isMachineActive;
        if(newAnimatorChain) newAnimatorChain.enabled = isMachineActive;
        if(chain) chain.enabled = isMachineActive;
        audioGroup.SetActive(isMachineActive);
        
        if(isMachineActive)
        {
            suaraMesin.Play();
        } else
        {
            suaraMesin.Stop();
        }

        if(rt)
            rt.ToggleUIPanel();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "PlayerController")
            TriggerEnter.Invoke();
    }
}
