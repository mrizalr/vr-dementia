using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TPMTagSelectable : MonoBehaviour
{
    [SerializeField]
    TPMTagSelectable opposite;
    private GameObject checkImage;
    public bool value;

    Toggle toggle;

    private void Start()
    {
        checkImage = transform.GetChild(0).GetChild(0).gameObject;
        toggle = GetComponent<Toggle>();
    }

    public void CheckToggle()
    {
        Debug.Log("clicked");
        checkImage.SetActive(!checkImage.activeInHierarchy);
        value = checkImage.activeInHierarchy;

        if (toggle != null)
        {
            if (toggle.group != null)
            {
                toggle.isOn = value;
            }
        }
        if(opposite){
            if(opposite.value == value){
                opposite.CheckToggle();
            }
        }
    }
}
