using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SandboxVRSeason2.Framework;

public class SequenceSocketActivator : MonoBehaviour
{
    public enum ScenarioType
    {
        SANDBOX,
        LINEAR,
        BOTH
    }

    public ScenarioType scenarioType = ScenarioType.BOTH;

    public List<SB_Socket> sb_sockets;
    private Sandbox_Section sb_section;

    private int currentSocketIndex = 0;

    private void Start()
    {
        sb_section = FindObjectOfType<Sandbox_Section>();
        if (sb_section == null) return;

        var scenarioManager = FindObjectOfType<Sandbox_ScenarioManager>();
        if (scenarioManager == null) return;

        if (scenarioType != ScenarioType.BOTH && scenarioType.ToString() != scenarioManager.mode.ToString()) return;
        
        sb_section.onSectionStart.AddListener(SetDeactiveAll);
    }

    public void SetDeactiveAll()
    {
    // this function called from scenario manager (OnScenarioStart)
        foreach (var s in sb_sockets)
        {
            s.gameObject.SetActive(false);
        }
    }

    public void ActivateNextSocket()
    {
        sb_sockets[currentSocketIndex++].gameObject.SetActive(true);
    }
}
