using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectAnchor : MonoBehaviour
{
    [SerializeField] private Transform anchor;
    [SerializeField] private bool rotAchorX, rotAchorY, rotAchorZ;

    private void Update()
    {
        transform.position = anchor.position;
        transform.eulerAngles = new Vector3(
                                    rotAchorX ? anchor.eulerAngles.x : transform.eulerAngles.x,
                                    rotAchorY ? anchor.eulerAngles.y : transform.eulerAngles.y,
                                    rotAchorZ ? anchor.eulerAngles.z : transform.eulerAngles.z
                                );
    }
}
