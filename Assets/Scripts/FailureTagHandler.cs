using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Sirenix.OdinInspector;
using Tictech.EnterpriseUniversity;

public class FailureTagHandler : MonoBehaviour
{
    enum InputType
    {
        Name,
        Date,
        RandomInt
    }

    private TextMeshProUGUI TMP;

    [SerializeField, HideIf("useCustomInput")] private InputType inputType;
    [SerializeField] bool useCustomInput;
    [SerializeField] bool showOnStart;
    [SerializeField, ShowIf("useCustomInput")] string customInput;

    private void Start()
    {
        TMP = GetComponentInChildren<TextMeshProUGUI>();
        SetTMPContent();
        if(!showOnStart) TMP.transform.gameObject.SetActive(false);
    }

    public void SetTMPContent()
    {
        if (useCustomInput)
        {
            TMP.text = customInput;
            return;
        }

        switch (inputType)
        {
            case InputType.Name:
                var user = EuRuntimeManager.Instance.User;
                TMP.text = user == null ? "Budi Samudra" : user.first_name+" "+user.last_name;
                break;
            case InputType.Date:
                TMP.text = System.DateTime.Now.ToString("dd/MM/yyyy");
                break;
            case InputType.RandomInt:
                var rand = Random.Range(0, 10);
                TMP.text = rand.ToString();
                break;
            default:
                break;
        }
    }
}
