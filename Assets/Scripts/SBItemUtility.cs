using SandboxVRSeason2.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SBItemUtility : MonoBehaviour
{
    SB_Item target;

    void Start()
    {
        target = GetComponent<SB_Item>();
    }

    public void LockPosX(bool _value)
    {
        target.lockPosX = _value;
    }

    public void LockPosY(bool _value)
    {
        target.lockPosY = _value;
    }

    public void LockPosZ(bool _value)
    {
        target.lockPosZ = _value;
    }

    public void LockAllPos(bool _value)
    {
        LockPosX(_value);
        LockPosY(_value);
        LockPosZ(_value);
    }

    public void LockRotX(bool _value)
    {
        target.lockRotX = _value;
    }

    public void LockRotY(bool _value)
    {
        target.lockRotY = _value;
    }

    public void LockRotZ(bool _value)
    {
        target.lockRotZ = _value;
    }

    public void LockAllRot(bool _value)
    {
        LockRotX(_value);
        LockRotY(_value);
        LockRotZ(_value);
    }

    public void LockAll(bool _value)
    {
        LockAllPos(_value);
        LockAllRot(_value);
    }
}
