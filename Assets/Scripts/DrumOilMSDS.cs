using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using DG.Tweening;
using TMPro;

public class DrumOilMSDS : MonoBehaviour
{
    [SerializeField] private Transform infoUIPanel;
    [SerializeField] private Transform poi;
    [SerializeField] private TextMeshProUGUI titleTMP;
    [SerializeField] private Image contentImage;

    [SerializeField] private Sprite[] contents;
    [SerializeField] private string title;
    private int contentIndex = 0;

    [HideInInspector] public UnityEvent OnOpenUI;

    private void Start()
    {
        //infoUIPanel.transform.localScale = Vector3.zero;
        infoUIPanel.gameObject.SetActive(false);
        titleTMP.text = title;

        foreach (var sprite in contents)
        {
            var img = Instantiate(contentImage, contentImage.transform.parent);
            img.sprite = sprite;
        }

        contentImage.gameObject.SetActive(false);
    }

    public void OpenPanel()
    {
        if (OnOpenUI != null) OnOpenUI.Invoke();
        //StartCoroutine(OpenInfoPanelCor());
        poi.gameObject.SetActive(false);
        infoUIPanel.gameObject.SetActive(true);
    }

    public void ClosePanel()
    {
        //StartCoroutine(CloseInfoPanelCor());
        poi.gameObject.SetActive(true);
        infoUIPanel.gameObject.SetActive(false);
    }

    public void NextContent()
    {
        contentIndex = contentIndex == contents.Length - 1 ? 0 : contentIndex+1;
        contentImage.sprite = contents[contentIndex];
    }

    public void PrevContent()
    {
        contentIndex = contentIndex == 0 ? contents.Length - 1 : contentIndex-1;
        contentImage.sprite = contents[contentIndex];
    }

    private IEnumerator CloseInfoPanelCor()
    {
        infoUIPanel.DOScale(0, .5f);
        yield return new WaitForSeconds(.5f);
        poi.DOScale(1, .5f);
    }

    private IEnumerator OpenInfoPanelCor()
    {
        poi.DOScale(0, .5f);
        yield return new WaitForSeconds(.5f);
        infoUIPanel.DOScale(1, .5f);
    }
}
