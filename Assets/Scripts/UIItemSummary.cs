using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIItemSummary : MonoBehaviour
{
    [SerializeField]
    GameObject iconTrue;
    [SerializeField]
    GameObject iconFalse;
    [SerializeField]
    TMPro.TextMeshProUGUI text;
    [SerializeField]
    TMPro.TextMeshProUGUI quizResultText;
    public bool isRight;
    public string step;
    public string quizResult;
    private void Start() {
        text.text = step;
        if(quizResultText) quizResultText.text = quizResult;
        if(iconTrue) iconTrue.SetActive(isRight);
        if(iconFalse) iconFalse.SetActive(!isRight);
    }
}
