using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Tictech.EnterpriseUniversity;

public class WarningTagManager : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI nameTMP, dateTMP;

    private void Start()
    {
        var user = EuRuntimeManager.Instance.User;
        nameTMP.text = user != null ? user.first_name+" "+user.last_name : "Budi Samudra";
        dateTMP.text = System.DateTime.Now.ToString("dd/MM/yyyy");
    }
}
