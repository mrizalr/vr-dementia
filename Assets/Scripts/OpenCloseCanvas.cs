using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class OpenCloseCanvas : MonoBehaviour
{
    [SerializeField] Vector3 openScale;

    private void Start()
    {
        transform.localScale = Vector3.zero;
    }

    public void OpenCanvas()
    {
        transform.DOScale(openScale, .5f);
    }

    public void CloseCanvas()
    {
        transform.DOScale(Vector3.zero, .5f);
    }
}
