using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChainAnimation : MonoBehaviour
{
    [SerializeField] private float speed;

    private Renderer rend;

    private void Start()
    {
        rend = GetComponent<Renderer>();
    }

    private void Update()
    {
        rend.material.mainTextureOffset = Vector2.right * (rend.material.mainTextureOffset.x + speed);
    }
}
