using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LookIndicator : MonoBehaviour
{
    private static GameObject lookArea;
    private GameObject playerController;

    private bool isViewed;
    [SerializeField] private bool usingMainCamera;

    public Animator lookAnimator;

    public UnityEvent LookCompleted; // called in animator event

    public UnityEvent OnStartViewing;

    public void SetIsView(bool value)
    {
        isViewed = value;
        lookAnimator.enabled = value;
    }

    private void Start()
    {
        playerController = usingMainCamera ? Camera.main.gameObject : GameObject.Find("PlayerController");
        Debug.Log("Main cam " + Camera.main.gameObject);
        InitLookArea();
        gameObject.SetActive(false);
    }

    private void InitLookArea()
    {
        if (lookArea != null) return;
        lookArea = GameObject.CreatePrimitive(PrimitiveType.Cube);
        lookArea.name = "CamLookArea";

        lookArea.GetComponent<MeshRenderer>().enabled = false;
        lookArea.GetComponent<BoxCollider>().isTrigger = true;

        lookArea.AddComponent<CamLookCheck>();
        //lookArea.GetComponent<CamLookCheck>().OnStart = new UnityEvent();
        //lookArea.GetComponent<CamLookCheck>().OnStart.AddListener(() => { OnStartViewing?.Invoke(); });

        var rb = lookArea.AddComponent<Rigidbody>();
        rb.isKinematic = true;

        // lookArea.transform.position = playerController.transform.position + Vector3.forward * 0.5f;
        lookArea.transform.localScale /= 2;
        lookArea.transform.parent = usingMainCamera ? playerController.transform : playerController.transform.Find("CameraRig");
        print("cam "+Camera.main.transform.position+"/"+Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 0.5f)));
        lookArea.transform.position = (usingMainCamera ? Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 0.5f)) : playerController.transform.position + playerController.transform.forward * 0.5f );
        lookArea.transform.rotation = new Quaternion();

        print("INIT COMPLETE");
    }

    private void OnLookingCompleted()
    {
        LookCompleted.Invoke();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.name != "CamLookArea") return;

        OnStartViewing?.Invoke();
    }
}