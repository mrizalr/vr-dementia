using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PositionLimiter : MonoBehaviour
{
    private enum DestinationAxis
    {
        NONE, MIN, MAX
    }

    [SerializeField] private Vector3 minPosition;
    [SerializeField] private Vector3 maxPosition;
    [SerializeField] private bool LimitOnXAxis;
    [SerializeField] private bool LimitOnYAxis;
    [SerializeField] private bool LimitOnZAxis;

    [SerializeField] private DestinationAxis destinationAxis = DestinationAxis.NONE;
    private Vector3 destination { 
        get {
            var d = Vector3.zero;
            if(destinationAxis == DestinationAxis.MAX)
            {
                d.x = LimitOnXAxis ? maxPosition.x : transform.localPosition.x;
                d.y = LimitOnYAxis ? maxPosition.y : transform.localPosition.y;
                d.z = LimitOnZAxis ? maxPosition.z : transform.localPosition.z;
            }
            else
            {
                d.x = LimitOnXAxis ? minPosition.x : transform.localPosition.x;
                d.y = LimitOnYAxis ? minPosition.y : transform.localPosition.y;
                d.z = LimitOnZAxis ? minPosition.z : transform.localPosition.z;
            }
            return d;
        } 
    }
    public UnityEvent OnReachDestination;

    private void Update()
    {
        var targetX = LimitOnXAxis ? Mathf.Clamp(transform.localPosition.x, minPosition.x, maxPosition.x) : transform.localPosition.x;
        var targetY = LimitOnYAxis ? Mathf.Clamp(transform.localPosition.y, minPosition.y, maxPosition.y) : transform.localPosition.y;
        var targetZ = LimitOnZAxis ? Mathf.Clamp(transform.localPosition.z, minPosition.z, maxPosition.z) : transform.localPosition.z;

        transform.localPosition = new Vector3(targetX, targetY, targetZ);
    }

    private void LateUpdate()
    {
        if (destinationAxis != DestinationAxis.NONE && transform.localPosition == destination)
            OnReachDestination.Invoke();
    }
    public void CopyPos(Transform target){
        target.localPosition = transform.localPosition;
    }
}
