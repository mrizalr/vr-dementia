using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SB_Timer : MonoBehaviour
{
    public float duration;

    public UnityEvent onComplete;

    private float counter;

    public void Update()
    {
        counter += Time.deltaTime;
        if(counter >= duration)
        {
            onComplete?.Invoke();
        }
    }
}
