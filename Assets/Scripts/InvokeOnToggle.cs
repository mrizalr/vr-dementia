using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class InvokeOnToggle : MonoBehaviour
{
    [ReadOnly] public bool isOn;

    public bool on;

    public UnityEvent onToggleOn;
    public UnityEvent onToggleOff;

    public void Start()
    {
        isOn = on;
    }

    public void SetOn(bool value)
    {
        if(value && on != value)
        {
            onToggleOn?.Invoke();
        }
        else if (!value && on != value)
        {
            onToggleOff?.Invoke();
        }
        on = value;
    }
}
