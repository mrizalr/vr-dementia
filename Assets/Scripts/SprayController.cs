using HurricaneVR.Framework.ControllerInput;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SprayController : MonoBehaviour
{
    #region VARIABLES
    bool canSpray;

    public bool CanSpray
    {
        set
        {
            canSpray = value;
            if(!value)
            {
                StopSpray();
            }
        }
    }

    public ParticleSystem sprayParticle;
    public GameObject sprayAnimation;
    bool spray = false;

    [SerializeField]
    HVRPlayerInputs VRPlayerInput;
    #endregion

    void Awake()
    {
        if (VRPlayerInput == null)
        {
            VRPlayerInput = FindObjectOfType<HVRPlayerInputs>();
        }
        sprayParticle.gameObject.SetActive(false);
        StopSpray();
        CanSpray = true;
    }

    void OnDisable()
    {
        StopSpray();
    }

    public void Spraying()
    {
        if(sprayParticle!=null)
        {
            if (canSpray)
            {
                sprayParticle.gameObject.SetActive(true);
                canSpray = false;
                print("spay");
                sprayAnimation.SetActive(true);
                sprayParticle.Play();
            }
        }
    }

    public void StopSpray()
    {
        if (sprayParticle != null)
        {
            if (sprayParticle.isPlaying)
            {
                canSpray=true;
                print("stop spay");
                sprayAnimation.SetActive(false);
                sprayParticle.Stop();
            }
        }
    }

    // void LateUpdate()
    // {
    //     if (VRPlayerInput.IsLeftTriggerHoldActive || VRPlayerInput.IsRightTriggerHoldActive)
    //     {
    //         Spraying();
    //         Debug.Log("BAJINGAN ASW");
    //     }
    //     else
    //     {
    //         StopSpray();
    //         Debug.Log("MBUT");
    //     }
    // }
}
