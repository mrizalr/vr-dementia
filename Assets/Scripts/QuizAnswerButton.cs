using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class QuizAnswerButton : MonoBehaviour
{
    [SerializeField] private int buttonID;
    [SerializeField] private TextMeshProUGUI textTMP;
    [SerializeField] private Image image, background;

    public int GetID()
    {
        return buttonID;
    }

    public void SetText(string text)
    {
        image.gameObject.SetActive(false);
        textTMP.transform.gameObject.SetActive(true);

        textTMP.text = text;
    }

    public void SetImage(Sprite sprite)
    {
        image.gameObject.SetActive(true);
        textTMP.transform.gameObject.SetActive(false);

        image.sprite = sprite;
    }

    public void OnClickedAnswer()
    {
        background.gameObject.SetActive(true);
    }

    public void Reset()
    {
        background.gameObject.SetActive(false);
    }
}
