using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;

public class KeyboardCustom : MonoBehaviour
{
    public GameObject panel;
    public TMP_InputField input;
    private bool isUpper = true;

    public UnityEvent onPressButton;

    // Start is called before the first frame update
    void Start()
    {
        
    }
    public void InputChar(string ch){
        input.text += (isUpper) ? ch.ToUpper() : ch.ToLower();
        onPressButton?.Invoke();
    }
    public void ToggleUpper(){
        isUpper = !isUpper;
        TextMeshProUGUI[] texts = GetComponentsInChildren<TextMeshProUGUI>();
        for (int i = 0; i < texts.Length; i++)
        {
            texts[i].text = (isUpper) ? texts[i].text.ToUpper() : texts[i].text.ToLower();
        }
    }
    public void DeleteAll(){
        input.text = "";
    }

    public void BackSpace()
    {
        if (input.text.Length == 0) return;
        input.text = input.text.Substring(0, input.text.Length - 1);
    }

    public void Open()
    {
        panel.SetActive(true);
    }

    public void InputFinish(){
        panel.SetActive(false);
        //gameObject.SetActive(false);
        // if(input.text != "")
            // networkManager.InitializeRoom(input.text);
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
