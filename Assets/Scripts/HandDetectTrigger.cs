using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class HandDetectTrigger : MonoBehaviour
{
    [SerializeField]
    private UnityEvent HandEnter;
    [SerializeField] private UnityEvent HandLeave;

    [SerializeField] private string handTag;
    
    [SerializeField]
    float radius;

    private bool _handEntered;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        HandDetect(transform.position,radius);
    }
    void HandDetect(Vector3 center, float radius)
    {
        Collider[] hitColliders = Physics.OverlapSphere(center, radius);
        foreach (var hitCollider in hitColliders)
        {
            // print(hitCollider.tag);
            if(hitCollider.CompareTag(handTag)){
                print(hitCollider.name);
                HandEnter.Invoke();
                _handEntered = true;
            }
            else if(_handEntered)
            {
                HandLeave?.Invoke();
                _handEntered = false;
            }
        }
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, radius);
    }
}
