using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CamLookCheck : MonoBehaviour
{
    [HideInInspector] public UnityEvent OnStart;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag != "LookTarget") return;

        //OnStart?.Invoke();
        //OnStart?.RemoveAllListeners();

        var lookIndicator = other.GetComponent<LookIndicator>();
        lookIndicator.SetIsView(true);
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag != "LookTarget") return;

        var lookIndicator = other.GetComponent<LookIndicator>();
        lookIndicator.SetIsView(false);
    }
}
