using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TangPengaitVisualHandler : MonoBehaviour
{
    public Transform mesh1, mesh2;
    public float openingDegree;
    private Vector3 mesh1DefaultRotation;
    private Vector3 mesh2DefaultRotation;
    // Start is called before the first frame update
    void Start()
    {
        mesh1DefaultRotation = mesh1.rotation.eulerAngles;
        mesh2DefaultRotation = mesh2.rotation.eulerAngles;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OpenTangPengait()
    {
        Vector3 mesh1OpenRotation = mesh1DefaultRotation + new Vector3(0, 0, -(openingDegree * 0.5f));
        Vector3 mesh2OpenRotation = mesh2DefaultRotation + new Vector3(0, 0, (openingDegree * 0.5f));
        mesh1.DOLocalRotate(mesh1OpenRotation, 0.5f, RotateMode.Fast);
        mesh2.DOLocalRotate(mesh2OpenRotation, 0.5f, RotateMode.Fast);
    }
    public void CloseTangPengait()
    {
        mesh1.DOLocalRotate(mesh1DefaultRotation, 0.5f, RotateMode.Fast);
        mesh2.DOLocalRotate(mesh2DefaultRotation, 0.5f, RotateMode.Fast);
    }
}
