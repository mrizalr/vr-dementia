using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SandboxVRSeason2.Framework;

public class HelperAnimationHandler : MonoBehaviour
{
    public bool isDone;
    public void Init()
    {
        if (isDone) return;
        gameObject.SetActive(true);
    }

    public void End()
    {
        gameObject.SetActive(false);
        isDone = true;
    }

    public void ResetIsDone()
    {
        isDone = false;
    }
}
