using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetTemperatureColor : MonoBehaviour
{
    public RenderTexture heatRenderTexture;
    public Vector2 centerPixel;
    public Color detectedColor;
    public Texture2D tex;

    private DetectTemperature detectTemp;

    private void Start()
    {
        centerPixel = new Vector2(heatRenderTexture.width / 2, heatRenderTexture.height / 2);
        detectTemp = GetComponent<DetectTemperature>();
    }

    private void Update()
    {
        tex = toTexture2D(heatRenderTexture);
        detectedColor = tex.GetPixel((int)centerPixel.x, (int)centerPixel.y);
        detectTemp.selectedColor = detectedColor;
    }

    Texture2D toTexture2D(RenderTexture rTex)
    {
        Texture2D tex = new Texture2D(rTex.width, rTex.height, TextureFormat.RGB24, false);
        // ReadPixels looks at the active RenderTexture.
        RenderTexture.active = rTex;
        tex.ReadPixels(new Rect(0, 0, rTex.width, rTex.height), 0, 0);
        tex.Apply();
        return tex;
    }
}
