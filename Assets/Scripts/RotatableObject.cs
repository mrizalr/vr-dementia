using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatableObject : MonoBehaviour
{
    public float initialRotation;
    public float maxRotation;
    public bool useDeltaRotation;
    public bool useSnapping;

    [ShowIf("useSnapping", true)] public float snapping;
    [ShowIf("useSnapping", true)] public float interpolant;
    public float deltaMultiplier;

    [ReadOnly] public Vector3 trackedRotation;
    [ReadOnly] public float delta = 0f;
    [ReadOnly] public float valveRot;
    [ReadOnly] public float targetRotation;

    public float TargetRotation
    {
        set
        {
            targetRotation = value;
        }
    }

    bool isPrevRotationTracked = false;
    float deg = 0f;
    float interp = 0f;
    float prevRotation = 0f;
    float currentSnap = 0f;
    float snap = 0f;
    Quaternion targetQuaternion;
    public float Interpolant
    {
        set
        {
            interpolant = value;
        }
    }


    private void Start()
    {
        trackedRotation = transform.localEulerAngles;
    }

    //TODO: Agak insecure pake delta rotasi euler
    public void RotateX(float _value)
    {
        if(useDeltaRotation)
        {
            if(!isPrevRotationTracked)
            {
                prevRotation = _value;
                isPrevRotationTracked = true;
                return;
            }
            delta = prevRotation - _value;
            prevRotation = _value;

            deg += delta * deltaMultiplier;

            if(useSnapping)
            {
                snap = deg;
                snap = ((int)(snap / snapping)) * snapping;
                trackedRotation.x = Mathf.Abs(snap);
                targetQuaternion = Quaternion.Euler(trackedRotation);

                return;
            }

            trackedRotation.x = Mathf.Abs(deg);
            targetQuaternion = Quaternion.Euler(trackedRotation);
        }
        else
        {
            trackedRotation = _value * Vector3.right;
            targetQuaternion = Quaternion.Euler(trackedRotation);
        }

    }

    public void RotateY(float _value)
    {
        if (useDeltaRotation)
        {
            if (!isPrevRotationTracked)
            {
                prevRotation = _value;
                isPrevRotationTracked = true;
                return;
            }
            delta = prevRotation - _value;
            prevRotation = _value;

            deg += delta * deltaMultiplier;

            if (useSnapping)
            {
                snap = deg;
                snap = ((int)(snap / snapping)) * snapping;
                trackedRotation.y = Mathf.Abs(snap);
                targetQuaternion = Quaternion.Euler(trackedRotation);

                return;
            }

            trackedRotation.y = Mathf.Abs(deg);
            targetQuaternion = Quaternion.Euler(trackedRotation);
        }
        else
        {
            trackedRotation = _value * Vector3.up;
            targetQuaternion = Quaternion.Euler(trackedRotation);
        }
    }

    public void RotateZ(float _value)
    {
        valveRot = _value;

        if (useDeltaRotation)
        {
            if (!isPrevRotationTracked)
            {
                prevRotation = _value;
                isPrevRotationTracked = true;
                return;
            }
            delta = prevRotation - _value;
            prevRotation = _value;

            deg += delta * deltaMultiplier;

            if (useSnapping)
            {
                snap = deg;
                snap = ((int)(snap / snapping)) * snapping;
                trackedRotation.z = Mathf.Abs(snap);
                targetQuaternion = Quaternion.Euler(trackedRotation);

                return;
            }

            trackedRotation.z = Mathf.Abs(deg);
            targetQuaternion = Quaternion.Euler(trackedRotation);
        }
        else
        {
            trackedRotation = _value * Vector3.forward;
            targetQuaternion = Quaternion.Euler(trackedRotation);
        }
    }

    public void SnapRotateZ(float value)
    {
        if(useSnapping)
        {
            snap = ((int)(value / snapping)) * snapping / maxRotation;
            currentSnap = Mathf.Lerp(currentSnap, snap, interpolant);
            trackedRotation.z = Mathf.Lerp(initialRotation, targetRotation, currentSnap);
        }
    }

    public void SetLocalEulerX(float _value)
    {
        prevRotation = _value;
        isPrevRotationTracked = true;
        trackedRotation.x = _value;
        targetQuaternion = Quaternion.Euler(trackedRotation);
    }

    public void SetLocalEulerY(float _value)
    {
        prevRotation = _value;
        isPrevRotationTracked = true;
        trackedRotation.y = _value;
        targetQuaternion = Quaternion.Euler(trackedRotation);
    }

    public void SetLocalEulerZ(float _value)
    {
        prevRotation = _value;
        isPrevRotationTracked = true;
        trackedRotation.z = _value;
        targetQuaternion = Quaternion.Euler(trackedRotation);
    }

    public void SetMultiplier(float value)
    {
        deltaMultiplier = value;
    }

    public void Update()
    {
        if (useSnapping)
        {
            //transform.localRotation = Quaternion.Slerp(transform.localRotation, targetQuaternion, interpolant);
            transform.localEulerAngles = Vector3.Lerp(transform.localEulerAngles, trackedRotation, interpolant);
        }
        else
        {
            transform.localEulerAngles = trackedRotation;
        }
    }
}
