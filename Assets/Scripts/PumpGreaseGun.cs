using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HurricaneVR.Framework.ControllerInput;
using HurricaneVR.Framework.Shared;
using HurricaneVR.Framework.Core;
using SandboxVRSeason2.Framework;
using UnityEngine.Events;

public class PumpGreaseGun : MonoBehaviour
{
    bool grabbed;
    [SerializeField]
    GameObject greaseGun;
    [SerializeField]
    int target=3;
    [SerializeField]
    int count=0;
    bool press = false;
    public SB_Socket slot;
    public UnityEvent pumpAction;
    public UnityEvent fullFilled;
    // Start is called before the first frame update
    void Start()
    {
        if(!greaseGun)
            greaseGun = GameObject.FindGameObjectWithTag("GreaseGun");
    }
    public void SetGrabbed(bool val){
        grabbed = val;
    }
    bool finish = false;
    // Update is called once per frame
    void Update()
    {
        if(grabbed){
            // HVRButtonState leftPrimary = HVRController.GetButtonState(HVRHandSide.Left, HVRButtons.Primary);
            HVRButtonState rightPrimary = HVRController.GetButtonState(GetComponent<HVRGrabbable>().IsLeftHandGrabbed ? HVRHandSide.Left : HVRHandSide.Right , HVRButtons.Primary);
            if(rightPrimary.Active && !press){
                press = true;
                count++;
                print("pump = "+count);
                greaseGun.GetComponent<Animator>().SetTrigger("Pump");
                if(!finish)
                    pumpAction.Invoke();
                CheckFinishBearing();
            }else if(!rightPrimary.Active){
                press = false;
            }
        }
    }
    void FinishPump(){
        if(slot)
            slot.slotLock = false;
    }
    public void CheckFinishBearing(){
        if(count >= target){
            finish = true;
            FinishPump();
            fullFilled.Invoke();
        }
    }
    
}
