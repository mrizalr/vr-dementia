using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TPMTagsUI : MonoBehaviour
{
    private void Start()
    {
        transform.localScale = Vector3.zero;
    }

    private void Update()
    {
        Vector3 targetLook = Camera.main.transform.position;
        targetLook.y = transform.position.y;
        transform.LookAt(targetLook);
    }

    public void Open()
    {
        transform.DOScale(.001f, .5f);
    }

    public void Close()
    {
        transform.DOScale(0, .5f);
    }
}
