using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GreaseGunDialController : MonoBehaviour
{
    [SerializeField] private bool isGreaseGunOpen = false;
    [SerializeField] private Transform coverDial, coverOrigin, pusherGrab, pusherOrigin, oilBucketTrigger;

    public UnityEvent OnGrabbedBeforeOpen, OnReleasedBeforeOpen, OnGrabbedAfterOpen, OnReleasedAfterOpen, OnDipGreaseGun;
    private bool isGrabbed, isOnBucket, isDipped;

    public void SetGreaseGunOpen(bool value)
    {
        isGreaseGunOpen = value;
    }

    private void SetCoverRotationOnReleased()
    {
        coverOrigin.localEulerAngles = coverDial.localEulerAngles;
    }

    private void SetPusherPosition()
    {
        pusherOrigin.localPosition = pusherGrab.localPosition;
    }

    private void Update()
    {
        if(isGrabbed)
        {
            if (isOnBucket) OnGrabbedAfterOpen.Invoke();
            else {
                SetPusherPosition();
                OnReleasedAfterOpen.Invoke(); 
            }
        }
    }
    public void OnGreaseGunGrabbed()
    {
        if (isGreaseGunOpen){
            isGrabbed = true;
            return;
        }
        OnGrabbedBeforeOpen.Invoke();
    }

    public void OnGreaseGunReleased()
    {
        if (isGreaseGunOpen) {
            isGrabbed = false;
            SetPusherPosition();
            OnReleasedAfterOpen.Invoke();
            return; 
        }
        SetCoverRotationOnReleased();
        OnReleasedBeforeOpen.Invoke();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (isDipped || other.transform != oilBucketTrigger) return;
        isDipped = true;
        OnDipGreaseGun.Invoke();
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.transform != oilBucketTrigger) return;
        isOnBucket = true;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.transform != oilBucketTrigger) return;
        isOnBucket = false;
    }

}
