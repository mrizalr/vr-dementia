using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UILookRotation : MonoBehaviour
{
    Transform cam;
    [SerializeField] bool lockY;
    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main.transform;
    }

    // Update is called once per frame
    void Update()
    {
        var camTransform = cam.position;
        if (lockY) camTransform.y = transform.position.y;
        transform.rotation = Quaternion.LookRotation(transform.position - camTransform);
    }
}
