using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BearingRenderTexture : MonoBehaviour
{
    private void Start()
    {
        transform.localScale = Vector3.zero;
    }

    private void Update()
    {
        if (transform.localScale == Vector3.zero) return;

        var lookPosition = Camera.main.transform.position;
        lookPosition.y = transform.position.y;
        transform.LookAt(lookPosition);
    }

    public void OpenPanel()
    {
        transform.DOScale(.001f, .5f);
    }

    public void ClosePanel()
    {
        transform.DOScale(0, .5f);
    }

    public void ToggleUIPanel()
    {
        if (transform.localScale == Vector3.zero) OpenPanel();
        else ClosePanel();
    }
}
