using HurricaneVR.Framework.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GloveCheck : MonoBehaviour
{
    public UnityEvent onGloveDetected;

    public void ChecksGlove(Collision other)
    {
        if (other.gameObject.tag == "Glove")
        {
            if (other.gameObject.GetComponent<HVRGrabbable>() != null)
            {
                other.gameObject.GetComponent<HVRGrabbable>().enabled = false;
            }
            Debug.Log("HUBLA");
            onGloveDetected?.Invoke();
        }
    }

    public void CheckGlove(Collider other)
    {
        if(other.gameObject.tag == "Glove")
        {
            if (other.gameObject.GetComponent<HVRGrabbable>() != null)
            {
                other.gameObject.GetComponent<HVRGrabbable>().enabled = false;
                other.gameObject.GetComponent<Collider>().enabled = false;
            }
            Debug.Log("HUBLA");
            onGloveDetected?.Invoke();
        }
    }

    
}
