using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DrumTriggerArea : MonoBehaviour
{
    public UnityEvent EnterArea;

    private void OnTriggerEnter(Collider other)
    {
        if (other.name != "PlayerController") return;
        EnterArea.Invoke();
    }
}
