using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HurricaneVR.Framework.ControllerInput;
using HurricaneVR.Framework.Shared;
using HurricaneVR.Framework.Core;
using SandboxVRSeason2.Framework;

public class GlovesGrab : MonoBehaviour
{
    [SerializeField]
    Transform followTarget;
    [SerializeField]
    bool follow = false;
    // Start is called before the first frame update
    void Start()
    {
        // ShowGloves();
        // GetComponentInChildren<HelperUI>().OpenUI();
    }
    public void ShowGloves(){
        follow = true;
    }
    public void GlovesGrap(){
        follow = false;
    }
    private void OnDisable() {
        print("disable");
    }
    // Update is called once per frame
    void Update()
    {
        if(follow){
            transform.position = followTarget.position;
        }
    }
}
