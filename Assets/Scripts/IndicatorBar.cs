using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class IndicatorBar : MonoBehaviour
{
    [SerializeField]
    Image fillImage;
    [SerializeField]
    TextMeshProUGUI text;
    [SerializeField]
    float value = 0; //1 - 100

    public float GetValue()
    {
        return value;
    }
    public void SetValIndicator(float val){
        value = val;
        SetUI();
    }
    public void AddValIndicator(float val){
        value += val;
        SetUI();
    }
    void SetUI(){
        if(value > 100) value = 100;
        if(value < 0) value = 0;
        fillImage.fillAmount = value/100;
        text.text = value.ToString("0")+" %";
    }
    // Start is called before the first frame update
    void Start()
    {
        SetUI();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
