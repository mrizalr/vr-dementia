using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowObject : MonoBehaviour
{
    [SerializeField]
    Transform target;
    public bool follow;
    [SerializeField]
    bool position;
    [SerializeField]
    bool rotation;
    // Update is called once per frame
    void Update()
    {
        if(follow){
            if(position) transform.position = target.position;
            if(rotation) transform.rotation = target.rotation;
        }
    }
    public void SetFollow(bool set){
        follow = set;
    }
}
