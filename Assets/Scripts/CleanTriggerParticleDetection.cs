using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using SandboxVRSeason2.Framework;

public class CleanTriggerParticleDetection : MonoBehaviour
{
    [SerializeField] ParticleSystem targetParticle;
    [SerializeField] float cleanTargetCount;
    [SerializeField] IndicatorBar indicator;

    CleaningTriggerController cleaningTriggerController;
    float currentCleanTarget;

    private void Start()
    {
        cleaningTriggerController = GetComponent<CleaningTriggerController>();
    }

    private void OnTriggerStay(Collider other)
    {
        var ps = other.GetComponent<ParticleSystem>();
        if (ps == null || ps != targetParticle) return;
        if (ps.isPlaying == false) return;

        if (currentCleanTarget >= cleanTargetCount) cleaningTriggerController.Trigger();
        else currentCleanTarget += Time.deltaTime;

        if (indicator) indicator.SetValIndicator(Mathf.CeilToInt((currentCleanTarget / cleanTargetCount) * 100));
    }
}
