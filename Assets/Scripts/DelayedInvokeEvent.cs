using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DelayedInvokeEvent : MonoBehaviour
{
    public UnityEvent Event;

    public void StartDelayedInvoke(float delay)
    {
        StartCoroutine(DelayedInvoke(delay));
    }

    private IEnumerator DelayedInvoke(float delay)
    {
        yield return new WaitForSeconds(delay);
        Event.Invoke();
    }
}
