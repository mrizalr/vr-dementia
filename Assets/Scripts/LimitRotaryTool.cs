using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using SandboxVRSeason2.Framework;
using System;

public class LimitRotaryTool : MonoBehaviour
{
    #region old
    //[SerializeField, ReadOnly] float startRotateValue = 0f, targetRotateValue;

    //[SerializeField, ReadOnly] private float valueModifier;
    //private SandBox_RotaryHandler rotary;
    //private string toolId;

    //private void Start()
    //{
    //    rotary = GetComponent<SandBox_RotaryHandler>();
    //    toolId = rotary.toolId;
    //}

    //private void OnTriggerEnter(Collider other)
    //{
    //    var sb_tool = other.GetComponentInChildren<Sandbox_Tool>();

    //    if (startRotateValue != 0 || sb_tool == null || toolId != sb_tool.toolId) return;

    //    StartCoroutine(SetRotateValue(rotary.attachPoint));
    //}

    //private IEnumerator SetRotateValue(Transform rotatePoint)
    //{
    //    yield return new WaitForEndOfFrame();
    //    startRotateValue = rotatePoint.eulerAngles.y;
    //    targetRotateValue = startRotateValue + rotary.rotateAmount;
    //    StartCoroutine(ClampToolsCor(rotatePoint));
    //}

    //private IEnumerator ClampToolsCor(Transform rotatePoint)
    //{
    //    bool isToolComplete = rotary.rotateAmount < 0 ? rotatePoint.eulerAngles.y < targetRotateValue : rotatePoint.eulerAngles.y > targetRotateValue;
    //    var min = Mathf.Min(startRotateValue, targetRotateValue) - 10;
    //    var max = Mathf.Max(startRotateValue, targetRotateValue) + 10;

    //    float lastValue = rotatePoint.eulerAngles.y, currentValue;
    //    while (!isToolComplete)
    //    {
    //        currentValue = rotatePoint.eulerAngles.y;
    //        var diff = currentValue - lastValue;
    //        if (Mathf.Abs(diff) > 300f)
    //        {
    //            valueModifier += diff > 0 ? -360 : 360;
    //        }

    //        var clampedAngleY = Mathf.Clamp(rotatePoint.eulerAngles.y + valueModifier, min, max);
    //        rotatePoint.eulerAngles = new Vector3(rotatePoint.eulerAngles.x, clampedAngleY, rotatePoint.eulerAngles.z);

    //        Debug.Log($"Clamp from {startRotateValue} to {targetRotateValue}, current value : {rotatePoint.eulerAngles.y + valueModifier}");
    //        yield return new WaitForEndOfFrame();
    //        lastValue = currentValue;
    //    }
    //}
    #endregion

    [SerializeField] Transform attachPoint;
    [SerializeField, ReadOnly] float currentAngle, startAngle, targetAngle;

    SandBox_RotaryHandler rotary;


    [SerializeField, ReadOnly] private float deltaEuler, lastEuler, currentEuler, valueModifier;
    private string toolId;

    private void Start()
    {
        rotary = GetComponentInParent<SandBox_RotaryHandler>();
        toolId = rotary.toolId;
    }

    private void OnTriggerEnter(Collider other)
    {
        var sb_tool = other.GetComponentInChildren<Sandbox_Tool>();
        if (sb_tool == null || toolId != sb_tool.toolId) return;

        currentEuler = lastEuler = attachPoint.eulerAngles.y;
        StartCoroutine(OnTriggerEnterCor());
    }

    private IEnumerator OnTriggerEnterCor()
    {
        yield return new WaitForEndOfFrame();
        if(startAngle == 0)
        {
            startAngle = currentEuler;
            targetAngle = startAngle + rotary.rotateAmount;
        }

        StartCoroutine(CalculateAngleCor());
    }

    //private void OnTriggerExit(Collider other)
    //{
    //    var sb_tool = other.GetComponentInChildren<Sandbox_Tool>();
    //    if (sb_tool == null || toolId != sb_tool.toolId) return;
    //}

    private IEnumerator CalculateAngleCor()
    {
        var min = Mathf.Min(startAngle, targetAngle);
        var max = Mathf.Max(startAngle, targetAngle);

        while (true)
        {
            currentEuler = attachPoint.eulerAngles.y;
            deltaEuler = lastEuler - currentEuler;

            if (Mathf.Abs(deltaEuler) > 300)
            {
                valueModifier += deltaEuler < 0 ? -360 : 360;
            }

            var modifiedAngle = attachPoint.eulerAngles;
            modifiedAngle.y += valueModifier; ;

            currentAngle = Mathf.Clamp(modifiedAngle.y, min, max);
            attachPoint.eulerAngles = new Vector3(attachPoint.eulerAngles.x, currentAngle, attachPoint.eulerAngles.z);

            yield return new WaitForEndOfFrame();
            lastEuler = currentEuler;
        }
    }
}
