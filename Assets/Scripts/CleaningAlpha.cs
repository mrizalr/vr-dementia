using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using SandboxVRSeason2.Framework;

public class CleaningAlpha : MonoBehaviour
{
    [SerializeField, ReadOnly] private int triggerCount;

    private MeshRenderer meshRenderer;
    private int currentTrigger;

    private void Start()
    {
        triggerCount = GetComponentsInChildren<CleaningTriggerController>().Length;
        meshRenderer = GetComponentInParent<MeshRenderer>();
    }

    public void OnCleaning()
    {
        currentTrigger += 1;
        var newColor = meshRenderer.material.color;
        newColor.a = (1 - ((float)currentTrigger / (float)triggerCount));
        meshRenderer.material.color = newColor;
    }
}
