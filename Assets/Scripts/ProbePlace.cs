using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ProbePlace : MonoBehaviour
{
    public UnityEvent probeEnter;
    public UnityEvent probeExit;
    private void OnTriggerEnter(Collider other) {
        if(other.tag == "probe"){
            probeEnter.Invoke();
        }
    }
    private void OnTriggerExit(Collider other) {
        if(other.tag == "probe"){
            probeExit.Invoke();
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
