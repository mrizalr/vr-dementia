using SandboxVRSeason2.Framework;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class SettingPressureHandler : MonoBehaviour
{
    #region VARIABLE
    public Transform pressureIndicatorTransform;
    public RotatableObject regulatorPressurePointer;

    public Sandbox_ValveController filterRegulator;

    public List<float> pressures = new List<float>();
    public List<float> targetPointerMultiplier = new List<float>();
    public List<float> targetRotation = new List<float>();
    public List<Toggle> inletOptions = new List<Toggle>();
    public List<GameObject> pressureSockets = new List<GameObject>();

    public UnityEvent onValidPressure = new UnityEvent();
    public UnityEvent onInvalidPressure = new UnityEvent();

    float rotationDelta = 32f;
    float rotationAtZero = -43.173f;


    [ReadOnly] public float activePressure;
    int id;
    float initialPressure;
    #endregion

    #region METHODS
    //public void Start()
    //{
    //    RandomizeIndicator();
    //}

    private void ActivatePressureSocket()
    {
        pressureSockets[id].SetActive(true);
    }

    public void SetPressure(float _value)
    {
        //activePressure = _value;
    }

    public void SetPointerRotation()
    {
        regulatorPressurePointer.SetLocalEulerZ(targetRotation[id]);
    }

    public void SetPressure()
    {
        for(int i=0; i<inletOptions.Count; i++)
        {
            if(inletOptions[i].isOn)
            {
                id = i;
                activePressure = pressures[i];
                filterRegulator.MaxRange = activePressure;
                regulatorPressurePointer.deltaMultiplier = targetPointerMultiplier[i];
                regulatorPressurePointer.targetRotation = targetRotation[i];
                break;
            }
        }

        ActivatePressureSocket();
    }

    [Button("Randomize Indicator")]
    private void RandomizeIndicator()
    {
        int pressureIndex = Random.Range(0, pressures.Count);
        initialPressure = pressures[pressureIndex];

        Vector3 indicatorRotation = pressureIndicatorTransform.localEulerAngles;
        indicatorRotation.y = rotationAtZero + initialPressure * rotationDelta * .5f;
        pressureIndicatorTransform.localEulerAngles = indicatorRotation;
    }

    [Button("Reset Indicator")]
    public void ResetIndicator()
    {
        Vector3 indicatorRotation = pressureIndicatorTransform.localEulerAngles;
        indicatorRotation.y = rotationAtZero;
        pressureIndicatorTransform.localEulerAngles = indicatorRotation;
    }

    public void CheckPressure(float pressureValue)
    {
        if(pressureValue != activePressure)
        {
            //do wrong
            Debug.Log("KOYA");
            onInvalidPressure?.Invoke();
        }
        else
        {
            //do the right thing
            Debug.Log("JOS");
            onValidPressure?.Invoke();
        }
    }
    #endregion
}
