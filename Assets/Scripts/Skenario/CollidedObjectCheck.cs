using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CollidedObjectCheck : MonoBehaviour
{
    public GameObject target;

    public UnityEvent onTargetEnter = new UnityEvent();
    public UnityEvent onTargetExit = new UnityEvent();

    public void CheckColliderEnter(Collider other)
    {
        if(other.gameObject == target)
        {
            onTargetEnter?.Invoke();
        }
    }

    public void CheckColliderExit(Collider other)
    {
        if (other.gameObject == target)
        {
            onTargetExit?.Invoke();
        }
    }
}
