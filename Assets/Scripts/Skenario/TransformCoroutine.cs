using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformCoroutine : SerializedMonoBehaviour
{
    public Dictionary<string, Vector3> rotationDictionary = new Dictionary<string, Vector3>();

    public float normalizedInterpolantSpeed;

    bool stillExecuting = false;

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    public void SetLocalRotation(string _key)
    {
        StartCoroutine(Rotating(_key));
    }

    IEnumerator Rotating(string _key)
    {
        stillExecuting = true;

        float interpolant = 0;
        Vector3 a = transform.localEulerAngles;

        if (a != rotationDictionary[_key])
        {
            while (interpolant <= 1f)
            {
                transform.localEulerAngles = Vector3.Lerp(a, rotationDictionary[_key], interpolant);
                interpolant += normalizedInterpolantSpeed;
                yield return null;
            }
        }

        stillExecuting = false;

        yield break;
    }

    [Button("Add Rotation")]
    public void AddRotation()
    {
        rotationDictionary.Add("", Vector3.zero);
    }

    //[Button("Debug Show Rotations")]
    //public void ShowRotation()
    //{
    //    foreach (var k in rotationDictionary.Keys)
    //    {
    //        Debug.Log(k);
    //    }
    //}
}
