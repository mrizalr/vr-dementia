using SandboxVRSeason2.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OilInspectionHandler : MonoBehaviour
{
    public bool autoFillTagsInLinearMode;

    public List<InspectionGroup> inspectionGroup = new List<InspectionGroup>();
    public List<TPMTagsUI> helperUIs = new List<TPMTagsUI>();

    public UnityEvent onCorrectAnswer = new UnityEvent();
    public UnityEvent onWrongAnswer = new UnityEvent();

    private void Start()
    {
        bool lastValue;

        for(int i = 0; i < inspectionGroup.Count; i++)
        {
            lastValue = false;
            for (int j = 0; j < inspectionGroup[i].inspectionConditions.Count; j++)
            {
                if (lastValue)
                {
                    break;
                }

                if(!lastValue && j == inspectionGroup[i].inspectionConditions.Count-1)
                {
                    inspectionGroup[i].inspectionConditions[j].isTrue = true;

                    if (Sandbox_ScenarioManager.Instance.mode == ScenarioModeEnum.LINEAR)
                    {
                        inspectionGroup[i].inspectionConditions[j].toggleCheckImage.gameObject.SetActive(autoFillTagsInLinearMode);
                    }

                    foreach (var o in inspectionGroup[i].inspectionConditions[j].showWhenTrue)
                    {
                        o.SetActive(true);
                    }

                    break;
                }

                inspectionGroup[i].inspectionConditions[j].isTrue = Random.Range(0, 8) % 2 == 0;
                lastValue = inspectionGroup[i].inspectionConditions[j].isTrue;

                if(Sandbox_ScenarioManager.Instance.mode == ScenarioModeEnum.LINEAR)
                {
                    inspectionGroup[i].inspectionConditions[j].toggleCheckImage.gameObject.SetActive(autoFillTagsInLinearMode);
                }

                foreach(var o in inspectionGroup[i].inspectionConditions[j].showWhenTrue)
                {
                    o.SetActive(lastValue);
                }
            }
        }
    }

    public void Check(int value)
    {
        int groupIndex = value / 2;
        int subGroupIndex = value % 2;

        if(inspectionGroup[groupIndex].inspectionConditions[subGroupIndex].isTrue)
        {
            Debug.Log($"User succeeded on answering condition \"{inspectionGroup[groupIndex].name}\"");

            onCorrectAnswer?.Invoke();

            helperUIs[groupIndex].Close();
        }
        else
        {
            if (Sandbox_ScenarioManager.Instance.mode == ScenarioModeEnum.SANDBOX)
            {
                Debug.Log($"User failed on answering condition \"{inspectionGroup[groupIndex].name}\"");
                onWrongAnswer?.Invoke();

                helperUIs[groupIndex].Close();
            }
        }
    }
}
