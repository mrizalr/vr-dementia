using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// THIS COMPONENT IS NOT OFFICIALLY PART OF MV's VR FRAMEWORK
/// </summary>

public class SB_FreeSocket : MonoBehaviour
{
    public GameObject objectTarget;
    public float distanceTolerance;
    public float eulerRotationTolerance;

    [FoldoutGroup("Debug"), ReadOnly] public bool targetCheck;
    [FoldoutGroup("Debug"), ReadOnly] public bool targetPositionCheck;
    [FoldoutGroup("Debug"), ReadOnly] public bool targetRotationCheck;
    [FoldoutGroup("Debug"), ReadOnly] public bool targetHasEntered;
    [FoldoutGroup("Debug"), ReadOnly] public bool targetHasExited;

    public UnityEvent onOccupied;
    public UnityEvent onUnoccupied;

    bool unoccupiedHasTriggered = false;

    public bool IsOccupied
    {
        get
        {
            return isOccupied;
        }
        private set
        {
            if(value == true && !isOccupied)
            {
                onOccupied?.Invoke();
            }
            else
            {
                onUnoccupied?.Invoke();
            }
            isOccupied = value;
        }
    }

    private bool isOccupied = false;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject == objectTarget)
        {
            targetCheck = true;
            targetHasEntered = true;
            targetHasExited = false;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if(other.gameObject == objectTarget)
        {
            if(Vector3.Distance(other.gameObject.transform.position, transform.position) < distanceTolerance && EulerRotationCheck(other.gameObject.transform.eulerAngles))
            {
                IsOccupied = true;
            }
            targetPositionCheck = Vector3.Distance(other.gameObject.transform.position, transform.position) < distanceTolerance;
            targetRotationCheck = EulerRotationCheck(other.gameObject.transform.eulerAngles);

            return;
        }

        targetPositionCheck = false;
        targetRotationCheck = false;
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject == objectTarget)
        {
            targetPositionCheck = false;
            targetRotationCheck = false;
            targetCheck = false;

            targetHasEntered = false;
            targetHasExited = true;
            if (!unoccupiedHasTriggered)
            {
                unoccupiedHasTriggered = true;
                IsOccupied = false;
            }
        }
    }

    bool EulerRotationCheck(Vector3 eulerRotation)
    {
        if(Mathf.Abs(eulerRotation.x - (transform.eulerAngles.x % 360f)) < eulerRotationTolerance && Mathf.Abs(eulerRotation.y - (transform.eulerAngles.y % 360f)) < eulerRotationTolerance && Mathf.Abs(eulerRotation.z - (transform.eulerAngles.z % 360f)) < eulerRotationTolerance)
        {
            return true;
        }
        return false;
    }
}
