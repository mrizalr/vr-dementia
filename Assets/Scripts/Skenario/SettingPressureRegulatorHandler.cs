using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SettingPressureRegulatorHandler : MonoBehaviour
{
    [ReadOnly] public bool isBroken = false;
    public QuizManager quizManager;
    public RotatableObject pointerObject;

    public List<float> brokenPointerFinalRotations = new List<float>();

    public UnityEvent onPneumaticNormal;
    
    public void SetBroken()
    {
        isBroken = Random.Range(0, 8) % 2 == 0;
        for(int i=0; i < quizManager.quizList[0].answers.Length; i++)
        {
            if(quizManager.quizList[0].answers[i].answerText == "Tidak")
            {
                quizManager.quizList[0].answers[i].isCorrect = isBroken;
            }
            else if(quizManager.quizList[0].answers[i].answerText == "Ya")
            {
                quizManager.quizList[0].answers[i].isCorrect = !isBroken;
            }
        }
    }

    public void Check()
    {
        if(isBroken)
        {
            int index = Random.Range(0, brokenPointerFinalRotations.Count - 1);
            Debug.Log(index);
            pointerObject.SetLocalEulerZ(brokenPointerFinalRotations[index]);
        }
        else
        {
            onPneumaticNormal?.Invoke();
        }
    }
}
