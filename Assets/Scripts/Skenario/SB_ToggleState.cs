using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SB_ToggleState : MonoBehaviour
{
    [ReadOnly] public bool isOn;

    public bool initialState;

    [SerializeField] public UnityEvent onToggleOn;
    [SerializeField] public UnityEvent onToggleOff;

    public void Toggle()
    {
        isOn = !isOn;

        if(isOn)
        {
            onToggleOn?.Invoke();
        }
        else
        {
            onToggleOff?.Invoke();
        }
    }
}
