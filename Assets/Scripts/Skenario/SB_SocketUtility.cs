using SandboxVRSeason2.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SB_SocketUtility : MonoBehaviour
{
    SB_Socket target;

    private void Start()
    {
        target = GetComponent<SB_Socket>();
    }

    public void EnableAllSocketHologram(bool value)
    {
        foreach(var s in target.slotList)
        {
            s.isNoHologramAnimation = value;
            foreach(var m in s.hologramObject.GetComponentsInChildren<MeshRenderer>())
            {
                m.enabled = value;
            }
        }
    }
}
