using SandboxVRSeason2.Framework;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[System.Serializable]
public class InspectionCondition
{
    [ReadOnly] public bool isTrue;
    public string conditionName;
    public List<GameObject> showWhenTrue;
    public Image toggleCheckImage;
}

[System.Serializable]
public class InspectionGroup
{
    public string name;
    public List<InspectionCondition> inspectionConditions;
}

public class InspectionChainHandler : MonoBehaviour
{
    public GameObject chainNotAlligned;
    public GameObject chainLoosened;
    public GameObject sprocketTipSharp;

    public ScenarioLookCheckPOI conditionalLookCheckPOI;

    public UnityEvent onChooseCorrectAnswer;
    public UnityEvent onChooseWrongAnswer;

    public float cameraViewAngleTolerance;

    public bool isCameraInRange;

    [SerializeField]
    public List<InspectionCondition> conditions;

    bool isChainNotAlligned;
    bool isChainLoosened;
    bool isSprocketTipSharp;

    int score = 0;

    private void Start()
    {
        bool[] values = new bool[conditions.Count];
        bool lastValue = false;

        for(int i=0; i<conditions.Count; i++)
        {
            if(!lastValue && i == conditions.Count-1)
            {
                conditions[i].isTrue = true;
                return;
            }

            if(lastValue)
            {
                values[i] = false;
            }
            else
            {
                values[i] = Random.Range(0, 8) % 2 == 0;
                lastValue = values[i];
                conditions[i].isTrue = values[i];
            }
        }
    }

    public void RandomizeCondition()
    {
        for (int i = 0; i < conditions.Count; i++)
        {
            if (conditions[i].isTrue)
            {
                foreach (var s in conditions[i].showWhenTrue)
                {
                    s.SetActive(true);
                }
            }
        }

        RandomizeToggle();
    }

    public void RandomizeToggle()
    {
        if (Sandbox_ScenarioManager.Instance.mode == ScenarioModeEnum.LINEAR)
        {
            for (int i = 0; i < conditions.Count; i++)
            {
                if (conditions[i].isTrue)
                {
                    conditions[i].toggleCheckImage.gameObject.SetActive(false);
                }
            }
        }
    }

    public void CheckAnswer(int value)
    {
        if(conditions[value].isTrue)
        {
            onChooseCorrectAnswer?.Invoke();
        }
        else
        {
            if (Sandbox_ScenarioManager.Instance.mode == ScenarioModeEnum.SANDBOX)
            {
                onChooseWrongAnswer?.Invoke();
            }
        }
    }
}
