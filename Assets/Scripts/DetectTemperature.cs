using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DetectTemperature : MonoBehaviour
{
    public Color selectedColor;
    public Color searchResult;
    public Gradient colorGradient;
    public float colorThreshold;

    [Range(0, 1)]
    public float time;
    public TextMeshProUGUI temperatureTMP;

    private void Update()
    {
        GetTime();
        temperatureTMP.text = (time * 40).ToString("##.#") + " C";
    }

    private void GetTime()
    {
        float start = 0f;
        float end = 1f;
        float mid = (start + end) / 2;
        for (float i = start; i <= mid / 2; i += .001f)
        {
            float firstMid = mid - i;
            float secondMid = mid + i;
            float fromEnd = end - i;

            if (IsColorMatch(selectedColor - colorGradient.Evaluate(i)))
            {
                time = i;
                searchResult = colorGradient.Evaluate(time);
                return;
            }
            if (IsColorMatch(selectedColor - colorGradient.Evaluate(firstMid)))
            {
                time = firstMid;
                searchResult = colorGradient.Evaluate(time);
                return;
            }
            if (IsColorMatch(selectedColor - colorGradient.Evaluate(secondMid)))
            {
                time = secondMid;
                searchResult = colorGradient.Evaluate(time);
                return;
            }
            if (IsColorMatch(selectedColor - colorGradient.Evaluate(fromEnd)))
            {
                time = fromEnd;
                searchResult = colorGradient.Evaluate(time);
                return;
            }
        }
    }

    private bool IsColorMatch(Color color)
    {
        return Mathf.Abs(color.r) < colorThreshold && Mathf.Abs(color.g) < colorThreshold && Mathf.Abs(color.b) < colorThreshold && Mathf.Abs(color.a) < colorThreshold;
    }

    //private void FindSimilarColor()
    //{
    //    for(float i=0; i<=1; i += .001f)
    //    {
    //        var test_red = Mathf.Pow(colorGradient.Evaluate(i).r - selectedColor.r, 2f);
    //        var test_green = Mathf.Pow(colorGradient.Evaluate(i).g - selectedColor.g, 2f);
    //        var test_blue = Mathf.Pow(colorGradient.Evaluate(i).b - selectedColor.b, 2f);

    //        var temp = Mathf.Sqrt(test_red + test_green + test_blue);
    //        if (temp < colorThreshold) {
    //            time = i;
    //            searchResult = colorGradient.Evaluate(time);
    //        }
    //    }
    //}
}
