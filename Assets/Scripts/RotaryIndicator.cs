using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;
using SandboxVRSeason2.Framework;
using TMPro;

public class RotaryIndicator : MonoBehaviour
{
    [SerializeField, ReadOnly] private SandBox_RotaryHandler rotary;

    [SerializeField] private Image indicatorValue;
    [SerializeField] private TextMeshProUGUI percentageTMP;

    private void Start()
    {
        rotary = GetComponentInParent<SandBox_RotaryHandler>();
        indicatorValue.fillAmount = 0;
    }

    private void Update()
    {
        indicatorValue.fillAmount = rotary.RotateValue / rotary.rotateAmount;
        percentageTMP.text = Mathf.CeilToInt(indicatorValue.fillAmount * 100) + "%";
    }
}
