﻿using UnityEngine;
using Sirenix.OdinInspector;

namespace HurricaneVR.Samples
{
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(HingeJoint))]
    public class LockedDoor : MonoBehaviour
    {
        private HingeJoint _hinge;
        private Rigidbody _rigidbody;

        public float MinAngle;
        public float MaxAngle;

        public bool LockOnStart = true;
        [ReadOnly] public bool isLocked = false;

        void Start()
        {
            if(!_hinge)
            _hinge = GetComponent<HingeJoint>();
            if(!_rigidbody)
            _rigidbody = GetComponent<Rigidbody>();

            if (LockOnStart)
                Lock();
        }

        private void Lock()
        {
            isLocked = true;
            var limit = _hinge.limits;
            limit.min = 0f;
            limit.max = 0f;
            _hinge.limits = limit;

            _rigidbody.constraints = RigidbodyConstraints.FreezeAll;
        }

        public void Unlock()
        {
            isLocked = false;
            var limits = _hinge.limits;
            limits.min = MinAngle;
            limits.max = MaxAngle;
            _hinge.limits = limits;

            _rigidbody.constraints = RigidbodyConstraints.None;
        }
    }
}
