using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

namespace Tictech.LoadManager
{
    public class LoadingHelper : MonoBehaviour
    {
        public GameObject loadingPanel;
        public Image loadingBar;
        public TextMeshProUGUI loadingMessage;
        public bool trackSceneLoading;
        public CanvasGroup cg;

        public bool ProgressToMessage { get; set; }

        private string _message;
        private LoadingProcess _process;
        private Coroutine _fading;

        private void Awake()
        {
            cg = loadingPanel.GetComponent<CanvasGroup>();
            if(!cg)
                cg = loadingPanel.AddComponent<CanvasGroup>();
            SetVisible(false);
        }

        private void Start()
        {
            if(trackSceneLoading)
            {
                RuntimeManager.onSceneLoading += OnLoadScene;
                //RuntimeManager.onAddressableSceneLoading += OnLoadScene;
            }
        }

        #region SCENE LOADING
        private void OnLoadScene(Scene scene, AsyncOperation async)
        {
            string key = "Loading scene: " + scene.name;
            if (LoadingManager.StartLoading(key, async))
            {
                Track(LoadingManager.GetLoading(key));
            }
        }

        /*private void OnLoadScene(AsyncOperationHandle async)
        {
            string key = "Loading scene: " + async.DebugName;
            if(LoadingManager.StartLoading(key, async))
            {
                Track(LoadingManager.GetLoading(key));
            }
        }*/
        #endregion

        public void Track(LoadingProcess process)
        {
            if(process == null)
            {
                SetVisible(false);
                return;
            }

            SetMessage("");
            UpdateProgress(0);
            SetVisible(true);
            _process = process;
            _process.onProgressNormalized += UpdateProgress;
            _process.onFinished += () =>
            {
                ProgressToMessage = false;
                SetVisible(false);
            };
        }

        public void SetMessage(string message)
        {
            _message = message;
            loadingMessage.text = _message;
        }

        private void UpdateProgress(float p)
        {
            loadingBar.fillAmount = p;
            if (ProgressToMessage)
                loadingMessage.text = _message + " (" + _process.NormalizedProgress.ToString("P") + ")";
        }

        public void SetVisible(bool value)
        {
            if (cg.interactable != value)
            {
                cg.interactable = value;
                if(_fading != null)
                    StopCoroutine(_fading);
                _fading = StartCoroutine(Fade(value));
            }
        }

        private IEnumerator Fade(bool visible)
        {
            cg.blocksRaycasts = visible;
            cg.interactable = visible;
            float a = 0;
            float from = cg.alpha;
            float to = visible ? 1f : 0f;
            while(a < 1f)
            {
                a += Time.deltaTime / .2f;
                cg.alpha = Mathf.Lerp(from, to, a);
                yield return new WaitForEndOfFrame();
            }
        }
    }
}
