using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Tictech.LoadManager;
using UnityEngine.SceneManagement;

namespace Tictech.LoadManager.Sample
{
    public class ExampleAddressableLoader : MonoBehaviour
    {
        public Button loadAssetButton;
        public Button releaseAssetButton;
        public Button loadSceneButton;
        public Scene sceneReference;
        public GameObject assetReference;
        public Transform parent;
        public int spawnCount;

        private int index;

        // Start is called before the first frame update
        void Start()
        {
            loadAssetButton.onClick.AddListener(Spawn);
            releaseAssetButton.onClick.AddListener(ReleaseAsset);
            loadSceneButton.onClick.AddListener(LoadScene);
        }

        private void Spawn()
        {
            for (int i = 0; i < spawnCount; i++)
            {
                /*ResourceManager.LoadAddressable(assetReference, () =>
                {
                    assetReference.InstantiateAsync(parent).Completed += (async) =>
                    {
                        async.Result.GetComponent<TMPro.TextMeshProUGUI>().text += " " + index;
                        index++;
                    };
                });*/
            }
        }

        private void LoadScene()
        {
            RuntimeManager.LoadScene(sceneReference, UnityEngine.SceneManagement.LoadSceneMode.Single);
        }

        private void ReleaseAsset()
        {
            //ResourceManager.UnloadResource(assetReference.AssetGUID, true);
        }
    }
}
