﻿using System.Collections;
using UnityEngine.Networking;
using UnityEngine;

namespace Tictech.LoadManager
{
    public class ExampleLoader : MonoBehaviour
    {
        public string textureUrl;
        public UnityEngine.UI.RawImage image;
        public bool noCache;

        void Start()
        {
            if (noCache)
                StartCoroutine(LoadNoCache());
            /*else
                ResourceManager.LoadResource(textureUrl, ResourceManager.ContentType.Texture, (tex) =>
                {
                    image.texture = tex as Texture;
                });*/
        }

        IEnumerator LoadNoCache()
        {
            float st = Time.timeSinceLevelLoad;
            using (UnityWebRequest www = UnityWebRequestTexture.GetTexture(textureUrl))
            {
                yield return www.SendWebRequest();
                if (www.error == null)
                {
                    image.texture = DownloadHandlerTexture.GetContent(www);
                }
            }
            Debug.Log("Download time: " + (Time.timeSinceLevelLoad - st));
        }
    }
}