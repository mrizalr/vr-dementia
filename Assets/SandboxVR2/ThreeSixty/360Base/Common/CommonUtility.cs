using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Common
{
    public static class UnityUtility
    {
        public static void SetChildsMeshRenderer(Transform parent, bool value)
        {
            for(int i=0; i<parent.childCount; i++)
            {
                parent.GetChild(i).GetComponent<MeshRenderer>().enabled = value;
            }
        }

        public static void ConfigureChildsComponent<T>(Transform parent, Action<T> predicate)
        {
            foreach(var c in parent.GetComponentsInChildren<T>())
            {
                predicate?.Invoke(c);
            }
        }

        public static void SetAciveChilds(Transform parent, bool value)
        {
            for(int i=0; i<parent.childCount; i++)
            {
                parent.GetChild(i).gameObject.SetActive(value);
            }
        }

        public static void DestroyAllChild(Transform parent)
        {
            Debug.Log(parent.name+", "+parent.childCount);
            if (parent.childCount > 0)
            {
                for (int i = 0; i < parent.childCount; i++)
                {
                    MonoBehaviour.Destroy(parent.GetChild(i).gameObject);
                }
            }
        }
    }
}
