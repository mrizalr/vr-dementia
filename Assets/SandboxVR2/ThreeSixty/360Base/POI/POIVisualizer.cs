using Common;
using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace ThreeSixty
{
    public class POIVisualizer : MonoBehaviour
    {
        public static POIVisualizer Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = FindObjectOfType<POIVisualizer>(true);
                }
                return instance;
            }
        }

        public static Camera CameraReference
        {
            get
            {
                return (Instance.targetCamera == null ? Camera.main : Instance.targetCamera);
            }
        }

        public float POIMinSize;
        public float POIMaxSize;
        public float POIMinDistance;
        public float POIMaxDistance;

        [ShowInInspector]
        public Camera targetCamera;
        public Transform POIParent;

        [Header("POI Prefabs")]
        public GameObject POIPrefab;

        public List<POIButton> activePOI;

        [HideInInspector]
        public bool isUpdating = false;

        [HideInInspector]
        public TeleportPoint currentTeleportPoint;

        static POIVisualizer instance;

        #region PUBLIC_STATIC_METHODS
        public static void UpdatePOI()
        {
            if (!Instance.isUpdating)
            {
                if (Instance.activePOI.Count > 0)
                {
                    Vector3 POIWorldPos = Vector3.zero;
                    for (int i = 0; i < instance.activePOI.Count; i++)
                    {
                        Visualize(instance.activePOI[i]);
                    }
                }
            }
        }

        public static void Visualize(POIButton pb)
        {
            Vector3 inViewport = (Instance.targetCamera == null ? Camera.main : Instance.targetCamera).WorldToViewportPoint(pb.worldPosition);

            if (inViewport.z > 0f)
            {
                if (!pb.gameObject.activeInHierarchy)
                {
                    pb.gameObject.SetActive(true);
                }

                float POIToPlayer = Vector3.Distance(CameraReference.transform.position, pb.worldPosition);
                float percentage = (1f - POIToPlayer - Instance.POIMinDistance) / (Instance.POIMaxDistance - Instance.POIMinDistance);
                percentage = Mathf.Clamp(percentage, 0f, 1f);

                pb.transform.position = MapPOI(pb.worldPosition);
                pb.transform.localScale = Vector3.one * Mathf.Lerp(Instance.POIMinSize, Instance.POIMaxSize, percentage);
                pb.transform.rotation = Quaternion.LookRotation((pb.transform.position - CameraReference.transform.position).normalized);
            }
            else
            {
                if (pb.gameObject.activeInHierarchy)
                {
                    pb.gameObject.SetActive(false);
                }
            }
        }

        public static void InitializePOIs(TeleportPoint point)
        {
            instance.activePOI.Clear();

            UnityUtility.DestroyAllChild(Instance.POIParent);

            GameObject go;
            POIButton pb;

            foreach (var poi in point.requiredPOI)
            {
                go = Instantiate(Instance.POIPrefab, Instance.POIParent);

                pb = go.GetComponent<POIButton>();
                pb.worldPosition = point.transform.position + (point.transform.right * poi.POIPosition.x) + (point.transform.up * poi.POIPosition.y) + (point.transform.forward * poi.POIPosition.z);
                pb.transform.position = pb.worldPosition;

                Instance.activePOI.Add(pb);

                Visualize(pb);

                TMP_Text[] texts = go.GetComponentsInChildren<TMP_Text>(true);

                for (int i = 0; i < pb.textFields.Length && i < poi.texts.Length; i++)
                {
                    pb.textFields[i].text = poi.texts[i];
                }

                for (int j = 0; j < pb.imageFields.Length && j < poi.images.Length; j++)
                {
                    pb.imageFields[j].sprite = poi.images[j];
                }
            }
        }
        #endregion

        #region PRIVATE_STATIC_METHODS

        /// <summary>
        /// Returns mapping of POI's world position according to Canvas' render mode. Please ensure that your POI prefab's anchor has been set properly.
        /// </summary>
        /// <param name="POIWorldPosition"></param>
        /// <returns></returns>
        private static Vector3 MapPOI(Vector3 POIWorldPosition)
        {
            Vector3 value = Vector3.zero;

            Canvas canvas = Instance.POIParent.GetComponent<Canvas>();

            switch (canvas.renderMode)
            {
                case RenderMode.ScreenSpaceOverlay: break; //Fill with previous Visualize function

                //Position type: Local
                case RenderMode.ScreenSpaceCamera:
                    Vector2 canvasDimension = canvas.pixelRect.size;
                    Vector2 centerToLowerLeft = canvasDimension / 2f;

                    Vector3 viewportPosition = CameraReference.WorldToViewportPoint(POIWorldPosition);

                    Debug.Log(viewportPosition);

                    value.x = canvasDimension.x * viewportPosition.x - centerToLowerLeft.x;
                    value.y = canvasDimension.y * viewportPosition.y - centerToLowerLeft.y;
                    value.z = 0f;

                    break;

                //Position type: World
                case RenderMode.WorldSpace:
                    value = POIWorldPosition;

                    break;
            }

            return value;
        }
        #endregion

        #region MONOBEHAVIOUR
        private void Awake()
        {
            CameraControl.OnCameraRotate += UpdatePOI;
        }

        private void Update()
        {
            if (activePOI.Count > 0)
            {
                foreach (var p in activePOI)
                {
                    Visualize(p);
                }
            }
        }
        #endregion
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(POIVisualizer))]
    public class POIVisualizerEditor : Editor
    {
        POIVisualizer dst;

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            if (GUILayout.Button("Show TMP_Text's texts"))
            {
                TMP_Text[] texts = dst.POIPrefab.GetComponentsInChildren<TMP_Text>(true);

                foreach (var t in texts)
                {
                    Debug.Log(t.name + ": " + t.text);
                }
            }
        }

        private void OnEnable()
        {
            dst = (POIVisualizer)target;
        }
    }
#endif
}