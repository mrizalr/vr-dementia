using Common;
using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;

namespace ThreeSixty
{

    [System.Serializable]
    public class RequiredPOI
    {
        public Vector3 POIPosition;
        public string[] texts;
        public Sprite[] images;
    }

    [System.Serializable]
    public struct TeleportImage
    {
        public Image target;
        public Sprite onPointerEnterImage;
        public Sprite onPointerExitImage;
        public Sprite onSelectImage;
    }

    public class TeleportPoint : MonoBehaviour
    {
        public int teleportID;
        public bool useRemoteHDRI;
        [HideIf("useRemoteHDRI")]
        public Texture localTexture;
        [ShowIf("useRemoteHDRI")]
        public int boothID;
        [ShowIf("useRemoteHDRI")]
        public Texture remoteTexture;

        [Space(5f)]
        public float customRotationY;

        [Space(5f)]
        public TeleportPoint nextTeleport;
        public TeleportPoint previousTeleport;

        [SerializeField]
        public List<RequiredPOI> requiredPOI = new List<RequiredPOI>();
        public List<GameObject> objectsToShowAfterTeleport = new List<GameObject>();
        public List<Transform> parentsToHideAfterTeleport = new List<Transform>();

        [Space(5f)]
        [FoldoutGroup("References")] public Transform auraTransform;
        [FoldoutGroup("References")] public Transform teleportRenderer;
        [FoldoutGroup("References")] public MeshRenderer auraRenderer;
        [FoldoutGroup("References")] public SpriteRenderer minimapDisplay;
        [FoldoutGroup("References")] public TeleportPoint[] otherPointToDisplay;

        public bool IsSelected
        {
            get
            {
                return isSelected;
            }
            set
            {
                foreach (var ti in teleportImages)
                {
                    if (value)
                    {
                        ti.target.color = Color.white;
                        ti.target.sprite = ti.onSelectImage;
                    }
                    else
                    {
                        if (ti.onPointerExitImage != null)
                        {
                            ti.target.color = Color.white;
                            ti.target.sprite = ti.onPointerExitImage;
                        }
                        else
                        {
                            ti.target.color = new Color(1, 1, 1, 0);
                            ti.target.sprite = null;
                        }
                    }
                }
                isSelected = value;
            }
        }
        public bool isSelected = false;
        public TeleportImage[] teleportImages;

        public Texture HDRI
        {
            get
            {
                if (!useRemoteHDRI)
                {
                    return localTexture;
                }
                else
                {
                    return remoteTexture;
                }
            }
        }

        Vector3 auraEuler;

        public void SetSelected(bool value)
        {
            foreach (var tp in View360Manager.Instance.teleportPoints)
            {
                tp.IsSelected = false;
            }

            IsSelected = value;
        }

        public void DisplaySeveralTeleportPoints()
        {
            /*UnityUtility.SetChildsMeshRenderer(PQMManager.instance.teleportPointParent, false);*/
            UnityUtility.ConfigureChildsComponent<TeleportPoint>(View360Manager.Instance.teleportPointParent, (c) =>
            {
                if (c.teleportRenderer != null)
                {
                    c.teleportRenderer.gameObject.SetActive(false);
                }
            });
            for (int i = 0; i < otherPointToDisplay.Length; i++)
            {
                otherPointToDisplay[i].teleportRenderer.gameObject.SetActive(true);
            }
        }

        public void RequestTexture(Action<Texture> onFoundTexture)
        {
            if (useRemoteHDRI)
            {
                if (remoteTexture != null)
                {
                    onFoundTexture(remoteTexture);
                }
                else
                {
                    //Request texture dulu

                    onFoundTexture(null);
                }
            }
            else
            {
                if (localTexture != null)
                {
                    onFoundTexture(localTexture);
                }
                else
                {
                    onFoundTexture(null);
                }
            }
        }

        private void OnDrawGizmos()
        {
            if (requiredPOI.Count > 0)
            {
                Vector3 toWorld;

                for (int i = 0; i < requiredPOI.Count; i++)
                {
                    toWorld = transform.position + (transform.right * requiredPOI[i].POIPosition.x) + (transform.up * requiredPOI[i].POIPosition.y) + (transform.forward * requiredPOI[i].POIPosition.z);

                    Gizmos.color = Color.red;
                    Gizmos.DrawSphere(toWorld, .5f);

                    //handleWorld = Handles.FreeMoveHandle(toWorld, Quaternion.identity, 0.5f, Vector3.zero, Handles.SphereHandleCap);
                }
            }
        }
    }
}