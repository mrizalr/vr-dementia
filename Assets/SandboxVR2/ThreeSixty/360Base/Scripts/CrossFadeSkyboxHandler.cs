using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ThreeSixty
{
    public class CrossFadeSkyboxHandler : MonoBehaviour
    {
        public static CrossFadeSkyboxHandler Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = FindObjectOfType<CrossFadeSkyboxHandler>(true);
                }
                return instance;
            }
        }
        static CrossFadeSkyboxHandler instance;

        public Material sphereMaterial;

        public Texture PrimaryTexture
        {
            get
            {
                return sphereMaterial.GetTexture("_PrimaryTex");
            }

            set
            {
                sphereMaterial.SetTexture("_PrimaryTex", value);
            }
        }

        public Texture SecondaryTexture
        {
            get
            {
                return sphereMaterial.GetTexture("_SecondaryTex");
            }

            set
            {
                sphereMaterial.SetTexture("_SecondaryTex", value);
            }
        }

        public float PrimaryRotation
        {
            get
            {
                return sphereMaterial.GetFloat("_PrimaryTexRotation");
            }

            set
            {
                sphereMaterial.SetFloat("_PrimaryTexRotation", value);
            }
        }

        public float SecondaryRotation
        {
            get
            {
                return sphereMaterial.GetFloat("_SecondaryTexRotation");
            }

            set
            {
                sphereMaterial.SetFloat("_SecondaryTexRotation", value);
            }
        }

        public static void SwapTexture()
        {
            Texture tmp;
            tmp = Instance.SecondaryTexture;
            Instance.SecondaryTexture = Instance.PrimaryTexture;
            Instance.PrimaryTexture = tmp;

            float rotationTmp;
            rotationTmp = Instance.SecondaryRotation;
            Instance.SecondaryRotation = Instance.PrimaryRotation;
            Instance.PrimaryRotation = rotationTmp;
        }

        public static void SetFadeValue(float value)
        {
            Instance.sphereMaterial.SetFloat("_FadeFactor", value);
        }


        /*private IEnumerator CrossFade(float time, Texture inputTex, Vector3 targetPosition)
        {
            float timeStamp = Time.time;
            float alpha = 0f;
            //CursorHandler camCH = Camera.main.GetComponent<CursorHandler>();
            //camCH.DisableControl();

            var startPos = _camTransform.position;
            var posOffset = new Vector3(0f, playerHeight, 0f);

            sphereMaterial.SetTexture("_SecondaryTex", inputTex);
            sphereMaterial.SetFloat("_SecondaryTexRotation", 0f);
            while (alpha < 1f)
            {
                alpha = (Time.time - timeStamp) / time;
                _camTransform.position = Vector3.Lerp(startPos, targetPosition + posOffset, Mathf.Clamp(alpha, 0f, 1f));
                RenderSettings.skybox.SetFloat("_FadeFactor", Mathf.Lerp(RenderSettings.skybox.GetFloat("_FadeFactor"), 1, Mathf.Clamp(alpha, 0f, 1f)));
                yield return null;
            }

            foreach (var e in _teleportPoints)
            {
                e.gameObject.SetActive(Vector3.Distance(e.transform.position, _camTransform.position) < maxTeleportDistance);
            }

            RenderSettings.skybox.SetFloat("_FadeFactor", 0f);
            RenderSettings.skybox.SetTexture("_PrimaryTex", RenderSettings.skybox.GetTexture("_SecondaryTex"));
            RenderSettings.skybox.SetFloat("_PrimaryTexRotation", RenderSettings.skybox.GetFloat("_SecondaryTexRotation"));

            onTeleport?.Invoke();
            CurrentSphere.SetActive(true);
        }*/
    }
}