using Common;
using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEditor;

namespace ThreeSixty
{
    public class VRTeleportPoint : TeleportPoint, IPointerEnterHandler, IPointerExitHandler
    {
        public string teleportPointName;
        public string Name
        {
            get
            {
                if (String.IsNullOrEmpty(teleportPointName))
                {
                    return name;
                }
                else
                {
                    return teleportPointName;
                }
            }
        }

        private Button teleportButton;

        #region INSTANCE_PRIVATE_METHODS
        private void InvokeTeleport()
        {
            VRHDRITeleportBehaviour.Teleport(this, null);
        }
        #endregion

        #region EVENT_SYSTEM

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (!IsSelected)
            {
                foreach (var ti in teleportImages)
                {
                    if (ti.onPointerEnterImage != null)
                    {
                        ti.target.color = Color.white;
                        ti.target.sprite = ti.onPointerEnterImage;
                    }
                    else
                    {
                        ti.target.color = new Color(1, 1, 1, 0);
                        ti.target.sprite = null;
                    }
                }
            }
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            if (!IsSelected)
            {
                foreach (var ti in teleportImages)
                {
                    if (ti.onPointerExitImage != null)
                    {
                        ti.target.color = Color.white;
                        ti.target.sprite = ti.onPointerExitImage;
                    }
                    else
                    {
                        ti.target.color = new Color(1, 1, 1, 0);
                        ti.target.sprite = null;
                    }
                }
            }
        }
        #endregion

        #region MONOBEHAVIOUR
        private void Start()
        {
            teleportButton = GetComponent<Button>();

            teleportButton.onClick.AddListener(InvokeTeleport);
        }
        #endregion
    }

}