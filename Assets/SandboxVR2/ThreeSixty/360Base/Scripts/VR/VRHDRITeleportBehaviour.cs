using Common;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ThreeSixty
{
    public class VRHDRITeleportBehaviour : MonoBehaviour
    {
        public static VRHDRITeleportBehaviour Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = FindObjectOfType<VRHDRITeleportBehaviour>(true);
                }
                return instance;
            }
        }
        public static event onClickTeleport OnClickTeleport;

        public Transform playerTransform;
        [HideInInspector]
        public TeleportPoint activeTeleportPoint;
        public float lerpRate;

        Ray viewportRay;

        [HideInInspector]
        public int lastBoothId = -1;

        private static VRHDRITeleportBehaviour instance;

        #region MONOBEHAVIOUR

        void Start()
        {
            OnClickTeleport += Teleport;
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawRay(viewportRay);
        }
        #endregion

        #region STATIC_METHOD
        public static void Teleport(TeleportPoint point, Action onTeleportSuccess)
        {
            point.RequestTexture((value) =>
            {
                Texture HDRI = value;

                CrossFadeSkyboxHandler.SwapTexture();
                CrossFadeSkyboxHandler.SetFadeValue(0);
                CrossFadeSkyboxHandler.Instance.SecondaryTexture = HDRI;
                CrossFadeSkyboxHandler.Instance.SecondaryRotation = point.customRotationY;

                Instance.StartCoroutine(Instance.Teleporting(point, Instance.lerpRate));
                point.DisplaySeveralTeleportPoints();
            });
        }
        #endregion

        #region COROUTINE
        IEnumerator Teleporting(TeleportPoint target, float interpolateRate)
        {
            View360Manager.SetControl(false);

            float interpolation = 0f;

            Vector3 movement = playerTransform.position - target.transform.position;

            Vector3 tpFrom = View360Manager.Instance.teleportPointParent.position;
            Vector3 tpTo = View360Manager.Instance.teleportPointParent.position + movement;
            tpTo.y = 0f;

            while (interpolation < 1f)
            {
                View360Manager.Instance.teleportPointParent.position = Vector3.Lerp(tpFrom, tpTo, interpolation);
                CrossFadeSkyboxHandler.SetFadeValue(interpolation);

                POIVisualizer.UpdatePOI();

                interpolation += interpolateRate;
                yield return null;
            }

            foreach (var p in target.parentsToHideAfterTeleport)
            {
                UnityUtility.SetAciveChilds(p, false);
            }

            foreach (var o in target.objectsToShowAfterTeleport)
            {
                o.SetActive(true);
            }

            activeTeleportPoint = target;

            POIVisualizer.InitializePOIs(target);

            View360Manager.SetControl(true);

            yield break;
        }
        #endregion
    }
}

