using System;
using System.Collections;
using System.Collections.Generic;
//using Tictech.ResourceManager;
using UnityEngine;
using UnityEngine.UI;

namespace ThreeSixty
{
    public delegate void onClickTeleport(TeleportPoint point, Action onTeleportSuccess);

    public class HDRIVixbitBehaviour : MonoBehaviour
    {
        public static HDRIVixbitBehaviour Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = FindObjectOfType<HDRIVixbitBehaviour>(true);
                }
                return instance;
            }
        }
        public static event onClickTeleport OnClickTeleport;

        public float lerpRate;

        public int teleportPointLayerIndex;

        bool canInteract = true;
        int layerMask = 0;
        GameObject hitObject;
        Ray viewportRay;
        RaycastHit hitInfo;

        [HideInInspector]
        public int lastBoothId = -1;

        private static HDRIVixbitBehaviour instance;
        private TeleportPoint activeTeleportPoint;

        #region MONOBEHAVIOUR

        void Start()
        {
            OnClickTeleport += Teleport;
            layerMask = 1 << teleportPointLayerIndex;
        }

        // Update is called once per frame
        void Update()
        {
            viewportRay = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (canInteract)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    hitObject = null;
                    if (Physics.Raycast(viewportRay, out hitInfo, 500f, layerMask))
                    {
                        hitObject = hitInfo.collider.gameObject;
                    }
                }

                if (Input.GetMouseButtonUp(0))
                {
                    if (Physics.Raycast(viewportRay, out hitInfo, 500f, layerMask))
                    {
                        if (hitObject == hitInfo.collider.gameObject)
                        {
                            TeleportPoint teleportPoint = hitInfo.collider.GetComponent<TeleportPoint>();
                            if (teleportPoint.teleportRenderer != null)
                            {
                                if (teleportPoint.teleportRenderer.gameObject.activeInHierarchy)
                                {
                                    OnClickTeleport(teleportPoint, null);
                                }
                            }
                        }
                    }
                }
            }
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawRay(viewportRay);
        }
        #endregion

        #region STATIC_METHOD
        public static void Teleport(TeleportPoint point, Action onTeleportSuccess)
        {
            Instance.activeTeleportPoint = point;

            point.RequestTexture((value) =>
            {
                Texture HDRI = value;

                CrossFadeSkyboxHandler.SwapTexture();
                CrossFadeSkyboxHandler.SetFadeValue(0);
                CrossFadeSkyboxHandler.Instance.SecondaryTexture = HDRI;

                instance.StartCoroutine(instance.Teleporting(point, instance.lerpRate));
                point.DisplaySeveralTeleportPoints();
            });
        }
        #endregion

        #region COROUTINE
        IEnumerator Teleporting(TeleportPoint target, float interpolateRate)
        {
            View360Manager.SetControl(false);

            float interpolation = 0f;

            Vector3 from = instance.transform.position;
            Vector3 to = target.transform.position;

            Vector3 cameraEulerAngles = CameraControl.Instance.rotationTrack;
            float rotationStart = cameraEulerAngles.y;

            while (interpolation < 1f)
            {
                instance.transform.position = Vector3.Lerp(from, to, interpolation);
                CrossFadeSkyboxHandler.SetFadeValue(interpolation);

                POIVisualizer.UpdatePOI();

                interpolation += interpolateRate;
                yield return null;
            }

            POIVisualizer.InitializePOIs(target);

            View360Manager.SetControl(true);

            yield break;
        }
        #endregion
    }
}