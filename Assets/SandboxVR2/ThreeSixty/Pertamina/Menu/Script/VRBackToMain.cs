using HurricaneVR.Framework.ControllerInput;
using HurricaneVR.Framework.Shared;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tictech.LoadManager;
using SandboxVRSeason2.Framework;

public class VRBackToMain : MonoBehaviour
{
    public void ReturnToMain()
    {
        RuntimeManager.LoadScene("LobbyScene - Ramdan");
    }

    public void RetryScenario() 
    {
        RuntimeManager.LoadScenario(RuntimeManager.Instance.currentSubScenario, true, 0);
    }
}
