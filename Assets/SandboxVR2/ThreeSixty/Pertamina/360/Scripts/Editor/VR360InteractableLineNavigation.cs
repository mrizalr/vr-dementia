using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace ThreeSixty
{
    public class VR360InteractableLineNavigation : MonoBehaviour
    {
        public VRTeleportPoint _VRTeleportPointA, _VRTeleportPointB;
        public LineRenderer _lineRenderer;

        /// <summary>
        /// A vector from VRTeleportPointA to VRTeleportPointB
        /// </summary>
        [HideInInspector]
        public Vector3 lineVector;

        Mesh mesh;
        MeshCollider _meshCollider;

        private Vector3 CalculatePoint(Vector3 position)
        {
            Vector3 result = Vector3.zero;
            result.x = position.x;
            result.y = position.z;

            return result;
        }

        public void LineSetup()
        {
            _lineRenderer.SetPosition(0, _VRTeleportPointA.transform.localPosition);
            _lineRenderer.SetPosition(1, _VRTeleportPointB.transform.localPosition);
        }

        private void Start()
        {
            _lineRenderer = GetComponent<LineRenderer>();
            _lineRenderer.useWorldSpace = false;

            mesh = new Mesh();

            _meshCollider = GetComponent<MeshCollider>();

            lineVector = _VRTeleportPointB.transform.position - _VRTeleportPointA.transform.position;
        }

        void FixedUpdate()
        {
            _lineRenderer.BakeMesh(mesh, false);
            _meshCollider.sharedMesh = mesh;
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawLine(transform.TransformPoint(_VRTeleportPointA.transform.localPosition), transform.TransformPoint(_VRTeleportPointB.transform.localPosition));
        }
    }

    [CustomEditor(typeof(VR360InteractableLineNavigation)), CanEditMultipleObjects]
    public class VR360InteractableLineNavigationEditor : Editor
    {
        VR360InteractableLineNavigation t;

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (GUILayout.Button("Setup Line"))
            {
                t.LineSetup();
            }
        }

        private void OnEnable()
        {
            t = (VR360InteractableLineNavigation)target;
        }
    }
}