using HurricaneVR.Framework.ControllerInput;
using HurricaneVR.Framework.Core.Grabbers;
using HurricaneVR.Framework.Shared;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace ThreeSixty
{
    public enum VRController { LeftController, RightController }
    public enum VRControllerButton { Primary, Secondary }
    public enum InteractionType { ScreenCursor, VR }
    public class VR360TeleportLineInteractor : MonoBehaviour
    {
        public bool isDebug;
        public InteractionType interactionType;
        public Transform debugRaycastHit;
        public int layerMaskID;
        public float raycastMaxDistance;

        [Header("UI")]
        public TMP_Text indicatorText;

        [FoldoutGroup("VR360's Cursor Interaction Required Components"), ShowIf("interactionType", InteractionType.ScreenCursor)]
        public Camera camera;

        [FoldoutGroup("VR360's VR Interaction Required Components"), ShowIf("interactionType", InteractionType.VR)]
        public VRController controller;
        [FoldoutGroup("VR360's VR Interaction Required Components"), ShowIf("interactionType", InteractionType.VR)]
        public VRControllerButton controllerButton;
        [FoldoutGroup("VR360's VR Interaction Required Components"), ShowIf("interactionType", InteractionType.VR)]
        public HVRHandGrabber leftHandGrabber;
        [FoldoutGroup("VR360's VR Interaction Required Components"), ShowIf("interactionType", InteractionType.VR)]
        public HVRHandGrabber rightHandGrabber;
        [FoldoutGroup("VR360's VR Interaction Required Components"), ShowIf("interactionType", InteractionType.VR)]
        public LineRenderer VRTeleportLineRenderer;

        [ReadOnly]
        public VR360InteractableLineNavigation hitVRLineNavigation;

        Ray navigationRay;
        RaycastHit hitInfo;
        VRTeleportPoint closestVRTeleportPoint;

        #region MONOBEHAVIOUR
        private void Start()
        {
            VRTeleportLineRenderer.gameObject.SetActive(interactionType == InteractionType.VR);
        }

        #endregion

        #region PRIVATE_INSTANCE_METHODS
        /// <summary>
        /// Returns true if the position is closer to VR360InteractableLineNavigation's pointA,
        /// otherwise returns false.
        /// </summary>
        /// <returns></returns>
        bool CheckRayHitResult(Vector3 position)
        {
            float distPointA = Vector3.Distance(position, hitVRLineNavigation._VRTeleportPointA.transform.position);
            float distPointB = Vector3.Distance(position, hitVRLineNavigation._VRTeleportPointB.transform.position);

            if (distPointA <= distPointB)
            {
                closestVRTeleportPoint = hitVRLineNavigation._VRTeleportPointA;
                return true;
            }
            else
            {
                closestVRTeleportPoint = hitVRLineNavigation._VRTeleportPointB;
                return false;
            }
        }

        void UpdateVRTeleportLine()
        {
            if (VRTeleportLineRenderer.gameObject.activeInHierarchy)
            {
                if (VRTeleportLineRenderer.positionCount != 2)
                {
                    VRTeleportLineRenderer.positionCount = 2;
                }

                if (!VRTeleportLineRenderer.useWorldSpace)
                {
                    VRTeleportLineRenderer.useWorldSpace = true;
                }

                VRTeleportLineRenderer.SetPosition(0, navigationRay.origin);
                VRTeleportLineRenderer.SetPosition(1, navigationRay.origin + navigationRay.direction.normalized * raycastMaxDistance);
            }
        }

        void LineNavigationCheck()
        {
            navigationRay = GetRay();

            if (Physics.Raycast(navigationRay, out hitInfo, raycastMaxDistance, 1 << layerMaskID))
            {
                debugRaycastHit.position = hitInfo.point;

                var ln = hitInfo.collider.GetComponent<VR360InteractableLineNavigation>();

                if (ln != null)
                {
                    hitVRLineNavigation = ln;
                }

                if (!debugRaycastHit.gameObject.activeInHierarchy)
                {
                    debugRaycastHit.gameObject.SetActive(true);
                }
                debugRaycastHit.transform.rotation = Quaternion.LookRotation(GetIndicatorDirection(hitInfo.point).normalized);

                indicatorText.text = closestVRTeleportPoint.Name;

                if (interactionType == InteractionType.VR && VRTeleportLineRenderer.enabled == false)
                {
                    VRTeleportLineRenderer.enabled = true;
                }
            }
            else
            {
                if (debugRaycastHit.gameObject.activeInHierarchy)
                {
                    hitVRLineNavigation = null;
                    debugRaycastHit.gameObject.SetActive(false);
                }

                if (interactionType == InteractionType.VR && VRTeleportLineRenderer.enabled == true)
                {
                    VRTeleportLineRenderer.enabled = false;
                }
            }
        }

        void Navigate()
        {
            bool isNavigate = false;

            switch (interactionType)
            {
                case InteractionType.ScreenCursor:
                    if (Input.GetMouseButtonUp(0) && hitVRLineNavigation != null)
                    {
                        isNavigate = true;
                    }
                    else
                    {
                        isNavigate = false;
                    }
                    break;
                case InteractionType.VR:
                    if (GetHVRButtonState().JustDeactivated)
                    {
                        isNavigate = true;
                    }
                    else
                    {
                        isNavigate = false;
                    }
                    break;
            }

            if (isNavigate && closestVRTeleportPoint != null)
            {
                VRHDRITeleportBehaviour.Teleport(closestVRTeleportPoint, null);
            }
        }

        HVRButtonState GetHVRButtonState()
        {
            HVRController c = null;
            HVRButtonState s = new HVRButtonState();

            switch (controller)
            {
                case VRController.LeftController:
                    c = HVRInputManager.Instance.LeftController;
                    break;
                case VRController.RightController:
                    c = HVRInputManager.Instance.RightController;
                    break;
            }

            switch (controllerButton)
            {
                case VRControllerButton.Primary:
                    s = c.PrimaryButtonState;
                    break;
                case VRControllerButton.Secondary:
                    s = c.SecondaryButtonState;
                    break;
            }

            return s;
        }

        Ray GetRay()
        {
            Ray result = new Ray();

            switch (interactionType)
            {
                case InteractionType.ScreenCursor:
                    result = camera.ScreenPointToRay(Input.mousePosition);
                    break;
                case InteractionType.VR:
                    UpdateVRTeleportLine();
                    switch (controller)
                    {
                        case VRController.LeftController:
                            result = new Ray(leftHandGrabber.RaycastOrigin.position, leftHandGrabber.transform.forward);
                            break;
                        case VRController.RightController:
                            result = new Ray(rightHandGrabber.RaycastOrigin.position, rightHandGrabber.transform.forward);
                            break;
                    }
                    break;
            }

            return result;
        }

        Vector3 GetIndicatorDirection(Vector3 position)
        {
            if (VRHDRITeleportBehaviour.Instance.activeTeleportPoint == hitVRLineNavigation._VRTeleportPointA)
            {
                closestVRTeleportPoint = hitVRLineNavigation._VRTeleportPointB;
                return hitVRLineNavigation.lineVector;
            }
            else if (VRHDRITeleportBehaviour.Instance.activeTeleportPoint == hitVRLineNavigation._VRTeleportPointB)
            {
                closestVRTeleportPoint = hitVRLineNavigation._VRTeleportPointA;
                return -hitVRLineNavigation.lineVector;
            }
            else
            {
                float distPointA = Vector3.Distance(position, hitVRLineNavigation._VRTeleportPointA.transform.position);
                float distPointB = Vector3.Distance(position, hitVRLineNavigation._VRTeleportPointB.transform.position);

                if (distPointA <= distPointB)
                {
                    closestVRTeleportPoint = hitVRLineNavigation._VRTeleportPointA;
                    return -hitVRLineNavigation.lineVector;
                }
                else
                {
                    closestVRTeleportPoint = hitVRLineNavigation._VRTeleportPointB;
                    return hitVRLineNavigation.lineVector;
                }
            }
        }

        #endregion

        private void Update()
        {
            LineNavigationCheck();
            Navigate();
        }
    }
}