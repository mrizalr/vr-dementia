using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VR360RayHitIndicator : MonoBehaviour
{
    public Transform cameraTransform;
    public Transform indicatorTransform;

    private void FixedUpdate()
    {
        indicatorTransform.rotation = Quaternion.LookRotation((indicatorTransform.transform.position - Camera.main.transform.position).normalized);
    }
}
