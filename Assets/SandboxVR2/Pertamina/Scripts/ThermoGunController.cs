using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ThermoGunController : MonoBehaviour
{
    [SerializeField] private ThermoGun[] thermoGuns;
    [SerializeField] private TextMeshProUGUI thermoText;
    
    // Update is called once per frame
    void Update()
    {
        int thermoGunChecking = 0;
        var distances = float.MaxValue;
        ThermoGun closest = null;
        
        foreach (var thermoGun in thermoGuns)
        {
            if (thermoGun.IsChecking)
            {
                thermoGunChecking++;
                if (thermoGun.DistanceToTarget < distances)
                {
                    distances = thermoGun.DistanceToTarget;
                    closest = thermoGun;
                }
            }
        }

        if (thermoGunChecking > 1)
        {
            foreach (ThermoGun thermoGun in thermoGuns)
            {
                if (thermoGun != closest)
                {
                    thermoGun.SetTimedCheckActive(false);
                }
            }
        }
        
        thermoText.gameObject.SetActive(thermoGunChecking <= 0);
    }
}
