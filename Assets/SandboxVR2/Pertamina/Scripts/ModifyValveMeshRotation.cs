using System;
using System.Collections;
using System.Collections.Generic;
using HurricaneVR.Framework.Core;
using HurricaneVR.Framework.Core.Grabbers;
using HurricaneVR.Framework.Shared.HandPoser;
using UnityEngine;

public class ModifyValveMeshRotation : MonoBehaviour
{
    [SerializeField] private float modifier = 2;
    [SerializeField] private Transform handle;
    [SerializeField] private Transform valveMesh;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 handleRotation = handle.localRotation.eulerAngles;
        valveMesh.localRotation = Quaternion.Euler(handleRotation.x, handleRotation.y * modifier, handleRotation.z);
    }

    private void FixedUpdate()
    {
        Quaternion handleRotation = handle.rotation;
        //Debug.Log($"{handleRotation.x:F1} {handleRotation.y:F1} {handleRotation.z:F1} {handleRotation.w:F1}");
        //Debug.Log($"{handleRotation.eulerAngles.x:F1} {handleRotation.eulerAngles.y:F1} {handleRotation.eulerAngles.z:F1}");
        if (handleRotation.y > 0)
        {
            //handle.localEulerAngles = new Vector3(handleRotation.x, (360 - handleRotation.y) * -1, handleRotation.z);
        }
    }

    public void SetupHandGrabbing(HVRGrabberBase grabberBase, HVRGrabbable grabbable)
    {
        HVRPosableHand[] poseHands = grabbable.transform.GetComponentsInChildren<HVRPosableHand>();

        foreach (var hand in poseHands)
        {
             hand.transform.SetParent(valveMesh);
        }
    }
}
