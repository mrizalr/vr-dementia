using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TemperatureNeedle : MonoBehaviour
{
    [SerializeField] private Transform needle;
    [SerializeField] private Vector3 minRotation, maxRotation;
    [SerializeField] private float updateSpeed;
    [SerializeField] private float smoothing;

    private float _lastUpdate;
    private Quaternion _minRotation, _maxRotation, _targetRotation;

    // Start is called before the first frame update
    void Start()
    {
        _lastUpdate = Time.time;
        _maxRotation = Quaternion.Euler(maxRotation);
        _minRotation = Quaternion.Euler(minRotation);
        if (needle == null)
            needle = transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time - _lastUpdate > updateSpeed)
        {
            _targetRotation = Quaternion.Lerp(_minRotation, _maxRotation, Random.Range(0f, 1f));//Quaternion.Lerp(needle.localRotation, Quaternion.Lerp(_minRotation, _maxRotation, Random.Range(0, 1)), Random.Range(0, 1));
            _lastUpdate = Time.time;
        }
        else
        {
            needle.localRotation = Quaternion.Lerp(needle.localRotation, _targetRotation, Time.deltaTime * smoothing);
        }
    }
}
