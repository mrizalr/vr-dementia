using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using Tictech.LoadManager;
using UnityEngine;

namespace SandboxVRSeason2.Framework.Multiplayer
{
    public class Sandbox_PlayerListingMenu : MonoBehaviourPunCallbacks
    {
        [SerializeField]
        private Transform _content;
        [SerializeField]
        private Sandbox_PlayerListing _playerListing;
        [SerializeField]
        private TMPro.TextMeshProUGUI _readyUpText;
        [SerializeField]
        private GameObject _startBtnGO;

        private Sandbox_LobbyCanvases _roomsCanvases;

        private List<Sandbox_PlayerListing> _listings = new List<Sandbox_PlayerListing>();

        private bool _ready = false;

        public override void OnEnable()
        {
            base.OnEnable();
            GetCurrentRoomPlayers();

            print("enable start if master client");

            if (PhotonNetwork.IsMasterClient)
                _startBtnGO.SetActive(true);
            else
                _startBtnGO.SetActive(false);
            //SetReadyUp(false);
        }

        public override void OnDisable()
        {
            base.OnDisable();
            for (int i = 0; i < _listings.Count; i++)
            {
                Destroy(_listings[i].gameObject);
            }

            _listings.Clear();
        }

        public void FirstInitialize(Sandbox_LobbyCanvases canvases)
        {
            _roomsCanvases = canvases;
        }

        private void SetReadyUp(bool state)
        {
            _ready = state;
            if (_ready)
                _readyUpText.text = "R";
            else
                _readyUpText.text = "N";
        }

        private void GetCurrentRoomPlayers()
        {
            if (!PhotonNetwork.IsConnected)
                return;
            if (PhotonNetwork.CurrentRoom == null || PhotonNetwork.CurrentRoom.Players == null)
                return;

            foreach (KeyValuePair<int, Player> playerInfo in PhotonNetwork.CurrentRoom.Players)
            {
                AddPlayerListing(playerInfo.Value);
            }
        }

        private void AddPlayerListing(Player player)
        {
            int index = _listings.FindIndex(x => x.Player == player);
            if (index != -1)
            {
                _listings[index].SetPlayerInfo(player);
            }
            else
            {
                Sandbox_PlayerListing listing = Instantiate(_playerListing, _content);
                if (listing != null)
                {
                    if (player.IsMasterClient)
                    {
                        listing.SetMasterClient();
                    }
                    else
                    {
                        if(PhotonNetwork.IsMasterClient)
                            listing.EnableRemoveBtn(true);
                    }

                    listing.SetPlayerInfo(player);
                    _listings.Add(listing);
                }
            }
        }

        // called whenever master client leave or master client changed by server
        public override void OnMasterClientSwitched(Player newMasterClient)
        {
            _roomsCanvases.CurrentRoomCanvas.LeaveRoomMenu.OnClick_LeaveRoom();
        }

        public override void OnPlayerEnteredRoom(Player newPlayer)
        {
            AddPlayerListing(newPlayer);
        }

        // will be called if you are master client and other player left the room
        public override void OnPlayerLeftRoom(Player otherPlayer)
        {
            int index = _listings.FindIndex(x => x.Player == otherPlayer);
            if (index != -1)
            {
                Destroy(_listings[index].gameObject);
                _listings.RemoveAt(index);
            }

            print(otherPlayer.NickName + " has left");
        }

        public void OnClick_StartGame()
        {
            if (PhotonNetwork.IsMasterClient)
            {
                // Enable only if using ready state
                //for (int i = 0; i < _listings.Count; i++)
                //{
                //    // jika bukan local maka disable fungsi start
                //    if (_listings[i].Player != PhotonNetwork.LocalPlayer)
                //    {
                //        if (!_listings[i].Ready)
                //            return;
                //    }
                //}

                PhotonNetwork.CurrentRoom.IsOpen = false;
                PhotonNetwork.CurrentRoom.IsVisible = false;
                PhotonNetwork.LoadLevel((string)PhotonNetwork.CurrentRoom.CustomProperties[Sandbox_CustomProperties.SCENARIO_NAME]);
            }
        }

        public void OnClick_ReadyUp()
        {
            if (!PhotonNetwork.IsMasterClient)
            {
                SetReadyUp(!_ready);
                base.photonView.RPC("RPC_ChangeReadyState", RpcTarget.MasterClient, PhotonNetwork.LocalPlayer, _ready);
            }
        }

        [PunRPC]
        public void RPC_ChangeReadyState(Player player, bool ready)
        {
            int index = _listings.FindIndex(x => x.Player == player);
            if (index != -1)
                _listings[index].Ready = ready;
        }
    }
}
