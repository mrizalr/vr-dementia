using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SandboxVRSeason2.Framework.Multiplayer
{
    public class Sandbox_NetworkConfig : MonoBehaviour
    {
        public static Sandbox_NetworkConfig Instance;

        private ExitGames.Client.Photon.Hashtable _playerProperties = new ExitGames.Client.Photon.Hashtable();
        public ExitGames.Client.Photon.Hashtable PlayerProperties { get { return _playerProperties; } set { _playerProperties = value; } }

        [SerializeField]
        private int _sendRate = 20;
        [SerializeField]
        private int _serializationRate = 5;
        [SerializeField]
        private bool _automaticallySyncScene = true;

        private void Awake()
        {
            Instance = this;
        }

        private void Start()
        {
            print("Connecting");
            PhotonNetwork.SendRate = _sendRate;
            PhotonNetwork.SerializationRate = _serializationRate; //10
            PhotonNetwork.AutomaticallySyncScene = _automaticallySyncScene;
            PhotonNetwork.NickName = Sandbox_MasterManager.GameSettings.NickName;
            PhotonNetwork.GameVersion = Sandbox_MasterManager.GameSettings.GameVersion;
            PhotonNetwork.ConnectUsingSettings();
        }

        public void SetCustomNumber()
        {
            System.Random rnd = new System.Random();
            int result = rnd.Next(0, 99);

            PlayerProperties["RandomNumber"] = result;
            //_myCustomProperties.Remove("RandomNumber");
            PhotonNetwork.SetPlayerCustomProperties(_playerProperties);
        }
    }
}