using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SandboxVRSeason2.Framework.Multiplayer
{
    public class Sandbox_CurrentRoom : MonoBehaviourPunCallbacks
    {
        [SerializeField]
        private TMPro.TextMeshProUGUI _roomNameTMP;
        [SerializeField]
        private TMPro.TextMeshProUGUI _scenarioNameTMP;
        [SerializeField]
        private Sandbox_PlayerListingMenu _playerListingMenu;

        [SerializeField]
        private Sandbox_LeaveRoomMenu _leaveRoomMenu;
        public Sandbox_LeaveRoomMenu LeaveRoomMenu { get { return _leaveRoomMenu; } }

        private Sandbox_LobbyCanvases _roomsCanvases;

        public void FirstInitialize(Sandbox_LobbyCanvases canvases)
        {
            _roomsCanvases = canvases;
            _playerListingMenu.FirstInitialize(canvases);
            LeaveRoomMenu.FirstInitialize(canvases);
        }

        public override void OnEnable()
        {
            base.OnEnable();
            _roomNameTMP.text = (string)PhotonNetwork.CurrentRoom.CustomProperties[Sandbox_CustomProperties.ROOM_NAME];
            _scenarioNameTMP.text = (string)PhotonNetwork.CurrentRoom.CustomProperties[Sandbox_CustomProperties.SCENARIO_NAME];
        }

        public void Show()
        {
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}