using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SandboxVRSeason2.Framework.Multiplayer
{
    public class Sandbox_PlayerListing : MonoBehaviourPunCallbacks
    {
        [SerializeField]
        private TMPro.TextMeshProUGUI _playerNameTMP;

        public Player Player { get; private set; }
        public bool Ready = false;

        [Header("Master Client")]
        [SerializeField]
        private GameObject _MCMark;
        [SerializeField]
        private GameObject _removePlayerBtn;

        public void SetPlayerInfo(Player player)
        {
            Player = player;
            SetPlayerText(player);

            if (!player.IsMasterClient)
            {
                _removePlayerBtn.GetComponent<SB_UIButton>().onDown.AddListener(delegate {
                    PhotonNetwork.CloseConnection(player);
                });
            }
        }

        public void SetMasterClient()
        {
            _MCMark.SetActive(true);
        }

        public void EnableRemoveBtn(bool value)
        {
            _removePlayerBtn.SetActive(value);
        }

        public override void OnPlayerPropertiesUpdate(Player targetPlayer, ExitGames.Client.Photon.Hashtable changedProps)
        {
            base.OnPlayerPropertiesUpdate(targetPlayer, changedProps);

            // this condition will affect OTHER PLAYERS and NOT MASTER CLIENT
            if (targetPlayer != null && targetPlayer == Player)
            {
                if (changedProps.ContainsKey("RandomNumber"))
                    SetPlayerText(targetPlayer);
            }
        }

        private void SetPlayerText(Player player)
        {
            int result = -1;
            if (player.CustomProperties.ContainsKey("RandomNumber"))
                result = (int)player.CustomProperties["RandomNumber"];
            _playerNameTMP.text = result.ToString() + ", " + Player.NickName;
        }
    }
}