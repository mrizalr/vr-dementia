using Sirenix.OdinInspector;
using UnityEngine;

namespace SandboxVRSeason2.Framework.Multiplayer
{
    public class Sandbox_NetworkSpawner : MonoBehaviour
    {
        [SerializeField]
        private GameObject _playerPrefab;
        [SerializeField]
        private GameObject _UIPrefab;

        private void Start()
        {
            
        }

        public void InitiateNetworkSpawn()
        {
            Sandbox_MasterManager.NetworkInstantiate(_playerPrefab, transform.position, Quaternion.identity);

            if(_UIPrefab)
                Sandbox_MasterManager.NetworkInstantiate(_UIPrefab, transform.position, Quaternion.identity);
        }
    }
}
