#if UNITY_EDITOR
using UnityEditor.Build;
using UnityEditor.Build.Reporting;

namespace SandboxVRSeason2.Framework.Multiplayer
{
    public class Sandbox_ResourcesPrefabPathBuilder : IPreprocessBuildWithReport
    {
        public int callbackOrder { get { return 0; } }

        public void OnPreprocessBuild(BuildReport report)
        {
            Sandbox_MasterManager.PopulateNetworkPrefabs();
        }
    }
}
#endif