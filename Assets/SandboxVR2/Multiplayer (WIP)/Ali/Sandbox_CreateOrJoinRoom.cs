using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SandboxVRSeason2.Framework.Multiplayer
{
    public class Sandbox_CreateOrJoinRoom : MonoBehaviour
    {
        [SerializeField]
        private Sandbox_CreateRoomMenu _createRoomMenu;
        [SerializeField]
        private Sandbox_RoomListingMenu _roomListingMenu;

        private Sandbox_LobbyCanvases _roomsCanvases;

        public void FirstInitialize(Sandbox_LobbyCanvases canvases)
        {
            _roomsCanvases = canvases;
            _createRoomMenu.FirstInitialize(canvases);
            _roomListingMenu.FirstInitialize(canvases);
        }

        public void Show()
        {
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}