using HurricaneVR.Framework.Core;
using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SandboxVRSeason2.Framework;
using HurricaneVR.Framework.Core.Grabbers;
using HurricaneVR.Framework.Components;

/// <summary>
/// Class for making broadcast method
/// </summary>
namespace SandboxVRSeason2.Framework.Multiplayer
{
    public class Sandbox_BroadcastMethods : MonoBehaviour
    {
        #region Dummy
        [PunRPC]
        public void RPC_CheckObj(int id)
        {
            Sandbox_ObjectID obj = ExtensionMethod.SearchObject(id);
            obj.GetComponent<CheckObject>().CheckTMP.text = "Checked";
        }
        #endregion

        [PunRPC]
        public void BroadcastRadio(int id)
        {
            Sandbox_ObjectID obj = ExtensionMethod.SearchObject(id);
            obj.GetComponent<HVRGrabbable>().ForceRelease();
        }

        [PunRPC]
        public void DisableObjectFromOther(int id)
        {
            Sandbox_ObjectID obj = ExtensionMethod.SearchObject(id);
            obj.GetComponent<HVRGrabbable>().ForceRelease();
        }

        #region SB_item
        [PunRPC]
        public void BroadcastItemToolsLock(bool value, int id)
        {
            Sandbox_ObjectID resultItem = ExtensionMethod.SearchObject(id);
            resultItem.GetComponent<SB_Item>().connection = true;
            resultItem.GetComponent<SB_Item>().toolsLock = value;
        }

        [PunRPC]
        public void BroadcastSocketed(int idSocket, int idItem)
        {
            Sandbox_ObjectID resultSocket = ExtensionMethod.SearchObject(idSocket);
            Sandbox_ObjectID resultItem = ExtensionMethod.SearchObject(idItem);
            resultItem.GetComponent<SB_Item>().connection = true;
            print("socket name : " + resultSocket.name);
            resultItem.GetComponent<SB_Item>().ItemSocketed(resultSocket.GetComponent<HVRSocket>(), resultItem.GetComponent<HVRGrabbable>());
        }

        [PunRPC]
        public void BroadcastUnSocketed(int idSocket, int idItem)
        {
            Sandbox_ObjectID resultSocket = ExtensionMethod.SearchObject(idSocket);
            Sandbox_ObjectID resultItem = ExtensionMethod.SearchObject(idItem);
            resultItem.GetComponent<SB_Item>().connection = true;
            print("unsocket");
            resultItem.GetComponent<SB_Item>().ItemUnSocketed(resultSocket.GetComponent<HVRSocket>(), resultItem.GetComponent<HVRGrabbable>());
        }

        [PunRPC]
        public void BroadcastHandGrabbed(int idSocket, int idItem)
        {
            Sandbox_ObjectID resultSocket = ExtensionMethod.SearchObject(idSocket);
            Sandbox_ObjectID resultItem = ExtensionMethod.SearchObject(idItem);
            resultItem.GetComponent<SB_Item>().connection = true;
            print("item id :" + idItem + " grabbed");
            resultItem.GetComponent<SB_Item>().ItemHandGrabbed(resultSocket.GetComponent<HVRGrabberBase>(), resultItem.GetComponent<HVRGrabbable>());
        }

        [PunRPC]
        public void BroadcastHandReleased(int idSocket, int idItem)
        {
            Sandbox_ObjectID resultSocket = ExtensionMethod.SearchObject(idSocket);
            Sandbox_ObjectID resultItem = ExtensionMethod.SearchObject(idItem);
            resultItem.GetComponent<SB_Item>().connection = true;
            print("hand released");
            resultItem.GetComponent<SB_Item>().ItemHandReleased(resultSocket.GetComponent<HVRGrabberBase>(), resultItem.GetComponent<HVRGrabbable>());
        }
        #endregion

        #region ChangeColor
        [PunRPC]
        public void ChangeColour(Renderer renderer, Vector3 randomColor)
        {
            renderer.material.SetColor("_Color", new Color(randomColor.x, randomColor.y, randomColor.z));
        }
        #endregion

        #region CleaningTriggerGroupController
        [PunRPC]
        public void BroadcastAllTriggered(int id)
        {
            Sandbox_ObjectID obj = ExtensionMethod.SearchObject(id);
            print(obj.name);
            obj.GetComponent<CleaningTriggerGroupController>().onAllTriggered?.Invoke();
        }

        [PunRPC]
        public void BroadcastAnyTriggered(int id)
        {
            Sandbox_ObjectID obj = ExtensionMethod.SearchObject(id);
            obj.GetComponent<CleaningTriggerGroupController>().onAnyTriggered?.Invoke();
        }
        #endregion

        #region Paintable
        [PunRPC]
        public void BroadcastDoPaint(int id, Vector3 pos, string color, float radius, float hardness, float strength)
        {
            print(color);
            Sandbox_ObjectID obj = ExtensionMethod.SearchObject(id);
            Color newCol;
            if (ColorUtility.TryParseHtmlString("#" + color, out newCol))
                PaintManager.instance.doPaint(obj.GetComponent<Paintable>(), pos, radius, hardness, strength, newCol);
        }
        #endregion

        #region PhotonParticleView
        [PunRPC]
        public void PlayParticle(bool stat, ParticleSystem particleSystem)
        {
            if (stat)
                particleSystem.Play();
            else
                particleSystem.Stop();
        }
        #endregion

        #region Sandbox_DialController
        [PunRPC]
        public void BroadcastAngleDial(int id, float currentAngle)
        {
            Sandbox_ObjectID obj = ExtensionMethod.SearchObject(id);
            obj.GetComponent<Sandbox_DialController>().CurrentAngle = currentAngle;
            obj.GetComponentInChildren<HVRDial>().RotateDial();
        }
        #endregion

        #region SB_Socket
        [PunRPC]
        public void BroadcastToolLocked(bool value, int id)
        {
            Sandbox_ObjectID resultSocket = ExtensionMethod.SearchObject(id);
            resultSocket.GetComponent<SB_Socket>().connection = true;
            resultSocket.GetComponent<SB_Socket>().toolLock = value;
            resultSocket.GetComponent<SB_Socket>().onToolLocked.Invoke();
        }

        [PunRPC]
        public void BroadcastUnToolLocked(bool value, int id)
        {
            Sandbox_ObjectID resultSocket = ExtensionMethod.SearchObject(id);
            resultSocket.GetComponent<SB_Socket>().connection = true;
            resultSocket.GetComponent<SB_Socket>().toolLock = value;
            resultSocket.GetComponent<SB_Socket>().onToolUnlocked.Invoke();
        }

        [PunRPC]
        public void BroadcastOccupied(int idSocket, int idItem)
        {
            Sandbox_ObjectID resultSocket = ExtensionMethod.SearchObject(idSocket);
            Sandbox_ObjectID resultItem = ExtensionMethod.SearchObject(idItem);
            print("occupied broadcast | item = " + resultItem.name + " - socket = " + resultSocket.name);
            if (resultItem.GetComponent<SB_Item>())
                resultSocket.GetComponent<SB_Socket>().ForceGrabItem(resultItem.GetComponent<SB_Item>());

            resultSocket.GetComponent<SB_Socket>().connection = true;
            resultSocket.GetComponent<SB_Socket>().OnSocketGrabbed(resultSocket.GetComponent<HVRGrabberBase>(), resultItem.GetComponent<HVRGrabbable>());
        }

        [PunRPC]
        public void BroadcastUnoccupied(int idSocket, int idItem)
        {
            print("unoccupied broadcast");
            Sandbox_ObjectID resultSocket = ExtensionMethod.SearchObject(idSocket);
            Sandbox_ObjectID resultItem = ExtensionMethod.SearchObject(idItem);
            resultSocket.GetComponent<SB_Socket>().connection = true;
            resultSocket.GetComponent<SB_Socket>().OnSocketReleased(resultSocket.GetComponent<HVRGrabberBase>(), resultItem.GetComponent<HVRGrabbable>());
        }
        #endregion

        #region Sandbox_ScenarioManager
        [PunRPC]
        public void BroadcastScenarioManagerInitialize(int idSM)
        {
            Sandbox_ObjectID resultSM = ExtensionMethod.SearchObject(idSM);

            resultSM.GetComponent<Sandbox_ScenarioManager>().Initialize();
        }

        [PunRPC]
        public void BroadcastBoolInitScenarioManager(int idSM, bool value)
        {
            Sandbox_ObjectID resultSM = ExtensionMethod.SearchObject(idSM);

            resultSM.GetComponent<Sandbox_ScenarioManager>().isInit = value;
        }
        #endregion
    }
}