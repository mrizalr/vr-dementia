using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static TMPro.TMP_Dropdown;

namespace SandboxVRSeason2.Framework.Multiplayer
{
    public static class Sandbox_CustomProperties
    {
        public const string ROOM_NAME = "RN";
        public const string PASSWORD_KEY = "PK";
        public const string SCENARIO_NAME = "SN";
    }

    public class Sandbox_RoomSettings : MonoBehaviour
    {
        [SerializeField]
        private List<Sandbox_ScenarioData> _scenarioList = new List<Sandbox_ScenarioData>();
        private List<Sandbox_SubScenarioData> _subScenarioList = new List<Sandbox_SubScenarioData>();

        [SerializeField]
        private TMPro.TMP_InputField _roomNameTMP;
        public string RoomName { get { return _roomNameTMP.text; } private set { _roomNameTMP.text = value; } }
        [SerializeField]
        private TMPro.TMP_InputField _passwordTMP;
        public string Password { get { return _passwordTMP.text; } private set { _passwordTMP.text = value; } }
        [SerializeField]
        private TMPro.TMP_Dropdown _roomCapDD;
        public TMPro.TMP_Dropdown RoomCapDD { get { return _roomCapDD; } set { _roomCapDD = value; } }
        [SerializeField]
        private TMPro.TMP_Dropdown _scenarioDD;
        public TMPro.TMP_Dropdown ScenarioDD { get { return _scenarioDD; } set { _scenarioDD = value; } }

        private Sandbox_LobbyCanvases _roomsCanvases;

        public void FirstInitialize(Sandbox_LobbyCanvases canvases)
        {
            _roomsCanvases = canvases;
        }

        private void OnEnable()
        {

        }

        private void OnDisable()
        {
            ResetRoomSettings();
        }

        private void Start()
        {
            SetScenarioDataRoom();
        }

        private void SetScenarioDataRoom()
        {
            CollectScenarioData();
            ScenarioDD.options.Clear();

            foreach (var sceneName in _subScenarioList)
            {
                ScenarioDD.options.Add(new OptionData(sceneName.SubScenarioScene));
            }
        }

        private void CollectScenarioData()
        {
            foreach (var scenario in _scenarioList)
            {
                foreach (var subScenario in scenario.SubScenariolist)
                {
                    _subScenarioList.Add(subScenario);
                }
            }
        }

        private void ResetRoomSettings()
        {
            RoomName = "";
            Password = "";
            RoomCapDD.value = 0;
            ScenarioDD.value = 0;
        }

        private void InitializeDefaultSettings()
        {
            string _st = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            string roomName = _st[Random.Range(0, _st.Length)] + _st[Random.Range(0, _st.Length)] + _st[Random.Range(0, _st.Length)] + _st[Random.Range(0, _st.Length)] + _st[Random.Range(0, _st.Length)] + "";
            RoomName = roomName;
            Password = "666";
            RoomCapDD.value = 0;
            ScenarioDD.value = 0;
        }

        public void CustomRoomSettings(out RoomOptions options)
        {
            options = new RoomOptions();

            if (RoomName == null || RoomName == "")
            {
                InitializeDefaultSettings();
            }

            print("room name2: " + RoomName);
            ExitGames.Client.Photon.Hashtable roomSettings = new ExitGames.Client.Photon.Hashtable() {
                { Sandbox_CustomProperties.ROOM_NAME, RoomName },
                { Sandbox_CustomProperties.PASSWORD_KEY, Password },
                { Sandbox_CustomProperties.SCENARIO_NAME, ScenarioDD.options[ScenarioDD.value].text }
            };

            options.BroadcastPropsChangeToAll = true;
            options.PublishUserId = true;
            options.MaxPlayers = byte.Parse(RoomCapDD.options[RoomCapDD.value].text);
            options.CustomRoomPropertiesForLobby = new string[] { Sandbox_CustomProperties.ROOM_NAME, Sandbox_CustomProperties.PASSWORD_KEY, Sandbox_CustomProperties.SCENARIO_NAME };
            options.CustomRoomProperties = roomSettings;

            print("max p : " + options.MaxPlayers);
            print("propeerties count: " + options.CustomRoomPropertiesForLobby.Length);
        }
    }
}