using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SandboxVRSeason2.Framework.Multiplayer
{
    public class Sandbox_CreateRoomMenu : MonoBehaviourPunCallbacks
    {
        private Sandbox_LobbyCanvases _lobbyCanvases;

        public void FirstInitialize(Sandbox_LobbyCanvases canvases)
        {
            _lobbyCanvases = canvases;
        }

        public void OnClick_CreateRoom()
        {
            if (!PhotonNetwork.IsConnected)
                return;

            Sandbox_NetworkConfig.Instance.SetCustomNumber();
            RoomOptions options;
            print("room name: " + _lobbyCanvases.RoomSettingsCanvas.RoomName);
            _lobbyCanvases.RoomSettingsCanvas.CustomRoomSettings(out options);
            PhotonNetwork.JoinOrCreateRoom(_lobbyCanvases.RoomSettingsCanvas.RoomName, options, null);
            print("room created");
        }

        public override void OnCreatedRoom()
        {
            print("Room created successfully");
            _lobbyCanvases.CreateOrJoinRoomCanvas.Hide();
            _lobbyCanvases.CurrentRoomCanvas.Show();
        }

        public override void OnCreateRoomFailed(short returnCode, string message)
        {
            print("Cannot create room: " + message);
        }
    }
}