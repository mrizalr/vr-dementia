using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SandboxVRSeason2.Framework.Multiplayer
{
    public class Sandbox_LobbyCanvases : MonoBehaviourPunCallbacks
    {
        [SerializeField]
        private Sandbox_CreateOrJoinRoom _createOrJoinRoomCanvas;
        public Sandbox_CreateOrJoinRoom CreateOrJoinRoomCanvas { get { return _createOrJoinRoomCanvas; } }

        [SerializeField]
        private Sandbox_CurrentRoom _currentRoomCanvas;
        public Sandbox_CurrentRoom CurrentRoomCanvas { get { return _currentRoomCanvas; } }

        [SerializeField]
        private Sandbox_RoomSettings _roomSettingsCanvas;
        public Sandbox_RoomSettings RoomSettingsCanvas { get { return _roomSettingsCanvas; } }

        private void Awake()
        {
            FirstInitialize();
        }

        private void FirstInitialize()
        {
            CreateOrJoinRoomCanvas.FirstInitialize(this);
            CurrentRoomCanvas.FirstInitialize(this);
            RoomSettingsCanvas.FirstInitialize(this);
        }

        public override void OnConnectedToMaster()
        {
            print("Connected");
            print(PhotonNetwork.LocalPlayer.NickName);

            if (!PhotonNetwork.InLobby)
            {
                PhotonNetwork.JoinLobby();
                CreateOrJoinRoomCanvas.Show();
                CurrentRoomCanvas.Hide();
            }
        }

        public override void OnDisconnected(DisconnectCause cause)
        {
            print("Disconnected by " + cause.ToString());
        }

    }
}