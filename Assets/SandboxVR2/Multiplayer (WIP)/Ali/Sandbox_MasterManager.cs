using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace SandboxVRSeason2.Framework.Multiplayer
{
    [CreateAssetMenu(menuName = "Sandbox/Multiplayer/MasterManager")]
    public class Sandbox_MasterManager : Sandbox_SingletonScriptableObject<Sandbox_MasterManager>
    {
        [SerializeField]
        private Sandbox_GameSettings _gameSettings;
        public static Sandbox_GameSettings GameSettings { get { return Instance._gameSettings; } }

        public List<NetworkedPrefab> _networkPrefabs = new List<NetworkedPrefab>();

        public static GameObject NetworkInstantiate(GameObject obj, Vector3 position, Quaternion rotation)
        {
            foreach (NetworkedPrefab networkedPrefab in Instance._networkPrefabs)
            {
                if (networkedPrefab.Prefab == obj)
                {
                    if (networkedPrefab.Path != string.Empty)
                    {
                        GameObject result = PhotonNetwork.Instantiate(networkedPrefab.Path, position, rotation);
                        return result;
                    }
                    else
                    {
                        Debug.LogError("Path is empty for gameobject name " + networkedPrefab.Prefab);
                        return null;
                    }
                }
            }

            return null;
        }

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        public static void PopulateNetworkPrefabs()
        {
#if UNITY_EDITOR
            if (Instance == null)
                return;

            Instance._networkPrefabs.Clear();

            GameObject[] results = Resources.LoadAll<GameObject>("");

            for (int i = 0; i < results.Length; i++)
            {
                if (results[i].GetComponent<PhotonView>() != null)
                {
                    string path = AssetDatabase.GetAssetPath(results[i]);
                    Instance._networkPrefabs.Add(new NetworkedPrefab(results[i], path));
                }
            }
#endif
        }
    }
}