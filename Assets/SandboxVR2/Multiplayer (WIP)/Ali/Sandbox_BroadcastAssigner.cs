using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SandboxVRSeason2.Framework.Multiplayer
{
    public class Sandbox_BroadcastAssigner : MonoBehaviour
    {
        void Start()
        {
            PhotonView[] photonView = GameObject.FindObjectsOfType<PhotonView>();

            foreach (var item in photonView)
            {
                if (!item.GetComponent<Sandbox_BroadcastMethods>())
                {
                    item.gameObject.AddComponent<Sandbox_BroadcastMethods>();
                }
            }
        }

        public void AddOnRuntime(GameObject objectAffected)
        {
            objectAffected.AddComponent<Sandbox_BroadcastMethods>();
        }
    }
}