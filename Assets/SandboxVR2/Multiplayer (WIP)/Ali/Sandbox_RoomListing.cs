using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SandboxVRSeason2.Framework.Multiplayer
{
    public class Sandbox_RoomListing : MonoBehaviour
    {
        [SerializeField]
        private TMPro.TextMeshProUGUI _roomNameTMP;
        [SerializeField]
        private TMPro.TextMeshProUGUI _playerCountTMP;
        [SerializeField]
        private GameObject _onGoingObj;
        [SerializeField]
        private TMPro.TextMeshProUGUI _scenarioNameTMP;
        [SerializeField]
        private Image _indicatorImg;
        [SerializeField]
        private Sprite _availableSprite;
        [SerializeField]
        private Sprite _unavailableSprite;
        [SerializeField]
        private Button _joinBtn;
        [SerializeField]
        private GameObject _joinObj;
        [SerializeField]
        private TMPro.TMP_InputField _passwordIF;
        [SerializeField]
        private TMPro.TextMeshProUGUI _incorrectTMP;

        private Sandbox_LobbyCanvases _roomsCanvases;

        public RoomInfo RoomInfo { get; private set; }

        public void SetRoomInfo(RoomInfo roomInfo)
        {
            RoomInfo = roomInfo;
            print("room listing properties: " + roomInfo.CustomProperties.Count);
            _roomNameTMP.text = (string)roomInfo.CustomProperties[Sandbox_CustomProperties.ROOM_NAME];
            _scenarioNameTMP.text = (string)roomInfo.CustomProperties[Sandbox_CustomProperties.SCENARIO_NAME];
            _playerCountTMP.text = "(" + roomInfo.PlayerCount + "/" + roomInfo.MaxPlayers + ")";// roomInfo.MaxPlayers + ", " + roomInfo.Name;
        }

        public void OnClick_Button()
        {
            // CRITICAL WARNING! NOT SAFE TO SET PASSWORD THIS WAY! Either use other backends like playfab or else
            if (_passwordIF.text != (string)RoomInfo.CustomProperties[Sandbox_CustomProperties.PASSWORD_KEY])
            {
                _incorrectTMP.gameObject.SetActive(true);
                _passwordIF.text = "";
                return;
            }

            Sandbox_NetworkConfig.Instance.SetCustomNumber();

            PhotonNetwork.JoinRoom(RoomInfo.Name);
        }

        public void RefreshRoom(RoomInfo info)
        {
            print("refresh room");
            _playerCountTMP.text = "(" + info.PlayerCount + "/" + info.MaxPlayers + ")";

            bool isRoomFull = info.PlayerCount == info.MaxPlayers;

            if (isRoomFull)
            {
                _indicatorImg.sprite = _unavailableSprite;
                _onGoingObj.SetActive(true);
                _joinBtn.gameObject.SetActive(false);
                _joinObj.SetActive(false);
            }
            else
            {
                _indicatorImg.sprite = _availableSprite;
                _onGoingObj.SetActive(false);
                _joinBtn.gameObject.SetActive(true);
            }
        }
    }
}