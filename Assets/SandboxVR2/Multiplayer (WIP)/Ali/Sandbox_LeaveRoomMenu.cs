using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SandboxVRSeason2.Framework.Multiplayer
{
    public class Sandbox_LeaveRoomMenu : MonoBehaviour
    {
        private Sandbox_LobbyCanvases _lobbyCanvases;

        public void FirstInitialize(Sandbox_LobbyCanvases canvases)
        {
            _lobbyCanvases = canvases;
        }

        public void OnClick_LeaveRoom()
        {
            PhotonNetwork.LeaveRoom(true);
            _lobbyCanvases.CurrentRoomCanvas.Hide();
            _lobbyCanvases.CreateOrJoinRoomCanvas.Show();
        }
    }
}