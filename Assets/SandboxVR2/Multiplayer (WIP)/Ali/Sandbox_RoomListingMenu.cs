using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SandboxVRSeason2.Framework.Extension;
using Photon.Realtime;

namespace SandboxVRSeason2.Framework.Multiplayer
{
    public class Sandbox_RoomListingMenu : MonoBehaviourPunCallbacks
    {
        [SerializeField]
        private Transform _content;
        [SerializeField]
        private Sandbox_RoomListing _roomListing;

        private List<Sandbox_RoomListing> _listings = new List<Sandbox_RoomListing>();
        private Sandbox_LobbyCanvases _roomsCanvases;

        private List<RoomInfo> _roomOnLobby = new List<RoomInfo>();

        public void FirstInitialize(Sandbox_LobbyCanvases canvases)
        {
            _roomsCanvases = canvases;
        }

        public override void OnEnable()
        {
            base.OnEnable();
            RefreshLobby(_roomOnLobby);
            print("room listing enable");
        }

        public override void OnJoinedRoom()
        {
            print("join room listing");
            _roomsCanvases.CreateOrJoinRoomCanvas.Hide();
            _roomsCanvases.CurrentRoomCanvas.Show();
            _content.DestroyChildren();
            _listings.Clear();
        }

        public override void OnRoomListUpdate(List<RoomInfo> roomList)
        {
            _roomOnLobby.Clear();
            _roomOnLobby = roomList;

            RefreshLobby(roomList);
        }

        private void RefreshLobby(List<RoomInfo> roomList)
        {
            foreach (RoomInfo info in roomList)
            {
                // Removed from room list
                if (info.RemovedFromList)
                {
                    int index = _listings.FindIndex(x => x.RoomInfo.Name == info.Name);
                    if (index != -1)
                    {
                        Destroy(_listings[index].gameObject);
                        _listings.RemoveAt(index);
                    }
                }
                // Added to rooms list
                else
                {
                    int index = _listings.FindIndex(x => x.RoomInfo.Name == info.Name);

                    if (index == -1)
                    {
                        // Instantiate room for anyone in lobby
                        Sandbox_RoomListing listing = Instantiate(_roomListing, _content);
                        if (listing != null)
                        {
                            listing.SetRoomInfo(info);
                            _listings.Add(listing);
                        }
                    }
                    else
                    {

                    }
                }

                int roomI = _listings.FindIndex(x => x.RoomInfo.Name == info.Name);
                if (roomI != -1)
                    _listings[roomI].RefreshRoom(info);
            }
        }
    }
}