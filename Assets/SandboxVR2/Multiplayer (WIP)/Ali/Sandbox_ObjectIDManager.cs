using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SandboxVRSeason2.Framework.Multiplayer
{
    public class Sandbox_ObjectIDManager : MonoBehaviour
    {
        public static Sandbox_ObjectIDManager I;

        [FoldoutGroup("Debug"), ReadOnly, SerializeField] private int _tempID = 0;

        [FoldoutGroup("Debug"), ReadOnly, SerializeField] private List<string> _debugObjectID = new List<string>();

        [FoldoutGroup("Debug"), ReadOnly, SerializeField] private List<Framework.Sandbox_ObjectID> _idPool = new List<Framework.Sandbox_ObjectID>();

        private void Awake()
        {
            I = this;
        }

        void Start()
        {
            GetIDCount();
        }

        [Button(ButtonSizes.Large, Name = "Setup Sandbox_Object IDs")]
        public void SetupSandboxObjectID()
        {
            _tempID = 0;
            _idPool.Clear();
            _debugObjectID.Clear();

            foreach (var item in FindObjectsOfType<Framework.Sandbox_ObjectID>(true))
            {
                item.id = _tempID++;
                _idPool.Add(item);
                _debugObjectID.Add(item.id + " | " + item.gameObject.name);
            }
        }

        public void AddSandboxObjectID(Framework.Sandbox_ObjectID sandbox_ObjectID)
        {
            _idPool.Add(sandbox_ObjectID);
            _debugObjectID.Add(sandbox_ObjectID.id + " | " + sandbox_ObjectID.gameObject.name);
        }

        public int GetIDCount()
        {
            print("id count: " + _idPool.Count);
            return _idPool.Count;
        }

        public Framework.Sandbox_ObjectID GetObjectID(int id)
        {
            return _idPool[id];
        }
    }
}
