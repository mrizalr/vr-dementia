using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SandboxVRSeason2.Framework.Multiplayer
{
    [CreateAssetMenu(menuName = "Sandbox/Multiplayer/GameSettings")]
    public class Sandbox_GameSettings : Sandbox_SingletonScriptableObject<Sandbox_GameSettings>
    {
        [SerializeField]
        private string _gameVersion = "0.0.0";
        public string GameVersion { get { return _gameVersion; } }
        [SerializeField]
        private string _nickName = "Player";
        public string NickName
        {
            get
            {
                int value = Random.Range(0, 999);
                return _nickName + value.ToString();
            }
        }
    }
}