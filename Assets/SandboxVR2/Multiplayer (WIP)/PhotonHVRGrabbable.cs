using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using HurricaneVR.Framework.Core;
using HurricaneVR.Framework.Core.Grabbers;
using SandboxVRSeason2.Framework;
using SandboxVRSeason2.Framework.Multiplayer;

public class PhotonHVRGrabbable : MonoBehaviourPun
{
    Sandbox_BroadcastMethods broadcast;

    void Start()
    {
        broadcast = GetComponent<Sandbox_BroadcastMethods>();

        if (Sandbox_ScenarioManager.instance.multiplayerMode == MultiplayerModeEnum.ONLINE){
            HVRGrabbable[] grabbables = GameObject.FindObjectsOfType<HVRGrabbable>();
            for (int i = 0; i < grabbables.Length; i++)
            {
                int j = i;
                grabbables[j].HandGrabbed.AddListener(
                    delegate {
                        if(grabbables[j].GetComponent<PhotonView>()){
                            print("grabb");
                            PhotonView photonView = grabbables[j].GetComponent<PhotonView>();
                            photonView.RequestOwnership();
                            if(grabbables[j].GetComponent<Sandbox_Object>())
                                photonView.RPC(nameof(Sandbox_BroadcastMethods.DisableObjectFromOther), RpcTarget.OthersBuffered, grabbables[j].GetComponent<Sandbox_Object>().id);

                            PhotonView[] childs = photonView.GetComponentsInChildren<PhotonView>();
                            if (childs.Length > 0)
                                for(int k = 0; k < childs.Length; k++)
                                {
                                    childs[k].RequestOwnership();
                                }
                        }else{
                            print(grabbables[j].name);
                        }
                    }
                );
            }
        }
    }
}
