using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using SandboxVRSeason2.Framework.Multiplayer;

public class PhotonParticleView : MonoBehaviourPun
{
    private PhotonView photonView;
    public ParticleSystem particleSystem;

    Sandbox_BroadcastMethods broadcast;
    
    void Awake()
    {
        photonView = GetComponent<PhotonView>();
    }

    private void Start()
    {
        broadcast = GetComponent<Sandbox_BroadcastMethods>();
    }

    public void UpdateParticlePlay(bool value){
        photonView.RPC(nameof(broadcast.PlayParticle), RpcTarget.OthersBuffered, value, particleSystem);
    }
}
