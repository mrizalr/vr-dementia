using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.UI;

public class NetworkManager : MonoBehaviourPunCallbacks
{
    public GameObject createButton;
    public Transform roomContainer;
    public GameObject RoomListItem;
    string st = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    public void ConnectToServer(){
        PhotonNetwork.ConnectUsingSettings();
        print("Try to connect...");
    }
    public override void OnConnectedToMaster()
    {
        print("connected to server");
        base.OnConnectedToMaster();
        PhotonNetwork.JoinLobby();
    }

    public override void OnJoinedLobby()
    {
        base.OnJoinedLobby();
        print("Joined Lobby");
        createButton.SetActive(true);
    }
    public void JoinRoom(string name){
        PhotonNetwork.LoadLevel(1);
        PhotonNetwork.JoinRoom(name);
    }
    public void InitializeRoom(){
        RoomOptions roomOptions = new RoomOptions();
        roomOptions.MaxPlayers = 10;
        roomOptions.IsVisible = true;
        roomOptions.IsOpen = true;
        string roomName = st[Random.Range(0,st.Length)]+st[Random.Range(0,st.Length)]+st[Random.Range(0,st.Length)]+st[Random.Range(0,st.Length)]+st[Random.Range(0,st.Length)]+"";

        PhotonNetwork.JoinOrCreateRoom(roomName, roomOptions, TypedLobby.Default);
        PhotonNetwork.LoadLevel(1);
    }
    public override void OnJoinedRoom(){
        print("Join room");
        base.OnJoinedRoom();
    }
    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        print("a new player joined the room");
        base.OnPlayerEnteredRoom(newPlayer);
    }
    // Start is called before the first frame update
    void Start()
    {
        ConnectToServer();
    }
    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        for (int i = 0; i < roomContainer.childCount; i++)
        {
            Destroy(roomContainer.GetChild(i).gameObject);
        }
        foreach (var item in roomList)
        {
            GameObject go = Instantiate(RoomListItem,roomContainer);
            go.GetComponentInChildren<TMPro.TextMeshProUGUI>().text = item.Name;
            go.GetComponent<SB_UIButton>().onDown.AddListener(()=>{
                JoinRoom(item.Name);
            });
        }
        base.OnRoomListUpdate(roomList);
    }
}
