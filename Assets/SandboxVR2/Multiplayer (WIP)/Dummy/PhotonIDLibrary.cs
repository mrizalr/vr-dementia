using Photon.Pun;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhotonIDLibrary : MonoBehaviour
{
    [FoldoutGroup("Debug"), ReadOnly, SerializeField]
    private List<int> _photonIDs;
    // Start is called before the first frame update
    void Start()
    {
        PhotonView[] photonViews = GameObject.FindObjectsOfType<PhotonView>(true);

        foreach (var item in photonViews)
        {
            _photonIDs.Add(item.ViewID);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
