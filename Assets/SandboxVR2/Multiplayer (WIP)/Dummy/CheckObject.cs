using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Photon.Pun;
using SandboxVRSeason2.Framework.Multiplayer;
using SandboxVRSeason2.Framework;

public class CheckObject : Sandbox_Object
{
    [SerializeField]
    private TextMeshProUGUI _checkTMP;
    public TextMeshProUGUI CheckTMP { get { return _checkTMP; } set { _checkTMP = value; } }
    [SerializeField]
    private SB_UIButton _checkBtn;

    private Sandbox_BroadcastMethods _broadcast;

    public override bool linearLock { get => throw new System.NotImplementedException(); set => throw new System.NotImplementedException(); }

    private void Start()
    {
        _broadcast = GetComponent<Sandbox_BroadcastMethods>();

        PhotonView photon = GetComponent<PhotonView>();

        _checkBtn.onDown.AddListener(delegate {
            photon.RPC(nameof(_broadcast.RPC_CheckObj), RpcTarget.AllBuffered, id);
        });
    }

    private void Reset()
    {
        base.Reset();
    }


}
