using System.Runtime.InteropServices;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using Photon.Pun;
using TMPro;

public class NetworkPlayer : MonoBehaviour
{
    public Transform head;
    public Transform leftHand;
    public Transform rightHand;
    public Animator leftHandAnimator;
    public Animator rightHandAnimator;
    private PhotonView photonView;
    public TextMeshProUGUI text;
    public string name = "player";
    // Start is called before the first frame update
    void Start()
    {
        photonView = GetComponent<PhotonView>();
        Collider[] leftHandColliders = leftHand.GetComponentsInChildren<Collider>();
        for (int i = 0; i < leftHandColliders.Length; i++)
        {
            leftHandColliders[i].enabled = false;
        }
        Collider[] rightHandColliders = rightHand.GetComponentsInChildren<Collider>();
        for (int i = 0; i < rightHandColliders.Length; i++)
        {
            rightHandColliders[i].enabled = false;
        }

        PlayerPrefs.SetInt("master", photonView.Owner.IsMasterClient ? 1 : 0);

        if (photonView.IsMine){
            photonView.Owner.NickName = name;
            leftHand.GetComponentInChildren<Renderer>().enabled = false;
            rightHand.GetComponentInChildren<Renderer>().enabled = false;
            head.GetComponentInChildren<Renderer>().enabled = false;

            // Debug
            print("head name obj: " + GameObject.FindGameObjectWithTag("Head").gameObject.name);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(photonView.IsMine){
            text.text = photonView.Owner.NickName;

            MirrorPosition(head,GameObject.FindGameObjectWithTag("Head").transform);
            MirrorPosition(leftHand, GameObject.FindGameObjectWithTag("LeftHand").transform);
            MirrorPosition(rightHand, GameObject.FindGameObjectWithTag("RightHand").transform);
            MirrorPositionWithChild(leftHand.GetChild(0),GameObject.FindGameObjectWithTag("LeftHand").transform.GetChild(0));
            MirrorPositionWithChild(rightHand.GetChild(0), GameObject.FindGameObjectWithTag("RightHand").transform.GetChild(0));

            //UpdateHandAnimation(InputDevices.GetDeviceAtXRNode(XRNode.LeftHand), leftHandAnimator);
            //UpdateHandAnimation(InputDevices.GetDeviceAtXRNode(XRNode.RightHand), rightHandAnimator);
        }
    }
    void UpdateHandAnimation(InputDevice targetDevice, Animator handAnimator)
    {
        if(targetDevice.TryGetFeatureValue(CommonUsages.trigger, out float triggerValue))
        {
            handAnimator.SetFloat("Flex", triggerValue);
        }
        else
        {
            handAnimator.SetFloat("Flex", 0);
        }
        if (targetDevice.TryGetFeatureValue(CommonUsages.grip, out float gripValue))
        {
            handAnimator.SetFloat("Pinch", gripValue);
        }
        else
        {
            handAnimator.SetFloat("Pinch", 0);
        }
    }
    void MirrorPositionWithChild(Transform target, Transform mirror){
        Transform[] targetTrans = target.GetComponentsInChildren<Transform>();
        Transform[] mirrorTrans = mirror.GetComponentsInChildren<Transform>();
        for (int i = 0; i < mirrorTrans.Length; i++)
        {
            targetTrans[i].position = mirrorTrans[i].position;
            targetTrans[i].rotation = mirrorTrans[i].rotation;
        }
    }
    void MirrorPosition(Transform target, Transform mirror){
        target.position = mirror.position;
        target.rotation = mirror.rotation;
    }
    void MapPosition(Transform target, XRNode node){
        InputDevices.GetDeviceAtXRNode(node).TryGetFeatureValue(CommonUsages.devicePosition, out Vector3 position);
        InputDevices.GetDeviceAtXRNode(node).TryGetFeatureValue(CommonUsages.deviceRotation, out Quaternion rotation);

        target.localPosition = position;
        target.localRotation = rotation;
    }
}
