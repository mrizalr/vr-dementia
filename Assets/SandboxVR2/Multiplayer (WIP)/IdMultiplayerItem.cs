using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Multiplayer
{
    public interface IdMultiplayerItem
    {
        int id 
        {
            get;
            set;
        }        
    }
}