using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.Events;

namespace SandboxVRSeason2.Framework
{
    public class Sandbox_DoorController : Sandbox_Object
    {

        [SerializeField] private float MinAngle;
        [SerializeField] private float MaxAngle;
        [SerializeField, ReadOnly] private float currentAngle;

        [Space]
        public bool LockOnStart = true;
        [SerializeField, ReadOnly] private bool isLocked = false;

        [FoldoutGroup("Event"), ListDrawerSettings(ShowIndexLabels = true)] public Sandbox_DoorEvent[] doorEvent;

        [FoldoutGroup("Setting"), SerializeField] private HingeJoint _hinge;
        [FoldoutGroup("Setting"), SerializeField] private Rigidbody _rigidbody;
        [FoldoutGroup("Setting"), SerializeField] private float offset = 2f;
        [FoldoutGroup("Debug"), ReadOnly, SerializeField] private bool _isLinearLocked;

        void Start()
        {
            if (!_hinge)
                _hinge = GetComponent<HingeJoint>();
            if (!_rigidbody)
                _rigidbody = GetComponent<Rigidbody>();

            if (LockOnStart)
                SetDoorLock(true);
            else SetDoorLock(false);
        }


        private void Update()
        {
            currentAngle = _hinge.angle;

            foreach (var _dial in doorEvent)
            {
                if (_dial.isFulfilled)
                {
                    if (CheckOffset(_dial.customAngle) < offset) return;
                    else
                    {
                        _dial.isFulfilled = false;
                        _dial.onUnfulfilled.Invoke();
                    }
                }

                if (!_dial.isActive) return;

                var isFulfilled = false;

                if (CheckOffset(_dial.customAngle) < offset) isFulfilled = true;

                if (isFulfilled)
                {
                    _dial.onFulfilled.Invoke();
                    _dial.isFulfilled = true;
                    if (_dial.deactiveOnFullfilled)
                        _dial.isActive = false;
                }

            }
        }

        public override bool linearLock
        {
            get { return _isLinearLocked; }
            set
            {
                if (!_linearException && Sandbox_ScenarioManager.instance.mode == ScenarioModeEnum.LINEAR)
                {
                    _isLinearLocked = value;
                    SetLock();
                    foreach (var outline in outlineList) outline.enabled = !value;
                }
            }
        }

        public void SetDoorLock(bool _value)
        {
            isLocked = _value;
            SetLock();
        }

        private void SetLock()
        {
            var _isLocked = isLocked || _isLinearLocked;
            var limits = _hinge.limits;
            limits.min = isLocked ? 0f : MinAngle;
            limits.max = isLocked ? 0f : MaxAngle;
            _hinge.limits = limits;

            _rigidbody.constraints = _isLocked ? RigidbodyConstraints.FreezeAll : RigidbodyConstraints.None;
        }

        public void SetEventActivated(int _index)
        {
            doorEvent[_index].isActive = true;
        }
        public void SetEventDeactivated(int _index)
        {

            doorEvent[_index].isActive = false;
        }
        private float CheckOffset(float _value)
        {
            return Mathf.Abs(currentAngle - _value);
        }

        [System.Serializable]
        public class Sandbox_DoorEvent
        {
            public int customAngle;

            public UnityEvent onFulfilled;
            public UnityEvent onUnfulfilled;

            [FoldoutGroup("Setting")] public bool isActive = true;
            [FoldoutGroup("Setting")] public bool deactiveOnFullfilled = false;
            [FoldoutGroup("Setting"), ReadOnly] public bool isFulfilled = false;
        }
    }
}
