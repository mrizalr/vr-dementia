using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SandboxVRSeason2.Framework
{
    [CreateAssetMenu(fileName = "New Instruction Data", menuName = "Sandbox/Instruction Data", order = 0)]
    public class Sandbox_InstructionData : ScriptableObject
    {
        [System.Serializable]
        public class Instruction
        {
            public string title;
            public Sprite image;
            [TextArea]
            public string content;
        }

        public List<Instruction> instructions;
    }
}
