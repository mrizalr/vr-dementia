using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using HurricaneVR.Framework.Core;
using HurricaneVR.Framework.Core.Sockets;
using HurricaneVR.Framework.Core.Grabbers;
using Sirenix.OdinInspector;
using System;
using UnityEditor;
using System.IO;
using DG.Tweening;
using Photon.Pun;
using SandboxVRSeason2.Framework.Multiplayer;
#if UNITY_EDITOR
using Sirenix.OdinInspector.Editor;
#endif

namespace SandboxVRSeason2.Framework
{
    [RequireComponent(typeof(HVRGrabbable), typeof(Sandbox_Socketable), typeof(Rigidbody))]
    public class SB_Item : Sandbox_Object
    {
        [HorizontalGroup]
        public Sandbox_ObjectProfile profile;

        [SerializeField] private bool isKinematic;

        public bool IsKinematic
        {
            get
            {
                return isKinematic;
            }
            set
            {
                isKinematic = value;
                SetPhyshics();
            }
        }

        [FoldoutGroup("Advance")] public Sandbox_ToolSlot tool;
        [Tooltip("If true, tools deactivated when item not in socket")]
        [FoldoutGroup("Advance"), ShowIf("@tool != null")] public bool toolNeedSocket;
        [FoldoutGroup("Advance")] public bool remainsKinematic;
        [FoldoutGroup("Advance")] public bool remainsTrigger;

        [FoldoutGroup("LockPos")] public bool lockPosX;
        [FoldoutGroup("LockPos")] public bool lockPosY;
        [FoldoutGroup("LockPos")] public bool lockPosZ;
        [FoldoutGroup("Advance")] Vector3 initLockPos;

        [FoldoutGroup("Advance")] public bool lockRotX;
        [FoldoutGroup("Advance")] public bool lockRotY;
        [FoldoutGroup("Advance")] public bool lockRotZ;
        [FoldoutGroup("Advance")] Vector3 initLockRot;

        [FoldoutGroup("Event")] public UnityEvent onSocketed;
        [FoldoutGroup("Event")] public UnityEvent onUnSocketed;

        [Space(3)]
        [FoldoutGroup("Event")] public UnityEvent onHandGrabbed;
        [FoldoutGroup("Event")] public UnityEvent onHandReleased;

        [FoldoutGroup("Debug"), ReadOnly, SerializeField] private bool _itemLock;
        [FoldoutGroup("Debug"), ReadOnly, SerializeField] private bool _toolsLock;
        [FoldoutGroup("Debug"), ReadOnly, SerializeField] private bool _linearLock;

        [FoldoutGroup("Debug"), ReadOnly, SerializeField] public HVRGrabbable grabbable;
        [FoldoutGroup("Debug"), ReadOnly, SerializeField] public Sandbox_Socketable socketable;
        [FoldoutGroup("Debug"), ReadOnly, SerializeField] private SB_Socket socket;
        [FoldoutGroup("Debug"), ReadOnly, SerializeField] private SB_Socket targetSocket;
        [FoldoutGroup("Debug"), ReadOnly, SerializeField] private Rigidbody rb;


        [Header("Hologram")]
        [FoldoutGroup("Debug"), ReadOnly] public GameObject _hologramListParent;
        [FoldoutGroup("Debug"), ReadOnly, SerializeField] private Dictionary<MeshRenderer, MeshRenderer> _hologramList = new Dictionary<MeshRenderer, MeshRenderer>();
        [FoldoutGroup("Debug"), ReadOnly, SerializeField] private Rigidbody hologramRB = null;
        [FoldoutGroup("Debug"), ReadOnly, SerializeField] public bool isInit = false;
        private GameObject temporaryHologramMesh;
        private bool isHologramOn = false;
        //private Coroutine hologramCoroutine;
        public UnityAction<bool> hologramAction;
        private bool isHologramMoveAnim;


        [FoldoutGroup("Debug"), ReadOnly, SerializeField] public List<SB_Item> higherItemList;

        Sandbox_BroadcastMethods broadcast;

        private Vector3 originScale;

        private void Awake()
        {
            originScale = transform.lossyScale;
        }

        private void Start()
        {
            grabbable = GetComponent<HVRGrabbable>();
            socketable = GetComponent<Sandbox_Socketable>();
            rb = GetComponent<Rigidbody>();

            grabbable.HandGrabbed.AddListener(ItemHandGrabbed);
            grabbable.HandReleased.AddListener(ItemHandReleased);
            grabbable.Socketed.AddListener(ItemSocketed);
            grabbable.UnSocketed.AddListener(ItemUnSocketed);
            grabbable.HoverEnter.AddListener(ItemHoverEnter);
            grabbable.HoverExit.AddListener(ItemHoverExit);

            if (tool) tool.ToolComplete.AddListener(() => { toolsLock = !toolsLock; });

            SetPhyshics();
            SetLayer();
            RefreshHologram();

            foreach (var lowerItem in GetComponentsInChildren<SB_Item>())
                if (lowerItem != this) lowerItem.higherItemList.Add(this);

            if (tool && toolNeedSocket)
            {
                if (socket) tool.SetToolActive(true);
                else tool.SetToolActive(false);
            }
            initLockPos = transform.localPosition;
            initLockRot = transform.localEulerAngles;

            isInit = true;

            broadcast = GetComponent<Sandbox_BroadcastMethods>();
        }

        private void Update()
        {
            SnapHologramControl();
            if (lockPosX || lockPosY || lockPosZ)
            {
                var n = transform.localPosition;
                if (lockPosX)
                    n.x = initLockPos.x;
                if (lockPosY)
                    n.y = initLockPos.y;
                if (lockPosZ)
                    n.z = initLockPos.z;

                transform.localPosition = n;
            }
            if (lockRotX || lockRotY || lockRotZ)
            {
                var n = transform.localEulerAngles;
                if (lockRotX)
                    n.x = initLockRot.x;
                if (lockRotY)
                    n.y = initLockRot.y;
                if (lockRotZ)
                    n.z = initLockRot.z;

                transform.localEulerAngles = n;
            }
        }

        private void LateUpdate()
        {
            //if (lockPosX || lockPosY || lockPosZ)
            //{
            //    var n = transform.localPosition;
            //    if (lockPosX)
            //        n.x = initLockPos.x;
            //    if (lockPosY)
            //        n.y = initLockPos.y;
            //    if (lockPosZ)
            //        n.z = initLockPos.z;

            //    transform.localPosition = n;
            //}
            //if (lockRotX || lockRotY || lockRotZ)
            //{
            //    var n = transform.localEulerAngles;
            //    if (lockRotX)
            //        n.x = initLockRot.x;
            //    if (lockRotY)
            //        n.y = initLockRot.y;
            //    if (lockRotZ)
            //        n.z = initLockRot.z;

            //    transform.localEulerAngles = n;
            //}
        }

        public bool itemLock
        {
            get { return _itemLock; }
            set { _itemLock = value; CheckLock(); }
        }
        public bool toolsLock
        {
            get { return _toolsLock; }
            set
            {
                _toolsLock = value;

                if (Sandbox_ScenarioManager.instance.multiplayerMode == MultiplayerModeEnum.ONLINE && !connection)
                {
                    PhotonView photonView = GetComponent<PhotonView>();
                    //photonView.RequestOwnership();
                    photonView.RPC(nameof(broadcast.BroadcastItemToolsLock), RpcTarget.OthersBuffered, value, id);
                }
                connection = false;

                CheckLock();
                if (socket) socket.toolLock = value;
            }
        }
        public void socketLock(bool _value)
        {
            if (socket)
            {
                socket.slotLock = _value;
            }
        }

        public bool connection = false;
        public void ItemSocketed(HVRSocket _socket, HVRGrabbable _grabbable)
        {
            if (Sandbox_ScenarioManager.instance.multiplayerMode == MultiplayerModeEnum.ONLINE && !connection)
            {
                PhotonView photonView = GetComponent<PhotonView>();
                photonView.RPC(nameof(broadcast.BroadcastSocketed), RpcTarget.OthersBuffered, _socket.GetComponent<Sandbox_Object>().id, _grabbable.GetComponent<Sandbox_Object>().id);
            }
            connection = false;

            if (_socket.GetComponent<SB_Socket>())
                socket = _socket.GetComponent<SB_Socket>();

            if (toolNeedSocket) tool.SetToolActive(true);

            onSocketed.Invoke();

            foreach (var _higherItem in GetComponentsInParent<SB_Item>())
            {
                if (_higherItem == this) continue;
                _higherItem.RefreshHologram();
                higherItemList.Add(_higherItem);
            }

            var parent = transform.parent;
            transform.parent = null;
            transform.localScale = originScale;
            transform.parent = parent;

        }

        public void ItemUnSocketed(HVRSocket _socket, HVRGrabbable _grabbable)
        {
            if (Sandbox_ScenarioManager.instance.multiplayerMode == MultiplayerModeEnum.ONLINE && !connection)
            {
                PhotonView photonView = GetComponent<PhotonView>();
                photonView.RPC(nameof(broadcast.BroadcastUnSocketed), RpcTarget.OthersBuffered, _socket.GetComponent<Sandbox_Object>().id, _grabbable.GetComponent<Sandbox_Object>().id);
            }
            connection = false;

            socket = null;
            if (tool && toolNeedSocket) tool.SetToolActive(false);

            onUnSocketed.Invoke();

            foreach (var _higherItem in higherItemList)
            {
                if (_higherItem == this) continue;

                foreach (var mrender in GetComponentsInChildren<MeshRenderer>())
                {
                    if (!_higherItem._hologramList.ContainsKey(mrender)) continue;
                    Destroy(_higherItem._hologramList[mrender].gameObject);
                    _higherItem._hologramList.Remove(mrender);
                }
            }
        }

        void ItemHoverEnter(HVRGrabberBase _grabber, HVRGrabbable _grabbable)
        {
            HVRSocket _socket = _grabber as HVRSocket;
            if (_socket != null && _socket.IsValid(_grabbable))
            {
                targetSocket = _socket.GetComponent<SB_Socket>();
                ToggleHologram(true);
                ToggleMesh(false);
            }
        }

        void ItemHoverExit(HVRGrabberBase _grabber, HVRGrabbable _grabbable)
        {
            if (_grabber is HVRSocket && !isHologramMoveAnim)
            {
                ToggleHologram(false);
                ToggleMesh(true);
            }
        }


        public void ItemHandGrabbed(HVRGrabberBase _grabber, HVRGrabbable _grabbable)
        {
            print("kepanggil");

            if (Sandbox_ScenarioManager.instance != null)
            {
                if (Sandbox_ScenarioManager.instance.multiplayerMode == MultiplayerModeEnum.ONLINE && !connection)
                {
                    PhotonView photonView = GetComponent<PhotonView>();
                    if (_grabber.GetComponent<Sandbox_Object>())
                        photonView.RPC(nameof(broadcast.BroadcastHandGrabbed), RpcTarget.OthersBuffered, _grabber.GetComponent<Sandbox_Object>().id, _grabbable.GetComponent<Sandbox_Object>().id);
                    else
                        print(_grabber.name);
                }
            }
            connection = false;

            if (isKinematic)
            {
                rb.isKinematic = remainsKinematic ? true : false;
            }

            if (profile)
                if (Sandbox_HologramManager.instance != null)
                    Sandbox_HologramManager.instance.CallHologram(profile, true);

            Debug.Log($"{_grabber.name}");
            onHandGrabbed.Invoke();
        }

        public void ItemHandReleased(HVRGrabberBase _grabber, HVRGrabbable _grabbable)
        {
            if (Sandbox_ScenarioManager.instance != null)
            {
                if (Sandbox_ScenarioManager.instance.multiplayerMode == MultiplayerModeEnum.ONLINE && !connection)
                {
                    PhotonView photonView = GetComponent<PhotonView>();
                    if (_grabber.GetComponent<Sandbox_Object>())
                        photonView.RPC(nameof(broadcast.BroadcastHandReleased), RpcTarget.OthersBuffered, _grabber.GetComponent<Sandbox_Object>().id, _grabbable.GetComponent<Sandbox_Object>().id);
                    else
                        print(_grabber.name);
                }
            }
            connection = false;

            if (isKinematic)
            {
                rb.isKinematic = true;
            }

            if (profile)
                if (Sandbox_HologramManager.instance != null)
                    Sandbox_HologramManager.instance.CallHologram(profile, false);

            if (_grabbable.SocketHoverer)
            {
                var _desiredPoint = targetSocket.currentSlot.desiredPoint != null ? targetSocket.currentSlot.desiredPoint : targetSocket.transform;
                StartCoroutine(AnimateHologram(targetSocket.GetComponent<HVRSocket>(), _desiredPoint));
            }

            onHandReleased.Invoke();

        }

        void SetPhyshics()
        {
            grabbable = GetComponent<HVRGrabbable>();
            rb = GetComponent<Rigidbody>();

            if (isKinematic)
            {
                grabbable.RemainsKinematic = remainsKinematic ? true : false;
                rb.isKinematic = true;
                //rb.useGravity = false;
            }
            else
            {
                grabbable.RemainsKinematic = true;
                rb.isKinematic = false;
                //rb.useGravity = true;
            }
        }

        void SetLayer()
        {
            foreach (var collider in GetComponentsInChildren<Collider>())
            {
                if (collider.GetComponent<SB_Socket>() || collider.GetComponent<Sandbox_ToolSlot>()) continue;
                collider.gameObject.layer = LayerMask.NameToLayer("Grabbable");
            }
        }

        void CheckLock()
        {
            bool locked = _itemLock || toolsLock ? true : false;

            GetComponent<Rigidbody>().detectCollisions = !locked;
        }

        public override bool linearLock
        {
            get { return _linearLock; }
            set
            {
                if (!_linearException) // Sandbox_ScenarioManager.instance.mode == ScenarioModeEnum.LINEAR)
                {
                    _linearLock = value;

                    if(grabbable == null)
                    {
                        grabbable = GetComponent<HVRGrabbable>();
                    }

                    grabbable.linearLock = value;

                    List<HVRGrabberBase> grabberList = new List<HVRGrabberBase>(grabbable.Grabbers);

                    foreach (var _grabber in grabberList)
                    {
                        if (_grabber is HVRHandGrabber)
                            _grabber.ForceRelease();
                    }

                    if (Sandbox_ScenarioManager.instance.mode == ScenarioModeEnum.LINEAR)
                        foreach (var outline in outlineList) outline.enabled = !value;
                }
            }
        }

        #region Hologram
        void RefreshHologram()
        {
            if (!profile) return;
            if (_hologramListParent == null)
            {
                _hologramListParent = new GameObject();
                var _rb = _hologramListParent.AddComponent<Rigidbody>();
                _rb.useGravity = false;
                _rb.isKinematic = true;
                _hologramListParent.transform.localScale = Vector3.one;
                _hologramListParent.transform.SetParent(transform);
                _hologramListParent.transform.localPosition = Vector3.zero;
                _hologramListParent.transform.localRotation = Quaternion.identity;

                _hologramListParent.gameObject.name = profile.name + "'s Hologram List Parent";
                _hologramListParent.transform.SetParent(transform.parent);
            }

            var s = 0;
            foreach (var mesh in GetComponentsInChildren<MeshRenderer>())
            {
                if (mesh.CompareTag("Hologram") || _hologramList.ContainsKey(mesh)) continue;

                temporaryHologramMesh = new GameObject("s", typeof(MeshRenderer), typeof(MeshFilter), typeof(Sandbox_HologramMesh));
                temporaryHologramMesh.tag = "Hologram";
                temporaryHologramMesh.transform.SetParent(mesh.transform);
                temporaryHologramMesh.transform.localRotation = Quaternion.Euler(Vector3.zero);
                temporaryHologramMesh.transform.localScale = Vector3.one;
                temporaryHologramMesh.transform.SetParent(_hologramListParent.transform);
                _hologramListParent.transform.position = transform.position;
                temporaryHologramMesh.transform.position = mesh.transform.position;
                temporaryHologramMesh.GetComponent<MeshFilter>().mesh = mesh.GetComponent<MeshFilter>().mesh;

                MeshRenderer _mr = temporaryHologramMesh.GetComponent<MeshRenderer>();
                _hologramList.Add(mesh, _mr);

                _mr.material = mesh.material;
                _mr.shadowCastingMode = mesh.shadowCastingMode;
                _mr.receiveShadows = mesh.receiveShadows;
                _mr.gameObject.name = gameObject.name + "'s Highlight " + s;
                s++;

                Sandbox_HologramMesh _hm = temporaryHologramMesh.GetComponent<Sandbox_HologramMesh>();
                _hm.SetAction(this);

                temporaryHologramMesh = null;
            }

            hologramRB = _hologramListParent.GetComponent<Rigidbody>();
            ToggleHologram(false);
        }

        public void ToggleHologram(bool value)
        {
            hologramAction?.Invoke(value);
            isHologramOn = value;
        }

        public void ToggleMesh(bool value)
        {
            foreach (var mesh in GetComponentsInChildren<MeshRenderer>())
                if (!mesh.transform.CompareTag("Hologram") && mesh.gameObject.activeInHierarchy)
                    mesh.enabled = value;
        }

        public void SnapHologramControl()
        {
            if (!grabbable || !grabbable.IsHandGrabbed || !grabbable.SocketHoverer)
            {
                if (isHologramOn)
                {
                    ToggleHologram(false);
                    ToggleMesh(true);
                }
                return;
            }

            var _targetSocket = grabbable.SocketHoverer.GetComponent<SB_Socket>();
            if (!_targetSocket || _targetSocket.currentSlot == null || !_targetSocket.currentSlot.desiredPoint)
            {
                if (isHologramOn)
                {
                    ToggleHologram(false);
                    ToggleMesh(true);
                }
                return;
            }

            var _desiredPoint = _targetSocket.currentSlot.desiredPoint;

            if (_targetSocket.currentSlot.alignPosType == PositionTypeEnum.LOCAL)
            {
                var _newPos = _desiredPoint.InverseTransformPoint(grabbable.transform.position);
                var _newRot = grabbable.transform.rotation;

                if (_targetSocket.currentSlot.alignAxis != AxisEnum.X)
                    _newPos.x = 0;
                if (_targetSocket.currentSlot.alignAxis != AxisEnum.Y)
                    _newPos.y = 0;
                if (_targetSocket.currentSlot.alignAxis != AxisEnum.Z)
                    _newPos.z = 0;

                _newRot = _desiredPoint.rotation;

                if (hologramRB)
                {
                    hologramRB.transform.position = _desiredPoint.TransformPoint(_newPos);
                    hologramRB.transform.rotation = _newRot;
                }
            }
            else
            {
                var _newPos = grabbable.transform.position;
                var _newRot = grabbable.transform.rotation;

                if (_targetSocket.currentSlot.alignAxis != AxisEnum.X)
                    _newPos.x = _desiredPoint.position.x;
                if (_targetSocket.currentSlot.alignAxis != AxisEnum.Y)
                    _newPos.y = _desiredPoint.position.y;
                if (_targetSocket.currentSlot.alignAxis != AxisEnum.Z)
                    _newPos.z = _desiredPoint.position.z;

                _newRot = _desiredPoint.rotation;

                if (hologramRB)
                    hologramRB.transform.position = _newPos;
                hologramRB.transform.rotation = _newRot;
            }
        }

        IEnumerator AnimateHologram(HVRSocket _hvrSocket, Transform _target, float _duration = 0.3f)
        {
            isHologramMoveAnim = true;
            _hvrSocket.isAnimatingHologramSnap = true;

            if (hologramRB)
                hologramRB.transform.DOMove(_target.position, _duration);
            hologramRB.transform.DOMove(_target.position, _duration);

            yield return new WaitForSeconds(_duration);
            ToggleHologram(false);
            ToggleMesh(true);

            isHologramMoveAnim = false;
            _hvrSocket.isAnimatingHologramSnap = false;

        }

        #endregion

#if UNITY_EDITOR
        #region Inspector

        bool isHVRComponentShown;

        [MenuItem("Assets/Create/Sandbox/Object Profile")]
        [HideLabel, LabelWidth(40)]
        [HorizontalGroup(40)]
        [Button("New")]
        public void CreateNewObjectProfile()
        {

            var path = EditorUtility.SaveFilePanel(
                     "Save new object profile",
                     "Assets/SandboxVR2/Example_Profiles",
                     "New Object Profile.asset",
                     "asset");

            if (path.Length != 0)
            {
                var relativePath = SandboxUtility.RelativePath("C:/Users/ajila/Documents/Projects/SandBox VR Season 2/", path);

                Sandbox_ObjectProfile asset = ScriptableObject.CreateInstance<Sandbox_ObjectProfile>();
                AssetDatabase.CreateAsset(asset, relativePath);
                AssetDatabase.SaveAssets();
                EditorUtility.FocusProjectWindow();

                profile = asset;
            }

        }

        string buttonName
        {
            get
            {
                if (isHVRComponentShown)
                    return "Hide HVR Compononent";
                else
                    return "Show HVR Compononent";
            }
            set { }
        }

        private void OnValidate()
        {
            GetComponent<Sandbox_Socketable>().objectProfile = profile;

            if (isKinematic != GetComponent<Rigidbody>().isKinematic)
                SetPhyshics();
        }

        [FoldoutGroup("Debug")]
        [Button("$buttonName")]
        public void ShowHVRComponent()
        {
            if (isHVRComponentShown)
            {
                GetComponent<HVRGrabbable>().hideFlags = HideFlags.HideInInspector;
                GetComponent<Sandbox_Socketable>().hideFlags = HideFlags.HideInInspector;
                GetComponent<Rigidbody>().hideFlags = HideFlags.HideInInspector;
                isHVRComponentShown = false;
            }
            else
            {
                GetComponent<HVRGrabbable>().hideFlags = HideFlags.None;
                GetComponent<Sandbox_Socketable>().hideFlags = HideFlags.None;
                GetComponent<Rigidbody>().hideFlags = HideFlags.None;
                isHVRComponentShown = true;
            }
        }

        void Reset()
        {
            base.Reset();

            print("reset");

            GetComponent<HVRGrabbable>().hideFlags = HideFlags.HideInInspector;
            UnityEditorInternal.ComponentUtility.MoveComponentDown(GetComponent<HVRGrabbable>());
            GetComponent<Sandbox_Socketable>().hideFlags = HideFlags.HideInInspector;
            UnityEditorInternal.ComponentUtility.MoveComponentDown(GetComponent<Sandbox_Socketable>());
            GetComponent<Rigidbody>().hideFlags = HideFlags.HideInInspector;
            UnityEditorInternal.ComponentUtility.MoveComponentDown(GetComponent<Rigidbody>());
            isHVRComponentShown = false;

            GetComponent<HVRGrabbable>().AutoApplyLayer = false;

            SetPhyshics();
        }


        [HideLabel, LabelWidth(40)]
        [HorizontalGroup(56)]
        [Button("Update"), ShowIf("ProfileHologramButton")]
        public void UpdateProfileHologram()
        {
            if (!profile) return;

            List<Mesh> _meshList = new List<Mesh>();
            List<Vector3> _meshScaleList = new List<Vector3>();
            List<Vector3> _meshPosList = new List<Vector3>();
            List<Quaternion> _meshRotList = new List<Quaternion>();

            foreach (var item in GetComponentsInChildren<MeshFilter>())
            {
                _meshList.Add(item.sharedMesh);
                _meshScaleList.Add(item.transform.lossyScale);

                if (item.transform.parent == transform || item.transform == transform)
                {
                    _meshPosList.Add(item.transform.localPosition);
                    _meshRotList.Add(item.transform.localRotation);
                }
                else
                {
                    GameObject tempGO = new GameObject();
                    tempGO.transform.parent = item.transform;
                    tempGO.transform.localPosition = Vector3.zero;
                    tempGO.transform.localRotation = Quaternion.Euler(Vector3.zero);
                    tempGO.transform.localScale = Vector3.one;
                    tempGO.transform.parent = transform;
                    _meshPosList.Add(tempGO.transform.localPosition);
                    _meshRotList.Add(tempGO.transform.localRotation);
                    DestroyImmediate(tempGO.gameObject);
                }
            }

            profile.meshList = _meshList.ToArray();
            profile.meshScaleList = _meshScaleList.ToArray();
            profile.meshPosList = _meshPosList.ToArray();
            profile.meshRotList = _meshRotList.ToArray();

            EditorUtility.SetDirty(profile);
        }

        bool ProfileHologramButton()
        {
            if (profile) return true;
            else return false;
        }

        #endregion
        #region Component Destroyer
        [CustomEditor(typeof(SB_Item))]
        public class SB_itemEditor : OdinEditor
        {
            GameObject go;

            protected override void OnEnable()
            {
                go = ((SB_Item)target).gameObject;
            }

            private void OnDestroy()
            {
                if (target == null && go != null)
                {
                    DestroyImmediate(go.GetComponent<Rigidbody>());
                    DestroyImmediate(go.GetComponent<Sandbox_Socketable>());
                    DestroyImmediate(go.GetComponent<HVRGrabbable>());
                    Debug.Log("The component and dependent (Banana) was removed.");
                }
                else if (target == null && go == null)
                {
                    //The gameobject was deleted
                    Debug.Log("The gameobject was deleted from the scene.");
                }
            }

            public override void OnInspectorGUI()
            {
                base.OnInspectorGUI();
            }
        }
        #endregion
#endif
    }
}
