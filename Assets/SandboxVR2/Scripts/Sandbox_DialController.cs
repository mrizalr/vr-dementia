using HurricaneVR.Framework.Components;
using HurricaneVR.Framework.Core;
using HurricaneVR.Framework.Core.Grabbers;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Photon.Pun;
using SandboxVRSeason2.Framework.Multiplayer;

[System.Serializable]
public class AngleChangedEvent : UnityEvent<float>
{ }


namespace SandboxVRSeason2.Framework
{
    public class Sandbox_DialController : Sandbox_Object
    {
        [SerializeField, Range(0, 360)] private int maxRotation;
        [SerializeField] private bool isClampRotation;
        [SerializeField, ReadOnly] private float currentAngle;
        public float CurrentAngle { get { return currentAngle; } set { currentAngle = value; } }
        public bool OverrideCurrentAngleOnStart= false;

        [FoldoutGroup("Event"), ListDrawerSettings(ShowIndexLabels = true)] public Sandbox_DialEvent[] dialEvent;

        [Space(3)]
        [FoldoutGroup("Event")] public UnityEvent onHandGrabbed;
        [FoldoutGroup("Event")] public UnityEvent onHandReleased;
        [SerializeField, FoldoutGroup("Event")] public AngleChangedEvent onAngleChanged;


        [FoldoutGroup("Setting"), SerializeField] private HVRGrabbable dialGrabbable;
        [FoldoutGroup("Setting"), SerializeField] private HVRDial dial;
        [Space]
        [FoldoutGroup("Setting"), SerializeField] private float offset = 2f;

        [FoldoutGroup("Debug"), ReadOnly, SerializeField] private bool _isLinearLocked;

        Sandbox_BroadcastMethods broadcast;


        private void Start()
        {
            StartCoroutine(Initialize());
            broadcast = GetComponent<Sandbox_BroadcastMethods>();
            if (OverrideCurrentAngleOnStart) currentAngle = dialGrabbable.transform.localEulerAngles.y;
        }

        IEnumerator Initialize()
        {
            yield return new WaitUntil(() => dial.isInit);
            dial.MaximumAngle = maxRotation;
            dial.ClampMaxAngle = isClampRotation;

            dialGrabbable.HandGrabbed.AddListener(ItemHandGrabbed);
            dialGrabbable.HandReleased.AddListener(ItemHandReleased);
        }


        private void Update()
        {

            foreach (var _dial in dialEvent)
            {
                if (_dial.isFulfilled)
                {
                    if (CheckOffset(_dial.customAngle) < offset) return;
                    else
                    {

                        _dial.isFulfilled = false;
                        _dial.onUnfulfilled.Invoke();
                    }
                }

                if (!_dial.isActive) return;

                var isFulfilled = false;

                if (CheckOffset(_dial.customAngle) < offset) isFulfilled = true;

                if (isFulfilled)
                {
                    _dial.onFulfilled.Invoke();

                    _dial.isFulfilled = true;
                    if (_dial.deactiveOnFullfilled)
                        _dial.isActive = false;

                }

            }
        }

        [PunRPC]
        void SetFulfilled(int id){
            Sandbox_ObjectID obj = ExtensionMethod.SearchObject(id);
            Sandbox_DialController dialController = obj.GetComponent<Sandbox_DialController>();
            foreach (var _dial in dialController.dialEvent)
            {
                _dial.onFulfilled.Invoke();
                _dial.isFulfilled = true;
                if (_dial.deactiveOnFullfilled)
                    _dial.isActive = false;
            }
        }

        [PunRPC]
        void SetUnFulfilled(int id)
        {
            Sandbox_ObjectID obj = ExtensionMethod.SearchObject(id);
            Sandbox_DialController dialController = obj.GetComponent<Sandbox_DialController>();
            foreach (var _dial in dialController.dialEvent)
            {
                _dial.isFulfilled = false;
                _dial.onUnfulfilled.Invoke();
            }
        }

        public void SetEventActivated(int _index)
        {
            dialEvent[_index].isActive = true;
        }

        public void SetEventDeactivated(int _index)
        {

            dialEvent[_index].isActive = false;
        }
        void ItemHandGrabbed(HVRGrabberBase _grabber, HVRGrabbable _grabbable)
        {
            onHandGrabbed.Invoke();
            onHandGrabbed?.Invoke();
        }

        void ItemHandReleased(HVRGrabberBase _grabber, HVRGrabbable _grabbable)
        {
            onHandReleased.Invoke();
            onHandReleased?.Invoke();
        }

        public override bool linearLock
        {
            get { return _isLinearLocked; }
            set
            {
                if (Sandbox_ScenarioManager.instance.mode == ScenarioModeEnum.LINEAR)
                if (!_linearException && Sandbox_ScenarioManager.instance.mode == ScenarioModeEnum.LINEAR)
                {
                    _isLinearLocked = value;
                    dialGrabbable.linearLock = value;
                    dialGrabbable.GetComponent<Rigidbody>().constraints = value ? RigidbodyConstraints.FreezeAll : RigidbodyConstraints.None;

                    List<HVRGrabberBase> grabberList = new List<HVRGrabberBase>(dialGrabbable.Grabbers);

                    foreach (var _grabber in grabberList)
                    {
                        if (_grabber is HVRHandGrabber)
                            _grabber.ForceRelease();
                    }
                    foreach (var outline in outlineList) outline.enabled = !value;
                }
            }
        }

        private float CheckOffset(float _value)
        {
            return Mathf.Abs(currentAngle - _value);
        }

        [System.Serializable]
        public class Sandbox_DialEvent
        {
            [Range(0, 360)] public int customAngle;

            public UnityEvent onFulfilled;
            public UnityEvent onUnfulfilled;

            [FoldoutGroup("Setting")] public bool isActive = true;
            [FoldoutGroup("Setting")] public bool deactiveOnFullfilled = false;
            [FoldoutGroup("Setting"), ReadOnly] public bool isFulfilled = false;
        }

        bool connection = false;
        public void OnAngleChanged(float angle, float delta, float percent)
        {
            currentAngle = angle;

            if (Sandbox_ScenarioManager.instance.multiplayerMode == MultiplayerModeEnum.ONLINE && !connection)
            {
                PhotonView photonView = GetComponentInChildren<PhotonView>();
                photonView.RequestOwnership();
                photonView.RPC(nameof(broadcast.BroadcastAngleDial), RpcTarget.OthersBuffered, id, currentAngle);

            }
            connection = false;
        }

        public float GetMaxRotation()
        {
            return maxRotation;
        }
        public bool GetIsClamp()
        {
            return isClampRotation;
        }
    }
}