using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

public class FluidContainer : MonoBehaviour
{
    public float fluidContained;
    public float fluidReceivedLimit;
    [Space]
    public UnityEvent onLimitReached;

    [HideInInspector] public bool done = false;

    // Start is called before the first frame update
    void Start()
    {

    }
    public void ResetContainer()
    {
        fluidContained = 0f;
        done = false;
    }
}
