using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
public class FluidEmitter : MonoBehaviour
{
    public Transform top;
    public Transform bottom;
    public ParticleSystem particle;
    [Space]
    public bool isInfinite;
    [HideIfGroup("isInfinite")]
    public float fluidAmount;
    [Space]
    public float emissionRate = 20;
    [Space]
    public float tiltTolerance = 0f;
    private float deltaTime;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame

    void Update()
    {
        var emission = particle.emission;
        if (fluidAmount > 0 || isInfinite) // kalo jumlah airnya masih ada
        {
            if (top.position.y + tiltTolerance >= bottom.position.y) // cek kemiringan
            {
                emission.rateOverTime = new ParticleSystem.MinMaxCurve(0); // gak keluar partikel
            }
            else
            {
                emission.rateOverTime = new ParticleSystem.MinMaxCurve(emissionRate); // keluar partikel
                if (!isInfinite)
                {
                    deltaTime += (Time.deltaTime - deltaTime) * 0.1f;
                    float fps = 1.0f / deltaTime;
                    fluidAmount -= emissionRate / fps;
                    //StartCoroutine(DecreaseFluidAmount()); // jumlah air berkurang over time
                }
            }
        }
        else
        {
            fluidAmount = 0;
            emission.rateOverTime = new ParticleSystem.MinMaxCurve(0); // air habis, parikel gak keluar
        }
    }
}
