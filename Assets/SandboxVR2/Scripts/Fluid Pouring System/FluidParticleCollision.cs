using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FluidParticleCollision : MonoBehaviour
{
    ParticleSystem particle;
    List<ParticleCollisionEvent> collisionEvents;
    private void Start()
    {
        particle = GetComponent<ParticleSystem>();
        collisionEvents = new List<ParticleCollisionEvent>();
    }

    private void OnParticleCollision(GameObject other)
    {
        int numCollisionEvents = particle.GetCollisionEvents(other, collisionEvents);

        FluidContainer container = other.GetComponent<FluidContainer>();

        if (container)
        {
            if (container != null)
            {
                Debug.Log("container ada");
                if (!container.done)
                {
                    for (int i = 0; i < numCollisionEvents; i++)
                    {
                        if (container.fluidContained < container.fluidReceivedLimit)
                        {
                            container.fluidContained += 1f;
                        }
                        else
                        {
                            container.onLimitReached?.Invoke();
                            container.done = true;
                        }
                    }
                }
            }
            else
            {
                Debug.Log("container nda ada");
            }
        }
    }
}
