using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;
using TMPro;
using HurricaneVR.Framework.Shared;
using Hellmade.Sound;
using HurricaneVR.Samples;

namespace SandboxVRSeason2.Framework
{
    public class Sandbox_RadioManager : MonoBehaviour
    {
        public static Sandbox_RadioManager instance;

        [FoldoutGroup("Setting"), SerializeField] private SB_Socket socket;
        [FoldoutGroup("Setting"), SerializeField] private SB_Item radioItem;
        [FoldoutGroup("Setting"), SerializeField] private Transform radioCanvas;
        [FoldoutGroup("Setting"), SerializeField] public Transform radioCanvasLookTarget;
        [FoldoutGroup("Setting"), SerializeField] private Transform radioBubble;
        [FoldoutGroup("Setting"), SerializeField] private Image radioPressIndicator;
        [FoldoutGroup("Setting"), SerializeField] private TextMeshProUGUI radioText;
        [FoldoutGroup("Setting"), SerializeField] private Sandbox_AudioReference audioReference;

        [FoldoutGroup("Debug"), SerializeField, ReadOnly] private SB_RadioMessage currentMessage;
        [FoldoutGroup("Debug"), SerializeField, ReadOnly] private bool isGrabbed;
        [FoldoutGroup("Debug"), SerializeField, ReadOnly] private bool isTalking;
        [FoldoutGroup("Debug"), SerializeField, ReadOnly] private float pressTimeCount;

        private void Awake()
        {
            instance = this;
        }

        private void Start()
        {
            if (!radioCanvasLookTarget)
                radioCanvasLookTarget = Camera.main.transform;
            moveSocket();
            CallMessage();
            radioItem.onHandGrabbed.AddListener(() => { RadioGrabbed(true); });
            radioItem.onHandReleased.AddListener(() => { RadioGrabbed(false); });
        }

        public void moveSocket()
        {
            if (socket && FindObjectsOfType<HVRShoulderSocket>().Length > 0)
            {
                print("poiu");
                foreach (var item in FindObjectsOfType<HVRShoulderSocket>())
                {
                    if (item.transform.localPosition.x < 0)
                    {
                        socket.transform.parent = item.transform.parent;
                        socket.transform.localPosition = item.transform.localPosition;
                        socket.transform.localRotation = item.transform.localRotation;
                        socket.transform.localScale = item.transform.localScale;
                    }
                }
            }
        }

        private void Update()
        {
            radioCanvas.LookAt(radioCanvasLookTarget);

            var buttonState = HVRController.GetButtonState(HVRHandSide.Right, HVRButtons.Trigger);

            if (isGrabbed && buttonState.Active && currentMessage)
            {
                pressTimeCount += Time.deltaTime;
                if (pressTimeCount >= currentMessage.messageTime)
                {
                    var _tempMessage = currentMessage;
                    CallMessage();

                    if (_tempMessage)
                        _tempMessage.onComplete.Invoke();
                }
            }
            else
            {
                if (pressTimeCount != 0)
                    pressTimeCount = 0;
            }

            if (currentMessage)
                radioPressIndicator.fillAmount = pressTimeCount / currentMessage.messageTime;
        }

        void RadioGrabbed(bool value)
        {
            isGrabbed = value;

            if (!value) socket.ForceGrabItem(radioItem);
        }
        public void CallMessage(SB_RadioMessage _message = null)
        {
            currentMessage = _message;
            if (_message != null)
            {
                radioText.text = _message.message;
                radioBubble.gameObject.SetActive(true);
                _message.onStart.Invoke();
            }
            else
            {
                radioText.text = "";
                radioBubble.gameObject.SetActive(false);
            }

        }

        public void CallAudio(SB_RadioMessage _message = null)
        {
            StartCoroutine(CallAudioCoroutine(_message));
        }

        public IEnumerator CallAudioCoroutine(SB_RadioMessage _message = null)
        {
            var audioClip = audioReference.GetClip(_message.audioID);
            if (audioClip)
            {
                _message.onStart.Invoke();
                EazySoundManager.PlayMusic(audioClip, 1f, false, false, 0f, 0f);
                yield return new WaitForSeconds(audioClip.length);
                _message.onComplete.Invoke();
            }
        }

        public SB_Item getRadioItem()
        {
            return radioItem;
        }

        public SB_Socket getRadioSocket()
        {
            return socket;
        }
    }
}
