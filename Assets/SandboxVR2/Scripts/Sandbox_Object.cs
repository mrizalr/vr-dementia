using System.Net.Sockets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using Photon.Pun;
using HurricaneVR.Framework.Core;

namespace SandboxVRSeason2.Framework
{
    public class Sandbox_ObjectID : SerializedMonoBehaviour
    {
        [PropertyOrder(2), FoldoutGroup("Debug"), ReadOnly] public int id;
        [PropertyOrder(2), FoldoutGroup("Debug"), ReadOnly] public string Name { get { return gameObject.name; } }

        public Sandbox_ObjectID()
        {

        }

        public Sandbox_ObjectID(int id)
        {
            this.id = id;
        }

        protected void Reset()
        {
            id = 0;
            Sandbox_ObjectID[] objects = GameObject.FindObjectsOfType<Sandbox_ObjectID>();
            id = objects.Length;
            if (!GetComponent<PhotonView>())
            {
                gameObject.AddComponent<PhotonView>().OwnershipTransfer = OwnershipOption.Request;
            }
        }
    }

    public abstract class Sandbox_Object : Sandbox_ObjectID
    {
        [PropertyOrder(2), FoldoutGroup("Debug"), SerializeField]
        public bool _linearException;
        public abstract bool linearLock { get; set; }
        [PropertyOrder(2), FoldoutGroup("Debug"), SerializeField] public List<SB_Outline> outlineList;
    }
}
