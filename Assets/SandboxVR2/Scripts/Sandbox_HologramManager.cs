using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;

namespace SandboxVRSeason2.Framework
{
    public class Sandbox_HologramManager : SerializedMonoBehaviour
    {
        public static Sandbox_HologramManager instance;

        //[HideInInspector]
        public Dictionary<Sandbox_ObjectProfile, UnityAction<bool>> actionDict;

        private void Awake()
        {
            if (instance != null && instance != this)
                Destroy(this.gameObject);
            else
                instance = this;
            actionDict = new Dictionary<Sandbox_ObjectProfile, UnityAction<bool>>();
        }

        public void CallHologram(Sandbox_ObjectProfile _profile, bool _value)
        {
            if (Sandbox_ScenarioManager.instance.mode == ScenarioModeEnum.LINEAR)
                if (actionDict.ContainsKey(_profile))
                    actionDict[_profile].Invoke(_value);
        }

        public void SetDict(Sandbox_ObjectProfile _profile)
        {
            if (!actionDict.ContainsKey(_profile))
            {
                UnityAction<bool> _newAction = null;

                actionDict.Add(_profile, _newAction);
            }
        }
    }
}
