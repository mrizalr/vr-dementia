using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Tictech.LoadManager;

namespace SandboxVRSeason2.Framework
{
    [CreateAssetMenu(fileName = "New Sub Scenario Data", menuName = "Sandbox/Sub Scenario Data", order = 0)]
    public class Sandbox_SubScenarioData : ScriptableObject
    {
        public int id;
        public string SubScenarioName;
        public Sprite SubScenarioSprite;
        public string Purpose;
        public string SubScenarioScene;
        public GameObject SubScenarioPrefab;
        public Status status;

        public bool isLinear = true;
        public bool isSandbox = true;

    }
}
