using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEditor;
using UnityEngine;

public class SemiProceduralFootIK : MonoBehaviour
{
    [Header("Weights")]
    
    [SerializeField] [Range(0, 1)] private float rightFootWeight = 1;
    [SerializeField] [Range(0, 1)] private float leftFootWeight = 1;
    
    [SerializeField] [Range(0, 1)] private float rightFootRotationWeight = 1;
    [SerializeField] [Range(0, 1)] private float leftFootRotationWeight = 1;
    
    [SerializeField] [Range(0, 1)] private float rightFootHintWeight = 1;
    [SerializeField] [Range(0, 1)] private float leftFootHintWeight = 1;

    [SerializeField] private Vector3 footRotation;

    [Space]
    [Header("Solver Transforms")]
    
    [SerializeField] private Transform rightFootIKHint;
    [SerializeField] private Transform leftFootIKHint;

    [SerializeField] private Transform rightFootSolver;
    [SerializeField] private Transform leftFootSolver;

    [SerializeField] private LayerMask groundMask;

    [Space]
    [Header("Heights")]
    
    [SerializeField] [Range(0, 10)] private float characterHeight;
    [SerializeField] private float heightOffset = 0;
    
    private Animator _animator;

    public float HeightOffset
    {
        get => heightOffset;
        set => heightOffset = value;
    }
    
    // Start is called before the first frame update
    void Start()
    {
        _animator = GetComponent<Animator>();
    }

    private void OnAnimatorIK(int layerIndex)
    {
        SetFootIK(AvatarIKGoal.LeftFoot, ref leftFootSolver, leftFootWeight, leftFootRotationWeight);
        SetFootIK(AvatarIKGoal.RightFoot, ref rightFootSolver, rightFootWeight, rightFootRotationWeight);
        
        SetFootIKHint(AvatarIKHint.LeftKnee, leftFootIKHint.position, leftFootHintWeight);
        SetFootIKHint(AvatarIKHint.RightKnee, rightFootIKHint.position, rightFootHintWeight);
        
        /*_animator.SetIKHintPosition(AvatarIKHint.RightKnee, rightFootIKHint.position);
        _animator.SetIKHintPositionWeight(AvatarIKHint.RightKnee, rightFootHintWeight);

        _hitPoint = _animator.GetIKHintPosition(AvatarIKHint.RightKnee);
        Debug.Log(_animator.GetIKHintPositionWeight(AvatarIKHint.RightKnee));*/
        
        /*_rightFoot.transform.position = _animator.GetIKPosition(AvatarIKGoal.RightFoot);
        _leftFoot.transform.position = _animator.GetIKPosition(AvatarIKGoal.LeftFoot);*/
        
    }

    private void SetFootIK(AvatarIKGoal goal,ref Transform solver, float weight, float rotationWeight)
    {
        RaycastHit hit;
        Vector3 solverPosition = solver.position;
        Vector3 footPosition = _animator.GetIKPosition(goal);
        Debug.DrawRay(solverPosition, Vector3.down * characterHeight, Color.red);

        if (Physics.Raycast(solverPosition, Vector3.down, out hit, characterHeight, groundMask))
        {
            _animator.SetIKPositionWeight(goal, weight);
            _animator.SetIKRotationWeight(goal, rotationWeight);
            _animator.SetIKPosition(goal, new Vector3(footPosition.x, hit.point.y + heightOffset, footPosition.z));
            _animator.SetIKRotation(goal, Quaternion.Euler(hit.normal.x, transform.rotation.eulerAngles.y, hit.normal.z));
            solver.position = new Vector3(footPosition.x, solver.position.y, footPosition.z);
        }
        else
        {
            _animator.SetIKPositionWeight(goal, 0);
            _animator.SetIKRotationWeight(goal, 0);
        }
    }

    private void SetFootIKHint(AvatarIKHint hint, Vector3 hintPos, float weight)
    {
        _animator.SetIKHintPosition(hint, hintPos);
        _animator.SetIKHintPositionWeight(hint, weight);
    }

    private void OnDrawGizmos()
    {
        if (rightFootSolver == null || leftFootSolver == null)
            return;
        
        Gizmos.color = Color.red;
        Gizmos.DrawRay(rightFootSolver.position, Vector3.down * characterHeight);
        Gizmos.DrawRay(leftFootSolver.position, Vector3.down * characterHeight);
        Gizmos.DrawWireCube(rightFootSolver.position, Vector3.one * 0.05f);
        Gizmos.DrawWireCube(leftFootSolver.position, Vector3.one * 0.05f);
        
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(rightFootIKHint.position, 0.05f);
        Gizmos.DrawWireSphere(leftFootIKHint.position, 0.05f);
    }
}
