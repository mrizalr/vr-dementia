using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEditor;
using UnityEngine;

public class VRAvatarRig : MonoBehaviour
{
    [Header("VR Rig Transforms")] 
    [SerializeField] private VRMap headVr;
    [SerializeField] private VRMap rightHandVr, leftHandVr;
    
    [Space] [Header("Skeleton Transforms")]
    [SerializeField] private Transform head;
    [SerializeField] private Transform rightHand, leftHand, rightShoulder, leftShoulder;

    [Space] [Header("Skeleton Transforms")]
    [SerializeField] private Transform skeleton;

    [SerializeField] [Range(0, 1)] private float bodyToHeadLerp;

    private Vector3 _pointBetweenHands = Vector3.zero;
    private float _leftArmLength = 0f, _rightArmLength = 0f;
    
    // Start is called before the first frame update
    void Start()
    {
        if (head == null || leftHand == null || rightHand == null)
            return;
        
        _pointBetweenHands = Vector3.Lerp(rightHand.position, leftHand.position, 0.5f);

        if (rightShoulder == null || leftShoulder == null)
            return;

        _leftArmLength = Vector3.Distance(leftHand.position, leftShoulder.position);
        _rightArmLength = Vector3.Distance(rightHand.position, rightShoulder.position);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void LateUpdate()
    {
        if (headVr == null || leftHandVr == null || rightHandVr == null)
            return;
        
        /*rightHandVr.Map();
        leftHandVr.Map();
        headVr.Map();*/

        _pointBetweenHands = Vector3.Lerp(rightHandVr.vrTarget.position, leftHandVr.vrTarget.position, 0.5f);

        skeleton.position = Vector3.Lerp(headVr.vrTarget.position + headVr.trackingPositionOffset, _pointBetweenHands, Vector3.Distance(_pointBetweenHands, transform.position) * bodyToHeadLerp);
        skeleton.position = new Vector3(skeleton.position.x, (headVr.vrTarget.position + headVr.trackingPositionOffset).y, skeleton.position.z);
        //skeleton.LookAt(_pointBetweenHands);
        //skeleton.rotation = headVr.vrTarget.rotation * Quaternion.Euler(headVr.trackingRotationOffset);
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(_pointBetweenHands, Vector3.one * 0.1f);
    }
}
