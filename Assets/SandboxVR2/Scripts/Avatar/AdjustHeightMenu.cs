using System.Collections;
using System.Collections.Generic;
using HurricaneVR.Framework.ControllerInput;
using UnityEngine;

public class AdjustHeightMenu : MonoBehaviour
{
    [SerializeField] private UIPopUp menuPopUp;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (HVRInputManager.Instance.LeftController.SecondaryButtonState.JustActivated)
        {
            if(!menuPopUp.IsOpen)
                menuPopUp.ShowUI(true);
            else 
                menuPopUp.ShowUI(false);
        }
    }
}
