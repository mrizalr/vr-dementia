using System;
using System.Collections;
using System.Collections.Generic;
using HurricaneVR.Framework.Core;
using HurricaneVR.Framework.Core.Grabbers;
using HurricaneVR.Framework.Core.Player;
using HurricaneVR.Framework.Shared.HandPoser;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;

[Serializable]
public class VRMap
{
    public string label;
    public Transform vrTarget;
    public Transform rigTarget;
    public Vector3 trackingPositionOffset;
    public Vector3 trackingRotationOffset;
    public bool mapPosition = true;
    public bool mapRotation = true;

    public void Map()
    {
        if(mapPosition) rigTarget.position = trackingPositionOffset.magnitude != 0 ? vrTarget.TransformPoint(trackingPositionOffset) : vrTarget.position;
        if(mapRotation) rigTarget.rotation = vrTarget.rotation * Quaternion.Euler(trackingRotationOffset);
    }
}

public class VRRig : MonoBehaviour
{
    [SerializeField] private float rotationDelay = 0;
    [SerializeField] [Range(0,100)] private float bodyLerp = 0.1f;
    [SerializeField] [Range(0,100)] private float forwardLerp = 0.1f;
    [SerializeField] [Range(0, 1)] private float headRotationOffset = 0.1f;

    //[ShowIf("@this.networkController == null")]
    [SerializeField] private HVRPlayerController playerController;
    //[SerializeField] private NetworkPlayer networkController;

    /*
    [SerializeField] private List<VRMap> targetMaps;
    */
    //[ShowIf("@this.networkController == null")]
    [SerializeField] private VRMap head;
    //[ShowIf("@this.networkController == null")]
    [SerializeField] private VRMap leftHand;
    //[ShowIf("@this.networkController == null")]
    [SerializeField] private VRMap rightHand;

    [SerializeField] private Transform headConstraint;
    [SerializeField] private Vector3 headBodyOffset;

    [Space]
    [Header("Adjust Height")]
    [SerializeField] private float defaultScale = 0.3f;
    [SerializeField] private float cameraDefaultHeight = 1.54f;
    [SerializeField] private TextMeshProUGUI heightDebug;
    [SerializeField]  private Animator spoofAnimator;
    
    private SemiProceduralFootIK _footIkController;

    [Space] 
    
    [SerializeField] private float networkAvatarAnimationSmoothing = 2;

    private float _distanceY = 0;
    private float _distanceX = 0;
    
    private readonly int _speedY = Animator.StringToHash("SpeedY");
    private readonly int _speedX = Animator.StringToHash("SpeedX");
    
    private bool _isheightDebugNotNull;
    private bool _playerControllerIsNull = true;
    
    private Vector3 _previousPosition = Vector3.zero;
    private Vector3 _previousHeadPosition = Vector3.zero;
    private Vector3 _previousHeadForward = Vector3.zero;

    private float _lastRotationUpdate = 0f;
    private float _lastAnimationUpdate = 0;

    private Transform _previousLeftHandTarget, _previousRightHandTarget;

    private void Awake()
    {
        /*if (networkController != null)
            return;*/
        
        /*head.vrTarget = null;
        rightHand.vrTarget = null;
        leftHand.vrTarget = null;*/
    }

    // Start is called before the first frame update
    void Start()
    {
        _isheightDebugNotNull = heightDebug != null;
        if(spoofAnimator == null)
            spoofAnimator = GetComponent<Animator>();
        
        _footIkController = GetComponentInChildren<SemiProceduralFootIK>();
        _previousPosition = transform.position;

        /*if (networkController != null)
        {
            return;
        }*/

        if (head.vrTarget == null)
        {
            head.vrTarget = Camera.main.transform;
            if (playerController == null)
                playerController = FindObjectOfType<HVRPlayerController>();
            
            _playerControllerIsNull = false;
        }

        _previousHeadPosition = head.vrTarget.TransformPoint(head.vrTarget.position);
        _previousHeadForward = Vector3.ProjectOnPlane(headConstraint.forward, Vector3.up).normalized;

        if (!_playerControllerIsNull)
        {
            HVRPosableHand[] poseHands = playerController.transform.parent.GetComponentsInChildren<HVRPosableHand>();

            foreach (var hand in poseHands)
            {
                switch (hand.IsLeft)
                {
                    case true when leftHand.vrTarget == null:
                        leftHand.vrTarget = hand.transform.GetChild(hand.transform.childCount - 1);
                        _previousLeftHandTarget = leftHand.vrTarget;
                        break;
                    case false when rightHand.vrTarget == null:
                        rightHand.vrTarget = hand.transform.GetChild(hand.transform.childCount - 1);
                        _previousRightHandTarget = rightHand.vrTarget;
                        break;
                }
            }
        }

        _lastRotationUpdate = Time.time;
        _lastAnimationUpdate = Time.time;

        transform.forward = _previousHeadForward;
        //StartCoroutine(WaitForCameraHeight());
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        transform.position = Vector3.Lerp( transform.position, transform.TransformPoint(headConstraint.localPosition + headBodyOffset), bodyLerp * Time.deltaTime);
        
        if(Time.time - _lastRotationUpdate > rotationDelay)
        {
            _lastRotationUpdate = Time.time;

            _previousHeadForward = Vector3.ProjectOnPlane(headConstraint.forward, Vector3.up).normalized;
            //Vector3.ProjectOnPlane(headConstraint.forward, Vector3.up).normalized;
        }
        
        transform.forward = Vector3.Lerp(transform.forward, _previousHeadForward, forwardLerp * Time.deltaTime);
        
        /*if(!_playerControllerIsNull)
        {
            spoofAnimator.SetFloat(_speedY, playerController.Inputs.MovementAxis.y);
            spoofAnimator.SetFloat(_speedX, playerController.Inputs.MovementAxis.x);
        }
        else
        {*/
            //spoofAnimator.SetFloat(_speedY, Vector3.Dot(_previousPosition - transform.position, transform.TransformDirection(transform.forward)) * -1);
            //spoofAnimator.SetFloat(_speedX, Vector3.Dot(_previousPosition - transform.position, transform.TransformDirection(transform.right)) * -1);
            spoofAnimator.SetFloat(_speedY, Vector3.Distance(transform.position, _previousPosition) * Vector3.Dot(transform.localPosition - _previousPosition, transform.forward));
            spoofAnimator.SetFloat(_speedX, Vector3.Distance(transform.position, _previousPosition) * Vector3.Dot(transform.localPosition - _previousPosition, transform.right));
            
            _previousPosition = Vector3.Lerp(_previousPosition, transform.localPosition, Time.fixedDeltaTime * networkAvatarAnimationSmoothing);
        //}
        
        head.Map();
        leftHand.Map();
        rightHand.Map();

        var rigTargetLocalPosition = head.rigTarget.localPosition;
        rigTargetLocalPosition = new Vector3(rigTargetLocalPosition.x, rigTargetLocalPosition.y, rigTargetLocalPosition.z -
            (Mathf.Abs(head.vrTarget.rotation.eulerAngles.x) * headRotationOffset));
        head.rigTarget.localPosition = rigTargetLocalPosition;
    }

    [Button]
    public void ScaleBodyToHeight()
    {
        if (cameraDefaultHeight <= 0)
        {
            cameraDefaultHeight = playerController.CameraHeight;
        }

        transform.localScale = Vector3.one * (playerController.CameraHeight / cameraDefaultHeight * defaultScale);
        //_footIkController.HeightOffset = -(playerController.CameraHeight / 2);
        if(_isheightDebugNotNull)
            heightDebug.text = $"Height: {playerController.CameraHeight:F1}\nOffset: {_footIkController.HeightOffset:F2}";
    }
    
    private IEnumerator WaitForCameraHeight()
    {
        while (playerController.CameraHeight <= 0)
        {
            yield return null;
        }

        yield return new WaitForSeconds(0.2f);
        ScaleBodyToHeight();
    }

    [Button]
    private void GetCameraHeight()
    {
        Debug.Log(playerController.CameraHeight);
    }

    [Button]
    private void ClearVrTargets()
    {
        head.vrTarget = null;
        leftHand.vrTarget = null;
        rightHand.vrTarget = null;
    }

    public void SetupHandGrabbing(HVRGrabberBase grabberBase, HVRGrabbable grabbable)
    {
        HVRPosableHand[] poseHands = grabbable.transform.GetComponentsInChildren<HVRPosableHand>();

        foreach (var hand in poseHands)
        {
            if (hand.IsLeft)
            {
                leftHand.vrTarget = hand.transform.GetChild(hand.transform.childCount - 1);
            }
            else
            {
                rightHand.vrTarget = hand.transform.GetChild(hand.transform.childCount - 1);
            }
        }
    }

    public void SetupHandReleasing(HVRGrabberBase grabberBase, HVRGrabbable grabbable)
    {
        HVRPosableHand[] poseHands = grabberBase.transform.GetComponentsInChildren<HVRPosableHand>();
        
        foreach (var hand in poseHands)
        {
            if (hand.IsLeft)
            {
                leftHand.vrTarget = _previousLeftHandTarget;
            }
            else
            {
                rightHand.vrTarget = _previousRightHandTarget;
            }
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.magenta;
        Gizmos.DrawWireSphere(_previousPosition, .2f);
    }
}
