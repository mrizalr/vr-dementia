using HurricaneVR.Framework.Shared;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace SandboxVRSeason2.Framework
{
    public class Sandbox_IndexPointTrigger : MonoBehaviour
    {

        [SerializeField] private string handSide;
        [ReadOnly] public bool isTriggering = false;

        private void Update()
        {
            if (handSide == "left")
            {
                var buttonState = HVRController.GetButtonState(HVRHandSide.Left, HVRButtons.Trigger);

                if (buttonState.Active) isTriggering = true;
                else isTriggering = false;
            }
            else if (handSide == "right")
            {
                var buttonState = HVRController.GetButtonState(HVRHandSide.Right, HVRButtons.Trigger);

                if (buttonState.Active) isTriggering = true;
                else isTriggering = false;
            }
        }
    }
}