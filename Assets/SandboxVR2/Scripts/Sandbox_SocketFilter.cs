﻿using HurricaneVR.Framework.Core.Sockets;
using HurricaneVR.Framework.Core.Grabbers;

namespace SandboxVRSeason2.Framework
{
    public class Sandbox_SocketFilter : HVRSocketFilter
    {
        public override bool IsValid(HVRSocketable socketable)
        {
            if (!socketable) return false;
            var sbFilter = socketable as Sandbox_Socketable;
            if (sbFilter == null) return false;
            if (sbFilter.objectProfile == null) return false;

            foreach (var slot in GetComponent<SB_Socket>().slotList)
            {
                if (slot.profile == null) continue;
                if (slot.profile == sbFilter.objectProfile)
                {
                    GetComponent<HVRSocket>().desiredPoint = slot.desiredPoint;
                    GetComponent<SB_Socket>().currentSlot = slot;

                    return true;
                }
            }
            return false;
        }


    }
}