using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SandboxVRSeason2.Framework;
using UnityEngine.Events;

public class LinearOnly : MonoBehaviour
{
    public UnityEvent action;
    void CheckMode(){
        if(Sandbox_ScenarioManager.instance)
            if(Sandbox_ScenarioManager.instance.mode == ScenarioModeEnum.SANDBOX){
                gameObject.SetActive(false);
                action.Invoke();
            }
    }
    private void Start() {
        CheckMode();
    }
    private void OnEnable() {
        CheckMode();
    }
}
