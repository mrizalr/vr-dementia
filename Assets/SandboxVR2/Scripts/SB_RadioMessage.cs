using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;

namespace SandboxVRSeason2.Framework
{
    public class SB_RadioMessage : MonoBehaviour
    {
        public string audioID;
        [TextArea(1, 5)] public string message;
        [ShowIf("@!string.IsNullOrEmpty(message)")] public float messageTime = 3f;

        public UnityEvent onStart;
        public UnityEvent onComplete;

        public void CallMessage()
        {
            print(gameObject + " Call Message : " + message);
            if (message == "") return;
            Sandbox_RadioManager.instance.CallMessage(this);
        }

        public void CallAudio()
        {
            print(gameObject +" Call Audio : " + audioID);
            if (audioID == "") return;
            Sandbox_RadioManager.instance.CallAudio(this);

        }

        private void OnValidate()
        {
            if (messageTime <= 0) messageTime = 0.5f;
        }
    }
}
