using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using Random = UnityEngine.Random;

namespace SandboxVRSeason2.Framework
{
    [CreateAssetMenu(fileName = "New Audio Reference", menuName = "Sandbox/Audio/Reference", order = 0)]
    public class Sandbox_AudioReference : SerializedScriptableObject
    {
        public Dictionary<string, AudioClip> clips = new Dictionary<string, AudioClip>();
        public Dictionary<string, AudioClip[]> clipsArray = new Dictionary<string, AudioClip[]>();

        public AudioClip GetClip(string key)
        {
            return clips.ContainsKey(key) ? clips[key] : null;
        }

        public AudioClip GetRandomClip(string key)
        {
            return clipsArray.ContainsKey(key) ? clipsArray[key][Random.Range(0, clipsArray[key].Length)] : null;
        }
    }
}