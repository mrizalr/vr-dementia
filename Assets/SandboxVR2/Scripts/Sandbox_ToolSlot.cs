using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace SandboxVRSeason2.Framework
{
    public class Sandbox_ToolSlot : Sandbox_Object
    {
        public UnityEvent ToolComplete;

        [FoldoutGroup("Debug"), ReadOnly, SerializeField] private bool _linearLock;
        public override bool linearLock
        {
            get { return _linearLock; }
            set
            {
                if (!_linearException && Sandbox_ScenarioManager.instance.mode == ScenarioModeEnum.LINEAR)
                {
                    _linearLock = value;
                    foreach (var outline in outlineList) outline.enabled = !value;
                }
            }
        }

        public virtual void SetToolActive(bool value)
        {

        }
    }
}