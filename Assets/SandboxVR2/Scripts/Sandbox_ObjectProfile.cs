using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace SandboxVRSeason2.Framework
{
    [CreateAssetMenu(fileName = "New Object Profile", menuName = "Sandbox/Object Profile", order = 0)]
    public class Sandbox_ObjectProfile : ScriptableObject
    {
        [Space]
        [ReadOnly]
        public Mesh[] meshList;
        [ReadOnly]
        public Vector3[] meshScaleList;
        [ReadOnly]
        public Vector3[] meshPosList;
        [ReadOnly]
        public Quaternion[] meshRotList;
    }
}

