using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HurricaneVR.Framework.Shared;
using UnityEngine.Events;
using Sirenix.OdinInspector;
using HurricaneVR.Framework.Core;
using TMPro;

namespace SandboxVRSeason2.Framework
{
    public class SB_StateChecker : MonoBehaviour
    {


        [SerializeField] private ScenarioModeCheck RunCheckerOnMode = ScenarioModeCheck.BOTH;

        [SerializeField] private List<StateCheckerSection> sectionState = new List<StateCheckerSection>();
        [SerializeField] private List<StateCheckerItem> itemState = new List<StateCheckerItem>();
        [SerializeField] private List<StateCheckerSocket> socketState = new List<StateCheckerSocket>();
        [SerializeField] private List<StateButtonControllerCheck> buttonControllerState = new List<StateButtonControllerCheck>();
        [SerializeField] private List<StateBoolCheck> boolState = new List<StateBoolCheck>();
        [SerializeField, ListDrawerSettings(ShowIndexLabels = true)]
        private List<CustomIntCheck> integerState = new List<CustomIntCheck>();
        [SerializeField, ListDrawerSettings(ShowIndexLabels = true)]
        private List<CustomFloatCheck> floatState = new List<CustomFloatCheck>();

        //Run when condition is fulfilled
        [SerializeField] private UnityEvent OnFulfilled = new UnityEvent();

        //Run when condition is not fulfilled
        [SerializeField] private UnityEvent OnUnFulfilled = new UnityEvent();

        //Run everytime check is called and condition is not fulfilled
        [SerializeField] private UnityEvent OnNotFulfilled = new UnityEvent();

        //Run everytime check is called and not even 1 condition is fulfilled
        [SerializeField] private UnityEvent OnNothingFulfilled = new UnityEvent();

        [SerializeField, ReadOnly] private bool isAllFulfilled;

        public void Reset()
        {
            foreach (var s in sectionState)
            {
                s.isFulfilled = false;
            }
            foreach (var s in itemState)
            {
                s.isFulfilled = false;
            }
            foreach (var s in socketState)
            {
                s.isFulfilled = false;
            }
            foreach (var s in buttonControllerState)
            {
                s.isFulfilled = false;
            }
            foreach (var s in boolState)
            {
                s.isFulfilled = false;
            }
            foreach (var s in integerState)
            {
                s.isFulfilled = false;
                s.count = 0;
            }
            foreach (var s in floatState)
            {
                s.isFulfilled = false;
                s.count = 0;
            }

            isAllFulfilled = false;
            CheckAll();

        }

        private void Start()
        {
            StartCoroutine(Initialize());
        }

        private IEnumerator Initialize()
        {
            yield return new WaitUntil(() => Sandbox_ScenarioManager.instance.isInit);
            ItemStateSetUp();
            SocketStateSetUp();
            SectionStateSetUp();
        }

        private bool CorrectMode()
        {
            if (RunCheckerOnMode == ScenarioModeCheck.BOTH)
                return true;
            else if (RunCheckerOnMode.ToString() != Sandbox_ScenarioManager.instance.mode.ToString())
                return false;

            return true;
        }
        #region SectionState
        private void SectionStateSetUp()
        {
            foreach (var _sectionState in sectionState)
            {
                foreach (var _section in _sectionState.section)
                {
                    _section.onSectionComplete.AddListener(() => { SectionStateCheck(_sectionState); });
                }

            }
        }
        private void SectionStateCheck(StateCheckerSection _sectionState)
        {
            if (!CorrectMode()) return;
            var _complete = true;
            foreach (var s in _sectionState.section)
            {
                if (!s.isCompleted)
                {
                    _complete = false;
                    break;
                }
            }
            _sectionState.isFulfilled = _complete;
            CheckAll();
        }
        #endregion
        #region ItemState
        private void ItemStateSetUp()
        {
            foreach (var _itemState in itemState)
            {
                _itemState.boolList = new bool[_itemState.item.Length];

                for (int i = 0; i < _itemState.item.Length; i++)
                {
                    var j = i;
                    _itemState.item[i].onSocketed.AddListener(() =>
                    {
                        if (!CorrectMode()) return;
                        if (!gameObject.activeInHierarchy || !enabled) return;
                        _itemState.boolList[j] = _itemState.itemState == StateCheckerItemEnum.OCCUPYING ? true : false;
                        ItemStateCheck(_itemState);
                    });

                    _itemState.item[i].onUnSocketed.AddListener(() =>
                    {
                        if (!CorrectMode()) return;
                        if (!gameObject.activeInHierarchy || !enabled) return;
                        _itemState.boolList[j] = _itemState.itemState == StateCheckerItemEnum.UNOCCUPYING ? true : false;
                        ItemStateCheck(_itemState);
                    });
                }

            }
        }

        private void ItemStateCheck(StateCheckerItem _itemState)
        {
            if (!CorrectMode()) return;
            if (_itemState.stayComplete && _itemState.isFulfilled) return;

            bool _fulfilled = true;
            foreach (var _bool in _itemState.boolList)
            {
                if (_bool == false) _fulfilled = false;
                break;
            }
            _itemState.isFulfilled = _fulfilled;
            CheckAll();
        }
        #endregion

        #region SocketState
        private void SocketStateSetUp()
        {
            foreach (var _socketState in socketState)
            {
                _socketState.boolList = new bool[_socketState.socket.Length];

                for (int i = 0; i < _socketState.socket.Length; i++)
                {
                    var j = i;
                    _socketState.socket[i].onOccupied.AddListener(() =>
                    {
                        if (!CorrectMode()) return;
                        if (!gameObject.activeInHierarchy || !enabled) return;

                        _socketState.boolList[j] = _socketState.socketState == StateCheckerSocketEnum.OCCUPIED
                        ? true : _socketState.socketState == StateCheckerSocketEnum.UNOCCUPIED
                        ? true : _socketState.boolList[j];

                        SocketStateCheck(_socketState);
                    });

                    _socketState.socket[i].onUnOccupied.AddListener(() =>
                    {
                        if (!CorrectMode()) return;
                        if (!gameObject.activeInHierarchy || !enabled) return;

                        _socketState.boolList[j] = _socketState.socketState == StateCheckerSocketEnum.UNOCCUPIED
                        ? true : _socketState.socketState == StateCheckerSocketEnum.OCCUPIED
                        ? true : _socketState.boolList[j];

                        SocketStateCheck(_socketState);
                    });

                    _socketState.socket[i].onToolLocked.AddListener(() =>
                    {
                        if (!CorrectMode()) return;
                        if (!gameObject.activeInHierarchy || !enabled) return;

                        _socketState.boolList[j] = _socketState.socketState == StateCheckerSocketEnum.TOOL_LOCKED
                        ? true : _socketState.socketState == StateCheckerSocketEnum.TOOL_UNLOCKED
                        ? true : _socketState.boolList[j];

                        SocketStateCheck(_socketState);
                    });

                    _socketState.socket[i].onToolUnlocked.AddListener(() =>
                    {
                        if (!CorrectMode()) return;
                        if (!gameObject.activeInHierarchy || !enabled) return;

                        _socketState.boolList[j] = _socketState.socketState == StateCheckerSocketEnum.TOOL_UNLOCKED
                        ? true : _socketState.socketState == StateCheckerSocketEnum.TOOL_LOCKED
                        ? true : _socketState.boolList[j];

                        SocketStateCheck(_socketState);
                    });
                }

            }
        }

        private void SocketStateCheck(StateCheckerSocket _socketState)
        {
            if (!CorrectMode()) return;
            if (_socketState.stayComplete && _socketState.isFulfilled) return;

            bool _fulfilled = true;
            foreach (var _bool in _socketState.boolList)
            {
                if (_bool == false) _fulfilled = false;
                break;
            }
            _socketState.isFulfilled = _fulfilled;
            CheckAll();
        }

        #endregion

        #region IntegerState
        public void IntegerStateAdd(int index)
        {
            if (!CorrectMode()) return;
            if (!gameObject.activeInHierarchy || !enabled) return;
            if (integerState[index].isFulfilled && integerState[index].stayComplete) return;
            integerState[index].count++;
            IntegerStateCheck(integerState[index]);
        }

        public void IntegerStateSubtract(int index)
        {
            if (!CorrectMode()) return;
            if (!gameObject.activeInHierarchy || !enabled) return;
            if (integerState[index].isFulfilled && integerState[index].stayComplete) return;
            integerState[index].count--;
            IntegerStateCheck(integerState[index]);
        }

        private void IntegerStateCheck(CustomIntCheck _integerState)
        {
            if (!CorrectMode()) return;
            if (_integerState.stayComplete && _integerState.isFulfilled) return;

            if (_integerState.count >= _integerState.targetCount) _integerState.isFulfilled = true;
            else _integerState.isFulfilled = false;
            CheckAll();
        }
        #endregion

        #region floatState
        public void FloatStateSet(string value)
        {
            print("set");
            string[] split = value.Split('/');
            int index = int.Parse(split[0]);
            if (!CorrectMode()) return;
            if (!gameObject.activeInHierarchy || !enabled) return;
            if (floatState[index].isFulfilled && floatState[index].stayComplete) return;
            if(floatState[index].useInputText)
                floatState[index].count = float.Parse(floatState[index].inputField.text);
            else
                floatState[index].count = (split.Length > 1) ? int.Parse(split[1]) : 0;
            FloatStateCheck(floatState[index]);
        }

        private void FloatStateCheck(CustomFloatCheck _floatState)
        {
            if (!CorrectMode()) return;
            if (_floatState.stayComplete && _floatState.isFulfilled) return;

            switch (_floatState.comparison)
            {
                case CustomFloatCheck.FloatComparison.Equal:
                    if (_floatState.count == _floatState.targetCount) _floatState.isFulfilled = true;
                    else _floatState.isFulfilled = false;
                break;
                case CustomFloatCheck.FloatComparison.Greater:
                    if (_floatState.count > _floatState.targetCount) _floatState.isFulfilled = true;
                    else _floatState.isFulfilled = false;
                break;
                case CustomFloatCheck.FloatComparison.EqualGreater:
                    if (_floatState.count >= _floatState.targetCount) _floatState.isFulfilled = true;
                    else _floatState.isFulfilled = false;
                break;
                case CustomFloatCheck.FloatComparison.Less:
                    if (_floatState.count < _floatState.targetCount) _floatState.isFulfilled = true;
                    else _floatState.isFulfilled = false;
                break;
                case CustomFloatCheck.FloatComparison.EqualLess:
                    if (_floatState.count <= _floatState.targetCount) _floatState.isFulfilled = true;
                    else _floatState.isFulfilled = false;
                break;

                default:
                break;
            }
            CheckAll();
        }
        #endregion

        #region BoolState
        public void BoolStateTrue(string id)
        {
            if (!CorrectMode()) return;
            if (!gameObject.activeInHierarchy || !enabled) return;
            foreach (var _bool in boolState)
            {
                if (_bool.checkName == id)
                {
                    if (_bool.isFulfilled && _bool.stayComplete) return;
                    _bool.isFulfilled = true;
                    CheckAll();
                    return;
                }
            }
        }
        public void BoolStateFalse(string id)
        {
            if (!CorrectMode()) return;
            if (!gameObject.activeInHierarchy || !enabled) return;
            foreach (var _bool in boolState)
            {
                if (_bool.checkName == id)
                {
                    if (_bool.isFulfilled && _bool.stayComplete) return;
                    _bool.isFulfilled = false;
                    CheckAll();
                    return;
                }
            }
        }

        #endregion

        #region ButtonControllerState
        public void ButtonControllerStateActive(int index){
            if (!CorrectMode()) return;
            if (!gameObject.activeInHierarchy || !enabled) return;
            if (buttonControllerState[index].isFulfilled && buttonControllerState[index].stayComplete) return;
            buttonControllerState[index].active = true;
            // ButtonControllerStateCheck(buttonControllerState[index]);
        }

        public void ButtonControllerStateDeactive(int index){
            if (!CorrectMode()) return;
            if (!gameObject.activeInHierarchy || !enabled) return;
            if (buttonControllerState[index].isFulfilled && buttonControllerState[index].stayComplete) return;
            buttonControllerState[index].active = false;
            // ButtonControllerStateCheck(buttonControllerState[index]);
        }
        private void Update() {
            foreach (var item in buttonControllerState)
            {
                bool invoke = false;
                if(item.active && !invoke){
                    invoke = true;
                    HVRHandSide hand;
                    if(item.handDetect) hand = (item.grab.IsLeftHandGrabbed) ? HVRHandSide.Left : HVRHandSide.Right;
                    else hand = item.hand;
                    HVRButtonState buttonState = HVRController.GetButtonState(hand, item.buttons);
                    item.buttonActive = buttonState.Active;
                    ButtonControllerStateCheck(item);
                }else{
                    invoke = false;
                }
            }
        }
        private void ButtonControllerStateCheck(StateButtonControllerCheck _buttonControllerState)
        {
            if (!CorrectMode()) return;
            if (_buttonControllerState.stayComplete && _buttonControllerState.isFulfilled) return;

            if (_buttonControllerState.buttonActive) _buttonControllerState.isFulfilled = true;
            else _buttonControllerState.isFulfilled = false;
            CheckAll();
        }

        #endregion

        private void CheckAll()
        {
            if (!CorrectMode()) return;
            if (!gameObject.activeInHierarchy || !enabled) return;

            bool somethingFulfilled = false;
            bool allFulfilled = true;

            foreach (var _sectionState in sectionState)
            {
                if (_sectionState.isFulfilled) somethingFulfilled = true;
                else allFulfilled = false;

                if (somethingFulfilled && !allFulfilled) break;

            }

            foreach (var _itemState in itemState)
            {
                if (_itemState.isFulfilled) somethingFulfilled = true;
                else allFulfilled = false;

                if (somethingFulfilled && !allFulfilled) break;

            }

            foreach (var _socketState in socketState)
            {
                if (_socketState.isFulfilled) somethingFulfilled = true;
                else allFulfilled = false;

                if (somethingFulfilled && !allFulfilled) break;

            }

            foreach (var _buttonControllerState in buttonControllerState)
            {
                if (_buttonControllerState.isFulfilled) somethingFulfilled = true;
                else allFulfilled = false;

                if (somethingFulfilled && !allFulfilled) break;

            }

            foreach (var _boolState in boolState)
            {
                if (_boolState.isFulfilled) somethingFulfilled = true;
                else allFulfilled = false;

                if (somethingFulfilled && !allFulfilled) break;

            }

            foreach (var _integerState in integerState)
            {
                if (_integerState.isFulfilled) somethingFulfilled = true;
                else allFulfilled = false;

                if (somethingFulfilled && !allFulfilled) break;
            }

            foreach (var _floatState in floatState)
            {
                if (_floatState.isFulfilled) somethingFulfilled = true;
                else allFulfilled = false;

                if (somethingFulfilled && !allFulfilled) break;
            }

            if (!somethingFulfilled)
                OnNothingFulfilled.Invoke();

            if (allFulfilled)
                OnFulfilled.Invoke();
            else
            {
                if (isAllFulfilled)
                    OnUnFulfilled.Invoke();

                OnNotFulfilled.Invoke();

            }

            isAllFulfilled = allFulfilled;
        }


        #region Class_and_Enum
        [System.Serializable]
        private class StateCheckerSection
        {
            public Sandbox_Section[] section;
            public StateCheckerSectionEnum itemState = StateCheckerSectionEnum.COMPLETE;

            [Tooltip("Kalo dah complete gabisa dibikin incomplete")]
            public bool stayComplete = true;
            [ReadOnly] public bool isFulfilled = false;
        }

        [System.Serializable]
        private class StateCheckerItem
        {
            public SB_Item[] item;
            [ReadOnly] public bool[] boolList;
            public StateCheckerItemEnum itemState = StateCheckerItemEnum.OCCUPYING;

            [Tooltip("Kalo dah complete gabisa dibikin incomplete")]
            public bool stayComplete = true;
            [ReadOnly] public bool isFulfilled = false;
        }

        [System.Serializable]
        private class StateCheckerSocket
        {
            public SB_Socket[] socket;
            [ReadOnly] public bool[] boolList;
            public StateCheckerSocketEnum socketState = StateCheckerSocketEnum.OCCUPIED;

            [Tooltip("Kalo dah complete gabisa dibikin incomplete")]
            public bool stayComplete = true;
            [ReadOnly] public bool isFulfilled = false;
        }

        [System.Serializable]
        private class StateBoolCheck
        {
            public string checkName = "";

            [Tooltip("Kalo dah complete gabisa dibikin incomplete")]
            public bool stayComplete = true;
            [ReadOnly] public bool isFulfilled = false;
        }

        [System.Serializable]
        private class StateButtonControllerCheck
        {
            public bool active = false;
            public bool handDetect;
            [ShowIf("handDetect",true)]
            public HVRGrabbable grab;
            [HideIf("handDetect",false)]
            public HVRHandSide hand;
            public HVRButtons buttons;

            [Tooltip("Kalo dah complete gabisa dibikin incomplete")]
            public bool stayComplete = true;
            [ReadOnly] public bool isFulfilled = false;
            [ReadOnly] public bool buttonActive = false;
        }

        [System.Serializable]
        private class CustomIntCheck
        {
            public int count;
            public int targetCount;

            [Tooltip("Kalo dah complete gabisa dibikin incomplete")]
            public bool stayComplete = true;
            [ReadOnly] public bool isFulfilled = false;
        }
        [System.Serializable]
        private class CustomFloatCheck
        {
            public enum FloatComparison
            {
                Equal,
                Greater,
                Less,
                EqualGreater,
                EqualLess
            }
            public bool useInputText;
            [HideIf("useInputText",true)]
            public float count;
            [ShowIf("useInputText",true)]
            public TMP_InputField inputField;
            public FloatComparison comparison;
            public float targetCount;

            [Tooltip("Kalo dah complete gabisa dibikin incomplete")]
            public bool stayComplete = true;
            [ReadOnly] public bool isFulfilled = false;
        }
        private enum StateCheckerSectionEnum
        {
            COMPLETE, INCOMPLETE
        }

        private enum StateCheckerItemEnum
        {
            OCCUPYING, UNOCCUPYING
        }

        private enum StateCheckerSocketEnum
        {
            OCCUPIED, UNOCCUPIED, TOOL_LOCKED, TOOL_UNLOCKED
        }

        [System.Serializable]
        private enum ScenarioModeCheck
        {
            BOTH, LINEAR, SANDBOX
        }
        #endregion

    }
}
