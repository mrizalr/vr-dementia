using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SB_UIReference : MonoBehaviour
{
    public Image[] imageList;
    public TextMeshProUGUI[] tmProList;
    public SB_UIButton[] customButtonList;
    public GameObject[] gameObjectList;
}
