﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

namespace Tictech.Utilities.PersistentCanvas
{
    [RequireComponent(typeof(CanvasGroup))]
    public class PersistentCanvas : SerializedMonoBehaviour
    {
        public static PersistentCanvas Instance;

        public GameObject loadingPanel;
        public TextMeshProUGUI loadingMessage;
        public Dictionary<string, GameObject> objects;
        public Dictionary<string, GameObject> modals;
        
        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else if (Instance != this)
            {
                Destroy(gameObject);
            }
        }

        private void Start()
        {
            SetLoading(false, "");
        }

        public void CreateModal(string key, UnityAction action, params string[] list)
        {
            if (!modals.ContainsKey(key))
                return;

            var modal = (IModal)Instantiate(modals[key], transform).GetComponent(typeof(IModal));
            modal.InitiateParameters(action, list);
        }

        public void SetLoading(bool active, string message)
        {
            loadingPanel.SetActive(active);
            if (active)
                loadingMessage.text = message;
        }

        public void Log(string message)
        {
            Debug.Log(message);
        }
    }
}
