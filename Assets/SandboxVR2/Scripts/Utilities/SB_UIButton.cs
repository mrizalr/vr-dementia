﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using Sirenix.OdinInspector;

public class SB_UIButton : MonoBehaviour, IPointerDownHandler, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
    [SerializeField] bool usingSpriteTrans;
    [SerializeField] bool usingColorTrans;

    [FoldoutGroup("SpriteTransition"), SerializeField, ShowIf("usingSpriteTrans")] Sprite upSprite;
    [FoldoutGroup("SpriteTransition"), SerializeField, ShowIf("usingSpriteTrans")] Sprite downSprite;
    [FoldoutGroup("SpriteTransition"), SerializeField, ShowIf("usingSpriteTrans")] Sprite hoverSprite;

    [FoldoutGroup("ColorTransition"), SerializeField, ShowIf("usingColorTrans")] Color upColor = Color.white;
    [FoldoutGroup("ColorTransition"), SerializeField, ShowIf("usingColorTrans")] Color downColor = Color.white;
    [FoldoutGroup("ColorTransition"), SerializeField, ShowIf("usingColorTrans")] Color hoverColor = Color.white;

    [SerializeField] public UnityEvent onDown;
    [SerializeField] public UnityEvent onClick;

    [SerializeField] private SB_UIButton[] otherButton;

    bool isClicked;

    [SerializeField, FoldoutGroup("Misc")] bool isCustomHoverObject;
    [SerializeField, FoldoutGroup("Misc"), ShowIf("isCustomHoverObject")] GameObject customHoverObject;
    [SerializeField, FoldoutGroup("Misc")] bool isCustomDownHover;
    [SerializeField, FoldoutGroup("Misc"), ShowIf("isCustomDownHover")] Sprite downHoverSprite;

    private void Start()
    {
        if (!usingColorTrans)
        {
            if (GetComponentInChildren<Button>(true))
            {
                Button b = GetComponentInChildren<Button>(true);
                upColor = b.colors.normalColor;
                downColor = b.colors.normalColor;
                hoverColor = b.colors.highlightedColor;
            }
        }

        onDown.AddListener(() =>
        {
            if (isCustomHoverObject)
            {
                customHoverObject.gameObject.SetActive(false);
            }
            if (usingSpriteTrans)
            {
                if (!isClicked)
                    GetComponent<Image>().sprite = upSprite;
            }
            if (usingColorTrans)
            {
                if (!isClicked)
                    GetComponent<Image>().color = upColor;
            }
        });
    }
    public void CheckOtherButton()
    {
        foreach (var item in otherButton)
        {

            if (item == this)
            {
                item.isClicked = true;
                item.TransitionCheck();
            }
            else
            {
                item.isClicked = false;
                item.TransitionCheck();
            }
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        Debug.Log("Clicked");
        onDown.Invoke();
        //onClick.Invoke();
        CheckOtherButton();
    }

    //0 is Up, 1 is Down
    public void TransitionCheck()
    {
        if (usingSpriteTrans)
        {
            if (!isClicked)
                GetComponent<Image>().sprite = upSprite;
            else
                GetComponent<Image>().sprite = downSprite;
        }
        if (usingColorTrans)
        {
            if (!isClicked)
                GetComponent<Image>().color = upColor;
            else
                GetComponent<Image>().color = downColor;
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (isCustomHoverObject)
        {
            customHoverObject.gameObject.SetActive(true);
        }
        if (usingSpriteTrans)
        {
            if (!isClicked)
                GetComponent<Image>().sprite = hoverSprite;
        }
        if (isCustomDownHover)
        {
            if (isClicked)
                GetComponent<Image>().sprite = downHoverSprite;
        }
        if (usingColorTrans)
        {
            if (!isClicked)
                GetComponent<Image>().color = hoverColor;
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (isCustomHoverObject)
        {
            customHoverObject.gameObject.SetActive(false);
        }
        if (usingSpriteTrans)
        {
            if (!isClicked)
                GetComponent<Image>().sprite = upSprite;
        }
        if (usingColorTrans)
        {
            if (!isClicked)
                GetComponent<Image>().color = upColor;
        }
    }

    public void InjectOtherButtons(List<SB_UIButton> otherButtons)
    {
        otherButton = otherButtons.ToArray();
    }

    public void InjectOtherButtons(SB_UIButton[] otherButtons)
    {
        otherButton = otherButtons;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        onClick.Invoke();
    }

    //Tmp
    public void SetEnable(bool value)
    {
        this.enabled = !value;
    }
}
