using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;
using HurricaneVR.Framework.Core;

namespace SandboxVRSeason2.Framework
{
    public class SB_AnimationTrigger : Sandbox_Object
    {
        [SerializeField] private Animator animator;
        [SerializeField] [ListDrawerSettings(ShowIndexLabels = true)] private string[] animationName;
        [SerializeField] private int animationIndex;

        [FoldoutGroup("Event")] public UnityEvent onStartAnimation;
        [FoldoutGroup("Event")] public UnityEvent onCompleteAnimation;

        [SerializeField, FoldoutGroup("Setting")] private bool playOnce = false;

        [Space]
        [SerializeField, FoldoutGroup("Setting")] private bool isPlayOnGrab = false;
        [SerializeField, FoldoutGroup("Setting"), ShowIf("isPlayOnGrab")] private HVRGrabbable grabbable = null;

        [Space]
        [SerializeField, FoldoutGroup("Setting")] bool isPlayOnTagTrigger = false;
        [SerializeField, FoldoutGroup("Setting"), ShowIf("isPlayOnTagTrigger")] string triggerTag;

        [Space]
        [SerializeField, FoldoutGroup("Setting")] bool isPlayOnObjectTrigger = false;
        [SerializeField, FoldoutGroup("Setting"), ShowIf("isPlayOnObjectTrigger")] GameObject triggerObject;

        [Space]
        [SerializeField, FoldoutGroup("Setting")] bool isPlayOnFingerTrigger = false;


        [FoldoutGroup("Debug"), ReadOnly, SerializeField] private bool isAnimating;
        [FoldoutGroup("Debug"), ReadOnly, SerializeField] private bool _isLinearLocked;
        [FoldoutGroup("Debug"), ReadOnly, SerializeField] private bool playOnceComplete;
        [SerializeField, FoldoutGroup("Debug"), ReadOnly, ShowIf("isPlayOnFingerTrigger")] Sandbox_IndexPointTrigger triggerFinger = null;

        private bool isAvailable()
        {
            if (isAnimating || enabled == false || !gameObject.activeInHierarchy || _isLinearLocked || playOnceComplete)
                return false;
            else return true;
        }

        private void Start()
        {
            if (!animator)
                animator = GetComponent<Animator>();

            SetPlayOnGrab();
        }

        private void Update()
        {
            if (isPlayOnFingerTrigger && triggerFinger && triggerFinger.isTriggering)
            {
                PlayToolAnimation();
                triggerFinger = null;
            }
        }

        public void PlayToolAnimation(int _index = 0)
        {
            if (!isAvailable()) return;

            StartCoroutine(_PlayToolAnimation(_index));
        }
        public void PlayToolAnimation()
        {
            if (!isAvailable()) return;

            StartCoroutine(_PlayToolAnimation(animationIndex));
        }

        private IEnumerator _PlayToolAnimation(int _index = 0)
        {
            isAnimating = true;
            onStartAnimation.Invoke();

            if (animationName[_index] != "")
            {
                animator.Play(animationName[_index]);
                yield return new WaitUntil(() => animator.GetCurrentAnimatorStateInfo(0).IsName(animationName[_index]));
                var animLength = animator.GetCurrentAnimatorStateInfo(0).length;
                yield return new WaitForSeconds(animLength);
            }

            onCompleteAnimation.Invoke();
            if (playOnce) playOnceComplete = true;
            isAnimating = false;
        }

        public override bool linearLock
        {
            get { return _isLinearLocked; }
            set
            {
                if (!_linearException && Sandbox_ScenarioManager.instance.mode == ScenarioModeEnum.LINEAR)
                {
                    _isLinearLocked = value;
                    foreach (var outline in outlineList) outline.enabled = !value;
                }
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (!isAvailable()) return;

            if (isPlayOnTagTrigger && other.CompareTag(triggerTag))
                PlayToolAnimation();

            if (isPlayOnObjectTrigger && other.gameObject == triggerObject)
                PlayToolAnimation();
        }

        private void OnTriggerStay(Collider other)
        {
            if (isPlayOnFingerTrigger && other.CompareTag("IndexPoint") && (triggerFinger == null || triggerFinger.transform != other.transform))
            {
                var t = other.GetComponent<Sandbox_IndexPointTrigger>();
                if (t && !t.isTriggering)
                {
                    triggerFinger = t;
                }
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.GetComponent<Sandbox_IndexPointTrigger>() == triggerFinger)
            {
                triggerFinger = null;
            }
        }

        private void SetPlayOnGrab()
        {
            if (isPlayOnGrab && grabbable)
                grabbable.HandGrabbed.AddListener((_grabber, _grabbable) => { PlayToolAnimation(); });
        }

    }
}
