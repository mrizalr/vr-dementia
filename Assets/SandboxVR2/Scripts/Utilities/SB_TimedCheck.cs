using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using Sirenix.OdinInspector;
using HurricaneVR.Framework.Core;
using Hellmade.Sound;
using Tictech.LoadManager;
using SandboxVRSeason2.Framework.Multiplayer;

namespace SandboxVRSeason2.Framework
{
    public class SB_TimedCheck : Sandbox_Object
    {
        [SerializeField] public string checkName;
        [SerializeField] private bool checkGrab;
        [SerializeField] private bool checkVisible;
        [SerializeField] private bool checkBool;
        [SerializeField] public bool timeIndicator;
        [ShowIf("timeIndicator")] public Transform timerLocation;

        [SerializeField] public float checkTime = 3f;
        [SerializeField, ShowIf("checkVisible")] private float checkDistance = 1f;

        [SerializeField, ShowIf("checkGrab")] HVRGrabbable[] grabbableList;

        [FoldoutGroup("Event"), SerializeField] public UnityEvent onComplete;

        [FoldoutGroup("Debug"), ReadOnly, SerializeField] private bool isVisible;
        [FoldoutGroup("Debug"), ReadOnly, SerializeField] private bool isGrabbing;
        [FoldoutGroup("Debug"), ReadOnly, SerializeField] private bool _isBoolChecking;
        [FoldoutGroup("Debug"), ReadOnly, SerializeField] private bool _isLinearLocked;
        [FoldoutGroup("Debug"), ReadOnly] public bool isBoolChecking { get { return _isBoolChecking; } set { _isBoolChecking = value; } }


        [FoldoutGroup("Debug"), ReadOnly, SerializeField] public float currentTime;
        [FoldoutGroup("Debug"), ReadOnly, SerializeField] public bool isComplete = false;

        private void Start()
        {
            if (checkGrab)
                foreach (var grabbable in grabbableList)
                {
                    grabbable.HandGrabbed.AddListener((x, y) =>
                    {
                        isGrabbing = true;

                    });
                    grabbable.HandReleased.AddListener((x, y) =>
                    {
                        isGrabbing = false;
                    });
                }
        }

        private void Update()
        {
            if (isComplete || (!checkVisible && !checkGrab && !checkBool) || _isLinearLocked)
                return;

            VisibleCheck();

            bool visCom = !checkVisible || checkVisible && isVisible ? true : false;
            bool grabCom = !checkGrab || checkGrab && isGrabbing ? true : false;
            bool boolCom = !checkBool || checkBool && _isBoolChecking ? true : false;

            if (visCom && grabCom && boolCom)
            {
                currentTime += Time.deltaTime;
                Sandbox_CheckManager.instance.currentCheck = this;
            }
            else
            {
                if (Sandbox_CheckManager.instance.currentCheck == this)
                    Sandbox_CheckManager.instance.currentCheck = null;
            }

            if (currentTime >= checkTime)
            {
                isComplete = true;
                Complete();
            }
        }

        private void OnDisable()
        {
            if (Sandbox_CheckManager.instance.currentCheck == this)
                Sandbox_CheckManager.instance.currentCheck = null;
        }

        void VisibleCheck()
        {
            if (Vector3.Distance(Camera.main.transform.position, transform.position) < checkDistance)
                isVisible = true;
            else isVisible = false;
        }
        public override bool linearLock
        {
            get { return _isLinearLocked; }
            set
            {
                if (!_linearException && Sandbox_ScenarioManager.instance.mode == ScenarioModeEnum.LINEAR)
                {
                    _isLinearLocked = value;
                    foreach (var outline in outlineList) outline.enabled = !value;
                }
            }
        }

        void Complete()
        {
            onComplete.Invoke();

            var audioClip = RuntimeManager.Instance.CommonAudioReference.GetClip("stepComplete");
            EazySoundManager.PlayMusic(audioClip, 1f, false, false, 0f, 0f);
            if (timerLocation)
            {
                GameObject temp;

                temp = Instantiate(Sandbox_ScenarioUIManager.instance.checkCompleteImageSpawn, timerLocation.position, Quaternion.identity);

                //if (Sandbox_ScenarioManager.instance.multiplayerMode == MultiplayerModeEnum.OFFLINE)
                //{
                //    temp = Instantiate(Sandbox_ScenarioUIManager.instance.checkCompleteImageSpawn, timerLocation.position, Quaternion.identity);
                //}
                //else
                //{
                //    temp = Instantiate(Sandbox_ScenarioUIManagerNetwork.instance.UINetwork.checkCompleteImageSpawn, timerLocation.position, Quaternion.identity);
                //}

                //d.transform.parent = null;
                Destroy(temp, 3f);
            }
            
            if (Sandbox_CheckManager.instance.currentCheck == this)
                Sandbox_CheckManager.instance.currentCheck = null;
        }
    }
}
