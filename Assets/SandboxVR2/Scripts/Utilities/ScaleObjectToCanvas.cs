using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScaleObjectToCanvas : MonoBehaviour
{
    public RectTransform rect;
    public Vector2 offsetSize = new Vector2(63f, 63f);
    [ReadOnly, SerializeField] Vector2 scaleRect;

    public void scaleToCanvas()
    {
        transform.localScale = new Vector3(
            rect.rect.width - offsetSize.x,
            rect.rect.height - offsetSize.y,
            transform.localScale.z);
        scaleRect = rect.sizeDelta;
    }
}
