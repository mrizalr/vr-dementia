﻿using System;

namespace Tictech.EnterpriseUniversity
{
    [Serializable]
    public struct EuUserLogin
    {
        public string email;
        public string password;

        public EuUserLogin(string un, string pw)
        {
            email = un;
            password = pw;
        }
    }

    [Serializable]
    public class EuKalbeGetSubScenariosResponse
    {
        public EuKalbeGetSubScenarios[] data;
    }

    [Serializable]
    public class EuKalbeGetSubScenarios
    {
        public int id;
        public int scenario_id;
        public string name;
        public int target_time;
    }

    [Serializable]
    public class EuKalbeTimeScenariosDataResponse
    {
        public EuKalbeTimeScenariosData[] data;
    }

    [Serializable]
    public class EuKalbeTimeScenariosData
    {
        public string user_id;
        public int scenario_id;
        public int sub_scenario_id;
        public string time;
    }

    [Serializable]
    public struct EuUserCreate
    {
        public string email;
        public string password;
        public string name;
        public string position;
        public string division;
        public string birthday;
        public string nik;
        public string role;

        public EuUserCreate(string un, string pw, string _name, string _position, string _division, string _birthday, string _nik)
        {
            email = un;
            password = pw;
            name = _name;
            position = _position;
            division = _division;
            birthday = _birthday;
            nik = _nik;
            role = "a4da8fc2-6a2c-40c6-8b32-f33af35fcf01";
        }
    }
    [Serializable]
    public struct EuUserLDAPLogin
    {
        public string nik;
        public string password;

        public EuUserLDAPLogin(string un, string pw)
        {
            nik = un;
            password = pw;
        }
    }
    
    [Serializable]
    public struct EuGuestLogin
    {
        public string email;
        public string phone;

        public EuGuestLogin(string un, string pw)
        {
            email = un;
            phone = pw;
        }
    }
    
    [Serializable]
    public class EuUserData
    {
        public int id;
        public string email;
        public string password;
        public string token;
        public string first_name;
        public object last_name;
        public object panggilan;
        public DateTime tanggal_lahir;
        public object profile_picture;
        public string gender;
        public object phone_number;
        public object nik;
        public object labour_cost;
        public object rfid_access;
        public object fingerprint_access;
        public object jobdescId;
        public object teamId;
        
        public EuUserData(string un, string pw)
        {
            email = un;
            password = pw;
        }

        public EuUserData(string tk)
        {
            token = tk;
        }
    }

    [Serializable]
    public class EuToyotaGuestData
    {
        public int id;
        public string name;
        public string email;
        public string phone;
    }

    [Serializable]
    public class EUKalbeUserData
    {
        public string id;
        public string first_name;
        public string last_name;
        public string position;
        public string division;
        public string birthday;
        public string email;
        public string role;
    }

    [Serializable]
    public class EuToyotaUserScoresResponse
    {
        public string status;
        public EuToyotaUserScores[] data;
    }

    [Serializable]
    public class EuToyotaUserScores
    {
        public int trainingId;
        public int score;
    }
    
    [Serializable]
    public class EuResponseData
    {
        public string message;
        public string token;
        public EuUserData GetUser()
        {
            return new EuUserData(token);
        }
    }

    [Serializable]
    public class EuKalbeLoginResponse
    {
        //public string success;
        public EuKalbeSignInResponseData data;
    }

    [Serializable]
    public class EuKalbeSignInResponseData
    {
        public string access_token;
        public string expires;
        public string refresh_token;
    }

    [Serializable]
    public class EuToyotaUserResponse
    {
        public EUKalbeUserData data;
    }
    
    [Serializable]
    public class EuResponse
    {
        public string status;
        public EuResponseData data;
    }

    [Serializable]
    public struct EuKalbeScoreData
    {
        public string user_id;
        public int scenario_id;
        public int sub_scenario_id;
        public int time;
        public int target_time;
        public bool is_passed;

        public EuKalbeScoreData(string user_id, int scenario_id, int sub_scenario_id, int time, int target_time)
        {
            this.user_id = user_id;
            this.scenario_id = scenario_id;
            this.sub_scenario_id = sub_scenario_id;
            this.time = time;
            this.target_time = target_time;
            this.is_passed = time <= target_time;
        }
    }
    [Serializable]
    public struct EuTrainingStepData
    {
        public string name;
        public int score;
        public string value;
    }
}
