using System.Collections;
using System.Collections.Generic;
using MEC;
using Sirenix.OdinInspector;
//using Tictech.Utilities.PersistentCanvas;
using UnityEngine;
using UnityEngine.Networking;
using SandboxVRSeason2.Framework;
using LitJson;
using System;
using UnityEngine.Events;
using System.Linq;
//using Tictech.Oculus.Scenario;

namespace Tictech.EnterpriseUniversity
{
    public class EuRuntimeManager : MonoBehaviour
    {
        public static EuRuntimeManager Instance;

        [Header("API Access")]
        [SerializeField]
        APISetting aPISetting;
        public bool useLDAP = true;
        public string baseUrl = "";
        public string LDAPUrl = "";
        public string loginHandle;
        // public string usersHandle;
        public string scoreHandle = "/items/scores";
        public string stepHandle = "items/step";
        public string scoreUserParameter;

        private string _userLink = "/users/me";
        private string _token = "";
        private string _guestSignInLink = "";

#if UNITY_EDITOR
        [Header("Debug")]
        public string debugUsername;
        public string debugPassword;
        public int debugTrainingId;
        public int debugScore;

        [Button]
        void TestPostScore()
        {
            // Timing.RunCoroutine(PostScore(debugTrainingId, debugScore));
        }

        [Button]
        void TestGetScores()
        {
            Timing.RunCoroutine(GetScores());
        }

        [Button]
        void TestGetGuest()
        {
            Timing.RunCoroutine(GetGuest(1));
        }

        [Button]
        void TestPostGuest()
        {
            Timing.RunCoroutine(SignGuestIn("Lanius@parkiran", "08219123"));
        }

        [Button]
        void TestLogin()
        {
            Timing.RunCoroutine(SignIn(debugUsername, debugPassword, (err) => { Debug.LogError(err); }));
        }
        #endif

        [ReadOnly]
        public EUKalbeUserData User { get; private set; }
        
        [ReadOnly]
        public EuToyotaUserScores[] Scores { get; private set; }
        [ReadOnly]
        public EuKalbeGetSubScenarios[] subScenarios{ get; private set; }

        [ReadOnly]
        public EuToyotaGuestData Guest { get; private set; }
    
        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else if (Instance != this)
            {
                Destroy(gameObject);
            }
            baseUrl = aPISetting.baseUrl;
            LDAPUrl = aPISetting.ldapUrl;
            loginHandle = aPISetting.loginHandle;
            scoreHandle = aPISetting.scoreHandle;
            stepHandle = aPISetting.stepHandle;

            if(!string.IsNullOrEmpty(PlayerPrefs.GetString("LDAPURL"))){
                LDAPUrl = PlayerPrefs.GetString("LDAPURL");
            }
            if(!string.IsNullOrEmpty(PlayerPrefs.GetString("BASEURL"))){
                baseUrl = PlayerPrefs.GetString("BASEURL");
            }
            if(PlayerPrefs.GetInt("USELDAP") != 0){
                useLDAP = PlayerPrefs.GetInt("USELDAP") == 2 ? true : false;
            }
            print(baseUrl);
        }
        private void Start() {
        }

        public IEnumerator<float> SignIn(string username, string password, System.Action<string> errorCB)
        {
            print("login");

            bool LDAPStatus = false;

            var userLDAPLogin = new EuUserLDAPLogin(username, password);
            var userBytes = System.Text.Encoding.UTF8.GetBytes(JsonUtility.ToJson(userLDAPLogin));
            var userCreate = new EuUserCreate();
            if(useLDAP){
                print("ldap");

                var web = UnityWebRequest.Get(LDAPUrl + "/api/vr/login?nik="+username+"&password="+password);
                // web.method = UnityWebRequest.kHttpVerbPOST;
                web.SetRequestHeader("Content-Type", "application/json");
                web.SetRequestHeader("Accept", "application/json");
                Debug.Log("Signing in to: " + web.url);
                var asyncs = web.SendWebRequest();
                while (!asyncs.isDone)
                {
                    yield return Timing.WaitForOneFrame;
                }

                if (web.isNetworkError || web.isHttpError)
                {
                    Debug.LogError(web.error);
                    Debug.LogError(web.downloadHandler.text);
                    LDAPStatus=false;
                }
                else
                {
                    Debug.Log(web.downloadHandler.text);
                    JsonData jsonvale = JsonMapper.ToObject(web.downloadHandler.text);
                    print(jsonvale["status"].ToString());
                    if(jsonvale["status"].ToString() == "True"){
                        LDAPStatus = true;
                        userCreate = new EuUserCreate(
                            username+"@email.com",
                            password, 
                            jsonvale["data"]["name"].ToString(),
                            jsonvale["data"]["position_name"].ToString(),
                            jsonvale["data"]["division_name"].ToString(),
                            jsonvale["data"]["birthday"].ToString(),
                            username
                        );
                    }else{
                        LDAPStatus = false;                    
                    }
                }
                // Post(, userBytes,(data) => {
                //     print(data);
                // });
            }
            print("login ldap = "+LDAPStatus);

            yield return Timing.WaitForOneFrame;

            if(useLDAP && !LDAPStatus){
                yield return 0;
                yield break;
            }
            print("continue");

            EuKalbeLoginResponse authStatus = null;

            //var userLogin = new EuUserLogin(username+"@email.com", password);
            var userLogin = new EuUserLogin(username, password);
            userBytes = System.Text.Encoding.UTF8.GetBytes(JsonUtility.ToJson(userLogin));

            var auth = UnityWebRequest.Put(baseUrl + loginHandle, userBytes);
            auth.method = UnityWebRequest.kHttpVerbPOST;
            auth.SetRequestHeader("Content-Type", "application/json");
            auth.SetRequestHeader("Accept", "application/json");
            Debug.Log("Signing in to: " + auth.url);
            var async = auth.SendWebRequest();
            while (!async.isDone)   
            {
                yield return Timing.WaitForOneFrame;
            }

            if (auth.isNetworkError || auth.isHttpError)
            {
                if(auth.downloadHandler.text == "")
                {
                    errorCB($"Failed to connect\n to {baseUrl}");
                } else
                {
                    var errorJson = JsonMapper.ToObject(auth.downloadHandler.text);
                    errorCB(errorJson["errors"][0]["message"].ToString());
                }

                if (LDAPStatus){
                    // JsonData jsonAuth = JsonMapper.ToObject(auth.downloadHandler.text);
                    userBytes = System.Text.Encoding.UTF8.GetBytes(JsonUtility.ToJson(userCreate));
                    print("create user");
                    var create = UnityWebRequest.Put(baseUrl+"/users", userBytes);
                    create.method = UnityWebRequest.kHttpVerbPOST;
                    create.SetRequestHeader("Content-Type", "application/json");
                    create.SetRequestHeader("Accept", "application/json");
                    Debug.Log("Signing in to: " + create.url);
                    var asyncCreate = create.SendWebRequest();
                    while (!asyncCreate.isDone)
                    {
                        yield return Timing.WaitForOneFrame;
                    }

                    if (create.isNetworkError || create.isHttpError)
                    {
                        Debug.LogError(create.error);
                    }
                    else
                    {
                        Debug.Log(create.downloadHandler.text);
                        JsonData jsonvale = JsonMapper.ToObject(create.downloadHandler.text);
                        // jsonvale["data"]["email"].ToString();
                        userLogin = new EuUserLogin(jsonvale["data"]["email"].ToString(), password);
                        userBytes = System.Text.Encoding.UTF8.GetBytes(JsonUtility.ToJson(userLogin));

                            var login = UnityWebRequest.Put(baseUrl+"/users", userBytes);
                            login.method = UnityWebRequest.kHttpVerbPOST;
                            login.SetRequestHeader("Content-Type", "application/json");
                            login.SetRequestHeader("Accept", "application/json");
                            Debug.Log("Signing in to: " + login.url);
                            var asynclogin = login.SendWebRequest();
                            while (!asynclogin.isDone)
                            {
                                yield return Timing.WaitForOneFrame;
                            }

                            if (login.isNetworkError || login.isHttpError)
                            {
                                Debug.LogError(login.error);
                            }
                            else
                            {
                                authStatus = JsonUtility.FromJson<EuKalbeLoginResponse>(login.downloadHandler.text);
                            }
                        // Post(baseUrl+loginHandle,userBytes,(data) => {
                        //     authStatus = JsonUtility.FromJson<EuToyotaLoginResponse>(data);
                        // });
                    };
                }
            }
            else
            {
                Debug.Log(auth.downloadHandler.text);
                authStatus = JsonUtility.FromJson<EuKalbeLoginResponse>(auth.downloadHandler.text);
            }
                
            if (authStatus == null)
            {
                User = null;
                yield return Timing.WaitForSeconds(1f);
            }
            else
            {
                _token = authStatus.data.access_token;
                yield return Timing.WaitUntilDone(GetUser(_token));
                yield return Timing.WaitUntilDone(GetScenario((err) =>
                {
                    errorCB(err);
                }));
            }

        }
        IEnumerable Post(string url, byte[] userBytes, Action<string> callback){
            var auth = UnityWebRequest.Put(url, userBytes);
            auth.method = UnityWebRequest.kHttpVerbPOST;
            auth.SetRequestHeader("Content-Type", "application/json");
            auth.SetRequestHeader("Accept", "application/json");
            Debug.Log("Signing in to: " + auth.url);
            var async = auth.SendWebRequest();
            while (!async.isDone)
            {
                yield return Timing.WaitForOneFrame;
            }

            if (auth.isNetworkError || auth.isHttpError)
            {
                Debug.LogError(auth.error);
            }
            else
            {
                Debug.Log(auth.downloadHandler.text);
                callback.Invoke(auth.downloadHandler.text);
            }
        }
        
        public IEnumerator<float> SignGuestIn(string email, string phone)
        {
            var userLogin = new EuGuestLogin(email, phone);
            var userBytes = System.Text.Encoding.UTF8.GetBytes(JsonUtility.ToJson(userLogin));

                var auth = UnityWebRequest.Put(_guestSignInLink, userBytes);
                auth.method = UnityWebRequest.kHttpVerbPOST;
                auth.SetRequestHeader("Content-Type", "application/json");
                auth.SetRequestHeader("Accept", "application/json");
                Debug.Log("Signing in to: " + auth.url);
                var async = auth.SendWebRequest();
                while (!async.isDone)
                {
                    yield return Timing.WaitForOneFrame;
                }

                if (auth.isNetworkError || auth.isHttpError)
                {
                    Debug.LogError(auth.error);
                }
                else
                {
                    Debug.Log(auth.downloadHandler.text);
                    Guest = JsonUtility.FromJson<EuToyotaGuestData>(auth.downloadHandler.text);
                }
        }
        public void PostScore(string scenarioPrefabName){
            Timing.RunCoroutine(SendScore(scenarioPrefabName));
        }
        public void LastScore(int idSubscenario, UnityAction<EuKalbeScoreData> callback){
            Timing.RunCoroutine(GetLastScore(idSubscenario,callback));
        }
        public IEnumerator<float> GetLastScore(int idSubscenario, UnityAction<EuKalbeScoreData> callback){
            if (User == null) yield break;
            var request = UnityWebRequest.Get(baseUrl + scoreHandle +"?sort=-date_created&filter[user]="+User.id+"&filter[subscenario]="+idSubscenario);
            print(request.url);
            request.method = UnityWebRequest.kHttpVerbGET;
            request.SetRequestHeader("Content-Type", "application/json");
            request.SetRequestHeader("Accept", "application/json");
            request.SetRequestHeader("Authorization", "Bearer " + _token);
            var async = request.SendWebRequest();
            
            while (!async.isDone)
            {
                yield return Timing.WaitForOneFrame;
            }

            EuKalbeScoreData data = new EuKalbeScoreData();
            if (request.isNetworkError || request.isHttpError)
            {
                Debug.LogError(request.error);
            }
            else
            {
                print(request.downloadHandler.text);
                JsonData jsonData = JsonMapper.ToObject(request.downloadHandler.text);
                if(jsonData["data"].Count>0)
                    data = JsonUtility.FromJson<EuKalbeScoreData>(jsonData["data"][0].ToJson());

                callback.Invoke(data);
            }
        }

        private EuKalbeGetSubScenarios GetSubscenarioData(string scenarioPrefabName)
        {
            for (int i = 0; i < subScenarios.Length; i++)
            {
                print(subScenarios[i].name + " " + scenarioPrefabName + " " + subScenarios[i].name == scenarioPrefabName);
                if(subScenarios[i].name == scenarioPrefabName)
                {
                    return subScenarios[i];
                }
            }

            return null;
        }

        public IEnumerator<float> SendScore(string scenarioPrefabName)
        {
            bool linear = (Sandbox_ScenarioManager.instance.mode == ScenarioModeEnum.LINEAR) ? true : false;
            float score = Sandbox_ScenarioManager.instance.playScore;
            int subscenario = Sandbox_ScenarioManager.instance.idSubscenario;
#if UNITY_EDITOR
            if (User == null || string.IsNullOrEmpty(_token))
                yield return Timing.WaitUntilDone(SignIn(debugUsername, debugPassword, (err) => { Debug.LogError(err); }));
#endif
            if (User == null)
                yield break;
            if (string.IsNullOrEmpty(_token))
            {
                Debug.LogWarning("User isn't signed in, failed to post score.");
                yield break;
            }
            else
            {
                Debug.Log("Uploading score to:\r\nscore: " + score + "\r\nbearer: " + _token);
            }
            var subScenario = GetSubscenarioData(scenarioPrefabName);
            if(subscenario == null)
            {
                Debug.LogError("sub scenario not found");
            }
            var playtime = (int)Sandbox_ScenarioManager.instance.playTime;
            var trainingData = JsonUtility.ToJson(new EuKalbeScoreData(
                User.id,
                subScenario.scenario_id,
                subScenario.id,
                playtime,
                subScenario.target_time
            ));
            
            //trainingData = trainingData.Replace("username\":", scoreUserParameter + "\":");
            var post = UnityWebRequest.Put(baseUrl + scoreHandle, System.Text.Encoding.UTF8.GetBytes(trainingData));
            Debug.Log(System.Text.Encoding.UTF8.GetBytes(trainingData));
            post.method = UnityWebRequest.kHttpVerbPOST;
            post.SetRequestHeader("Content-Type", "application/json");
            post.SetRequestHeader("Accept", "application/json");
            post.SetRequestHeader("Authorization", "Bearer " + _token);
            post.uploadHandler.contentType = "application/json";
            var async = post.SendWebRequest();
            while (!async.isDone)
            {
                yield return Timing.WaitForOneFrame;
            }
            
            if (post.isNetworkError || post.isHttpError)
            {
                Debug.LogError(post.error);
                Debug.Log(post.downloadHandler.text);
            }
            else
            {
                Debug.Log(post.downloadHandler.text);
                Debug.Log("Score submitted. response: " + post.downloadHandler.text);
                JsonData jsonvale = JsonMapper.ToObject(post.downloadHandler.text);
                Timing.RunCoroutine(SendStep(int.Parse(jsonvale["data"]["id"].ToString())));
            }
        }
        IEnumerator<float> SendStep(int idScore){
            yield return Timing.WaitForOneFrame;
            print(Sandbox_ScenarioManager.instance.sectionList.Count);
            List<EuTrainingStepData> stepDatas = new List<EuTrainingStepData>();

            foreach (var section in Sandbox_ScenarioManager.instance.sectionList)
            {
                foreach (var step in section.stepList)
                {
                    EuTrainingStepData stepData = new EuTrainingStepData();
                    stepData.name = step.stepName;
                    stepData.score = idScore;
                    stepData.value = (step.stepCountTarget <= step.stepCountCurrent) ? "pass" : "fail";
                    stepDatas.Add(stepData);
                }
            }

            if(Sandbox_ScenarioManager.instance.quizList.Count > 0){
                foreach (var quiz in Sandbox_ScenarioManager.instance.quizList)
                {
                    EuTrainingStepData stepData = new EuTrainingStepData();
                    stepData.name = quiz.name;
                    stepData.score = idScore;
                    stepData.value = quiz.value+"/"+quiz.total;
                    stepDatas.Add(stepData);
                }
            }
            print(stepDatas.Count);
            print(JsonMapper.ToJson(stepDatas.ToArray()));
            var post = UnityWebRequest.Put(baseUrl + stepHandle, System.Text.Encoding.UTF8.GetBytes(JsonMapper.ToJson(stepDatas.ToArray())));
            Debug.Log(post.url);
            post.method = UnityWebRequest.kHttpVerbPOST;
            post.SetRequestHeader("Content-Type", "application/json");
            post.SetRequestHeader("Accept", "application/json");
            post.SetRequestHeader("Authorization", "Bearer " + _token);
            post.uploadHandler.contentType = "application/json";
            var async = post.SendWebRequest();
            while (!async.isDone)
            {
                yield return Timing.WaitForOneFrame;
            }
            
            if (post.isNetworkError || post.isHttpError)
            {
                Debug.LogError(post.error);
            }
            else
            {
                print(post.downloadHandler.text);
            }
        }

        public IEnumerator<float> GetUser(string token)
        {
            EuToyotaUserResponse authStatus = null;
            
            var request = UnityWebRequest.Get(baseUrl + _userLink);
            request.method = UnityWebRequest.kHttpVerbGET;
            request.SetRequestHeader("Content-Type", "application/json");
            request.SetRequestHeader("Accept", "application/json");
            request.SetRequestHeader("Authorization", "Bearer " + token);
            var async = request.SendWebRequest();
            
            while (!async.isDone)
            {
                yield return Timing.WaitForOneFrame;
            }

            if (request.isNetworkError || request.isHttpError)
            {
                Debug.LogError(request.error);
            }
            else
            {
                Debug.Log(request.downloadHandler.text);

                authStatus = JsonUtility.FromJson<EuToyotaUserResponse>(request.downloadHandler.text);
                User = authStatus.data;
                Debug.Log(User.email);
            }
        }
        
        public IEnumerator<float> GetGuest(int id)
        {
            var request = UnityWebRequest.Get($"{_guestSignInLink}/{id}");
            request.method = UnityWebRequest.kHttpVerbGET;
            request.SetRequestHeader("Content-Type", "application/json");
            request.SetRequestHeader("Accept", "application/json");
            var async = request.SendWebRequest();
            
            while (!async.isDone)
            {
                yield return Timing.WaitForOneFrame;
            }

            if (request.isNetworkError || request.isHttpError)
            {
                Debug.LogError(request.error);
            }
            else
            {
                Debug.Log(request.downloadHandler.text);
                Guest = JsonUtility.FromJson<EuToyotaGuestData>(request.downloadHandler.text);
                Debug.Log(Guest.email);
            }
        }

        public IEnumerator<float> GetScores()
        {
            EuToyotaUserScoresResponse scoreResponse = null;
            
            var request = UnityWebRequest.Get(baseUrl + "training/user-VR");
            request.method = UnityWebRequest.kHttpVerbGET;
            request.SetRequestHeader("Content-Type", "application/json");
            request.SetRequestHeader("Accept", "application/json");
            request.SetRequestHeader("Authorization", "Bearer " + _token);
            var async = request.SendWebRequest();
            
            while (!async.isDone)
            {
                yield return Timing.WaitForOneFrame;
            }

            if (request.isNetworkError || request.isHttpError)
            {
                Debug.LogError(request.error);
            }
            else
            {
                Debug.Log(request.downloadHandler.text);
                scoreResponse = JsonUtility.FromJson<EuToyotaUserScoresResponse>(request.downloadHandler.text);
                Scores = scoreResponse.data;
            }
        }

        public bool IsLoggedIn()
        {
            return Guest != null || User != null;
        }

        public void PertaminaGuestUser(string guestName, string email)
        {
            Guest = new EuToyotaGuestData
            {
                name = guestName,
                email = email
            };
        }

        public void Logout()
        {
            User = null;
            Scores = null;
            Guest = null;
        }

        public IEnumerator<float> GetScenario(System.Action<string> errorCB)
        {
            EuKalbeGetSubScenariosResponse subScenarios = null;

            var request = UnityWebRequest.Get(baseUrl + "/items/sub_scenarios");
            request.method = UnityWebRequest.kHttpVerbGET;
            request.SetRequestHeader("Content-Type", "application/json");
            request.SetRequestHeader("Accept", "application/json");
            request.SetRequestHeader("Authorization", "Bearer " + _token);
            var async = request.SendWebRequest();

            while (!async.isDone)
            {
                yield return Timing.WaitForOneFrame;
            }

            if (request.isNetworkError || request.isHttpError)
            {
                if (request.downloadHandler.text == "")
                {
                    errorCB($"Failed to connect\n to {baseUrl}");
                }
                else
                {
                    var errorJson = JsonMapper.ToObject(request.downloadHandler.text);
                    errorCB(errorJson["errors"][0]["message"].ToString());
                }

                Debug.LogError(request.error);
            }
            else
            {
                Debug.Log(request.downloadHandler.text);
                subScenarios = JsonUtility.FromJson<EuKalbeGetSubScenariosResponse>(request.downloadHandler.text);
                this.subScenarios = subScenarios.data;
            }
        }

        public void GetTime(int scenarioId, System.Action<EuKalbeTimeScenariosData[]> callback, System.Action<string> errorCB)
        {
            Timing.RunCoroutine(getTime(scenarioId, callback, errorCB));
        }

        private IEnumerator<float> getTime(int scenarioId, System.Action<EuKalbeTimeScenariosData[]> callback, System.Action<string> errorCB)
        {
            EuKalbeTimeScenariosDataResponse timeResponse = null;

            var request = UnityWebRequest.Get(baseUrl + $"/items/scores?filter[_and][0][user_id][_eq]={User.id}&filter[_and][1][scenario_id][_eq]={scenarioId}&sort=time");
            print(request.url);
            request.method = UnityWebRequest.kHttpVerbGET;
            request.SetRequestHeader("Content-Type", "application/json");
            request.SetRequestHeader("Accept", "application/json");
            request.SetRequestHeader("Authorization", "Bearer " + _token);
            var async = request.SendWebRequest();

            while (!async.isDone)
            {
                yield return Timing.WaitForOneFrame;
            }

            if (request.isNetworkError || request.isHttpError)
            {
                if (request.downloadHandler.text == "")
                {
                    errorCB($"Failed to connect\n to {baseUrl}");
                }
                else
                {
                    var errorJson = JsonMapper.ToObject(request.downloadHandler.text);
                    errorCB(errorJson["errors"][0]["message"].ToString());
                }

                Debug.LogError(request.error);
            }
            else
            {
                Debug.Log(request.downloadHandler.text);
                timeResponse = JsonUtility.FromJson<EuKalbeTimeScenariosDataResponse>(request.downloadHandler.text);
                callback(timeResponse.data);
            }
        }
    }
}