using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Tictech.LoadManager;
using UnityEngine.SceneManagement;
using Tictech.EnterpriseUniversity;

namespace SandboxVRSeason2.Framework
{
    public class MainMenuManager : MonoBehaviour
    {
        [SerializeField] List<Sandbox_ScenarioData> scenarioList;
        
        [Space]
        [SerializeField] GameObject scenarioListPanel;
        [SerializeField] GameObject subScenarioListPanel;
        [SerializeField] GameObject scenarioDetailPanel;


        [Space]
        [SerializeField] Transform scenariolistParent;
        [SerializeField] SB_UIReference scenarioListPrefab;

        [Space]
        [SerializeField] Transform subScenariolistParent;
        [SerializeField] TextMeshProUGUI subScenariolistName;
        [SerializeField] SB_UIReference subScenarioListPrefab;

        [Space]
        [SerializeField] TextMeshProUGUI scenarioTitle;
        [SerializeField] TextMeshProUGUI scenarioDetailTitleText;
        [SerializeField] TextMeshProUGUI scenarioDetailTimeText;
        [SerializeField] TextMeshProUGUI scenarioDetailScoreText;
        [SerializeField] Image scenarioDetailImage;
        [SerializeField] TextMeshProUGUI scenarioPurpose;
        [SerializeField] TMP_InputField minute;
        [SerializeField] TMP_InputField second;
        [SerializeField] SB_UIButton scenarioDetailStartSandboxTimer;
        [SerializeField] SB_UIButton scenarioDetailStartSandboxNoTimer;
        [SerializeField] SB_UIButton scenarioDetailStartLinear;

        [Space] 
        [SerializeField] private Sandbox_SubScenarioData tutorialData;
        [SerializeField] private SB_UIButton tutorialButton;

        Sandbox_SubScenarioData targetSubsScenario;

        public static float activeTimer;

        private void Start()
        {
            scenarioListPanel.SetActive(true);
            subScenarioListPanel.SetActive(false);
            scenarioDetailPanel.SetActive(false);

            SetUpScenarioList();

            // scenarioDetailStartSandbox.onDown.AddListener(() => { LoadScenario(false); });
            // scenarioDetailStartLinear.onDown.AddListener(() => { LoadScenario(true); });
            
            tutorialButton.onDown.AddListener(() => {RuntimeManager.LoadScene(tutorialData.SubScenarioScene);});
        }

        void SetUpScenarioList()
        {
            foreach (Transform child in scenariolistParent)
                Destroy(child.gameObject);

            foreach (var scenarioData in scenarioList)
            {
                var _bt = Instantiate(scenarioListPrefab, scenariolistParent);
                _bt.imageList[0].sprite = scenarioData.ScenarioSprite;
                _bt.tmProList[0].text = scenarioData.ScenarioName;
                _bt.customButtonList[0].onDown.AddListener(() => { SetUpSubScenarioList(scenarioData); print("show scenario detail panel"); scenarioDetailPanel.SetActive(false); scenarioListPanel.SetActive(false); subScenarioListPanel.SetActive(true); });
            }
        }

        void SetUpSubScenarioList(Sandbox_ScenarioData scenario)
        {
            print("subscenario list");
            subScenariolistName.text = scenario.ScenarioName;
            scenarioTitle.text = scenario.ScenarioName;
            foreach(Transform child in subScenariolistParent)
                Destroy(child.gameObject);

            foreach (var subScenarioData in scenario.SubScenariolist)
            {
                var _bt = Instantiate(subScenarioListPrefab, subScenariolistParent);
                _bt.imageList[0].sprite = subScenarioData.SubScenarioSprite;
                _bt.tmProList[0].text = subScenarioData.SubScenarioName;
                _bt.customButtonList[0].onDown.AddListener(() => { SetUpScenarioDetails(subScenarioData); scenarioDetailPanel.SetActive(true); subScenarioListPanel.SetActive(false);});
            }
        }

        void SetUpScenarioDetails(Sandbox_SubScenarioData subScenarioData)
        {
            scenarioDetailTitleText.text = subScenarioData.SubScenarioName;
            scenarioDetailImage.sprite = subScenarioData.SubScenarioSprite;
            scenarioPurpose.text = subScenarioData.Purpose;


            //scenarioDetailStartSandbox.gameObject.SetActive(subScenarioData.isSandbox ? true : false);
            scenarioDetailStartLinear.gameObject.SetActive(subScenarioData.isLinear ? true : false);
            scenarioDetailStartSandboxTimer.onDown.AddListener(() => {LoadScenarioWithTimer(true);});
            scenarioDetailStartSandboxNoTimer.onDown.AddListener(() => { LoadScenarioWithTimer(false); });
            scenarioDetailStartLinear.onDown.AddListener(() => {LoadScenario(true);});

            targetSubsScenario = subScenarioData;
            //EuRuntimeManager.Instance.LastScore(subScenarioData.id,(data) => {
            //    scenarioDetailScoreText.text = data.score != 0? data.score+"" : "--";
            //    if(!string.IsNullOrEmpty(data.time)){
            //        var span = TimeSpan.FromSeconds((double.Parse(data.time)));
            //        scenarioDetailTimeText.text =  span.Minutes+" : "+span.Seconds;
            //    }
            //});
            // LoadScenario(true);
        }
        [SerializeField]
        KeyboardCustom keyboardCustom;
        public void OpenKeyboard(TMPro.TMP_InputField inputField)
        {
            print("open keyboard");
            keyboardCustom.panel.SetActive(true);
            keyboardCustom.input = inputField;
            // overlayKeyboard = TouchScreenKeyboard.Open("",TouchScreenKeyboardType.Default);
            // if(overlayKeyboard != null){
            //     inputField.text = overlayKeyboard.text;
            // }
        }
        void LoadScenarioWithTimer(bool isTimer)
        {
            int minuteF = string.IsNullOrEmpty(minute.text) ? 0 : int.Parse(minute.text);
            int secondF = string.IsNullOrEmpty(second.text) ? 0 : int.Parse(second.text);
            activeTimer = isTimer ? (minuteF*60) + secondF : 0;
            RuntimeManager.LoadScenario(targetSubsScenario, false, activeTimer);
        }
        void LoadScenario(bool isLinear, float timer = 0)
        {
            RuntimeManager.LoadScenario(targetSubsScenario, isLinear,timer);
        }
    }
}
