using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Tictech.EnterpriseUniversity;
using MEC;
using TMPro;
using UnityEngine.Events;
using Tictech.Utilities.PersistentCanvas;
using System.Diagnostics;
using Debug = UnityEngine.Debug;

public class LoginManager : MonoBehaviour
{
    [Header("Auth")]

    public Button enterLoginPanelButton;
    public SB_UIButton loginButton;
    public TMP_InputField idField;
    public TMP_InputField passwordField;

    [Space]
    public Button enterLoginPanelGuestButton;
    public Button guestLoginButton;
    public TMP_InputField guestIdField;
    public TMP_InputField guestPasswordField;

    [Space]
    public TextMeshProUGUI errorMessageTMP;
    public Button retryButton;

    [Space]
    public TextMeshProUGUI nameHolder;

    [Header("Menu")]
    public GameObject panelTryLogin;
    public GameObject panelFailedLogin;

    [Space]
    [SerializeField] private UnityEvent onSignedIn;
    [SerializeField] private UnityEvent onGuestSignedIn;
    [SerializeField] private UnityEvent onLogout;

    private EventSystem _eventSystem;

    private bool _asGuest = false;
    public bool asGuest { get { return _asGuest; } set { _asGuest = value; } }



    void Start()
    {
        print("login manager start");
        var name = "";
        if (EuRuntimeManager.Instance.User != null) { 
            //if(!string.IsNullOrEmpty(EuRuntimeManager.Instance.User.id)){
            //    print("user login");
            onSignedIn.Invoke();
            name = EuRuntimeManager.Instance.User.first_name+" "+ EuRuntimeManager.Instance.User.last_name;
        }else{
            print("user not login");
        }

        _eventSystem = EventSystem.current;

        enterLoginPanelButton.onClick.AddListener(delegate { asGuest = false; });
        // loginButton.onClick.AddListener(delegate { print("login click");Timing.RunCoroutine(SignIn()); });

        enterLoginPanelGuestButton.onClick.AddListener(delegate { asGuest = true; });
        guestLoginButton.onClick.AddListener(delegate { Timing.RunCoroutine(SignIn()); });

        retryButton.onClick.AddListener(delegate { Timing.RunCoroutine(SignIn()); });

        if(EuRuntimeManager.Instance.Guest != null && !string.IsNullOrEmpty(EuRuntimeManager.Instance.Guest.name))
        {
            Debug.Log("Guest Signed In");
            name = EuRuntimeManager.Instance.Guest.name;
            onGuestSignedIn?.Invoke();
        }

        if (nameHolder)
            nameHolder.text = name;
    }
    public void MoveScene(){
        Application.LoadLevel("Test");
    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab) && _eventSystem.currentSelectedGameObject.GetComponent<Selectable>())
        {
            Selectable next = _eventSystem.currentSelectedGameObject.GetComponent<Selectable>().FindSelectableOnDown();

            if (next != null)
            {
                InputField inputfield = next.GetComponent<InputField>();
                if (inputfield != null) inputfield.OnPointerClick(new PointerEventData(_eventSystem));

                _eventSystem.SetSelectedGameObject(next.gameObject, new BaseEventData(_eventSystem));
            }
        }
        if (Input.GetKeyDown(KeyCode.Return))
        {
            Login();
        }
    }
    private TouchScreenKeyboard overlayKeyboard;
    [SerializeField]
    KeyboardCustom keyboardCustom;
    public void OpenKeyboard(TMPro.TMP_InputField inputField){
        print("open keyboard");
        keyboardCustom.panel.SetActive(true);
        keyboardCustom.input = inputField;
        // overlayKeyboard = TouchScreenKeyboard.Open("",TouchScreenKeyboardType.Default);
        // if(overlayKeyboard != null){
        //     inputField.text = overlayKeyboard.text;
        // }
    }
    public void InputKeyboard(){
    }
    public void Login(){
        Timing.RunCoroutine(SignIn());
    }
    IEnumerator<float> SignIn()
    {
        print("sign in");
        if(!asGuest)
        {
            bool valid = !string.IsNullOrEmpty(idField.text);

            if (string.IsNullOrEmpty(passwordField.text))
            {
                valid = false;
            }

            if (!valid)
                yield break;
        }

        else
        {
            bool valid = !string.IsNullOrEmpty(guestIdField.text);

            if (string.IsNullOrEmpty(guestPasswordField.text))
            {
                valid = false;
            }

            if (!valid)
                yield break;
        }

        panelTryLogin.SetActive(true);

        if (!asGuest)
            yield return Timing.WaitUntilDone(EuRuntimeManager.Instance.SignIn(idField.text, passwordField.text, (err) => { errorMessageTMP.text = err; }));
        else yield return Timing.WaitUntilDone(EuRuntimeManager.Instance.SignGuestIn(guestIdField.text, guestPasswordField.text));

        if(asGuest)
            EuRuntimeManager.Instance.PertaminaGuestUser(guestIdField.text, guestPasswordField.text);
        
        Debug.Log(guestIdField.text);
        
        panelTryLogin.SetActive(false);

        if ((!asGuest && EuRuntimeManager.Instance.User == null) || (asGuest && EuRuntimeManager.Instance.Guest == null))
        {
            panelFailedLogin.SetActive(true);
            yield break;
        }

        if(EuRuntimeManager.Instance.subScenarios == null)
        {
            panelFailedLogin.SetActive(true);
            yield break;
        }

        if (nameHolder)
            nameHolder.text = asGuest ? $"{EuRuntimeManager.Instance.Guest.name}" : $"{EuRuntimeManager.Instance.User.first_name+" "+ EuRuntimeManager.Instance.User.last_name}";


        if (!asGuest) onSignedIn?.Invoke();
        else onGuestSignedIn?.Invoke();
    }
    
    public void Logout()
    {
        print("Logout");
        onLogout?.Invoke();
        EuRuntimeManager.Instance.Logout();
    }

    public void ShutDown()
    {
        PersistentCanvas.Instance.CreateModal("Confirmation", delegate
        {
            UnityEngine.Debug.Log("Confirmed.");

            var psi = new ProcessStartInfo("shutdown", "/s /t 0") { CreateNoWindow = true, UseShellExecute = false };
            Process.Start(psi);
        }, "Tutup Program", "Tutup program virtual training dan matikan komputer?", "Yakin", "Batalkan");
    }
    public void ExitApp()
    {
        Application.Quit();
    }
}
