using HurricaneVR.Framework.Core;
using HurricaneVR.Framework.Core.Grabbers;
using HurricaneVR.Framework.Core.Utils;
using UnityEngine;
using Sirenix.OdinInspector;

namespace HurricaneVR.Framework.Components
{
    [RequireComponent(typeof(HVRGrabbable))]
    public class HVRDialMK2 : MonoBehaviour
    {
        public bool FlipControllerRotation;

        public DialSteppedEvent DialStepChanged = new DialSteppedEvent();
        public DialTurnedEvent DialTurned = new DialTurnedEvent();

        public Transform RotationTarget;
        public float startAngle;
        public int Steps;
        public float StepSize;
        public bool SnapTarget = true;
        public bool ClampMaxAngle = true;
        public float MaximumAngle = 360f;
        public AudioClip AudioClip;
        [Range(.5f, 2)]
        public float Deadzone = 1f;
        public Vector3 LocalRotationAxis;
        public Vector3 LocalAxisStart;
        public bool DiscardAngle;

        [Space]
        [FoldoutGroup("Read Only"), ReadOnly, SerializeField] private int loop, totalLoop;
        [FoldoutGroup("Read Only"), ReadOnly] public float extendedAngle;
        [FoldoutGroup("Read Only"), ReadOnly, SerializeField] private float loopProgress;
        [FoldoutGroup("Read Only"), ReadOnly, SerializeField] private float previousLoopProgress;
        [FoldoutGroup("Read Only"), ReadOnly] public float currentAngle;

        private float _previousAngle;
        private Vector3 _originalVector;
        private Rigidbody _rigidBody;
        private Rigidbody _targetRigidBody;
        private float _previousAngleFromStart;



        public int Step { get; protected set; }

        [HideInInspector] public bool isInit;
        public HVRGrabbable Grabbable { get; private set; }
        public HVRHandGrabber PrimaryGrabber { get; private set; }

        protected virtual void Start()
        {
            //tambahan
            if (MaximumAngle >= 360)
                totalLoop = Mathf.FloorToInt(MaximumAngle / 360);
            else
                totalLoop = 0;
            //tambahan end

            _rigidBody = transform.GetComponent<Rigidbody>();
            _targetRigidBody = RotationTarget.GetComponent<Rigidbody>();

            if (Steps <= 1)
                StepSize = 0f;
            else if (Steps > 1 && Mathf.Approximately(StepSize, 0f))
                StepSize = MaximumAngle / Steps;

            LocalAxisStart.Normalize();
            LocalRotationAxis.Normalize();

            _originalVector = transform.localRotation * LocalAxisStart;

            Grabbable = GetComponent<HVRGrabbable>();
            Grabbable.Grabbed.AddListener(OnGrabbed);
            Grabbable.Released.AddListener(OnReleased);

            _previousAngle = 0f;
            _previousAngleFromStart = 0f;

            //tambahan
            startAngle = Mathf.Clamp(startAngle, 0, MaximumAngle);
            if(startAngle > 0)
            {
                SetStartAngle();
            }
            else
            {
                isInit = true;
            }
            //tambahan end
            
        }

        //tambahan
        public void SetStartAngle()
        {
            transform.Rotate((LocalRotationAxis * startAngle), Space.Self);

            float rotationProgress = startAngle / 360;
            loop = Mathf.FloorToInt(rotationProgress);
            loopProgress = rotationProgress - loop;
            previousLoopProgress = loopProgress;
            currentAngle = startAngle;
            _previousAngleFromStart = startAngle;
            isInit = true;
        }
        //tambahan end

        protected virtual void FixedUpdate()
        {
            //Debug.DrawLine(transform.position, transform.position + transform.TransformDirection(currentVector).normalized * 1f, Color.red);
            CheckForRotation();
        }


        protected void CheckForRotation(bool force = false)
        {
            if (force || PrimaryGrabber)
            {
                if (!force && Mathf.Abs(PrimaryGrabber.HVRTrackedController.DeltaEulerZ) < Deadzone)
                    return;

                var currentVector = transform.localRotation * LocalAxisStart;
                var rotationAxis = LocalRotationAxis;

                var conRotation = new Vector3(
                    PrimaryGrabber.HVRTrackedController.DeltaEulerZ,
                    PrimaryGrabber.HVRTrackedController.DeltaEulerZ,
                    PrimaryGrabber.HVRTrackedController.DeltaEulerZ);

                var controllerRotation = FlipControllerRotation ? -conRotation : conRotation;

                controllerRotation.Scale(-rotationAxis);

                var rotation = Quaternion.Euler(controllerRotation);

                var newRotation = transform.localRotation * rotation;

                var newVector = newRotation * LocalAxisStart;

                var delta = FlipControllerRotation ? PrimaryGrabber.HVRTrackedController.DeltaEulerZ : - PrimaryGrabber.HVRTrackedController.DeltaEulerZ;

                var angleFromStart = Vector3.SignedAngle(_originalVector, newVector, rotationAxis);

                if (angleFromStart < 0)
                    angleFromStart = 360 + angleFromStart;

                var clockWise = delta >= 0f;

                //tambahan
                if (totalLoop > 0)
                {
                    loopProgress = angleFromStart / 360;
                    Debug.Log("prev angle = " + _previousAngleFromStart + " angle = " + angleFromStart + "\nprev progress = " + previousLoopProgress + " progress = " + loopProgress + " loop = " + loop);

                    if (clockWise && loopProgress < previousLoopProgress)
                    {
                        if(loop < totalLoop)
                            loop++;
                    }
                    else if(!clockWise && loopProgress > previousLoopProgress /*&& _previousAngleFromStart != MaximumAngle*/)
                    {
                        if (loop > 0)
                            loop--;
                        else
                            loopProgress = 0;
                    }
                    angleFromStart = (angleFromStart) + (360 * loop);
                }
                //tambahan end

                if (ClampMaxAngle)
                {

                    if (clockWise && _previousAngleFromStart + delta >= MaximumAngle - .1)
                    {
                        angleFromStart = MaximumAngle;
                    }
                    else if (!clockWise && _previousAngleFromStart + delta <= 0.1)
                    {
                        angleFromStart = 0;
                    }
                    else if (angleFromStart > MaximumAngle)
                    {
                        float upDiff = 360 - angleFromStart;
                        float lowerDiff = angleFromStart - MaximumAngle;

                        if (upDiff < lowerDiff) angleFromStart = 0;
                        else angleFromStart = MaximumAngle;
                    }
                }

                var stepAngle = angleFromStart;
                if (Steps > 1)
                {
                    Step = Mathf.RoundToInt(angleFromStart / StepSize);
                    stepAngle = Step * StepSize;
                    if (Mathf.Approximately(stepAngle, 360))
                    {
                        Step = 0;
                    }
                }

                var finalVector = Quaternion.AngleAxis(angleFromStart, rotationAxis) * _originalVector;
                var syncAngle = Vector3.SignedAngle(currentVector, finalVector, rotationAxis);
                var syncRotation = Quaternion.AngleAxis(syncAngle, rotationAxis) * transform.localRotation;

                if (Steps > 1)
                {
                    if (!Mathf.Approximately(stepAngle, _previousAngle))
                    {
                        OnStepChanged(Step, true);
                        _previousAngle = stepAngle;

                        if (SnapTarget)
                        {
                            var percent = Mathf.Approximately(0f, MaximumAngle) ? 0f : stepAngle / MaximumAngle;
                            OnAngleChanged(stepAngle, syncAngle, percent, true);
                        }
                    }
                }

                if (!SnapTarget && !Mathf.Approximately(_previousAngleFromStart, angleFromStart))
                {
                    var percent = Mathf.Approximately(0f, MaximumAngle) ? 0f : angleFromStart / MaximumAngle;
                    OnAngleChanged(angleFromStart, syncAngle, angleFromStart / percent, true);
                }

                var steppedVector = Quaternion.AngleAxis(stepAngle, rotationAxis) * _originalVector;
                var steppedAngle = Vector3.SignedAngle(currentVector, steppedVector, rotationAxis);
                var steppedRotation = Quaternion.AngleAxis(steppedAngle, rotationAxis) * transform.localRotation;

                var targetRotation = SnapTarget ? steppedRotation : syncRotation;

                //if (_targetRigidBody)
                //{
                //    _targetRigidBody.MoveRotation(_targetRigidBody.rotation * targetRotation);
                //}
                //else
                {
                    RotationTarget.localRotation = targetRotation;
                }

                //if (_rigidBody)
                //{
                //    _rigidBody.MoveRotation(_rigidBody.rotation * syncRotation);
                //}
                //else
                {
                    transform.localRotation = syncRotation;
                }

                previousLoopProgress = loopProgress;
                _previousAngleFromStart = angleFromStart;

                currentAngle = angleFromStart;
            }
        }



        protected void OnGrabbed(HVRGrabberBase grabber, HVRGrabbable hvrGrabbable)
        {
            PrimaryGrabber = grabber as HVRHandGrabber;

            if (DiscardAngle)
            {
                if (_rigidBody)
                {
                    _rigidBody.MoveRotation(RotationTarget.rotation);
                }
                else
                {
                    transform.localRotation = RotationTarget.localRotation;
                }
            }
        }

        protected void OnReleased(HVRGrabberBase grabber, HVRGrabbable hvrGrabbable)
        {
            PrimaryGrabber = null;

            if (DiscardAngle)
            {
                if (_rigidBody)
                {
                    _rigidBody.MoveRotation(RotationTarget.rotation);
                }
                else
                {
                    transform.localRotation = RotationTarget.localRotation;
                }
            }
        }

        protected virtual void OnStepChanged(int step, bool raiseEvents)
        {
            if (AudioClip)
                SFXPlayer.Instance.PlaySFXRandomPitch(AudioClip, transform.position, .9f, 1.1f);
            if (raiseEvents)
                DialStepChanged.Invoke(step);
        }

        protected virtual void OnAngleChanged(float angle, float delta, float percent, bool raiseEvents)
        {
            if (raiseEvents)
                DialTurned.Invoke(angle, delta, percent);
        }


        protected virtual void Update()
        {
        }
    }
}
