using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEditor;
using Sirenix.OdinInspector;
using TMPro;
using SandboxVRSeason2.Framework.Multiplayer;
using Photon.Pun;

namespace SandboxVRSeason2.Framework
{
    public class Sandbox_Section : Sandbox_ObjectID
    {
        public string sectionName;
        [TextArea(2, 10)] public string sectionDesc;

        [Space(15), ListDrawerSettings(ShowIndexLabels = true)]
        public List<ScenarioSectionStep> stepList;
        public List<Sandbox_Object> objectList;

        [FoldoutGroup("Event")] public UnityEvent onSectionStart;
        [FoldoutGroup("Event")] public UnityEvent onSectionComplete;

        [FoldoutGroup("Advance")] public bool isCountAfterComplete = false;
        [FoldoutGroup("Advance")] public bool invokeStartEventOnSandbox = false;
        [FoldoutGroup("Advance")] public bool skipMistakeCount = false;

        [FoldoutGroup("Debug"), ReadOnly, SerializeField] public bool isCompleted = false;
        [FoldoutGroup("Debug"), ReadOnly, SerializeField] public bool isCorrect = false;
        [FoldoutGroup("Debug"), ReadOnly] public bool isInit;
        [FoldoutGroup("Debug"), ReadOnly] public SB_UIReference uiReference;
        [FoldoutGroup("Debug"), ReadOnly] public SB_UIReference descUIReference;
        [FoldoutGroup("Debug"), ReadOnly] public List<SB_UIReference> stepUIReference;
        [FoldoutGroup("Debug"), ReadOnly] private Coroutine updateCor;

        private PhotonView _photonView;
        private Sandbox_BroadcastMethods _broadcast;

        private Sandbox_ScenarioManager scenarioManager;

        private void Start()
        {
            _photonView = GetComponent<PhotonView>();
            _broadcast = GetComponent<Sandbox_BroadcastMethods>();
            scenarioManager = FindObjectOfType<Sandbox_ScenarioManager>();
            StartCoroutine(Initialize());
        }

        private IEnumerator Initialize()
        {
            print("SB : Initiating Section " + sectionName);
            CheckComplete();
            isInit = true;
            print("SB : Section " + sectionName + " Initiated");
            yield break;
        }

        public void StartSection()
        {
            onSectionStart.Invoke();
        }

        public void StepCountAdd(int stepIndex)
        {
            if ((!isCountAfterComplete && isCompleted) || Sandbox_ScenarioManager.instance.isScenarioFinished) return;
            if (stepList[stepIndex].stepCountCurrent == stepList[stepIndex].stepCountTarget) return;

            stepList[stepIndex].stepCountCurrent += 1;

            if(stepList[stepIndex].stepCountCurrent == stepList[stepIndex].stepCountTarget)
            {
                stepList[stepIndex].OnStepCompleted?.Invoke();
                Sandbox_ScenarioManager.instance.onEachSectionComplete?.Invoke();
            }

            if (stepIndex > 0 && scenarioManager.mode == ScenarioModeEnum.SANDBOX)
            {
                var isPrevStepCompleted = CheckPrevStepIsComplete(stepIndex);
                if (!isPrevStepCompleted) {
                    stepList[stepIndex].stepCountCurrent = 0;
                    scenarioManager.CompleteScenario();
                    return; 
                }
            }

            Debug.Log("HUBLA");

            CheckComplete();

            UpdateUI();

            Kalbe.SoundEffectManager.Play("task clear");
        }

        private bool CheckPrevStepIsComplete(int currentIndex)
        {
            if (stepList[currentIndex - 1].stepCountCurrent < stepList[currentIndex - 1].stepCountTarget)
            {
                ForceComplete();
                return false;
            }
            return true;
        }

        public void StepCountSubtract(int stepIndex)
        {
            if (!isCountAfterComplete && isCompleted) return;

            stepList[stepIndex].stepCountCurrent -= 1;

            CheckComplete();

            UpdateUI();
        }

        public void ForceComplete()
        {
            isCompleted = true;
            onSectionComplete.Invoke();
        }

        void CheckComplete()
        {
            foreach (var _step in stepList)
            {
                if(_step.stepMode != ScenarioSectionStep.Mode.BOTH && _step.stepMode.ToString() != Sandbox_ScenarioManager.instance.mode.ToString()){
                    break;
                }
                if (_step.stepCountCurrent < _step.stepCountTarget)
                {
                    if (Sandbox_ScenarioManager.instance.mode == ScenarioModeEnum.SANDBOX && isCompleted)
                    {
                        isCompleted = false;
                        isCorrect = false;
                        Sandbox_ScenarioUIManager.instance.UpdateSectionList(uiReference, this);
                    }

                    return;
                }
            }

            if (!isCompleted)
            {
                isCompleted = true;
                if (Sandbox_ScenarioManager.instance.isInit)
                    Sandbox_ScenarioManager.instance.CheckSectionCorrect(this);

                Sandbox_ScenarioUIManager.instance.UpdateSectionList(uiReference, this);
            }

            onSectionComplete.Invoke();

            if (Sandbox_ScenarioManager.instance.mode == ScenarioModeEnum.LINEAR)
            {
                Sandbox_ScenarioManager.instance.NextSection();
            }
            else
            {
                print("check complete completed");
                Sandbox_ScenarioManager.instance.CheckAllSectionCompletion();
            }
        }

        public void UpdateUI()
        {
            if (!gameObject.activeInHierarchy) return;

            if (updateCor != null) StopCoroutine(updateCor);

            print("update UI");
            updateCor = StartCoroutine(UpdateSectionDetailCoroutine());
        }

        public IEnumerator UpdateSectionDetailCoroutine()
        {
            yield return new WaitUntil(() => Sandbox_ScenarioUIManager.instance.sectionDetailsInit == true);

            if (sectionDesc != "")
                descUIReference.tmProList[0].text = sectionDesc;

            print("update coroutine");

            for (int i = 0; i < stepList.Count; i++)
            {
                ScenarioSectionStep step = null;
                Image stepCompleteIcon = null;

                step = stepList[i];
                if(step.stepMode == ScenarioSectionStep.Mode.BOTH || step.stepMode.ToString() == Sandbox_ScenarioManager.instance.mode.ToString()){
                    SB_UIReference _SectionItem = stepUIReference[i];

                    var stepNameText = _SectionItem.tmProList[0];
                    var stepDescText = _SectionItem.tmProList[1];
                    stepCompleteIcon = _SectionItem.imageList[0];
                    var stepDescObject = _SectionItem.gameObjectList[0];

                    int c = Mathf.Clamp(step.stepCountCurrent, 0, step.stepCountTarget);
                    stepNameText.text = $"{i+1}. {step.stepName} ({c}/{step.stepCountTarget})";

                    if (step.stepDescription != "")
                        stepDescText.text = step.stepDescription;
                    else stepDescObject.SetActive(false);

                    if (step.stepCountTarget > 0)
                    {
                        if (step.stepCountCurrent >= step.stepCountTarget)
                            stepCompleteIcon.sprite = Sandbox_ScenarioUIManager.instance.completeIcon;
                        else
                            stepCompleteIcon.sprite = Sandbox_ScenarioUIManager.instance.incompleteIcon;
                    }
                    else
                    {
                        stepCompleteIcon.sprite = Sandbox_ScenarioUIManager.instance.infoOnlyIcon;
                    }
                }
            }
        }
    }



    [System.Serializable]
    public class ScenarioSectionStep
    {
        public string stepName;
        [TextArea(2, 7)] public string stepDescription;
        public int stepCountTarget;
        public enum Mode
        {
            BOTH, SANDBOX, LINEAR
        }
        public Mode stepMode = Mode.BOTH;

        [FoldoutGroup("Step Completion")] public UnityEvent OnStepCompleted;

        [FoldoutGroup("Debug"), ReadOnly] public int stepCountCurrent;

        [FoldoutGroup("Debug"), Button("INVOKE STEP COMPLETE"), DisableInEditorMode]
        private void invokeStepComplete()
        {
            OnStepCompleted.Invoke();
        }
    }
}
