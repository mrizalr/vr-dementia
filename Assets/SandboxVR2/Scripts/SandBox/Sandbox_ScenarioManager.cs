using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;
using System;
using System.Linq;
using System.IO;
using HurricaneVR.Framework.Core.Player;
using DG.Tweening;
using Sinbad;
using Tictech.EnterpriseUniversity;
using Tictech.LoadManager;
using Photon.Pun;
using SandboxVRSeason2.Framework.Multiplayer;

namespace SandboxVRSeason2.Framework
{
    public class Sandbox_ScenarioManager : Sandbox_ObjectID
    {
        [System.Serializable]
        public struct QuizResult
        {
            public string name;
            public int value;
            public int total;
        }
        public static Sandbox_ScenarioManager instance;
        public static Sandbox_ScenarioManager Instance
        {
            get
            {
                return FindObjectOfType<Sandbox_ScenarioManager>();
            }
        }

        public int idSubscenario;
        public string prefabName;
        public ScenarioModeEnum mode;
        public float timer;
        bool useTimer = false;
        public MultiplayerModeEnum multiplayerMode;
        public List<Sandbox_Section> sectionList;
        public List<QuizResult> quizList;

        public Material highlightMaterial;

        public bool IncludeDisabledSection;
        public bool DisableAllObjectAfterSectionComplete;
        public bool DisablePreviouslyCompletedSection;
        public bool IsRuntimeInitiation = true;

        [FoldoutGroup("Event")] public UnityEvent onEachSectionComplete;

        [FoldoutGroup("Debug"), ReadOnly] public bool isInit;
        [FoldoutGroup("Debug"), ReadOnly] public bool isStarted;
        [FoldoutGroup("Debug"), ReadOnly] private bool isTimePause;
        [FoldoutGroup("Debug"), ReadOnly] public float playTime;
        [FoldoutGroup("Debug"), ReadOnly] public float playScore;
        [FoldoutGroup("Debug"), ReadOnly] public Sandbox_Section currentSection;

        [ReadOnly] public bool isScenarioFinished = false;

        private List<ScoreValue> _scores = new List<ScoreValue>();

        private float initTimer;

        private HVRPlayerController playerController;
        private PhotonView _photonView;
        [FoldoutGroup("Debug"), ReadOnly, SerializeField] private Sandbox_BroadcastMethods _broadcast;

        private void Awake()
        {
            playerController = FindObjectOfType<HVRPlayerController>(true);

            if (multiplayerMode == MultiplayerModeEnum.OFFLINE)
                PhotonNetwork.OfflineMode = true;
            else
                FindObjectOfType<Sandbox_NetworkSpawner>().InitiateNetworkSpawn();

            if (instance != null && instance != this)
                Destroy(this.gameObject);
            else
                instance = this;

            ScreenFade(1, 0);
        }

        private void Start()
        {
            _photonView = GetComponent<PhotonView>();
            _broadcast = GetComponent<Sandbox_BroadcastMethods>();

            if (File.Exists(Application.dataPath + "/scores.csv"))
                _scores = CsvUtil.LoadObjects<ScoreValue>(Application.dataPath + "/scores.csv");

            if (!IsRuntimeInitiation)
            {
                //if (multiplayerMode == MultiplayerModeEnum.OFFLINE)
                    //Initialize();
                //else
                //{
                //    if (_photonView.IsMine)
                //        _photonView.RPC(nameof(_broadcast.BroadcastScenarioManagerInitialize), RpcTarget., id);
                //}
                Initialize();

            }
        }

        //public List<GameObject> playerList;

        private void Update()
        {
            TimeCount();
        }

        public void Initialize()
        {
            StartCoroutine(InitializeCoroutine());
        }

        private IEnumerator InitializeCoroutine()
        {
            isScenarioFinished = false;

            useTimer = (timer > 0);
            print("SB : Initiating Scenario Manager");
            foreach (var _outline in GetComponentsInChildren<SB_Outline>())
            {
                _outline.enabled = false;
            }
            foreach (var _section in GetComponentsInChildren<Sandbox_Section>(IncludeDisabledSection))
            {
                yield return new WaitUntil(() => _section.isInit == true);
                sectionList.Add(_section);
                _section.gameObject.SetActive(false);
            }

            if (mode == ScenarioModeEnum.LINEAR)
            {
                foreach (var sobj in GetComponentsInChildren<Sandbox_Object>())
                    sobj.linearLock = true;

                NextSection();

                //if (multiplayerMode == MultiplayerModeEnum.OFFLINE)
                //    NextSection();
                //else
                //{
                //    RPC_NextSection();
                //}
            }
            else
            {
                foreach (var _section in sectionList)
                    if (_section.invokeStartEventOnSandbox)
                        _section.onSectionStart.Invoke();
            }

            isInit = true;

            //_photonView.RPC(nameof(_broadcast.BroadcastBoolInitScenarioManager), RpcTarget.OthersBuffered, id, isInit);

            print("SB : Initiating Scenario Manager done: " + isInit);

            yield return new WaitUntil(() => Sandbox_ScenarioUIManager.instance.isInit == true);

            print("SB : Scenario Manager Initiated");

            ScreenFade(0, 1f);
        }

        public void StartScenario()
        {
            if (!isStarted)
            {
                isStarted = true;
                if (mode == ScenarioModeEnum.LINEAR)
                {
                    NextSection();
                    //if (multiplayerMode == MultiplayerModeEnum.OFFLINE)
                    //    NextSection();
                    //else
                    //{
                    //    RPC_NextSection();
                    //}
                }
                else
                {
                    foreach (var _section in sectionList)
                        if (_section.invokeStartEventOnSandbox)
                            _section.onSectionStart.Invoke();
                }
            }
        }

        //public void RPC_StartScenario()
        //{

        //}

        //[PunRPC]
        public void NextSection()
        {
            StartCoroutine(NextSectionCor());
        }

        //public void RPC_NextSection()
        //{
        //    _photonView.RPC(nameof(NextSection), RpcTarget.AllBuffered);
        //}

        IEnumerator NextSectionCor()
        {
            var _index = (currentSection != null) ? sectionList.IndexOf(currentSection) + 1 : 0;

            if (sectionList.Count > _index)
            {
                // if (currentSection)
                //     foreach (var sobj in currentSection.objectList)
                //         sobj.linearLock = true;

                if(DisablePreviouslyCompletedSection)
                {
                    currentSection?.gameObject.SetActive(false);
                }
                sectionList[_index].gameObject.SetActive(true);

                currentSection = sectionList[_index];
                currentSection.StartSection();

                float _t = Sandbox_ScenarioUIManager.instance.sdCompleteAnimator.GetAnimationLength("SectionCompletePlay");
                Sandbox_ScenarioUIManager.instance.sdCompleteAnimator.Play("SectionCompletePlay");
                print(_t);
                yield return new WaitForSeconds(_t);

                print("section="+currentSection);
                foreach (var sobj in currentSection.objectList)
                {
                    Debug.Log($"{sobj.name} : {sobj.linearLock}");
                    sobj.linearLock = false;
                }

                Sandbox_ScenarioUIManager.instance.OpenStepDetail(currentSection);
                
                currentSection.UpdateUI();
            }
            else
            {
                print("next section complete");
                CompleteScenario();
            }
        }

        public void CheckAllSectionCompletion()
        {
            foreach (var section in sectionList)
            {
                if (!section.isCompleted) return;
            }
            CompleteScenario();
        }

        public void CompleteScenario()
        {
            isScenarioFinished = true;

            if (DisableAllObjectAfterSectionComplete)
            {
                foreach (var sect in sectionList)
                {
                    foreach (var sobj in sect.objectList)
                    {
                        Debug.Log($"{sobj.name} : {sobj.linearLock}");
                        sobj.linearLock = true;
                    }
                }
            }

            isTimePause = true;

            playScore = CalculatingScore();

            SetupScore();

            Sandbox_ScenarioUIManager.instance.CompleteScenario();
            Kalbe.SoundEffectManager.Play("game finish");
        }

        public void TimeCount()
        {
            if (isTimePause && isInit) return;
            playTime += Time.deltaTime;

            if (useTimer)
            {
                timer -= Time.deltaTime;
                float second = playTime % 60;
                float minute = (int) playTime / 60;
                Sandbox_ScenarioUIManager.instance.SetTimerText(Mathf.FloorToInt(minute) +" : "+Mathf.FloorToInt(second));
                if (timer <= 0)
                    CompleteScenario();
            }
        }

        public void ScreenFade(float alpha, float time)
        {
            var finder = FindObjectOfType<HVRGlobalFadeFinder>();
            if (finder)
            {
                var ScreenFader = finder.gameObject.GetComponent<HVRCanvasFade>();
                if (ScreenFader)
                    ScreenFader.CanvasGroup.DOFade(alpha, time);
            }
        }

        public void CheckSectionCorrect(Sandbox_Section section)
        {
            foreach (var _section in sectionList)
            {
                if (_section != section && !_section.isCompleted)
                    break;

                if (_section == section)
                {
                    section.isCorrect = true;
                    break;
                }
            }
        }
        float CalculatingScore(){
            int allStep=0;
            int rightStep=0;
            foreach (var section in Sandbox_ScenarioManager.instance.sectionList)
            {
                foreach (var step in section.stepList)
                {
                    allStep++;
                    if (step.stepCountTarget <= step.stepCountCurrent) rightStep++;
                }
            }
            float resultPercent = (float) rightStep/allStep*100;
            if(Sandbox_ScenarioManager.instance.quizList.Count > 0){
                float rightQuiz = 0;
                foreach (var quiz in Sandbox_ScenarioManager.instance.quizList)
                {
                    rightQuiz += (float) quiz.value/quiz.total;
                }
                float quizPercent = (float) rightQuiz/Sandbox_ScenarioManager.instance.quizList.Count*100;
                resultPercent = (resultPercent+quizPercent)/2;
            }
            return resultPercent;
        }
        private void SetupScore()
        {
            if(EuRuntimeManager.Instance.IsLoggedIn()){
                EuRuntimeManager.Instance.PostScore(prefabName);
            }
            // if (EuRuntimeManager.Instance.IsLoggedIn())
            // {
            //     _scores.Add(new ScoreValue(DateTime.Now, RuntimeManager.Instance.currentSubScenario.SubScenarioName, 100, ($"{playTime / 60:F0}:{playTime % 60:F0}").ToString(), EuRuntimeManager.Instance.Guest.name, EuRuntimeManager.Instance.Guest.email));
            //     CsvUtil.SaveObjects(_scores, Application.dataPath + "/scores.csv");
            // }
        }

        public void SetupCurrentSection()
        {
            currentSection = sectionList[0];
        }
    }

    [Serializable]
    public class ScoreValue
    {
        public DateTime dateTime;
        public string playerName;
        public string playerEmail;
        public string course;
        public float score;
        public string playTime;

        public ScoreValue()
        {

        }

        public ScoreValue(DateTime dateTime, string course, float score, string playTime, string playerName, string playerEmail)
        {
            this.dateTime = dateTime;
            this.course = course;
            this.score = score;
            this.playTime = playTime;
            this.playerName = playerName;
            this.playerEmail = playerEmail;
        }
    }
}
