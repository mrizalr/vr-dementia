using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Sirenix.OdinInspector;
using System;
using Tictech.LoadManager;
using UnityEngine.SceneManagement;
using HurricaneVR.Framework.Shared;
using DG.Tweening;
using System.Linq;
using UnityEngine.Events;
using Tictech.EnterpriseUniversity;
using HurricaneVR.Framework.Core.UI;

namespace SandboxVRSeason2.Framework
{
    public class Sandbox_ScenarioUIManager : MonoBehaviour
    {
        public static Sandbox_ScenarioUIManager instance;

        [Header("Main")]
        [FoldoutGroup("Main"), SerializeField] private Rigidbody canvasMain;
        [FoldoutGroup("Main"), SerializeField] private Transform canvasCallTargetPos;
        private float callPressButtonTime;
        private bool callPressing;
        private Coroutine callCoroutine;

        [Header("Instruction")]
        [FoldoutGroup("Instruction"), SerializeField] private Sandbox_InstructionData instructionData;
        [FoldoutGroup("Instruction"), SerializeField] private Transform instructionCanvas;
        [FoldoutGroup("Instruction"), SerializeField] private Transform instructionPanel;
        [FoldoutGroup("Instruction"), SerializeField] private TextMeshProUGUI insTitle;
        [FoldoutGroup("Instruction"), SerializeField] private Image insImage;
        [FoldoutGroup("Instruction"), SerializeField] private TextMeshProUGUI insContent;
        int instructionIndex;

        [Header("Section List")]
        [FoldoutGroup("SectionList"), SerializeField] private GameObject sectionListObject;
        [FoldoutGroup("SectionList"), SerializeField] private Transform sectionListContentParent;
        [FoldoutGroup("SectionList"), SerializeField] private GameObject sectionListPrefab; //SB_UIReference

        [Header("Section Detail")] //sd = Section Detail
        [FoldoutGroup("SectionList"), SerializeField] private GameObject sdObject;
        [FoldoutGroup("SectionList"), SerializeField] private Transform sdContentParent;
        [FoldoutGroup("SectionList"), SerializeField] private GameObject sdDescPrefab;
        [FoldoutGroup("SectionList"), SerializeField] private GameObject sdItemPrefab;
        [FoldoutGroup("SectionList"), SerializeField] private GameObject sdItemZeroPrefab;
        [FoldoutGroup("SectionList"), SerializeField] private GameObject sdBackButton;
        [FoldoutGroup("SectionList"), SerializeField] private GameObject sdLogo;
        [FoldoutGroup("SectionList"), SerializeField] private TextMeshProUGUI sdTitle;
        [FoldoutGroup("SectionList"), SerializeField] public Animator sdCompleteAnimator;
        [FoldoutGroup("SectionList"), SerializeField, ReadOnly] public Sandbox_Section selectedSection;

        [FoldoutGroup("TimedCheck")] public GameObject checkCanvas;
        [FoldoutGroup("TimedCheck")] public TextMeshProUGUI checkTitleText;
        [FoldoutGroup("TimedCheck")] public Image checkFillImage;
        [FoldoutGroup("TimedCheck")] public GameObject checkCompleteImage;
        [FoldoutGroup("TimedCheck")] public GameObject checkCompleteImageSpawn;

        [FoldoutGroup("Complete"), SerializeField] public SB_UIReference completePanel;

        [FoldoutGroup("IGM"), SerializeField] public SB_UIReference igmPanel;

        [FoldoutGroup("Others"), SerializeField] public Sprite completeIcon;
        [FoldoutGroup("Others"), SerializeField] public Sprite incompleteIcon;
        [FoldoutGroup("Others"), SerializeField] public Sprite infoOnlyIcon;
        [FoldoutGroup("Others"), SerializeField] private GameObject LoadingCanvas;
        [FoldoutGroup("Others"), SerializeField, ReadOnly] public bool isInit;
        [FoldoutGroup("Others"), SerializeField] private GameObject TimerCanvas;
        [FoldoutGroup("Others"), SerializeField] private TextMeshProUGUI TimerText;
        [FoldoutGroup("Others"), SerializeField] private GameObject TargetTimeCanvas;
        [FoldoutGroup("Others"), SerializeField] private TextMeshProUGUI TargetTimeText;

        Coroutine setUpSectionListCor;
        Coroutine setUpSectionDetailCor;

        bool referenceLoaded;
        bool sectionListInit;
        [HideInInspector] public bool sectionDetailsInit;
        private void Awake()
        {
            if (instance != null && instance != this) //  && Sandbox_ScenarioManager.instance.multiplayerMode == MultiplayerModeEnum.ONLINE
                Destroy(this.gameObject);
            else
                instance = this;
        }

        private void Start()
        {
            //if (Sandbox_ScenarioManager.instance.multiplayerMode == MultiplayerModeEnum.ONLINE)
            //    Destroy(this.gameObject);

            StartCoroutine(Initialize());

            if (GameObject.FindGameObjectsWithTag("UITargetPos").Length > 0)
                canvasCallTargetPos = GameObject.FindGameObjectsWithTag("UITargetPos")[0].transform;

            GetComponent<HVRInputModule>().RefreshCollectedCanvas();
        }

        private void Update()
        {
            CallCanvas();

            if (callPressing)
                callPressButtonTime += Time.deltaTime;

            var buttonState = HVRController.GetButtonState(HVRHandSide.Left, HVRButtons.Secondary);

            if (buttonState.JustActivated)
                callPressing = true;

            if (buttonState.JustDeactivated)
            {
                callPressing = false;
                if (callPressButtonTime <= 1f)
                {
                    if (callCoroutine != null) StopCoroutine(callCoroutine);
                    callCoroutine = StartCoroutine(CallCanvasCoroutine());
                }

                callPressButtonTime = 0;
            }

            if (callPressButtonTime > 1f)
            {
                igmPanel.gameObject.SetActive(true);
            }
        }

        private IEnumerator Initialize()
        {
            print("SB : Initiating Scenario UI Manager: " + Sandbox_ScenarioManager.instance.isInit);
            yield return new WaitUntil(() => Sandbox_ScenarioManager.instance.isInit == true);
            print("SB : Initiating Scenario UI Manager: " + Sandbox_ScenarioManager.instance.isInit);
            yield return SetUpSectionDetailCoroutine();

            // if (Sandbox_ScenarioManager.instance.mode == ScenarioModeEnum.SANDBOX)
            // {
            //     yield return SetUpSectionListCoroutine();
            // }
            // else
            // {
            // }
            OpenStepDetail(Sandbox_ScenarioManager.instance.sectionList[0]);
            if(sdBackButton!=null) sdBackButton.SetActive(false);
            sdLogo.SetActive(true);

            igmPanel.customButtonList[0].onDown.AddListener(() => { RuntimeManager.LoadScene(RuntimeManager.Instance.defaultScene); print("lobby"); });
            igmPanel.customButtonList[1].onDown.AddListener(() => { print("repeat"); RuntimeManager.LoadScenario(RuntimeManager.Instance.currentSubScenario, Sandbox_ScenarioManager.instance.mode == ScenarioModeEnum.LINEAR ? true : false); });

            print("SB : Scenario UI Manager Initiated");

            isInit = true;

            SetupUInstructionList();

            SetUpTimer();

            print("is init UIManager: " + isInit);
        }
        void SetUpTimer()
        {
            if (Sandbox_ScenarioManager.instance.timer > 0)
            {
                TimerCanvas.SetActive(true);
            }
            else
            {
                TimerCanvas.SetActive(false);
            }

            var prefabName = Sandbox_ScenarioManager.instance.prefabName;
            var scenarioData = EuRuntimeManager.Instance.subScenarios;

            if (scenarioData == null)
            {
                TargetTimeCanvas.SetActive(false);
                return;
            }

            var targetTime = scenarioData.First(e => e.name == prefabName).target_time;

            var minutes = targetTime / 60;
            var minutesString = minutes < 10 ? $"0{minutes}" : minutes.ToString();

            var seconds = targetTime % 60;
            var secondsString = seconds < 10 ? $"0{seconds}" : seconds.ToString();

            TargetTimeText.text = $"{minutesString}:{secondsString}";
        }
        public void SetTimerText(string time)
        {
            TimerText.text = time;
        }

        public void SetupUInstructionList()
        {
            if (!instructionPanel || !instructionData) return;
            instructionCanvas.gameObject.SetActive(true);
            instructionIndex = 0;
            StartCoroutine(fillInstruction(instructionIndex));
            SB_UIButton sbButton = instructionPanel.GetComponentInChildren<SB_UIButton>();

            //sectionListObject.gameObject
        }


        ScaleObjectToCanvas scaler;
        public IEnumerator fillInstruction(int index)
        {
            Debug.Log("berubah jadi " + index);
            if (instructionData.instructions.Count < index + 1)
            {
                // if (Sandbox_ScenarioManager.instance.mode == ScenarioModeEnum.SANDBOX)
                // {
                //     sectionListObject.gameObject.SetActive(true);
                //     sdObject.gameObject.SetActive(false);
                // }
                // else
                // {
                // }
                sectionListObject.gameObject.SetActive(false);
                sdObject.gameObject.SetActive(true);
                instructionCanvas.gameObject.SetActive(false);
                instructionIndex = 0;

                Sandbox_ScenarioManager.instance.StartScenario();
            }
            else
            {
                insTitle.text = instructionData.instructions[index].title;
                insImage.sprite = instructionData.instructions[index].image;
                insContent.text = instructionData.instructions[index].content;
                instructionIndex = index;

                SB_UIButton sbButton = instructionPanel.GetComponentInChildren<SB_UIButton>();
                sbButton.onDown = new UnityEvent();
                sbButton.onDown.AddListener(() => StartCoroutine(fillInstruction(instructionIndex + 1)));

                LayoutRebuilder.ForceRebuildLayoutImmediate(instructionPanel.GetComponent<RectTransform>());
                yield return new WaitForEndOfFrame();
                sdObject.gameObject.SetActive(false);
                sectionListObject.gameObject.SetActive(true);  //false
                if (!scaler)
                    if (instructionCanvas.GetComponentInChildren<ScaleObjectToCanvas>())
                        scaler = instructionCanvas.GetComponentInChildren<ScaleObjectToCanvas>();
                    else scaler = null;
                if (scaler)
                    scaler.scaleToCanvas();

                yield return new WaitForEndOfFrame();
                if (instructionData.instructions.Count <= index + 1)
                    sbButton.GetComponentInChildren<TextMeshProUGUI>().text = "Tutup";
                instructionPanel.transform.localScale = Vector3.zero;
                instructionPanel.transform.DOScale(Vector3.one, .25f);

                yield return new WaitForSeconds(.30f);
            }
        }

        public void SetUpSectionList()
        {
            if (setUpSectionListCor != null) StopCoroutine(setUpSectionListCor);
            setUpSectionListCor = StartCoroutine(SetUpSectionListCoroutine());
        }

        public IEnumerator SetUpSectionListCoroutine()
        {
            if (sectionListInit) yield break;
            sectionListInit = true;
            LoadingCanvas.SetActive(true);

            foreach (Transform child in sectionListContentParent)
                Destroy(child.gameObject);

            foreach (var section in Sandbox_ScenarioManager.instance.sectionList)
            {
                SB_UIReference _section = Instantiate(sectionListPrefab, sectionListContentParent).GetComponent<SB_UIReference>();

                yield return new WaitUntil(() => _section != null);
                section.uiReference = _section;
                UpdateSectionList(_section, section);
            }

            if (instructionCanvas)
                if (instructionCanvas.gameObject.activeSelf)
                {
                    instructionCanvas.gameObject.SetActive(true);
                    sectionListObject.SetActive(false);
                }
                else
                {
                    instructionCanvas.gameObject.SetActive(false);
                    sectionListObject.SetActive(true);
                }

            sdObject.SetActive(false);
            LoadingCanvas.SetActive(false);
        }

        public void UpdateSectionList(SB_UIReference _target, Sandbox_Section _section)
        {
            if (_target == null) return;
            print("Meraung " + _target);
            var _sectionNameText = _target.tmProList[0];
            _sectionNameText.text = _section.sectionName;

            var _sectionOpenButton = _target.customButtonList[0];
            _sectionOpenButton.onDown.RemoveAllListeners();
            _sectionOpenButton.onDown.AddListener(() =>
            {
                OpenStepDetail(_section);
            });


            var _notEmptyStep = _section.stepList.Find(p => p.stepCountTarget != 0);

            var _uncompleteStep = _section.stepList.Find(p => p.stepCountCurrent < p.stepCountTarget);

            var _sectionCompleteIcon = _target.imageList[0];

            if (_notEmptyStep == null) _sectionCompleteIcon.sprite = infoOnlyIcon;
            else if (_uncompleteStep == null) _sectionCompleteIcon.sprite = completeIcon;
        }

        public void SetUpSectionDetail(Sandbox_Section _section)
        {
            if (setUpSectionDetailCor != null) StopCoroutine(setUpSectionDetailCor);
            setUpSectionDetailCor = StartCoroutine(SetUpSectionDetailCoroutine());
        }

        public IEnumerator SetUpSectionDetailCoroutine()
        {
            print("SB : Scenario UI Manager setup");
            LoadingCanvas.SetActive(true);

            foreach (Transform child in sdContentParent)
                Destroy(child.gameObject);
            foreach (var section in Sandbox_ScenarioManager.instance.sectionList)
            {
                if (section.sectionDesc != "")
                {
                    SB_UIReference _desc = Instantiate(sdDescPrefab, sdContentParent).GetComponent<SB_UIReference>();

                    yield return new WaitUntil(() => _desc != null);
                    section.descUIReference = _desc;
                    _desc.gameObject.SetActive(false);

                }
                foreach (var step in section.stepList)
                {
                    if (step.stepMode != ScenarioSectionStep.Mode.BOTH && step.stepMode.ToString() != Sandbox_ScenarioManager.instance.mode.ToString())
                    {
                        break;
                    }
                    SB_UIReference _SectionItem = null;
                    if (step.stepCountTarget <= 0)
                        // No counter
                        _SectionItem = Instantiate(sdItemZeroPrefab, sdContentParent).GetComponent<SB_UIReference>();
                    else
                        // With counter
                        _SectionItem = Instantiate(sdItemPrefab, sdContentParent).GetComponent<SB_UIReference>();
                    yield return new WaitUntil(() => _SectionItem != null);
                    section.stepUIReference.Add(_SectionItem);
                    _SectionItem.gameObject.SetActive(false);
                }
                section.UpdateUI();
            }
            sectionDetailsInit = true;

            print("SB : Scenario UI Manager setup done");
            LoadingCanvas.SetActive(false);
        }

        /// <summary>
        /// Open step detail of section
        /// </summary>
        /// <param name="_section">Step detail on choosen section</param>
        public void OpenStepDetail(Sandbox_Section _section)
        {
            if (sectionListObject.activeInHierarchy)
                sectionListObject.SetActive(false);

            sdObject.SetActive(true);

            if (selectedSection)
            {
                if (selectedSection.descUIReference)
                    selectedSection.descUIReference.gameObject.SetActive(false);
                foreach (var refs in selectedSection.stepUIReference)
                {
                    refs.gameObject.SetActive(false);
                }
            }

            selectedSection = _section;
            sdTitle.text = _section.sectionName;

            if (selectedSection.descUIReference)
                selectedSection.descUIReference.gameObject.SetActive(true);
            foreach (var refs in selectedSection.stepUIReference)
            {
                refs.gameObject.SetActive(true);
            }

            if (instructionCanvas)
            {
                if (instructionCanvas.gameObject.activeSelf)
                {
                    instructionCanvas.gameObject.SetActive(true);
                    sdObject.SetActive(false);
                }
                else
                {
                    instructionCanvas.gameObject.SetActive(false);
                    sdObject.SetActive(true);
                }
            }
            LoadingCanvas.SetActive(false);
            //sectionListObject.SetActive(false);

        }

        /// <summary>
        /// Call this canvas on player
        /// </summary>
        void CallCanvas()
        {
            if (callPressing)
                callPressButtonTime += Time.deltaTime;

            var buttonState = HVRController.GetButtonState(HVRHandSide.Left, HVRButtons.Secondary);

            if (buttonState.JustActivated)
            {
                callPressing = true;
            }

            if (buttonState.JustDeactivated)
            {
                callPressing = false;
                if (callPressButtonTime <= 1f)
                {
                    if (callCoroutine != null) StopCoroutine(callCoroutine);
                    callCoroutine = StartCoroutine(CallCanvasCoroutine());
                }

                callPressButtonTime = 0;
            }

            if (callPressButtonTime > 1f)
            {
                igmPanel.gameObject.SetActive(true);
            }
        }

        /// <summary>
        /// Move canvas to player until it reached the desired position
        /// </summary>
        /// <returns></returns>
        private IEnumerator CallCanvasCoroutine()
        {
            if (canvasMain)
            {
                Debug.Log("main - " + Camera.main);
                canvasMain.detectCollisions = false;
                Sequence seq = DOTween.Sequence();
                seq.Append(canvasMain.transform.DOMove(canvasCallTargetPos.position, 0.4f));
                yield return new WaitForSeconds(0.4f);
                seq.Insert(0, canvasMain.transform.DOLookAt(Camera.main.transform.position, 0.4f, AxisConstraint.Y));
                yield return new WaitForSeconds(0.4f);
                canvasMain.detectCollisions = true;
            }
        }

        /// <summary>
        /// Finish the whole scenario
        /// </summary>
        public void CompleteScenario()
        {
            var span = TimeSpan.FromSeconds((double)(new decimal(Sandbox_ScenarioManager.instance.playTime)));
            completePanel.tmProList[0].text = "Selamat, Anda telah berhasil menyelesaikan subskenario " + Sandbox_ScenarioManager.instance.Name;
            completePanel.tmProList[2].text = span.Minutes + ":" + span.Seconds;
            // Sandbox_ScenarioManager.instance.sectionList[0].stepList[0].stepCountCurrent
            int allStep = 0;
            int rightStep = 0;
            foreach (var section in Sandbox_ScenarioManager.instance.sectionList)
            {
                foreach (var step in section.stepList)
                {
                    if (step.stepMode != ScenarioSectionStep.Mode.BOTH && step.stepMode.ToString() != Sandbox_ScenarioManager.instance.mode.ToString())
                    {
                        break;
                    }
                    allStep++;
                    GameObject go = Instantiate(completePanel.gameObjectList[1], completePanel.gameObjectList[0].transform);
                    go.GetComponent<UIItemSummary>().isRight = (step.stepCountTarget <= step.stepCountCurrent) ? true : false;
                    go.GetComponent<UIItemSummary>().step = step.stepName;
                    if (step.stepCountTarget <= step.stepCountCurrent) rightStep++;
                }
            }
            float resultPercent = (float)rightStep / allStep * 100;
            if (Sandbox_ScenarioManager.instance.quizList.Count > 0)
            {
                completePanel.gameObjectList[4].SetActive(true);
                float rightQuiz = 0;
                foreach (var quiz in Sandbox_ScenarioManager.instance.quizList)
                {
                    GameObject go = Instantiate(completePanel.gameObjectList[3], completePanel.gameObjectList[2].transform);
                    go.GetComponent<UIItemSummary>().step = quiz.name;
                    go.GetComponent<UIItemSummary>().isRight = true;
                    go.GetComponent<UIItemSummary>().quizResult = quiz.value + "/" + quiz.total;
                    rightQuiz += (float)quiz.value / quiz.total;
                }
                float quizPercent = (float)rightQuiz / Sandbox_ScenarioManager.instance.quizList.Count * 100;
                resultPercent = (resultPercent + quizPercent) / 2;
            }
            else
            {
                completePanel.gameObjectList[4].SetActive(false);
            }
            completePanel.tmProList[1].text = Mathf.Round(resultPercent) + "%";
            completePanel.customButtonList[0].onDown.AddListener(() => { RuntimeManager.LoadScene(RuntimeManager.Instance.defaultScene); });
            completePanel.customButtonList[1].onDown.AddListener(() => { print("reload"); RuntimeManager.LoadScenario(RuntimeManager.Instance.currentSubScenario, Sandbox_ScenarioManager.instance.mode == ScenarioModeEnum.LINEAR ? true : false, MainMenuManager.activeTimer); });

            completePanel.gameObject.SetActive(true);
        }

        public void ReturnToLobby()
        {
            RuntimeManager.LoadScene(RuntimeManager.Instance.defaultScene);
        }

        public void RetrySection()
        {
            print("reload");
            RuntimeManager.LoadScenario(RuntimeManager.Instance.currentSubScenario, Sandbox_ScenarioManager.instance.mode == ScenarioModeEnum.LINEAR ? true : false, MainMenuManager.activeTimer);
        }
    }
}
