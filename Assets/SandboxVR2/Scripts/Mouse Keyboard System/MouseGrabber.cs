using DG.Tweening;
using HurricaneVR.Framework.Components;
using HurricaneVR.Framework.ControllerInput;
using HurricaneVR.Framework.Core;
using HurricaneVR.Framework.Core.Grabbers;
using HurricaneVR.Framework.Core.Player;
using HurricaneVR.Framework.Core.UI;
using HurricaneVR.Framework.Shared;
using HurricaneVR.Samples;
using SandboxVRSeason2.Framework;
using SandboxVRSeason2.Framework.Multiplayer;
using Sirenix.OdinInspector;
using Sirenix.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class MouseGrabber : MonoBehaviour
{
    public Camera mainCamera;
    public HVRHandGrabber leftHandGrabber, rightHandGrabber;
    public Vector3 mousePos;
    public Quaternion cameraRotation;
    public StereoTargetEyeMask targetEye;
    StereoTargetEyeMask defaultTargetEye;

    HVRPlayerInputs input;
    HVRPlayerController playerController;
    HVRCameraRig cameraRig;
    [ReadOnly] public CursorLockMode lockMode = CursorLockMode.None;
    public float scrollSensitivity = .1f;

    public float speed = 5.0f;
    public float jumpSpeed = 10.0f;
    public float gravity = 0;
    float lookSpeed = 2f, rotationX, lookXLimit = 75.0f, deltaTime;
    private Vector3 moveDirection = Vector3.zero;
    private CharacterController characterController;
    HVRButtonState buttonState;
    Transform leftTrack, rightTrack;
    CollisionObjects leftCollision, rightCollision;
    public Sandbox_RadioManager radio;
    public List<Calltransform> Summons/* = new List<Calltransform>() { new Calltransform(KeyCode.Tab, null) }*/;
    public bool UseFixedUpdateTime = true;
    Transform t;
    bool prepared = false;
    bool useEvents = false;
    public UnityEvent OnActive, OnInactive;

    [System.Serializable]
    public class Calltransform
    {
        public KeyCode key;
        public GameObject @object;
        public Calltransform(KeyCode _key, GameObject _transform)
        {
            key = _key;
            @object = _transform;
        }
    }

    private void OnEnable()
    {
        prepare(true);
    }

    private void OnDisable()
    {
        prepare(false);
    }

    public T fill<T>(T target)
    {
        if (target == null)
            target = GetComponent<T>() != null ? GetComponent<T>() : UnityEngine.SceneManagement.SceneManager.GetActiveScene().GetRootGameObjects().Where(x => x.GetComponentInChildren<T>() != null).FirstOrDefault().GetComponentInChildren<T>();
        return target;
    }

    void prepare(bool b)
    {
        if (!mainCamera)
            mainCamera = Camera.main;

        if (!input)
            input = GetComponent<HVRPlayerInputs>() ? GetComponent<HVRPlayerInputs>() : FindObjectOfType<HVRPlayerInputs>();

        if (!playerController)
            playerController = GetComponent<HVRPlayerController>() ? GetComponent<HVRPlayerController>() : FindObjectOfType<HVRPlayerController>();

        if (!characterController)
            characterController = GetComponent<CharacterController>() ? GetComponent<CharacterController>() : FindObjectOfType<CharacterController>();

        if (!cameraRig)
            cameraRig = GetComponent<HVRCameraRig>() ? GetComponent<HVRCameraRig>() : FindObjectOfType<HVRCameraRig>();

        if (defaultTargetEye == StereoTargetEyeMask.None)
            defaultTargetEye = mainCamera.stereoTargetEye;

        /*input = fill<HVRPlayerInputs>(input);
        playerController = fill<HVRPlayerController>(playerController);
        characterController = fill<CharacterController>(characterController);
        cameraRig = fill<HVRCameraRig>(cameraRig);*/

        if (playerController)
            if (!leftHandGrabber || !rightHandGrabber)
            {
                leftHandGrabber = playerController.LeftHand;
                rightHandGrabber = playerController.RightHand;
            }
        if (Summons.Count > 0)
        {
            if (Sandbox_ScenarioUIManager.instance)
            {
                if (Summons[0].key == KeyCode.Tab && Summons[0].@object == null && Sandbox_ScenarioUIManager.instance)
                    if (Sandbox_ScenarioUIManager.instance.transform.Find("Scenario Canvas"))
                        Summons[0].@object = Sandbox_ScenarioUIManager.instance.transform.Find("Scenario Canvas").gameObject;
                    else Summons[0].@object = new GameObject("Fake");
            }
        }

        if (lockMode == CursorLockMode.None && enabled) return;

        playerController.MovementEnabled = !b;

        t = leftHandGrabber ? leftHandGrabber.TrackedController.transform.parent : leftHandGrabber.TrackedController.transform.parent;
        if (b)
        {
            Application.targetFrameRate = 90;
            mainCamera.stereoTargetEye = targetEye;
            mainCamera.transform.localPosition = Vector3.zero;
            //t.localPosition = new Vector3(0, 1.3f, 0);
            playerController.Gravity = gravity;
            playerController.MaxFallSpeed = 5 * playerController.Gravity;
            playerController.JumpVelocity = 0;
            playerController.DoubleClickThreshold = 0;
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            leftHandGrabber.GetComponentInChildren<HVRUIPointer>(true).enabled = false;
            rightHandGrabber.GetComponentInChildren<HVRUIPointer>(true).enabled = false;

            if(useEvents) OnActive.Invoke();
        }
        else
        {
            Application.targetFrameRate = 144;
            mainCamera.stereoTargetEye = defaultTargetEye;
            mainCamera.transform.localPosition = new Vector3(0, 1.5f, 0);
            //t.localPosition = Vector3.zero;
            playerController.Gravity = 0.98f;
            playerController.MaxFallSpeed = 5 * playerController.Gravity;
            playerController.JumpVelocity = 2;
            playerController.DoubleClickThreshold = 0.25f;
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            leftHandGrabber.GetComponentInChildren<HVRUIPointer>(true).enabled = true;
            rightHandGrabber.GetComponentInChildren<HVRUIPointer>(true).enabled = true;

            if (useEvents) OnInactive.Invoke();
        }
        Camera.main.stereoTargetEye = mainCamera.stereoTargetEye;
        Camera.main.transform.localPosition = mainCamera.transform.localPosition;

        leftTrack = leftHandGrabber.TrackedController;
        rightTrack = rightHandGrabber.TrackedController;
        prepared = true;
    }

    private void Awake()
    {
        defaultTargetEye = mainCamera.stereoTargetEye;
    }

    private void Start()
    {
        prepare(true);
    }

    // Update is called once per frame
    void Update()
    {
        deltaTime = Time.fixedDeltaTime;
        if (!UseFixedUpdateTime) deltaTime = Time.deltaTime;
        UpdateLockMode();
        if (!enabled || lockMode == CursorLockMode.None) return;

        if (!prepared)
            prepare(true);
        ControlUpdate("Jump");

        HandDistance(leftHandGrabber);
        HandDistance(rightHandGrabber);

        leftCollision = UpdateCollision(leftHandGrabber);
        rightCollision = UpdateCollision(rightHandGrabber);

        RadioSummon("left ctrl");

        if (!enabled || lockMode == CursorLockMode.None || UseFixedUpdateTime) return;
        UpdateTaskActivity("q", "e");
    }

    private void FixedUpdate()
    {
        if (!enabled || lockMode == CursorLockMode.None || !UseFixedUpdateTime) return;
        UpdateTaskActivity("q", "e");
    }

    private void LateUpdate()
    {
        if (!enabled || lockMode == CursorLockMode.None) return;
    }

    void UpdateLockMode()
    {
        if (Input.GetKeyDown("`") || Input.GetKeyDown("left alt"))
        {
            if (Cursor.lockState == CursorLockMode.None)
            {
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
            }
            else
            {
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
            }

            lockMode = Cursor.lockState;
        }
        else if (Cursor.lockState != lockMode)
            if (Cursor.visible == false) Cursor.lockState = CursorLockMode.Locked;
            else Cursor.lockState = CursorLockMode.None;
    }

    void RadioSummon(string key)
    {
        if (Input.GetKeyDown(key))
        {
            if (!radio)
                radio = FindObjectOfType<Sandbox_RadioManager>();

            if (radio)
                if (Vector3.Distance(radio.getRadioItem().transform.position, rightHandGrabber.transform.position) > .5f)
                {
                    radio.getRadioSocket().transform.DOMove(rightHandGrabber.transform.position, 1);
                    radio.getRadioItem().transform.DOMove(rightHandGrabber.transform.position, 1);
                }
                else
                {
                    if (FindObjectsOfType<HVRShoulderSocket>().Where(x => x.transform.localPosition.x < 0).ToList().Count > 0)
                    {
                        HVRShoulderSocket leftShoulder = FindObjectsOfType<HVRShoulderSocket>().Where(x => x.transform.localPosition.x < 0).FirstOrDefault();
                        radio.getRadioSocket().transform.DOMove(leftShoulder.transform.position, 1);
                        radio.getRadioItem().transform.DOMove(leftShoulder.transform.position, 1);
                    }
                }
        }
    }


    IEnumerator<float> CallSummons()
    {
        foreach (var item in Summons)
        {
            if (Input.GetKeyDown(item.key))
            {
                if (Vector3.Distance(item.@object.transform.position, rightHandGrabber.transform.position) > .5f)
                {
                    item.@object.transform.rotation = mainCamera.transform.rotation;
                    item.@object.transform.DOMove(mainCamera.transform.position + (transform.forward * 1.2f), .5f);
                    yield return .5f;
                }
            }
        }
        yield return 0f;
    }

    //Update control input
    void ControlUpdate(string jumpKey)
    {
        moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        moveDirection = transform.TransformDirection(moveDirection);
        moveDirection *= speed;

        moveDirection.y -= gravity * Time.deltaTime;
        if (Input.GetButtonDown(jumpKey))
        {
            cameraRig.transform.localEulerAngles = Vector3.zero;
            //characterController.transform.position += (Vector3.up * jumpSpeed);
            moveDirection.y = jumpSpeed * .01f; 
            characterController.Move(moveDirection);
        }
        else if (Input.GetButton(jumpKey))
        {
            moveDirection.y = jumpSpeed * 2f * -Time.deltaTime;
            characterController.Move(moveDirection * Time.deltaTime);
        }
        else characterController.Move(moveDirection * Time.deltaTime);

        StartCoroutine(CallSummons());

        /*rotationX += -Input.GetAxis("Mouse Y") * lookSpeed;
        rotationX = Mathf.Clamp(rotationX, -lookXLimit, lookXLimit);
        mainCamera.transform.localRotation = Quaternion.Euler(rotationX, 0, 0);
        transform.rotation *= Quaternion.Euler(0, Input.GetAxis("Mouse X") * lookSpeed, 0);*/
        transform.rotation *= Quaternion.Euler(Mathf.Clamp(-Input.GetAxis("Mouse Y") * lookSpeed, -lookXLimit, lookXLimit), Input.GetAxis("Mouse X") * lookSpeed, 0);
        //transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, 0);
    }

    Coroutine leftTurnCoroutine, rightTurnCoroutine;
    private void UpdateTaskActivity(string leftKey, string rightKey)
    {

        if (Input.GetKey(leftKey))
        {
            turn(leftCollision, -30);
            turn(rightCollision, -30);
        }

        if (Input.GetKey(rightKey))
        {
            turn(leftCollision, 30);
            turn(rightCollision, 30);
        }
        
        if (!Input.GetKey(rightKey) && !Input.GetKey(leftKey) && !input.IsLeftGripHoldActive && !input.IsRightGripHoldActive)
        {
            t = leftHandGrabber ? leftHandGrabber.TrackedController.transform.parent : leftHandGrabber.TrackedController.transform.parent;
            t.transform.localEulerAngles = Vector3.zero;
        }
    }

    void turn(CollisionObjects collided, float rotaryValue)
    {
        //Debug.Log("turning " + collided.name);
        if (collided && collided != null)
        {
            if (collided.triggeredObject.Count > 0)
                foreach (var item in collided.triggeredObject.Where(x => x.GetComponent<SandBox_RotaryHandler>()))
                {
                    if (item.GetComponent<SandBox_RotaryHandler>())
                    {
                        item.GetComponent<SandBox_RotaryHandler>().addValue(rotaryValue * deltaTime);
                    }
                }
            if (collided.GetComponent<HVRDial>())
            {
                collided.transform.localEulerAngles += new Vector3(0, rotaryValue * deltaTime, 0);
                HVRDial dialComponent = collided.GetComponent<HVRDial>();
                Vector3 angles = collided.transform.localEulerAngles;
                
                if (collided.GetComponentInParent<Sandbox_DialController>())
                {
                    Sandbox_DialController dialControl = collided.GetComponentInParent<Sandbox_DialController>();
                    if (dialControl.GetIsClamp())
                        if (collided.transform.localEulerAngles.y < (360f + dialControl.GetMaxRotation()) * .5f)
                            collided.transform.localEulerAngles = new Vector3(angles.x, Mathf.Clamp(angles.y, -1, dialControl.GetMaxRotation()), angles.z);
                        else collided.transform.localEulerAngles = new Vector3(angles.x, 0, angles.z);
                    dialComponent.RotationTarget.localEulerAngles = new Vector3(dialComponent.RotationTarget.localEulerAngles.x, collided.transform.localEulerAngles.y, dialComponent.RotationTarget.localEulerAngles.z);
                }

                collided.GetComponent<HVRDial>().DialTurned.Invoke(collided.transform.localEulerAngles.y, 0, 0);
                //Debug.Log("dial rotary " + collided.name);
            }
            else if (collided.GetComponentInParent<HVRPhysicsDial>())
            {
                if (leftHandGrabber || rightHandGrabber)
                {
                    t = leftHandGrabber ? leftHandGrabber.TrackedController.transform.parent : leftHandGrabber.TrackedController.transform.parent;
                    t.transform.localEulerAngles += new Vector3(0, 0, rotaryValue * deltaTime);
                }
            }
        }
    }

    //Update hand distance
    void HandDistance(HVRHandGrabber grabber)
    {
        if (grabber)
        {
            Transform trackedPos = grabber.HandSide == HVRHandSide.Left ? leftTrack : rightTrack;
            Transform local = grabber.TrackedController.transform;
            Vector3 localBase = trackedPos.transform.localPosition;
            local.localPosition = new Vector3(localBase.x, localBase.y, Mathf.Clamp(local.localPosition.z + (Input.mouseScrollDelta.y * scrollSensitivity), 0, 5));
        }
    }

    //Update on grabed grabbable's collider with object list return
    CollisionObjects UpdateCollision(HVRHandGrabber grabber)
    {
        if (grabber)
        {
            /*if (grabber.HandSide == HVRHandSide.Left)
                return leftCollision;
            else return rightCollision;*/
            if (grabber.IsGrabbing)
                if (!grabber.GrabbedTarget.GetComponent<CollisionObjects>())
                {
                    grabber.GrabbedTarget.gameObject.AddComponent<CollisionObjects>();
                    return grabber.GrabbedTarget.gameObject.GetComponent<CollisionObjects>();
                }
                else return grabber.GrabbedTarget.gameObject.GetComponent<CollisionObjects>();
            else return null;
        }
        else return null;
    }

}