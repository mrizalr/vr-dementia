using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionObjects : MonoBehaviour
{
    [ReadOnly]
    public List<GameObject> triggeredObject;
    // Start is called before the first frame update
    void Start()
    {
        triggeredObject = new List<GameObject>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other)
            triggeredObject.Add(other.gameObject);
    }

    private void OnTriggerExit(Collider other)
    {
        if (other)
            if (triggeredObject.Contains(other.gameObject))
                triggeredObject.Remove(other.gameObject);
    }
}
