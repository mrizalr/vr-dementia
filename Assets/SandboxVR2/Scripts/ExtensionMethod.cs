using SandboxVRSeason2.Framework;
using SandboxVRSeason2.Framework.Multiplayer;
using System;
using System.Collections;
using System.Collections.Generic;
using Tictech.LoadManager;
using UnityEngine;

public static class ExtensionMethod
{
    public static Sandbox_ObjectID SearchObject(int id)
    {
        Sandbox_ObjectID[] objects = GameObject.FindObjectsOfType<Sandbox_ObjectID>();
        for (int i = 0; i < objects.Length; i++)
        {
            if (objects[i].id == id)
            {
                return objects[i];
            }
        }
        return null;
    }

    public static Sandbox_ObjectID SearchObjectViaManager(int id)
    {
        return Sandbox_ObjectIDManager.I.GetObjectID(id);
    }

    public static float GetAnimationLength(this Animator _animator, string _animationName)
    {
        _animator.Play(_animationName);
        List<AnimationClip> clipList = new List<AnimationClip>(_animator.runtimeAnimatorController.animationClips);

        var _animClip = clipList.Find(x => x.name == _animationName);
        return _animClip.length;

    }
}
