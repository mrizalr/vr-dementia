using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SandboxVRSeason2.Framework
{
    public class ParticlesController : MonoBehaviour
    {
        public Color paintColor;

        public float minRadius = 0.05f;
        public float maxRadius = 0.2f;
        public float strength = 1;
        public float hardness = 1;

        public float Strength
        {
            set
            {
                strength = value;
            }
        }
        [Space]
        ParticleSystem part;
        List<ParticleCollisionEvent> collisionEvents;

        void Start()
        {
            part = GetComponent<ParticleSystem>();
            collisionEvents = new List<ParticleCollisionEvent>();
        }

        void InvokeCleaningTrigger(GameObject other)
        {
            int numCollisionEvents = part.GetCollisionEvents(other, collisionEvents);

            Paintable p = other.GetComponent<Paintable>();
            if (p != null)
            {
                for (int i = 0; i < numCollisionEvents; i++)
                {
                    Vector3 pos = collisionEvents[i].intersection;
                    float radius = Random.Range(minRadius, maxRadius);
                    PaintManager.instance.paint(p, pos, radius, hardness, strength, paintColor);
                }
            }

            if (other.GetComponent<CleaningTriggerController>())
            {
                other.GetComponent<CleaningTriggerController>().Trigger();
            }
        }

        void OnParticleCollision(GameObject other)
        {
            InvokeCleaningTrigger(other);
        }

        private void OnParticleTrigger(GameObject other)
        {
            InvokeCleaningTrigger(other);
        }
    }
}