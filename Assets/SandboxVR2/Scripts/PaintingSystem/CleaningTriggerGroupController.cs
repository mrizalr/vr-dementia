using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Photon.Pun;
using Sirenix.OdinInspector;
using SandboxVRSeason2.Framework.Multiplayer;

namespace SandboxVRSeason2.Framework
{
    public class CleaningTriggerGroupController : Sandbox_Object
    {
        public List<CleaningTriggerController> triggers;
        public UnityEvent onAnyTriggered;
        public UnityEvent onAllTriggered;

        [FoldoutGroup("Debug"), ReadOnly, SerializeField] private bool _linearLock;
        [FoldoutGroup("Debug"), ReadOnly, SerializeField] private bool _init;

        Sandbox_BroadcastMethods broadcast;

        void Start()
        {
            broadcast = GetComponent<Sandbox_BroadcastMethods>();

            if (triggers.Count == 0)
            {
                foreach (Transform child in transform)
                {
                    triggers.Add(child.GetComponent<CleaningTriggerController>());
                }
            }
            _init = true;
        }

        public void CheckAllTriggers()
        {
            int count = 0;
            for (int i = 0; i < triggers.Count; i++)
            {
                if (triggers[i].isTriggered)
                {
                    count++;
                }
            }

            if (count > 0)
            {
                if (Sandbox_ScenarioManager.instance != null)
                {
                    if (Sandbox_ScenarioManager.instance.multiplayerMode == MultiplayerModeEnum.ONLINE)
                    {
                        PhotonView photonView = GetComponent<PhotonView>();
                        photonView.RPC(nameof(broadcast.BroadcastAnyTriggered), RpcTarget.OthersBuffered, id);
                    }
                }
                onAnyTriggered?.Invoke();
                //Debug.Log("ONE of the paint triggers is triggered");
            }
            if (count == triggers.Count)
            {
                onAllTriggered?.Invoke();
                if (Sandbox_ScenarioManager.instance.multiplayerMode == MultiplayerModeEnum.ONLINE)
                {
                    PhotonView photonView = GetComponent<PhotonView>();
                    photonView.RPC(nameof(broadcast.BroadcastAllTriggered), RpcTarget.OthersBuffered, id);
                }
                //Debug.Log("ALL of the paint triggers is triggered");
            }
        }

        public override bool linearLock
        {
            get { return _linearLock; }
            set
            {
                if (!_linearException && Sandbox_ScenarioManager.instance.mode == ScenarioModeEnum.LINEAR)
                {
                    _linearLock = value;
                    if(gameObject.activeInHierarchy) StartCoroutine(SetLock(value)); 
                    foreach (var outline in outlineList) outline.enabled = !value;
                }
            }
        }
        public void ResetTrigger(){
            foreach (var item in triggers)
            {
                item.gameObject.SetActive(true);
                item.isTriggered = false;
            }
            //GetComponentInParent<Paintable>().ResetTexture();
        }
        IEnumerator SetLock(bool _value)
        {
            yield return new WaitUntil(() => _init);
            SetLockOnTrigger(_value);
        }

        void SetLockOnTrigger(bool _value)
        {
            foreach (var _trigger in triggers)
            {
                _trigger.linearLock = _value;
            }
        }
    }
}
