using System.IO;
using UnityEngine;
using Sirenix.OdinInspector;
using Photon.Pun;
using SandboxVRSeason2.Framework.Multiplayer;

namespace SandboxVRSeason2.Framework
{
    public class Paintable : Sandbox_Object
    {
        public string painterID;

        [FoldoutGroup("Debug"), ReadOnly, SerializeField] private bool _linearLock;
        public override bool linearLock
        {
            get { return _linearLock; }
            set
            {
                if (!_linearException && Sandbox_ScenarioManager.instance.mode == ScenarioModeEnum.LINEAR)
                {
                    _linearLock = value;
                    if (GetComponentInChildren<CleaningTriggerGroupController>()) GetComponentInChildren<CleaningTriggerGroupController>().linearLock = value;
                    foreach (var outline in outlineList) outline.enabled = !value;
                }
            }
        }

        const int TEXTURE_SIZE = 1024;

        public float extendsIslandOffset = 1;

        RenderTexture[] extendIslandsRenderTexture;
        RenderTexture[] uvIslandsRenderTexture;
        RenderTexture[] maskRenderTexture;
        RenderTexture[] supportTexture;

        Renderer rend;

        int maskTextureID = Shader.PropertyToID("_MaskTexture");

        public RenderTexture[] getMask() => maskRenderTexture;
        public RenderTexture[] getUVIslands() => uvIslandsRenderTexture;
        public RenderTexture[] getExtend() => extendIslandsRenderTexture;
        public RenderTexture[] getSupport() => supportTexture;
        public Renderer getRenderer() => rend;

        Sandbox_BroadcastMethods broadcast;

        [Button]
        void SaveRenderTextureMask()
        {
            SaveTexturePNG(toTexture2D(maskRenderTexture[0]));
        }
        [Button]
        public void ResetTexture(){
            print("reset texture");

            maskRenderTexture = new RenderTexture[rend.materials.Length];
            uvIslandsRenderTexture = new RenderTexture[rend.materials.Length];
            extendIslandsRenderTexture = new RenderTexture[rend.materials.Length];
            supportTexture = new RenderTexture[rend.materials.Length];

            for (int i = 0; i < maskRenderTexture.Length; i++)
            {
                maskRenderTexture[i] = new RenderTexture(TEXTURE_SIZE, TEXTURE_SIZE, 0);
                maskRenderTexture[i].filterMode = FilterMode.Bilinear;

                extendIslandsRenderTexture[i] = new RenderTexture(TEXTURE_SIZE, TEXTURE_SIZE, 0);
                extendIslandsRenderTexture[i].filterMode = FilterMode.Bilinear;

                uvIslandsRenderTexture[i] = new RenderTexture(TEXTURE_SIZE, TEXTURE_SIZE, 0);
                uvIslandsRenderTexture[i].filterMode = FilterMode.Bilinear;

                supportTexture[i] = new RenderTexture(TEXTURE_SIZE, TEXTURE_SIZE, 0);
                supportTexture[i].filterMode = FilterMode.Bilinear;
            }

            for (int i = 0; i < rend.materials.Length; i++)
            {
                rend.materials[i].SetTexture("_MaskTexture",null);

                if (rend.materials[i].GetTexture(maskTextureID) != null)
                {
                    Graphics.Blit(rend.materials[i].GetTexture(maskTextureID), maskRenderTexture[i]);
                    Graphics.Blit(rend.materials[i].GetTexture(maskTextureID), uvIslandsRenderTexture[i]);
                    Graphics.Blit(rend.materials[i].GetTexture(maskTextureID), extendIslandsRenderTexture[i]);
                    Graphics.Blit(rend.materials[i].GetTexture(maskTextureID), supportTexture[i]);
                }
                rend.materials[i].SetTexture(maskTextureID, extendIslandsRenderTexture[i]);
            }

            PaintManager.instance.ResetTexture();
            PaintManager.instance.initTextures(this);
        }


        void Start()
        {
            rend = GetComponent<Renderer>();

            ResetTexture();

            broadcast = GetComponent<Sandbox_BroadcastMethods>();
        }

        void OnDisable()
        {
            for (int i = 0; i < maskRenderTexture.Length; i++)
            {
                maskRenderTexture[i].Release();
                uvIslandsRenderTexture[i].Release();
                extendIslandsRenderTexture[i].Release();
                supportTexture[i].Release();
            }
        }

        // Save Texture as PNG
        void SaveTexturePNG(Texture2D tex)
        {

            // Encode texture into PNG
            byte[] bytes = tex.EncodeToPNG();
            Object.Destroy(tex);

            // For testing purposes, also write to a file in the project folder
            File.WriteAllBytes(Application.dataPath + "/../SavedScreen.png", bytes);
            Debug.Log("File saved to /../SavedScreen.png");
        }
        //convert render texture to texture2d
        public Texture2D toTexture2D(RenderTexture rTex)
        {
            Texture2D tex = new Texture2D(1024, 1024, TextureFormat.RGB24, false);
            // ReadPixels looks at the active RenderTexture.
            RenderTexture.active = rTex;
            tex.ReadPixels(new Rect(0, 0, rTex.width, rTex.height), 0, 0);
            tex.Apply();
            return tex;
        }
        //compare two texture2d to check if they're identical
        public bool CompareTexture(Texture2D first, Texture2D second, int pixelTolerance)
        {
            Color[] firstPix = first.GetPixels();
            Color[] secondPix = second.GetPixels();
            int wrongPixel = 0;
            if (firstPix.Length != secondPix.Length)
            {
                Debug.Log("beda ukuran");
                return false;
            }
            for (int i = 0; i < firstPix.Length; i++)
            {
                if (firstPix[i] != secondPix[i])
                {
                    wrongPixel++;
                    if (wrongPixel > pixelTolerance)
                    {
                        Debug.Log("beda warna melebihi toleransi");
                        return false;
                    }
                    else
                    {
                        Debug.Log("beda warna masih diolerir");
                    }
                }
            }
            Debug.Log("identik");
            return true;
        }
        public void BroadcastPaint(Vector3 pos, float radius = 1f, float hardness = .5f, float strength = .5f, Color? color = null)
        {
            PhotonView photonView = GetComponent<PhotonView>();
            photonView.RequestOwnership();
            print("paint id: " + id);
            photonView.RPC(nameof(broadcast.BroadcastDoPaint), RpcTarget.OthersBuffered, id, pos, ColorUtility.ToHtmlStringRGBA(color ?? Color.red), radius, hardness, strength);
        }
    }
}
