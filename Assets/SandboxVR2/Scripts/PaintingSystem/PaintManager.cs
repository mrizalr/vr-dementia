using UnityEngine;
using UnityEngine.Rendering;
using Photon.Pun;

namespace SandboxVRSeason2.Framework
{
    public class PaintManager : Singleton<PaintManager>
    {

        public Shader texturePaint;
        public Shader extendIslands;

        int prepareUVID = Shader.PropertyToID("_PrepareUV");
        int positionID = Shader.PropertyToID("_PainterPosition");
        int hardnessID = Shader.PropertyToID("_Hardness");
        int strengthID = Shader.PropertyToID("_Strength");
        int radiusID = Shader.PropertyToID("_Radius");
        int blendOpID = Shader.PropertyToID("_BlendOp");
        int colorID = Shader.PropertyToID("_PainterColor");
        int textureID = Shader.PropertyToID("_MainTex");
        int uvOffsetID = Shader.PropertyToID("_OffsetUV");
        int uvIslandsID = Shader.PropertyToID("_UVIslands");

        public Material paintMaterial;
        public Material extendMaterial;

        CommandBuffer command;

        public override void Awake()
        {
            base.Awake();

            paintMaterial = new Material(texturePaint);
            extendMaterial = new Material(extendIslands);
            command = new CommandBuffer();
            command.name = "CommmandBuffer - " + gameObject.name;
        }
        public void ResetTexture(){
            paintMaterial = new Material(texturePaint);
            extendMaterial = new Material(extendIslands);
            command = new CommandBuffer();
            command.name = "CommmandBuffer - " + gameObject.name;
        }

        public void initTextures(Paintable paintable)
        {
            for (int i = 0; i < paintable.getMask().Length; i++)
            {
                RenderTexture mask = paintable.getMask()[i];
                RenderTexture uvIslands = paintable.getUVIslands()[i];
                RenderTexture extend = paintable.getExtend()[i];
                RenderTexture support = paintable.getSupport()[i];
                Renderer rend = paintable.getRenderer();

                command.SetRenderTarget(mask);
                command.SetRenderTarget(extend);
                command.SetRenderTarget(support);

                paintMaterial.SetFloat(prepareUVID, 1);
                command.SetRenderTarget(uvIslands);
                command.DrawRenderer(rend, paintMaterial, i);

                Graphics.ExecuteCommandBuffer(command);
                command.Clear();                
            }
        }


        public void paint(Paintable paintable, Vector3 pos, float radius = 1f, float hardness = .5f, float strength = .5f, Color? color = null)
        {
            if (Sandbox_ScenarioManager.instance != null)
            {
                if (Sandbox_ScenarioManager.instance.multiplayerMode == MultiplayerModeEnum.ONLINE)
                {
                    paintable.BroadcastPaint(pos, radius, hardness, strength, color);
                }
            }
            doPaint(paintable, pos, radius, hardness, strength, color);
        }
        public void doPaint(Paintable paintable, Vector3 pos, float radius = 1f, float hardness = .5f, float strength = .5f, Color? color = null)
        {
            for (int i = 0; i < paintable.getMask().Length; i++)
            {
                RenderTexture mask = paintable.getMask()[i];
                RenderTexture uvIslands = paintable.getUVIslands()[i];
                RenderTexture extend = paintable.getExtend()[i];
                RenderTexture support = paintable.getSupport()[i];
                Renderer rend = paintable.getRenderer();

                paintMaterial.SetFloat(prepareUVID, 0);
                paintMaterial.SetVector(positionID, pos);
                paintMaterial.SetFloat(hardnessID, hardness);
                paintMaterial.SetFloat(strengthID, strength);
                paintMaterial.SetFloat(radiusID, radius);
                paintMaterial.SetTexture(textureID, support);
                paintMaterial.SetColor(colorID, color ?? Color.red);
                extendMaterial.SetFloat(uvOffsetID, paintable.extendsIslandOffset);
                extendMaterial.SetTexture(uvIslandsID, uvIslands);

                command.SetRenderTarget(mask);
                command.DrawRenderer(rend, paintMaterial, i);

                command.SetRenderTarget(support);
                command.Blit(mask, support);

                command.SetRenderTarget(extend);
                command.Blit(mask, extend, extendMaterial);

                Graphics.ExecuteCommandBuffer(command);
                command.Clear();
            }
        }
    }
}