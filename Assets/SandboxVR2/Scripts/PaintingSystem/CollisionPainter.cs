using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SandboxVRSeason2.Framework
{
    public class CollisionPainter : MonoBehaviour
    {
        public string painterID;

        public Color paintColor;

        public enum BrushMode
        {
            Cleaning,
            Overwrite
        }

        public BrushMode currentBrushMode = BrushMode.Cleaning;

        public float lowStrength = 0.01f;
        public float highStrength = 1f;

        public float radius = 1;
        public float strength = 0.01f;
        public float hardness = 1;

        public float Radius
        {
            set
            {
                radius = value;
            }
        }

        private void OnCollisionStay(Collision other)
        {
            Paintable p = other.collider.GetComponent<Paintable>();
            if (p == null) return;
            if ((p.painterID == painterID || p.painterID == "") && p != null && !p.linearLock)
            {
                Vector3 pos = other.contacts[0].point;
                PaintManager.instance.paint(p, pos, radius, hardness, strength, paintColor);
            }
        }

        private void OnTriggerStay(Collider other)
        {
            Paintable p = other.GetComponent<Paintable>();
            //if (p == null) p = other.GetComponentInChildren<Paintable>();
            if (p == null) return;
            if ((p.painterID == painterID || p.painterID == "") && p != null && !p.linearLock)
            {
                Vector3 pos = other.ClosestPoint(transform.position);
                PaintManager.instance.paint(p, pos, radius, hardness, strength, paintColor);
            }
        }
    }
}