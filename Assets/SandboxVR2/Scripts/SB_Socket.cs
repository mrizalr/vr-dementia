using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using HurricaneVR.Framework.Core;
using HurricaneVR.Framework.Core.Sockets;
using HurricaneVR.Framework.Core.Grabbers;
using Sirenix.OdinInspector;
using UnityEngine.Rendering;
using DG.Tweening;
using Photon.Pun;
using SandboxVRSeason2.Framework.Multiplayer;

namespace SandboxVRSeason2.Framework
{
    [RequireComponent(typeof(HVRSocket), typeof(Sandbox_SocketFilter))]
    public class SB_Socket : Sandbox_Object
    {
        public List<Sandbox_SocketSlot> slotList;

        [SerializeField] public SB_Item initItem;

        [FoldoutGroup("Event")] public UnityEvent onOccupied;
        [FoldoutGroup("Event")] public UnityEvent onUnOccupied;
        [FoldoutGroup("Event")] public UnityEvent onToolLocked;
        [FoldoutGroup("Event")] public UnityEvent onToolUnlocked;

        [FoldoutGroup("Debug"), ReadOnly, SerializeField] private bool _slotLock;
        [FoldoutGroup("Debug"), ReadOnly, SerializeField] private bool _toolLock;
        [FoldoutGroup("Debug"), ReadOnly, SerializeField] private bool _linearLock;

        [FoldoutGroup("Debug"), ReadOnly, SerializeField] private HVRSocket hvrSocket;
        [FoldoutGroup("Debug"), ReadOnly, SerializeField] private Sandbox_SocketFilter socketFilter;
        [FoldoutGroup("Debug"), ReadOnly, SerializeField] public Sandbox_SocketSlot currentSlot;

        private Sandbox_BroadcastMethods _broadcast;

        private void Start()
        {
            hvrSocket = GetComponent<HVRSocket>();
            socketFilter = GetComponent<Sandbox_SocketFilter>();

            foreach (var slot in slotList)
                if (!slot.desiredPoint) slot.desiredPoint = transform;

            hvrSocket.Grabbed.AddListener(OnSocketGrabbed);
            hvrSocket.Released.AddListener(OnSocketReleased);

            SetLayer();
            RefreshHologram();

            StartCoroutine(SetInitItem());

            _broadcast = GetComponent<Sandbox_BroadcastMethods>();
            print("name of: " + nameof(_broadcast.BroadcastOccupied));
        }

        SB_Item currentItem;
        public bool connection = false;

        public void OnSocketGrabbed(HVRGrabberBase _grabber, HVRGrabbable _grabbable)
        {
            currentItem = _grabbable.GetComponent<SB_Item>();

            onOccupied.Invoke();
            currentSlot.onSlotOccupied.Invoke();

            if (_grabbable.GetComponent<SB_Item>())
                _grabbable.GetComponent<SB_Item>().onSocketed.Invoke();

            if (Sandbox_ScenarioManager.instance.multiplayerMode == MultiplayerModeEnum.ONLINE && !connection)
            {
                if (GetComponent<PhotonView>())
                {
                    PhotonView photonView = GetComponent<PhotonView>();
                    //photonView.RequestOwnership();
                    photonView.RPC(nameof(_broadcast.BroadcastOccupied), RpcTarget.OthersBuffered, id, currentItem.id);
                }
                else
                {
                    print(gameObject.name);
                }
            }
            connection = false;
        }

        public void OnSocketReleased(HVRGrabberBase _grabber, HVRGrabbable _grabbable)
        {
            if(Sandbox_ScenarioManager.instance.multiplayerMode == MultiplayerModeEnum.ONLINE && !connection){
                if(GetComponent<PhotonView>()){
                    PhotonView photonView = GetComponent<PhotonView>();
                    //photonView.RequestOwnership();
                    photonView.RPC(nameof(_broadcast.BroadcastUnoccupied), RpcTarget.OthersBuffered, id, currentItem.id);
                }
                else
                {
                    print(gameObject.name);
                }
            }
            connection = false;

            onUnOccupied.Invoke();
            currentSlot.onSlotUnoccupied.Invoke();

            if (_grabbable.GetComponent<SB_Item>())
                _grabbable.GetComponent<SB_Item>().onUnSocketed.Invoke();
        }

        IEnumerator SetInitItem()
        {
            if (!initItem) yield break;
            yield return new WaitUntil(() => initItem.isInit);
            ForceGrabItem(initItem);
        }

        public bool slotLock
        {
            get { return _slotLock; }
            set { 
                _slotLock = value;
                print("slot lock = "+value);
                if (currentItem != null)
                {
                    if (currentItem.GetComponent<PhotonTransformView>())
                    {
                        //currentItem.GetComponent<PhotonView>().RequestOwnership();
                        currentItem.GetComponent<PhotonTransformView>().enabled = !value;
                        //currentItem.GetComponent<PhotonTransformView>().m_SynchronizePosition = !value;
                        //currentItem.GetComponent<PhotonTransformView>().m_SynchronizeRotation = !value;
                        if (value)
                        {
                            print("reset zero");
                            currentItem.transform.localPosition = Vector3.zero;
                            currentItem.transform.localRotation = new Quaternion();
                        }
                    }

                    CheckLock();
                }
            }
        }
        public bool toolLock
        {
            get { return _toolLock; }
            set 
            {
                if (_toolLock != value)
                {
                    _toolLock = value;
                    CheckLock();
                    if (value){
                        onToolLocked.Invoke();
                    }
                    else{
                        onToolUnlocked.Invoke();
                    }

                    if(Sandbox_ScenarioManager.instance.multiplayerMode == MultiplayerModeEnum.ONLINE && !connection)
                    {
                        PhotonView photonView = GetComponent<PhotonView>();
                        //photonView.RequestOwnership();
                        if(value)
                        {
                            photonView.RPC(nameof(_broadcast.BroadcastToolLocked), RpcTarget.OthersBuffered, value, id);
                        }
                        else
                        {
                            photonView.RPC(nameof(_broadcast.BroadcastUnToolLocked), RpcTarget.OthersBuffered, value, id);
                        }
                    }
                    connection = false;

                    if (value) onToolLocked.Invoke();
                    else onToolUnlocked.Invoke();
                }
            }
        }

        public override bool linearLock
        {
            get { return _linearLock; }
            set
            {
                if (!_linearException)
                {
                    _linearLock = value;
                    if(hvrSocket==null)
                    {
                        hvrSocket = GetComponent<HVRSocket>();
                    }
                    hvrSocket.linearLock = value;
                    foreach (var outline in outlineList) outline.enabled = !value;
                }
            }
        }
        [Button]
        void ForceGrab(){
            StartCoroutine(ForceGrabItemCor(initItem));
        }
        public void ForceGrabItem(SB_Item _item)
        {
            StartCoroutine(ForceGrabItemCor(_item));
        }
        public IEnumerator ForceGrabItemCor(SB_Item _item)
        {
            currentItem = _item;
            yield return new WaitUntil(() => _item.isInit);
            if (!_item) yield return null;
            var _socketable = _item.socketable;
            if (_socketable == null) _socketable = _item.GetComponent<Sandbox_Socketable>();
            if (_socketable == null) yield return null;

            if (socketFilter.IsValid(_socketable))
            {
                print("valid socket");
                hvrSocket.TryGrab(_item.grabbable);
            }
            if(_item.GetComponent<PhotonTransformView>()){
                print("reset zero");
                _item.GetComponent<PhotonTransformView>().enabled = false;
                _item.transform.localPosition = Vector3.zero;
                _item.transform.localRotation = new Quaternion();
            }
        }

        void SetLayer()
        {
            foreach (var collider in GetComponentsInChildren<Collider>())
            {
                if (collider.GetComponent<SB_Socket>()) continue;
                collider.gameObject.layer = LayerMask.NameToLayer("Socket");
            }
        }

        void CheckLock()
        {
            bool locked = _slotLock || _toolLock ? true : false;

            hvrSocket.CanInteract = !locked;
        }

        #region Hologram
        private void RefreshHologram()
        {
            foreach (var slot in slotList)
            {
                if (!slot.profile) continue;
                if (slot.profile.meshList.Length <= 0) continue;

                slot.hologramObjectParent = new GameObject();
                slot.hologramObjectParent.transform.localScale = Vector3.one;
                slot.hologramObjectParent.transform.SetParent(slot.desiredPoint);
                slot.hologramObjectParent.transform.localPosition = slot.hologramDesiredPoint == null ? Vector3.zero : slot.desiredPoint.InverseTransformPoint(slot.hologramDesiredPoint.position);
                slot.hologramObjectParent.transform.localRotation = Quaternion.Euler(Vector3.zero);
                slot.hologramObjectParent.gameObject.name = slot.profile.name + "'s Hologram List Parent";

                slot.hologramObject = new GameObject();
                slot.hologramObject.transform.localScale = Vector3.one;
                slot.hologramObject.transform.SetParent(slot.hologramObjectParent.transform);
                slot.hologramObject.transform.localPosition = Vector3.zero;
                slot.hologramObject.transform.localRotation = Quaternion.identity;
                slot.hologramObject.gameObject.name = slot.profile.name + "'s Hologram List";

                for (int i = 0; i < slot.profile.meshList.Length; i++)
                {
                    var tempMesh = GameObject.CreatePrimitive(PrimitiveType.Cube);
                    tempMesh.tag = "Hologram";

                    tempMesh.transform.SetParent(slot.hologramObject.transform);
                    tempMesh.transform.localScale = slot.profile.meshScaleList[i];

                    tempMesh.transform.localPosition = slot.profile.meshPosList[i];

                    tempMesh.transform.localRotation = slot.profile.meshRotList[i];

                    Destroy(tempMesh.GetComponent<Collider>());

                    tempMesh.GetComponent<MeshFilter>().mesh = slot.profile.meshList[i];

                    var _mr = tempMesh.GetComponent<MeshRenderer>();

                    _mr.material = Sandbox_ScenarioManager.instance.highlightMaterial;
                    _mr.shadowCastingMode = ShadowCastingMode.Off;
                    _mr.receiveShadows = false;

                    _mr.enabled = true;
                    _mr.gameObject.name = gameObject.name + "'s Hologram of : " + slot.profile;

                    slot.hologramList.Add(_mr);
                    slot.hologramObjectParent.SetActive(false);
                }

                if (!slot.isNoHologramAnimation)
                    slot.hologramObject.transform.DOScale(slot.hologramObject.transform.localScale + (slot.hologramObject.transform.localScale / 7f), 0.5f).SetLoops(-1, LoopType.Yoyo);


                Sandbox_HologramManager.instance.SetDict(slot.profile);
                Sandbox_HologramManager.instance.actionDict[slot.profile] += slot.ToggleHologram;
            }
        }


        #endregion

#if UNITY_EDITOR
        #region Inspector

        bool isHVRComponentShown;

        string buttonName
        {
            get
            {
                if (isHVRComponentShown)
                    return "Hide HVR Compononent";
                else
                    return "Show HVR Compononent";
            }
            set { }
        }

        private void OnDestroy()
        {
            GetComponent<HVRSocket>().hideFlags = HideFlags.None;
            GetComponent<Sandbox_SocketFilter>().hideFlags = HideFlags.None;
        }


        [FoldoutGroup("Debug"), Button("$buttonName")]
        public void ShowHVRComponent()
        {
            if (isHVRComponentShown)
            {
                GetComponent<HVRSocket>().hideFlags = HideFlags.HideInInspector;
                GetComponent<Sandbox_SocketFilter>().hideFlags = HideFlags.HideInInspector;
                isHVRComponentShown = false;
            }
            else
            {
                GetComponent<HVRSocket>().hideFlags = HideFlags.None;
                GetComponent<Sandbox_SocketFilter>().hideFlags = HideFlags.None;
                isHVRComponentShown = true;
            }
        }
        void Reset()
        {
            base.Reset();

            GetComponent<HVRSocket>().hideFlags = HideFlags.HideInInspector;
            GetComponent<HVRSocket>().ScaleGrabbable = false;
            UnityEditorInternal.ComponentUtility.MoveComponentDown(GetComponent<HVRSocket>());
            GetComponent<Sandbox_SocketFilter>().hideFlags = HideFlags.HideInInspector;
            UnityEditorInternal.ComponentUtility.MoveComponentDown(GetComponent<Sandbox_SocketFilter>());
            isHVRComponentShown = false;            
        }

        #endregion
#endif


    }
}