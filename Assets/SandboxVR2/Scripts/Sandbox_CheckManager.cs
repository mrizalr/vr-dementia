using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.UI;
using SandboxVRSeason2.Framework.Multiplayer;

namespace SandboxVRSeason2.Framework
{
    public class Sandbox_CheckManager : MonoBehaviour
    {
        public static Sandbox_CheckManager instance;

        [ReadOnly] public SB_TimedCheck currentCheck;

        [FoldoutGroup("OverObjectTimer"), SerializeField] public Transform onObjectCheck;
        [FoldoutGroup("OverObjectTimer"), SerializeField] public Image onObjectFillImage;
        [FoldoutGroup("OverObjectTimer"), SerializeField] public GameObject onObjectComplete;

        Sandbox_ScenarioUIManager UIM;

        private void Awake()
        {
            instance = this;
        }

        private void Start()
        {
            UIM = Sandbox_ScenarioUIManager.instance;
            if (!onObjectCheck)
            {
                onObjectCheck = new GameObject("OnObjectCheckTimer", typeof(Canvas), typeof(CanvasScaler), typeof(CanvasRenderer)).GetComponent<Transform>();
                onObjectCheck.GetComponent<Canvas>().renderMode = RenderMode.WorldSpace;
                onObjectCheck.GetComponent<Canvas>().worldCamera = Camera.main;
                onObjectCheck.transform.localScale = UIM.checkCanvas.transform.localScale;
                onObjectFillImage = Instantiate(UIM.checkFillImage.gameObject, onObjectCheck.transform).GetComponent<Image>();
                onObjectComplete = Instantiate(UIM.checkCompleteImage, onObjectCheck.transform);

            }

            onObjectCheck.gameObject.SetActive(false);
        }

        private void Update()
        {
            if (currentCheck)
            {
                if (currentCheck.timeIndicator)
                {
                    onObjectCheck.transform.position = currentCheck.timerLocation? currentCheck.timerLocation.position : currentCheck.transform.position;
                    onObjectCheck?.transform.LookAt(Camera.main.transform);
                    if (onObjectFillImage) onObjectFillImage.GetComponent<Image>().fillAmount = currentCheck.currentTime / currentCheck.checkTime + .01f;
                    onObjectCheck?.gameObject.SetActive(true);
                }

                UIM.checkFillImage.fillAmount = currentCheck.currentTime / currentCheck.checkTime;
                UIM.checkTitleText.text = "Checking " + currentCheck.checkName;
                UIM.checkCanvas.SetActive(true);

                if (currentCheck.isComplete)
                {
                    UIM.checkCompleteImage.SetActive(true);
                    onObjectComplete?.SetActive(true);
                }
                else
                {
                    UIM.checkCompleteImage.SetActive(false);
                    onObjectComplete?.SetActive(false);
                }
            }
            else
            {
                UIM.checkCanvas.SetActive(false);
                onObjectCheck?.gameObject.SetActive(false);
                UIM.checkCompleteImage.SetActive(false);
                onObjectComplete?.SetActive(false);
            }
        }

    }
}
