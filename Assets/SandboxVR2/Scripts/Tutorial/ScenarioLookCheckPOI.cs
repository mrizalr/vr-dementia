﻿using UnityEngine;
using UnityEngine.Events;

public class ScenarioLookCheckPOI : MonoBehaviour
{
    public float lookTime = 0f;
    public float lookDistance = 0f;
    public bool completed = false;
    public UnityEvent onDoneLookedAt;
}