using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Rendering;

public class UIPopUp : MonoBehaviour
{
    [SerializeField] private bool openOnInstantiated = true;
    [SerializeField] private float duration = 0.5f;
    [SerializeField] private Vector3 openedScale = Vector3.one, closedScale = Vector3.zero;
     
    private bool _readyToTransform = true;

    public bool IsOpen { get; private set; } = true;
    
    // Start is called before the first frame update
    void Start()
    {        
        if (!openOnInstantiated)
        {
            ShowUI(false);
        }
    }
    
    public void ShowUI(bool show)
    {
        if (_readyToTransform)
        {
            IsOpen = show;
            _readyToTransform = false;
            transform.DOScale(show ? openedScale : closedScale, duration).OnComplete(() => { _readyToTransform = true;});
        }
    }
}
