using System;
using System.Collections;
using System.Collections.Generic;
using HurricaneVR.Framework.ControllerInput;
using HurricaneVR.Framework.Shared;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

[Flags]
public enum JoystickDirection
{
    None = 0,
    PrimaryUp = 1,
    PrimaryDown = 2,
    PrimaryLeft = 4,
    PrimaryRight = 8,
    SecondaryUp = 16,
    SecondaryDown = 32,
    SecondaryLeft = 64,
    SecondaryRight = 128
}

public class ScenarioTutorialJoystickTimedInteraction : MonoBehaviour
{
    [SerializeField] private JoystickDirection requiredDirection;
    [SerializeField] private float requiredTime;

    [SerializeField] private UnityEvent onRequiredReach;
    
    [SerializeField] private TextMeshProUGUI counterHolder;

    private float _timeElapsed = 0;
    private bool _completed = false;

    // Update is called once per frame
    void Update()
    {
        if (_completed)
            return;
        
        JoystickDirection primary = JoystickDirection.None;
        bool assertion = GetAxisAssertion(HVRInputManager.Instance.RightController, JoystickDirection.PrimaryUp, JoystickDirection.PrimaryDown, JoystickDirection.PrimaryRight, JoystickDirection.PrimaryLeft);

        if (assertion)
        {
            CheckRequired();
        }
        else
        {
            assertion = GetAxisAssertion(HVRInputManager.Instance.LeftController, JoystickDirection.SecondaryUp, JoystickDirection.SecondaryDown, JoystickDirection.SecondaryRight, JoystickDirection.SecondaryLeft);
            
            if(assertion)
                CheckRequired();
        }
        
        if (_timeElapsed >= requiredTime - 0.1f && !_completed)
        {
            onRequiredReach?.Invoke();
            if(counterHolder)
                counterHolder.text = "(100/100)";
            _completed = true;
        }
    }

    private bool GetAxisAssertion(HVRController controller, JoystickDirection up, JoystickDirection down, JoystickDirection right, JoystickDirection left)
    {
        if ((requiredDirection & up) != 0)
        {
            if (controller.JoystickAxis.y > 0.1f)
                return true;
        }
        if ((requiredDirection & down) != 0)
        {
            if (controller.JoystickAxis.y < -0.1f)
                return true;
        }
        if ((requiredDirection & left) != 0)
        {
            if (controller.JoystickAxis.x < -0.1f)
                return true;
        }
        if ((requiredDirection & right) != 0)
        {
            if (controller.JoystickAxis.x > 0.1f)
                return true;
        }

        return false;
    }
    
    private void CheckRequired()
    {
        if (_timeElapsed < requiredTime)
        {
            _timeElapsed += Time.deltaTime;
            
            if (counterHolder)
            {
                counterHolder.text = $"({(Mathf.InverseLerp(0, requiredTime, _timeElapsed) * 100):F2}/100)";
            }
        }
    }
}
