using System;
using System.Collections;
using System.Collections.Generic;
using Tictech.LoadManager;
using UnityEngine;

public class TutorialEnd : MonoBehaviour
{
    public void EndTutorial()
    {
        StartCoroutine(BackToMenu());
    }

    IEnumerator BackToMenu()
    {
        yield return new WaitForSeconds(2f);
        RuntimeManager.LoadScene(RuntimeManager.Instance.defaultScene);
    }
}
