﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum LookCheckModes
{
    CHECK_ALL,
    CHECK_EACH
}

public class ScenarioLookCheck : MonoBehaviour
{
    [SerializeField] private LookCheckModes mode;
    
    [SerializeField] private List<ScenarioLookCheckPOI> pois = new List<ScenarioLookCheckPOI>();
    
    private List<ScenarioLookCheckPOI> _confirmedPois = new List<ScenarioLookCheckPOI>();
    private Camera _mainCamera;
    private RaycastHit _hits;

    private float _closestPoi;
    
    // Start is called before the first frame update
    void Start()
    {
        _mainCamera = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        switch (mode)
        {
            case LookCheckModes.CHECK_EACH:
                _confirmedPois.Clear();

                pois.ForEach(poi =>
                {
                    if(CheckIfPoiValid(poi))
                        _confirmedPois.Add(poi);
                });

                int closestPoiIndex = 0;
                _closestPoi = Single.MaxValue;
                
                if (_confirmedPois.Count <= 0) break;
                
                _confirmedPois.ForEach(poi =>
                {
                    float distance = Vector3.Distance(_mainCamera.transform.position, poi.transform.position);

                    if (distance < _closestPoi)
                    {
                        _closestPoi = distance;
                        closestPoiIndex = _confirmedPois.IndexOf(poi);
                    }
                });

                if (_closestPoi > _confirmedPois.Count) break;
                
                _confirmedPois[closestPoiIndex].lookTime -= Time.deltaTime;
                if (_confirmedPois[closestPoiIndex].lookTime <= 0)
                {
                    _confirmedPois[closestPoiIndex].onDoneLookedAt?.Invoke();
                }
                break;
            case LookCheckModes.CHECK_ALL:
                foreach (var poi in pois)
                {
                    if(CheckIfPoiValid(poi))
                    {
                        poi.lookTime -= Time.deltaTime;
                        if (poi.lookTime <= 0 && !poi.completed)
                        {
                            poi.completed = true;
                            poi.onDoneLookedAt?.Invoke();
                        }
                    }
                }
                break;
        }
    }

    private bool CheckIfObjectIsInFront(Vector3 target)
    {
        float dotProduct = Vector3.Dot(_mainCamera.transform.TransformDirection(Vector3.forward).normalized,
            (target - _mainCamera.transform.position).normalized);
        return dotProduct > 0.707;
    }

    private bool CheckIfPoiValid(ScenarioLookCheckPOI poi)
    {
        if(!CheckIfObjectIsInFront(poi.transform.position) || !poi.enabled || !poi.gameObject.activeInHierarchy) return false;

        return Vector3.Distance(_mainCamera.transform.position, poi.transform.position) < poi.lookDistance;
        //Ray poiRay = new Ray(_mainCamera.transform.position, poi.transform.position);
        //Debug.DrawRay(poiRay.origin, poiRay.direction, Color.red);
        //return Physics.Raycast(poiRay, poi.lookDistance);
    }
}