﻿using System;
using System.Collections;
using System.Collections.Generic;
using HurricaneVR.Framework.ControllerInput;
using HurricaneVR.Framework.Core.Player;
using HurricaneVR.Framework.Core.UI;
using HurricaneVR.Framework.Shared;
using UnityEngine;
using UnityEngine.Events;
using TMPro;

public class ScenarioTutorialButtonInteraction : MonoBehaviour
{
    //[SerializeField] private OVRInput.Button requiredButton = OVRInput.Button.One;

    [SerializeField] private HVRXRInputFeatures requiredState;
    [SerializeField] private bool requiredRightController;
    
    [SerializeField] private int requiredTimes = 0;
    
    [SerializeField] private UnityEvent onEachPress;
    [SerializeField] private UnityEvent onRequiredReach;

    [SerializeField] private TextMeshProUGUI counterHolder;

    private int _buttonPressed = 0;
    private bool _completed = false;
    private HVRController _requiredController;

    private void Start()
    {
        _requiredController = requiredRightController
            ? HVRInputManager.Instance.RightController
            : HVRInputManager.Instance.LeftController;
    }

    // Update is called once per frame
    void Update()
    {
        switch (requiredState)
        {
            case HVRXRInputFeatures.PrimaryButton :
                CheckRequiredButtonPressed(_requiredController.PrimaryButtonState.JustActivated);
                break;
            case HVRXRInputFeatures.SecondaryButton :
                CheckRequiredButtonPressed(_requiredController.SecondaryButtonState.JustActivated);
                break;
            case HVRXRInputFeatures.Trigger :
                CheckRequiredButtonPressed(_requiredController.TriggerButtonState.JustActivated);
                break;
            case HVRXRInputFeatures.Grip :
                CheckRequiredButtonPressed(_requiredController.GripButtonState.JustActivated);
                break;
        }
        
        if (_buttonPressed >= requiredTimes && !_completed)
        {
            onRequiredReach?.Invoke();
            _completed = true;
        }
    }

    private void CheckRequiredButtonPressed(bool pressed)
    {
        if (pressed && _buttonPressed < requiredTimes)
        {
            _buttonPressed++;
            onEachPress?.Invoke();

            if (counterHolder)
            {
                counterHolder.text = $"({_buttonPressed}/{requiredTimes})";
            }
        }
    }
}
