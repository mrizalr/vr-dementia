using System.Collections;
using System.Collections.Generic;
using HurricaneVR.Framework.Core.Player;
using UnityEngine;

public class TutorialLock : MonoBehaviour
{
    [SerializeField] private HVRPlayerController playerController;
    [SerializeField] private HVRTeleporter playerTeleportHandler;
    [SerializeField] private bool enableMovement, enableRotation, enableTeleportation;
    
    // Start is called before the first frame update
    void Start()
    {
        playerController = GetComponent<HVRPlayerController>();
        playerTeleportHandler = GetComponent<HVRTeleporter>();
        
        playerController.MovementEnabled = enableMovement;
        playerController.RotationEnabled = enableRotation;
        playerTeleportHandler.enabled = enableTeleportation;
    }

    public void SetRotationEnabled(bool value)
    {
        playerController.RotationEnabled = value;
    }

    public void SetMovementEnabled(bool value)
    {
        playerController.MovementEnabled = value;
    }

    public void SetTeleportationEnabled(bool value)
    {
        playerTeleportHandler.enabled = value;
    }
}
