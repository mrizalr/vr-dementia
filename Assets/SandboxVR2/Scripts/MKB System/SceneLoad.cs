using SandboxVRSeason2.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoad : MonoBehaviour
{
    ScenarioModeEnum scenarioMode;
    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(this);
        /*Display.displays[2].Activate();
        Display.displays[1] = Display.displays[2];*/
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnLevelWasLoaded(int level)
    {
        if (GameObject.FindObjectOfType<Sandbox_ScenarioManager>())
        {
            Sandbox_ScenarioManager sm = GameObject.FindObjectOfType<Sandbox_ScenarioManager>();
            sm.mode = scenarioMode;
        }
    }

    public void changeMode(ScenarioModeEnum sme)
    {
        scenarioMode = sme;
    }

    public void SingleLoadLinearSceneAsync(string s)
    {
        changeMode(ScenarioModeEnum.LINEAR);
        SceneManager.LoadSceneAsync(s, LoadSceneMode.Single);
    }

    public void SingleLoadSandboxSceneAsync(string s)
    {
        changeMode(ScenarioModeEnum.SANDBOX);
        SceneManager.LoadSceneAsync(s, LoadSceneMode.Single);
    }

    public void SingleLoadSceneAsync(string s) => SceneManager.LoadSceneAsync(s, LoadSceneMode.Single);

}
