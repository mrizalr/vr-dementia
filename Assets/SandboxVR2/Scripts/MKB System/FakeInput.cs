using HurricaneVR.Framework.ControllerInput;
using HurricaneVR.Framework.Shared;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FakeInput : MonoBehaviour
{
    HVRPlayerInputs input;
    // Start is called before the first frame update
    void Start()
    {
        if (!input)
            input = GetComponent<HVRPlayerInputs>() ? GetComponent<HVRPlayerInputs>() : FindObjectOfType<HVRPlayerInputs>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.LeftControl) && Input.GetKey(KeyCode.D) && Input.GetKey(KeyCode.E) && Input.GetKeyDown(KeyCode.V))
        {
            MouseGrabber mg = GetComponent<MouseGrabber>();
            if (mg)
            {
                mg.enabled = mg.enabled ? false : true;
            }
        }
        checkInput();
    }

    private void LateUpdate()
    {
        checkInput();
    }

    void checkInput()
    {
        if (GetComponent<MouseGrabber>())
        {
            if (!GetComponent<MouseGrabber>().enabled && GetComponent<MouseGrabber>().lockMode == CursorLockMode.None)
                return;
        }
        else return;

        if (!input)
            input = GetComponent<HVRPlayerInputs>() ? GetComponent<HVRPlayerInputs>() : FindObjectOfType<HVRPlayerInputs>();
        HVRButtonState leftGripState = HVRController.GetButtonState(HVRHandSide.Left, HVRButtons.Grip);
        HVRButtonState rightGripState = HVRController.GetButtonState(HVRHandSide.Right, HVRButtons.Grip);
        HVRButtonState leftTriggerState = HVRController.GetButtonState(HVRHandSide.Left, HVRButtons.Trigger);
        HVRButtonState rightTriggerState = HVRController.GetButtonState(HVRHandSide.Right, HVRButtons.Trigger);
        HVRButtonState leftPrimary = HVRController.GetButtonState(HVRHandSide.Left, HVRButtons.Primary);
        HVRButtonState rightPrimary = HVRController.GetButtonState(HVRHandSide.Right, HVRButtons.Primary);
        HVRButtonState leftSecondary = HVRController.GetButtonState(HVRHandSide.Left, HVRButtons.Secondary);
        HVRButtonState rightSecondary = HVRController.GetButtonState(HVRHandSide.Right, HVRButtons.Secondary);
        if (Input.GetKeyDown("f"))
        {
            input.IsLeftGrabActivated = true;
            input.IsRightGrabActivated = true;
            leftGripState.JustActivated = true;
            rightGripState.JustActivated = true;
        }
        else if (Input.GetKey("f"))
        {
            input.IsLeftGripHoldActive = true;
            input.IsRightGripHoldActive = true;
            leftGripState.Active = true;
            rightGripState.Active = true;
        }

        if (Input.GetButtonDown("Fire1"))
        {
            input.IsLeftGrabActivated = true;
            leftGripState.JustActivated = true;
            leftPrimary.JustActivated = true;
        }
        else if (Input.GetButton("Fire1"))
        {
            input.IsLeftGripHoldActive = true;
            leftGripState.Active = true;
            leftPrimary.Active = true;
        }

        if (Input.GetButtonDown("Fire2"))
        {
            input.IsRightGrabActivated = true;
            rightGripState.JustActivated = true;
            rightPrimary.JustActivated = true;
        }
        else if (Input.GetButton("Fire2"))
        {
            input.IsRightGripHoldActive = true;
            rightGripState.Active = true;
            rightPrimary.Active = true;
        }

        HVRController.SetButtonState(HVRHandSide.Left, HVRButtons.Grip, leftGripState);
        HVRController.SetButtonState(HVRHandSide.Right, HVRButtons.Grip, rightGripState);


        //if (Input.GetKeyDown("1"))
        //    leftPrimary.JustActivated = true;
        //else if (Input.GetKey("1"))
        //    leftPrimary.Active = true;

        //if (Input.GetKeyDown("2"))
        //    leftSecondary.Active = true;
        //else if (Input.GetKey("2"))
        //    leftSecondary.Active = true;

        //if (Input.GetKeyDown("3"))
        //    rightPrimary.JustActivated = true;
        //else if (Input.GetKey("3"))
        //    rightPrimary.Active = true;

        //if (Input.GetKeyDown("4"))
        //    rightSecondary.Active = true;
        //else if (Input.GetKey("4"))
        //    rightSecondary.Active = true;

        HVRController.SetButtonState(HVRHandSide.Left, HVRButtons.Primary, leftPrimary);
        HVRController.SetButtonState(HVRHandSide.Right, HVRButtons.Primary, rightPrimary);
        HVRController.SetButtonState(HVRHandSide.Left, HVRButtons.Secondary, leftSecondary);
        HVRController.SetButtonState(HVRHandSide.Right, HVRButtons.Secondary, rightSecondary);


        if (Input.GetKey("x"))
        {
            input.IsLeftTriggerHoldActive = true;
            input.IsRightTriggerHoldActive = true;
            leftTriggerState.Active = true;
            rightTriggerState.Active = true;
            leftTriggerState.JustActivated = true;
            rightTriggerState.JustActivated = true;
        }

        if (Input.GetKey("z"))
        {
            input.IsLeftTriggerHoldActive = true;
            leftTriggerState.Active = true;
            leftTriggerState.JustActivated = true;
        }

        if (Input.GetKey("c"))
        {
            input.IsRightTriggerHoldActive = true;
            rightTriggerState.Active = true;
            rightTriggerState.JustActivated = true;
        }

        if (Input.GetKeyUp("x") || Input.GetKeyUp("z") || Input.GetKeyUp("c"))
        {
            leftTriggerState.JustActivated = false;
            rightTriggerState.JustActivated = false;
        }

        HVRController.SetButtonState(HVRHandSide.Left, HVRButtons.Trigger, leftTriggerState);
        HVRController.SetButtonState(HVRHandSide.Right, HVRButtons.Trigger, rightTriggerState);
    }
}
