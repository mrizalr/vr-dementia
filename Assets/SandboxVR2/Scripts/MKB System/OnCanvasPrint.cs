using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class OnCanvasPrint : MonoBehaviour
{
    //public List<Transform> PrintQueu;
    public Canvas canvasToDuplicate;
    public int targetDisplay = 2;
    [SerializeField, ReadOnly] Canvas duplicate;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            if (!duplicate)
            {
                duplicate = Instantiate(canvasToDuplicate);
                duplicate.renderMode = RenderMode.ScreenSpaceOverlay;
                duplicate.targetDisplay = targetDisplay;

            }
        }
    }

    private void OnGUI()
    {
        /*string s = "";
        foreach (var item in PrintQueu)
            foreach (var item2 in item.GetComponentsInChildren<TextMeshProUGUI>())
            {
                s += item2.text + "\n";
            }
        GUI.Label(new Rect(Screen.width * .01f, Screen.height * .01f, Screen.width, Screen.height), s);*/
    }
}
