﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
#if UNITY_EDITOR
[CustomEditor(typeof(LayoutHandler))]
public class LayoutHandlerEditor : Editor
{
    LayoutHandler reference;

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        if(GUILayout.Button("Save Active Layout Setting"))
        {
            if (reference.activeLayoutSetting != null)
            {
                EditorUtility.SetDirty(reference.activeLayoutSetting);
                RectTransform[] childObjects = reference.GetComponentsInChildren<RectTransform>(true);
                List<RectTransform> newChildObjects = new List<RectTransform>();

                for (int i = 0; i < childObjects.Length; i++)
                {
                    if(!reference.ignoredTags.Contains(childObjects[i].tag))
                    {
                        newChildObjects.Add(childObjects[i]);
                    }
                }

                reference.activeLayoutSetting.SaveLayoutSetting(newChildObjects);
                AssetDatabase.SaveAssets();
            }
        }

        if (GUILayout.Button("Load Active Layout Setting"))
        {
            Undo.RecordObject(reference, "Set UI properties");
            reference.LoadLayoutSetting(reference.activeLayoutSetting);
        }
    }

    private void OnEnable()
    {
        reference = (LayoutHandler)target;
    }
}
#endif
