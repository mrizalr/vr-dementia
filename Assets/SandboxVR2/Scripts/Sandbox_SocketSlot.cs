using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.Events;


namespace SandboxVRSeason2.Framework
{
    [System.Serializable]
    public class Sandbox_SocketSlot
    {
        public Sandbox_ObjectProfile profile;
        public Transform desiredPoint;
        public Transform hologramDesiredPoint;
        public AxisEnum alignAxis;
        public PositionTypeEnum alignPosType = PositionTypeEnum.LOCAL;


       // public bool isMatchRotation;
       // [ShowIf("isMatchRotation")] [SerializeField] private float matchRotationOffset;

        [FoldoutGroup("Advance")] [SerializeField] private bool isNoHologram;
        [FoldoutGroup("Advance")] [SerializeField] public bool isNoHologramAnimation;

        [FoldoutGroup("Slot Event")] public UnityEvent onSlotOccupied;
        [FoldoutGroup("Slot Event")] public UnityEvent onSlotUnoccupied;

        [FoldoutGroup("Slot Debug")] [SerializeField, ReadOnly] public GameObject hologramObjectParent;
        [FoldoutGroup("Slot Debug")] [SerializeField, ReadOnly] public GameObject hologramObject;
        [FoldoutGroup("Slot Debug")] [SerializeField, ReadOnly] public List<MeshRenderer> hologramList;

        public void ToggleHologram(bool value)
        {
            hologramObjectParent.SetActive(value);
        }

    }
}