using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SandboxVRSeason2.Framework;

public class SandboxOnly : MonoBehaviour
{
    private void OnEnable() {
        CheckMode();
    }
    void CheckMode(){
        if(Sandbox_ScenarioManager.instance)
            if(Sandbox_ScenarioManager.Instance.mode == ScenarioModeEnum.LINEAR){
                gameObject.SetActive(false);
            }
    }
    private void Start() {
        CheckMode();
    }
}
