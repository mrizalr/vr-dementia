using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioControllerMaster : MonoBehaviour
{
    public List<SettingsUI> settings;
    public AudioSource bgm;
    public int bgmVolume, sfxVolume, currentBGMIndex;
    public List<PilihanMusik> bgmList;
    public List<SFX> sfxList;
    private static Dictionary<string, SFX> sfxMap = new Dictionary<string, SFX>();



    // public static AudioControllerMaster Instance;
    private void Awake() 
    { 
        var mastas = GameObject.FindObjectsOfType<AudioControllerMaster>();
        foreach(AudioControllerMaster masta in mastas)
        {
            if(masta != this)
            {
                Destroy(this.gameObject);
            }
        }

        foreach(SFX sfx in sfxList)
        {
            var item = new GameObject("sfx " + sfx.name);
            item.transform.SetParent(transform);
            sfx.source = item.AddComponent<AudioSource>();
            sfx.source.clip = sfx.clip;
            sfxMap.Add(sfx.name, sfx);
        }
    }

    public void Refresh()
    {
        var search = FindObjectsOfType<OnEnableEvent>(true);
        if(search.Length != 0)
        {
            foreach(OnEnableEvent item in search)
            {
                if(item.CompareTag("Complete")) item.GetComponent<OnEnableEvent>().onEnable.AddListener(delegate { sfxMap["complete"].source.Play(); });
                else if (item.CompareTag("Correct")) item.GetComponent<OnEnableEvent>().onEnable.AddListener(delegate { sfxMap["correct answer"].source.Play(); });
                else if (item.CompareTag("Failed")) item.GetComponent<OnEnableEvent>().onEnable.AddListener(delegate { sfxMap["wrong answer"].source.Play(); });

            }
        }

        FindObjectOfType<HurricaneVR.Framework.Core.Player.HVRTeleporter>().PositionUpdate.AddListener(delegate { sfxMap["teleport"].source.Play(); });

        foreach (Button button in FindObjectsOfType<Button>())
        {
            if(button.CompareTag("Menu Button")) 
            {
                button.onClick.AddListener(delegate { sfxMap["menu button"].source.Play(); });
            }
            else
            { 
                button.onClick.AddListener(delegate { sfxMap["button click"].source.Play(); });
            }
        }
    }

    public void PlayQuizAnswerSFX(bool correctAnswer)
    {
        if(correctAnswer) sfxMap["correct answer"].source.Play();
        else sfxMap["wrong answer"].source.Play();
    }

    private void SetSFXVolume(float value)
    {
        foreach (KeyValuePair<string, SFX> sfx in sfxMap)
        {
            sfx.Value.source.volume = value;
        }
    }

    public void IncreaseVolMusik(SettingsUI settingInControl)
    {
        foreach(SettingsUI setting in settings)
        {
            if(setting != settingInControl)
            {
                setting.IncreaseVolMusik();
            }
        }

        bgmVolume = settingInControl.musikVol;
        bgm.volume = ((bgmVolume) * 0.333f);
    }

    public void DecreaseVolMusik(SettingsUI settingInControl)
    {
        foreach(SettingsUI setting in settings)
        {
            if(setting != settingInControl)
            {
                setting.DecreaseVolMusik();
            }
        }

        bgmVolume = settingInControl.musikVol;
        bgm.volume = ((bgmVolume) * 0.333f);
    }

    public void IncreaseVolMusikEfek(SettingsUI settingInControl)
    {
        foreach(SettingsUI setting in settings)
        {
            if(setting != settingInControl)
            {
                setting.IncreaseVolMusikEfek();
            }
        }

        sfxVolume = settingInControl.musikEfekVol;
        SetSFXVolume(((sfxVolume) * 0.333f));
    }
    public void DecreaseVolMusikEfek(SettingsUI settingInControl)
    {
        foreach(SettingsUI setting in settings)
        {
            if(setting != settingInControl)
            {
                setting.DecreaseVolMusikEfek();
            }
        }

        sfxVolume = settingInControl.musikEfekVol;
        SetSFXVolume(((sfxVolume) * 0.333f));
    }

    public void PilihanMusikNext(SettingsUI settingInControl)
    {
        foreach(SettingsUI setting in settings)
        {
            if(setting != settingInControl)
            {
                setting.PilihanMusikNext();
            }
        }

        currentBGMIndex = settingInControl.pilihanMusikIndex;
        bgm.clip = bgmList[currentBGMIndex].audio;
        bgm.Play();
    }

    public void PilihanMusikPrev(SettingsUI settingInControl)
    {
        foreach(SettingsUI setting in settings)
        {
            if(setting != settingInControl)
            {
                setting.PilihanMusikPrev();
            }
        }

        currentBGMIndex = settingInControl.pilihanMusikIndex;
        bgm.clip = bgmList[currentBGMIndex].audio;
        bgm.Play();
    }
}

[Serializable]
public class SFX
{
    public string name;
    public AudioClip clip;
    public AudioSource source;
}
