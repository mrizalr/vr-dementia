using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FilterStartLevel : MonoBehaviour
{
    private static int levelEasy = 0;
    private static int levelNormal = 0;

    [SerializeField]
    private List<Button> levelListEasy;
    [SerializeField]
    private List<Button> levelListNormal;

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < levelListEasy.Count; i++)
        {
            Button item = levelListEasy[i];
            item.interactable = (levelEasy >> (levelListEasy.Count - 1 - i)) % 2 == 0;
        }

        for (int i = 0; i < levelListNormal.Count; i++)
        {
            Button item = levelListNormal[i];
            item.interactable = (levelNormal >> (levelListNormal.Count - 1 - i)) % 2 == 0;
        }
    }

    public void SetLevelEasy(int index)
    {
        levelEasy |= 1 << (4 - index);

        for (int i = 0; i < 5; i++)
        {
            Debug.Log($"Filter: {(levelEasy >> (5 - 1 - i)) % 2}");
        }
    }

    public void SetLevelNormal(int index)
    {
        levelNormal |= 1 << (4 - index);

        for (int i = 0; i < 5; i++)
        {
            Debug.Log($"Filter: {(levelNormal >> (5 - 1 - i)) % 2}");
        }
    }
}
