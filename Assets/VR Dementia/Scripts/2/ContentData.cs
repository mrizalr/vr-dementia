using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContentData : MonoBehaviour
{
    [System.Serializable]
    public struct Content
    {
        public string data;
    }

    public Content[] calendar;
    public Content[] enCalendar;
    public Content[] clock;
    public Content[] roomNumber;

}
