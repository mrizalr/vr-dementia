using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MCAnswer : MonoBehaviour
{
    private QuizAnswer quizAnswer;
    private MCQuiz mcQuiz;

    [SerializeField] private GameObject wrongMessageGO;

    private static bool waiting;
    private void Start()
    {
        quizAnswer = GetComponent<QuizAnswer>();
        mcQuiz = GetComponentInParent<MCQuiz>();
    }

    public void CheckMCAnswer()
    {
        if (waiting) return;

        var found = FindObjectOfType<AudioControllerMaster>();
        found?.Refresh();

        if (quizAnswer.isCorrect)
        {
            found?.PlayQuizAnswerSFX(true);
            mcQuiz.SetNextQuiz();
        }
        else 
        {
            found?.PlayQuizAnswerSFX(false);
            TurnWrongMessageOn();
        }
    }

    private void TurnWrongMessageOn()
    {
        waiting = true;
        StartCoroutine(TurnWrongMessageOnCor());
    }

    private IEnumerator TurnWrongMessageOnCor()
    {
        wrongMessageGO.SetActive(true);
        yield return new WaitForSeconds(1f);
        wrongMessageGO.SetActive(false);
        waiting = false;
    }
}
