using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Events;

public class MCQuiz : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI questionTMP;
    [SerializeField] private QuizAnswer[] answerTMP;

    private FigureQuiz quizData;
    private int quizIdx;

    public UnityEvent onQuizCleared;

    private void Start()
    {
        quizData = GetComponent<FigureQuiz>();
    }

    public void SetNextQuiz()
    {
        if (quizIdx < quizData.figureDatas.Length - 1) quizIdx++;
        else
        {
            GetComponentInParent<OpenCloseCanvas>().CloseCanvas();
            onQuizCleared?.Invoke();
            return;
        }

        SetUpQuiz(quizIdx);
    }

    public void Init()
    {
        SetUpQuiz(0);
    }

    private void SetUpQuiz(int index)
    {
        var qd = quizData.figureDatas[index];

        //questionTMP.text = qd.question;
        questionTMP.gameObject.GetComponent<GeneratedItemLocalization>().SetupString(qd.question, qd.questionEn);
        for (int i = 0; i < qd.answers.Length; i++)
        {
            answerTMP[i].Init(qd.answers[i].answerText, qd.answers[i].answerTextEn, qd.answers[i].isCorrect);
        }
    }
}
