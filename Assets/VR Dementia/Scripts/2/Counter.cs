using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Counter : MonoBehaviour
{
    [SerializeField] private int targetCount;
    public UnityEvent OnTargetCountReached;
    private bool isFinish;

    public void AddCounter(int value)
    {
        targetCount -= value;
        CheckCounter();
    }

    private void CheckCounter()
    {
        if (isFinish) return;
        if (targetCount <= 0)
        {
            OnTargetCountReached.Invoke();
            isFinish = true;
        }
    }
}
