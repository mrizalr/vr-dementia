using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Linq;
using UnityEngine.Localization.Settings;

public class SetContent : MonoBehaviour
{
    [SerializeField] private FigureQuiz quizData;
    [SerializeField] private MCQuiz mcQuiz;
    [SerializeField] private ContentData contentData;
    [SerializeField] private TextMeshProUGUI[] contentTMP;


    private enum ContentType { Calendar, Clock, RoomNumber }

    [SerializeField] private ContentType contentType;

    private void Start()
    {
        contentData = GetComponentInParent<ContentData>();

        Set();
    }

    private void Set()
    {
        var contents = new string[2];
        var rdm = Random.Range(0, 3);
        switch (contentType)
        {
            case ContentType.Calendar:
                contents = contentData.calendar[rdm].data.Split('-');
                SetQuizAnswer("Tanggal", contents[0]);
                SetQuizAnswer("Bulan", contents[1]);
                contents = LocalizationSettings.SelectedLocale.Identifier == "id" ? contentData.calendar[rdm].data.Split('-') : contentData.enCalendar[rdm].data.Split('-');
                break;
            case ContentType.Clock:
                contents = contentData.clock[rdm].data.Split('-');
                SetQuizAnswer("Jam", contents[0] + "." + contents[1]);
                break;
            case ContentType.RoomNumber:
                contents = contentData.roomNumber[rdm].data.Split('-');
                SetQuizAnswer("nomor ruangan", contents[0]);
                break;
        }

        for (int i = 0; i < contents.Length; i++)
        {
            contentTMP[i].text = contents[i];
        }
    }

    private void SetQuizAnswer(string keyword, string data)
    {
        var qd = quizData.figureDatas.First(e => e.question.Contains(keyword));
        for (int i = 0; i < qd.answers.Length; i++)
        {
            qd.answers[i].isCorrect = qd.answers[i].answerText == data ? true : false;
        }

        var idx = quizData.figureDatas.ToList().IndexOf(qd);
        quizData.figureDatas[idx] = qd;
    }
}
