using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SandboxVRSeason2.Framework;
using Tictech.LoadManager;

public class ScenarioStart : MonoBehaviour
{
    private Sandbox_SubScenarioData _currentData;

    public void LoadScenario(Sandbox_SubScenarioData data) 
    {
        _currentData = data;
        LoadScenario();
    }

    public void LoadScenario() 
    {
        RuntimeManager.LoadScenario(_currentData, true, 0);
    }

    public void SetSubscenarioData(Sandbox_SubScenarioData data) 
    {
        _currentData = data;
    }
}
