using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using TMPro;
using UnityEngine;
using UnityEngine.Localization;
using UnityEngine.Localization.Settings;

public class GeneratedTMPLocalization : MonoBehaviour
{
    [SerializeField] private TextMeshPro stringHolder;

    [SerializeField][TextArea(2, 10)] private string idString;
    [SerializeField][TextArea(2, 10)] private string enString;

    public void SetupString(string id, string en)
    {
        idString = id;
        enString = en;
        OnSelectedLocaleChanged(LocalizationSettings.SelectedLocale);
    }

    private void OnEnable()
    {
        LocalizationSettings.Instance.OnSelectedLocaleChanged += OnSelectedLocaleChanged;
        OnSelectedLocaleChanged(LocalizationSettings.SelectedLocale);
    }

    private void OnDisable()
    {
        LocalizationSettings.Instance.OnSelectedLocaleChanged -= OnSelectedLocaleChanged;
    }

    private void OnSelectedLocaleChanged(Locale locale)
    {
        string content = locale.Identifier.Code == "id" ? Regex.Unescape(idString) : Regex.Unescape(enString);
        stringHolder.text = content;
        stringHolder.ForceMeshUpdate();
    }
}
