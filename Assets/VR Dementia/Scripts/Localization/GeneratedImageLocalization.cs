using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.Localization;
using UnityEngine.Localization.Settings;
using UnityEngine.UI;

public class GeneratedImageLocalization : MonoBehaviour
{
    [SerializeField] private Image imageHolder;

    [SerializeField] private Sprite idSprite;
    [SerializeField] private Sprite enSprite;

    public void SetupString(Sprite id, Sprite en)
    {
        idSprite = id;
        enSprite = en;
        OnSelectedLocaleChanged(LocalizationSettings.SelectedLocale);
    }

    private void OnEnable()
    {
        LocalizationSettings.Instance.OnSelectedLocaleChanged += OnSelectedLocaleChanged;
        OnSelectedLocaleChanged(LocalizationSettings.SelectedLocale);
    }

    private void OnDisable()
    {
        LocalizationSettings.Instance.OnSelectedLocaleChanged -= OnSelectedLocaleChanged;
    }

    private void OnSelectedLocaleChanged(Locale locale)
    {
        imageHolder.sprite = locale.Identifier.Code == "id" ? idSprite : enSprite;
    }
}
