using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Localization;
using UnityEngine.Localization.Settings;
using UnityEngine.UI;

public class LanguageSelector : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI languageHolder;
    [SerializeField] private Button changeBtn;
    [SerializeField] private UnityEvent onLanguageSelected;

    private Coroutine _localeChangeHandler;
    private int _currentLocaleId;

    // Start is called before the first frame update
    void Start()
    {
        LocalizationSettings_SelectedLocaleChanged(LocalizationSettings.SelectedLocale);
        LocalizationSettings.SelectedLocaleChanged += LocalizationSettings_SelectedLocaleChanged;
    }

    private void OnDestroy()
    {
        LocalizationSettings.SelectedLocaleChanged -= LocalizationSettings_SelectedLocaleChanged;
    }

    public void OnSelectionChanged()
    {
        if (_localeChangeHandler != null)
            return;

        _currentLocaleId = (_currentLocaleId + 1)%2;
        
        _localeChangeHandler = StartCoroutine(WaitForChanges(LocalizationSettings.Instance.GetAvailableLocales().Locales[_currentLocaleId].Identifier));
    }

    private IEnumerator WaitForChanges(LocaleIdentifier id)
    {
        yield return new WaitForEndOfFrame();
        
        // Unsubscribe from SelectedLocaleChanged so we don't get an unnecessary callback from the change we are about to make.
        LocalizationSettings.SelectedLocaleChanged -= LocalizationSettings_SelectedLocaleChanged;

        LocalizationSettings.Instance.SetSelectedLocale(LocalizationSettings.AvailableLocales.GetLocale(id));
        LocalizationSettings_SelectedLocaleChanged(LocalizationSettings.SelectedLocale);
        
        changeBtn.interactable = false;

        yield return new WaitForSeconds(2);
        
        changeBtn.interactable = true;

        // Resubscribe to SelectedLocaleChanged so that we can stay in sync with changes that may be made by other scripts.
        LocalizationSettings.SelectedLocaleChanged += LocalizationSettings_SelectedLocaleChanged;

        _localeChangeHandler = null;
        
        onLanguageSelected?.Invoke();
    }

    void LocalizationSettings_SelectedLocaleChanged(Locale locale)
    {
        languageHolder.text = locale.Identifier.Code == "en" ? "English" : "Bahasa Indonesia";
    }
}
