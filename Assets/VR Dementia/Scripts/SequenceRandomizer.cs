using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SequenceRandomizer : MonoBehaviour
{
    public List<int> LastGeneratedSequence {get; private set;} = new List<int>();

    public List<int> GenerateRandom(int numberOfRandom)
    {
        var randomSequence = new List<int>();
        var availableSequence = new List<int>();

        for (int i = 0; i < numberOfRandom; i++)
        {
            availableSequence.Add(i);
        }

        while (availableSequence.Count > 0)
        {
            var rdm = Random.Range(0, availableSequence.Count);

            randomSequence.Add(availableSequence[rdm]);
            availableSequence.RemoveAt(rdm);
        }

        LastGeneratedSequence = randomSequence;

        return randomSequence;
    }
}
