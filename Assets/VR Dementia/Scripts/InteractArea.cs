using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class InteractArea : MonoBehaviour
{
    public UnityEvent OnPlayerEnter, OnPlayerExit;

    private void OnTriggerEnter(Collider other)
    {
        if (!other.gameObject.name.Contains("PlayerController")) return;

        OnPlayerEnter.Invoke();
    }

    private void OnTriggerExit(Collider other)
    {
        if (!other.gameObject.name.Contains("PlayerController")) return;

        OnPlayerExit.Invoke();
    }
}
