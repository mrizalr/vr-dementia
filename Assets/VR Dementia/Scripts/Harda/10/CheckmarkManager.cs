using System.Collections.Generic;
using UnityEngine;

public class CheckmarkManager : MonoBehaviour
{
    public GameObject checkmarkPrefab;
    [HideInInspector] public List<GameObject> checkmarks;

    public void AddCheckmark()
    {
        GameObject c = Instantiate(checkmarkPrefab, transform);
        checkmarks.Add(c);
    }
    public void DeleteCheckmark()
    {
        if(checkmarks.Count > 0)
        {
            Destroy(checkmarks[0]);
            checkmarks.Remove(checkmarks[0]);
        }

    }
}
