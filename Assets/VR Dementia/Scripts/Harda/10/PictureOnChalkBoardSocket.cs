using UnityEngine;
using UnityEngine.Events;

public class PictureOnChalkBoardSocket : MonoBehaviour
{   
    public PictureOnChalkBoardCategory categorySocket;
    public bool matched = false;
    public UnityEvent onPictureAndSocketMatched, onPictureAndSocketNotMatched;
    public bool CategoryMatched()
    {
        if(GetComponentInChildren<PictureOnChalkBoardItem>())
        {
            return (categorySocket == GetComponentInChildren<PictureOnChalkBoardItem>().categoryPicture)? true : false;
        }
        else
        {
            return false;
        }
    }

    public void PictureSocketed()
    {
        if (CategoryMatched() && !matched)
        {
            onPictureAndSocketMatched?.Invoke();
            matched = true;
        }
    }
    public void PictureUnsocketed()
    {
        if (CategoryMatched() && matched) 
        {
            onPictureAndSocketNotMatched?.Invoke();
            matched = false;
        }
    }
}
