using UnityEngine;
using System.Collections.Generic;
using System;
using UnityEngine.Events;

[CreateAssetMenu(fileName = "Dementia Instruction Data", menuName = "Dementia/DementiaInstructionData", order = 1)]
public class DementiaInstructionData : ScriptableObject
{
    public string titleCode;
    public string titleText;
    public List<Instruction> instruction;
}
[Serializable] public class Instruction
{
    [TextArea(3,5)] public string instructionText;
    public Sprite instructionImage;
}