using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class TelephoneTrackerV2 : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI numberHolder;
    [SerializeField] private UnityEvent onAnswerChecked;

    private bool _handleRemoved = true;
    
    private readonly int[] _numbers = {0, 0, 0, 0, 0, 0};
    private int _numberIndex = 0;

    private readonly int[] _answer = { 9, 9, 8, 6, 5, 7 };
    

    public void AddNumber(int number)
    {
        if (_numberIndex >= _answer.Length)
        {
            CheckAnswer();
            return;
        }
        
        _numbers[_numberIndex] = number;
        _numberIndex += 1;
        
        PrintText();
    }

    public void RemoveNumber()
    {
        if (_numberIndex <= 0)
            return;

        _numberIndex -= 1;
        _numbers[_numberIndex] = 0;

        PrintText();
    }

    private void CheckAnswer()
    {
        if(_numbers.SequenceEqual(_answer)) onAnswerChecked?.Invoke();
    }

    public void HandleRemoved(bool isRemoved)
    {
        _handleRemoved = isRemoved;
        
        CheckAnswer();
    }

    private void PrintText()
    {
        numberHolder.text = $"{_numbers[0]}{_numbers[1]}{_numbers[2]}-{_numbers[3]}{_numbers[4]}{_numbers[5]}";
    }
}
