using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneLoader : MonoBehaviour
{
    public string sceneDestinationName;

    public void GoToScene()
    {
        Tictech.LoadManager.RuntimeManager.LoadScene(sceneDestinationName);
    }
}
