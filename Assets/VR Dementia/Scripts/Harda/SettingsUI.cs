using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[Serializable] public class SettingsUI : MonoBehaviour
{
    public List<Image> musikBar;
    public List<Image> musikEfekBar;
    public TMPro.TextMeshProUGUI pilihanMusikText;
    public List<PilihanMusik> pilihanMusik;

    public int musikVol = 2;
    public int musikEfekVol = 2;
    public int pilihanMusikIndex = 0;

    public Button musikIncreaseVol;
    public Button musikDecreaseVol;
    public Button musikEfekIncreaseVol;
    public Button musikEfekDecreaseVol;
    public Button pilihanMusikNext;
    public Button pilihanMusikPrev;

    public Sprite barFill;
    public Sprite barUnfill;
    private AudioControllerMaster master;
    private bool masterFound;

    void Awake()
    {
        var found = GameObject.FindObjectOfType<AudioControllerMaster>();
        if(found) 
        {
            found.Refresh();
        }
    }

    void OnEnable()
    {
        var found = GameObject.FindObjectOfType<AudioControllerMaster>();
        if(!master && found) 
        {
            masterFound = true;
            master = found;
            master.settings.Add(this);

            musikVol = master.bgmVolume;
            musikEfekVol = master.sfxVolume;

            for(int i = 0; i < 3; i++)
            {
                if(musikVol == 0)
                {
                    musikBar[i].sprite = barUnfill;
                }
                else if(musikVol == 3)
                {
                    musikBar[i].sprite = barFill;
                }
                else
                {
                    if(i < musikVol)
                    {
                        musikBar[i].sprite = barFill;
                    }
                    else
                    {
                        musikBar[i].sprite = barUnfill;
                    }
                }
            }

            pilihanMusik = master.bgmList;
            pilihanMusikIndex = master.currentBGMIndex;
            pilihanMusikText.text = master.bgmList[master.currentBGMIndex].title;
        }
    }
    void OnDisable()
    {
        if(masterFound) master.settings.Remove(this);
    }

    public void IncreaseVolMusik()
    {
        if(musikVol < musikBar.Count)
        {
            musikBar[musikVol].sprite = barFill;
            musikVol++;

            if(masterFound) master.IncreaseVolMusik(this);
        }
    }

    public void DecreaseVolMusik()
    {
        if(musikVol > 0)
        {
            musikVol--;
            musikBar[musikVol].sprite = barUnfill;

            if(masterFound) master.DecreaseVolMusik(this);
        }
    }

    public void IncreaseVolMusikEfek()
    {
        if(musikEfekVol < musikEfekBar.Count)
        {
            musikEfekBar[musikEfekVol].sprite = barFill;
            musikEfekVol++;

            if(masterFound) master.IncreaseVolMusikEfek(this);
        }
    }

    public void DecreaseVolMusikEfek()
    {
        if(musikEfekVol > 0)
        {
            musikEfekVol--;
            musikEfekBar[musikEfekVol].sprite = barUnfill;

            if(masterFound) master.DecreaseVolMusikEfek(this);
        }
    }

    public void PilihanMusikNext()
    {
        if (pilihanMusikIndex < pilihanMusik.Count-1) 
            { pilihanMusikIndex++; }
        else 
            { pilihanMusikIndex = 0; }

        pilihanMusikText.text = pilihanMusik[pilihanMusikIndex].title;

        if(masterFound) master.PilihanMusikNext(this);
    }

    public void PilihanMusikPrev()
    {
        if(pilihanMusikIndex > 0)
            { pilihanMusikIndex--; }
        else
            { pilihanMusikIndex = pilihanMusik.Count-1; }

        pilihanMusikText.text = pilihanMusik[pilihanMusikIndex].title;

        if(masterFound) master.PilihanMusikPrev(this);
    }
}

[Serializable]
public class PilihanMusik
{
    public string title;
    public AudioClip audio;
}
