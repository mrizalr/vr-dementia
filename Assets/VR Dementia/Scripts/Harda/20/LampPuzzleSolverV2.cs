using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class LampPuzzleSolverV2 : MonoBehaviour
{    
    [SerializeField] private TextMeshProUGUI scoreHolder;
    [SerializeField] private List<LampPuzzleOrientationCheckV2> orientationCheckers;
    [SerializeField] private UnityEvent onScoringFinished;

    private float _score;
    
    public void ScoreCheck()
    {
        foreach (var lampPuzzleOrientationCheck in orientationCheckers)
        {
            if (lampPuzzleOrientationCheck.IsCorrect)
            {
                _score += 0.2f;
                lampPuzzleOrientationCheck.TurnLampOn();
            }
        }

        scoreHolder.text = _score.ToString("F1");
        onScoringFinished?.Invoke();
    }
}
