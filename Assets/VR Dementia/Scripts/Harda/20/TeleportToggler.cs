using System.Collections;
using System.Collections.Generic;
using HurricaneVR.Framework.Core.Player;
using UnityEngine;

public class TeleportToggler : MonoBehaviour
{
    private HVRTeleporter teleporter;
    // Start is called before the first frame update
    void Start()
    {
        teleporter = FindObjectOfType<HVRTeleporter>();
    }

    public void ToggleTeleport(bool value)
    {
        if(value) {teleporter.Enable();}
        else {teleporter.Disable();}

        Debug.Log("CAN TELEPORT : " + teleporter.CanTeleport);
    }
}
