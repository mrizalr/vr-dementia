using System.Collections;
using System.Collections.Generic;
using SandboxVRSeason2.Framework;
using UnityEngine;
using UnityEngine.Events;

public class LampPuzzleOrientationCheckV2 : MonoBehaviour
{
    [SerializeField] private float targetAngle;
    [SerializeField] private Transform connectedBulb;
    
    private Sandbox_DialController _dialController;
    public UnityEvent onDialCorrect;
    
    // Start is called before the first frame update
    void Start()
    {
        _dialController = GetComponent<Sandbox_DialController>();
    }

    public bool IsCorrect => Mathf.DeltaAngle(targetAngle, _dialController.CurrentAngle) <= 5f && Mathf.DeltaAngle(targetAngle, _dialController.CurrentAngle) >= -5f;

    public void TurnLampOn()
    {
        connectedBulb.GetComponentInChildren<Light>(true).gameObject.SetActive(true);
        onDialCorrect?.Invoke();
    }
}
