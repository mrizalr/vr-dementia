using UnityEngine;
using UnityEngine.Events;

public class ObjectIDChecker : MonoBehaviour
{
    public bool correctItemSocketed = false;
    public UnityEvent onCorrectItemSocketed, onCorrectItemUnsocketed;
    public void CheckSocketed()
    {
        if(GetComponent<ScoreObject>().Id == GetComponentsInChildren<ScoreObject>()[1].Id)
        {
            correctItemSocketed = true;
            onCorrectItemSocketed?.Invoke();
        }
    }
    public void CheckUnsocketed()
    {
        if(correctItemSocketed)
        {
            correctItemSocketed = false;
            onCorrectItemUnsocketed?.Invoke();
        }
    }
}
