using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MEC;

public class OnEnableEvent : MonoBehaviour
{
    public float delay;
    public UnityEngine.Events.UnityEvent onEnable;

    private CoroutineHandle _handle;

    void OnEnable()
    {
        _handle = Timing.RunCoroutine(EnableEvent());
    }

    private void OnDisable()
    {
        Timing.KillCoroutines(_handle);
    }

    private IEnumerator<float> EnableEvent()
    {
        yield return Timing.WaitForSeconds(delay);

        onEnable?.Invoke();
    }
}
