using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;

public class DementiaInstructionHolder : MonoBehaviour
{
    public TextMeshProUGUI titleCodeText;
    public TextMeshProUGUI titleText;
    public TextMeshProUGUI instructionText;
    public Image instructionImage;
    public Button instructionPrev;
    public Button instructionNext;
    public Transform panelDotIndicator;
}
