using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.Events;
using DG.Tweening;

public class DementiaInstructionUI : MonoBehaviour
{
    [ShowInInspector] public bool followPlayer {get; set;}
    public float followingDistance = 0.5f;
    private Transform canvasCallTargetPos;
    private Coroutine callCoroutine;
    private Camera mainCamera;
    private bool onCamera;

    void Start()
    {
        canvasCallTargetPos = GameObject.FindGameObjectWithTag("UITargetPos").transform;
        mainCamera = Camera.main;
    }

    // void OnBecameInvisible()
    // {
    //     if(followPlayer) CallCanvas();
    // }
    void Update()
    {
        Vector3 viewPos = mainCamera.WorldToViewportPoint(transform.position);
        if (viewPos.x >= 0.2f && viewPos.x <= 0.8f && viewPos.y >= 0.2f && viewPos.y <= 0.8f && viewPos.z > 0)
        {
            // Your object is in the range of the camera, you can apply your behaviour
            onCamera = true;
        }
        else
            onCamera = false;

        if(!onCamera && followPlayer)
        {
            CallCanvas();
        }
    }

    [Button] public void CallCanvas()
    {
        if (callCoroutine != null) return;
        StartCoroutine(CallCanvasCoroutine(transform));
    }

    private IEnumerator CallCanvasCoroutine(Transform canvas)
    {
        if (canvas)
        {
            Debug.Log("main - " + Camera.main);
            Sequence seq = DOTween.Sequence();
            seq.Append(canvas.DOMove(canvasCallTargetPos.position + (canvasCallTargetPos.forward * followingDistance), 0.4f));
            seq.Append(canvas.DOLookAt(mainCamera.transform.position + (mainCamera.transform.forward * (followingDistance + 1)), 0.4f, AxisConstraint.Y));
            yield return new WaitForSeconds(0.4f);
        }
    }
}