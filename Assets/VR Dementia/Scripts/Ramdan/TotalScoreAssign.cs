using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TotalScoreAssign : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI scoreHolder;

    public void SetScore(int index)
    {
        if(TotalScoreTracker.TotalScores == null)
        {
            TotalScoreTracker.Init();
        }

        TotalScoreTracker.TotalScores[index - 1] = new Score(){Finished = true, CurrentScore = float.Parse(scoreHolder.text)};
    }
}
