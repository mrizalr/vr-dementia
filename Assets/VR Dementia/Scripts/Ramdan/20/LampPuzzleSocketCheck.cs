using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LampPuzzleSocketCheck : MonoBehaviour
{
    [SerializeField] private int socketTargetCount;
    [SerializeField] private UnityEvent onTargetReached;

    private int _currentCount;

    public void SocketPlaced()
    {
        if (_currentCount + 1 >= socketTargetCount)
        {
            onTargetReached?.Invoke();
            return;
        }

        _currentCount++;
    }
}
