using System.Collections;
using System.Collections.Generic;
using SandboxVRSeason2.Framework;
using UnityEngine;

public class LampPuzzleOrientationCheck : MonoBehaviour
{
    [SerializeField] private float targetAngle;
    [SerializeField] private Transform connectedBulb;
    
    private Sandbox_DialController _dialController;
    
    // Start is called before the first frame update
    void Start()
    {
        _dialController = GetComponent<Sandbox_DialController>();
    }

    public bool IsCorrect => Mathf.Approximately(targetAngle, _dialController.CurrentAngle);

    public void TurnLampOn()
    {
        connectedBulb.GetComponentInChildren<Light>(true).gameObject.SetActive(true);
    }
}
