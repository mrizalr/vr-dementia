using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Sirenix.OdinInspector;
using UnityEngine.Events;

[Serializable]
public class Graph : IEquatable<Graph>
{
    public Transform pointA;
    public Transform pointB;

    public bool Equals(Graph other)
    {
        if(other == null)
            return false;

        return (other.pointA == pointA && other.pointB == pointB) || (other.pointA == pointB && other.pointB == pointA);
    }
}
public class ChalkManager : MonoBehaviour
{
    [SerializeField] private GameObject chalkTrailPrefab;
    [SerializeField] private Transform chalkTip;
    [SerializeField] private UnityEvent onSuccess;
    [SerializeField] private UnityEvent onFailed;
    
    [SerializeField] private List<Graph> answerGraphs = new List<Graph>();

    private List<Graph> _currentGraphs = new List<Graph>();

    private bool _drawValid;
    private ChalkTrail _trail;
    private Transform _trailInitialPosition;

    public void ChalkEnterPoint(Transform initialPosition)
    {
        if(_trail == null)
            AddTrailToChalk(initialPosition);
        else if(_trailInitialPosition != initialPosition)
            TrailFind(initialPosition);
    }

    private void AddTrailToChalk(Transform initialPosition)
    {
        _trail = Instantiate(chalkTrailPrefab, transform).GetComponent<ChalkTrail>();
        _trail.SetTrail(initialPosition, chalkTip);
        _trailInitialPosition = initialPosition;
    }

    private void TrailFind(Transform endPoint)
    {
        _currentGraphs.Add(new Graph(){pointA = _trailInitialPosition, pointB = endPoint});
        _trail.FixTrail(endPoint);
        _trail = null;
    }
    
    [Button]
    public void FinishDrawing()
    {

        for(int i = 0; i < answerGraphs.Count; i++)
        {
            if(!_currentGraphs.Contains(answerGraphs[i]))
            {
                onFailed?.Invoke();
                return;
            }
            else
                continue;
        }

        onSuccess?.Invoke();
        Debug.LogWarning("Finished");
    }

    
    /* 
    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Scenario"))
        {
            _tracker = other.GetComponent<ChalkTracker>();
            _drawValid = true;
        }
    } */

    void OnTriggerExit(Collider other)
    {
        if(other.gameObject.CompareTag("Scenario"))
        {
            if(_trail != null)
                Destroy(_trail.gameObject);

            _drawValid = false;
        }
    }
}
