using System;
using UnityEngine;
using Sirenix.OdinInspector;

public class ChalkTrail : MonoBehaviour
{
    [SerializeField] private Transform initialPoint;
    [SerializeField] private Transform chalkTip;
    [SerializeField] private float scaleMult;

    private float _initialXPos, _initialXRot;
    private float _initialYPos, _initialYRot;
    private bool _trackChalk;

    // Start is called before the first frame update
    void Start()
    {
        _initialXPos = transform.position.x;
        _initialXRot = transform.rotation.eulerAngles.x;

        _initialYPos = transform.position.y;
        _initialYRot = transform.rotation.eulerAngles.y;
    }

    // Update is called once per frame
    void Update()
    {
        if(!_trackChalk)
            return;

        var newPos = (chalkTip.position + initialPoint.position)/2;
        transform.position = new Vector3(_initialXPos, newPos.y, newPos.z);

        transform.rotation = Quaternion.Euler(_initialXRot, _initialYRot, Mathf.Rad2Deg * Mathf.Atan2((chalkTip.position.z - initialPoint.position.z),(chalkTip.position.y - initialPoint.position.y)) * -1 );
        transform.localScale = new Vector3(transform.localScale.x, Vector3.Distance(initialPoint.position, chalkTip.position)  * scaleMult, transform.localScale.z);
    }

    public void SetTrail(Transform initPoint, Transform newChalkTip)
    {
        initialPoint = initPoint;
        chalkTip = newChalkTip;
        _trackChalk = true;
    }

    [Button]
    public void FixTrail(Transform target)
    {
        _trackChalk = false;
        var newPos = (target.position + initialPoint.position)/2;
        transform.position = new Vector3(_initialXPos, newPos.y, newPos.z);

        transform.rotation = Quaternion.Euler(_initialXRot, _initialYRot, Mathf.Rad2Deg * Mathf.Atan2((target.position.z - initialPoint.position.z),(target.position.y - initialPoint.position.y)) * -1 );
        transform.localScale = new Vector3(transform.localScale.x, Vector3.Distance(initialPoint.position, target.position) * scaleMult, transform.localScale.z);
    }
}
