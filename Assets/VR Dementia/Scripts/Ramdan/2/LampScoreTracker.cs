using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LampScoreTracker : PlaceInScoreTracker
{
    public override void OnScoreObjectPlaced(Transform socket)
    {
        socket.GetComponentInChildren<Light>(true).gameObject.SetActive(true);
        
        base.OnScoreObjectPlaced(socket);
    }

    public override void OnScoreObjectRemoved(Transform socket)
    {
        socket.GetComponentInChildren<Light>(true).gameObject.SetActive(false);
        
        base.OnScoreObjectRemoved(socket);
    }
}
