using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class TreasureKeyQuiz : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI scoreHolder;
    [SerializeField] private UnityEvent onKeysCollected;

    private int _score;
    private int _currentRound = 0;
    private int _currentLock;

    private SequenceRandomizer _randomizer;
    private List<int> _randomSequence = new List<int>();

    private void Start()
    {
        _randomizer = GetComponent<SequenceRandomizer>();
        _randomSequence = _randomizer.GenerateRandom(3);
    }

    public void KeyLockInserted()
    {
        _currentLock++;

        if (_currentLock >= 3)
        {
            onKeysCollected?.Invoke();
        }
    }
    
    public void SubmitAnswer(int index)
    {
        if (index == _randomSequence[_currentRound])
            _score += 1;
        
        _currentRound += 1;
    }

    public void RevealScore()
    {
        float score = 0;
        for (var i = 0; i < _score; i++)
        {
            score += 1;
        }

        scoreHolder.text = score.ToString("F1");
    }
}
