using System.Collections;
using System.Collections.Generic;
using SandboxVRSeason2.Framework;
using UnityEngine;

/*
 * NOTE:
 * Index 0 = Ruang Tamu
 * Index 1 = Ruang Makan
 * Index 2 = Ruang Tidur
 */

public class TreasureKeyObjectAssign : MonoBehaviour
{
    [SerializeField] private List<SB_Socket> initialSockets;
    [SerializeField] private List<SB_Item> treasureKeys;

    private SequenceRandomizer _randomizer;
    private List<int> _randomSequence = new List<int>();
    
    // Start is called before the first frame update
    void Start()
    {
        _randomizer = GetComponent<SequenceRandomizer>();
        _randomSequence = _randomizer.GenerateRandom(3);
    }

    public void StartAssigning()
    {
        for (var i = 0; i < initialSockets.Count; i++)
        {
            initialSockets[i].slotList[0].profile = treasureKeys[_randomSequence[i]].profile;
            initialSockets[i].ForceGrabItem(treasureKeys[_randomSequence[i]]);
        }
    }
}
