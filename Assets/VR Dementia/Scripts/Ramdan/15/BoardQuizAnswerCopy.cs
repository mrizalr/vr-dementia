using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using WebSocketSharp;

public class BoardQuizAnswerCopy : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI origin;
    [SerializeField] private TextMeshPro dest;
    
    // Start is called before the first frame update
    IEnumerator Start()
    {
        while (origin.text.IsNullOrEmpty())
            yield return null;

        dest.text = origin.text;
    }
}
