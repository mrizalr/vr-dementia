using System.Collections;
using System.Collections.Generic;
using SandboxVRSeason2.Framework;
using UnityEngine;

public class BoardQuizSocketAssign : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        var socket = GetComponentInChildren<SB_Socket>();
        socket.onOccupied.AddListener(() => GetComponentInParent<BoardQuizTracker>().QuizPlaced(socket.transform));
    }
}
