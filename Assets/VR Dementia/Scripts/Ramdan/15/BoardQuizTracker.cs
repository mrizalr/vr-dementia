using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using HurricaneVR.Framework.Core;
using MEC;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class BoardQuizTracker : MonoBehaviour
{
    [SerializeField] private UnityEvent onQuizPlaced;
    [SerializeField] private GameObject[] quizList;
    [SerializeField] private TextMeshProUGUI scoreHolder;
    [SerializeField] private float delayInSec;

    private int _score;
    private int _currentIndex;

    public void QuizPlaced(Transform quiz)
    {
        if (quiz.GetComponentInChildren<QuizAnswer>().isCorrect)
            _score++;
        
        //Instantiate(quizList[_currentIndex].transform.GetChild(0).gameObject, quizList[_currentIndex].transform);
        foreach (var hvrGrabbable in quizList[_currentIndex].GetComponentsInChildren<HVRGrabbable>())
        {
            hvrGrabbable.ForceRelease();
            hvrGrabbable.CanBeGrabbed = false;
        }

        _currentIndex++;

        if (_currentIndex >= quizList.Length)
        {
            onQuizPlaced?.Invoke();
            scoreHolder.text = (_score * (.5/quizList.Length)).ToString("F1");
            return;
        }

        Timing.RunCoroutine(QuizPlacedCoroutine());
    }
    
    private IEnumerator<float> QuizPlacedCoroutine()
    {
        yield return Timing.WaitForSeconds(delayInSec);
        
        quizList[_currentIndex - 1].SetActive(false);
        quizList[_currentIndex].SetActive(true);
    }
}
