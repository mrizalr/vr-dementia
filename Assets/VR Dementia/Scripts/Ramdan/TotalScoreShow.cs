using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Linq;

public class TotalScoreShow : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI scoreHolder;
    [SerializeField] private List<GameObject> levelButtons;

    // Start is called before the first frame update
    void Start()
    {
        if(TotalScoreTracker.TotalScores == null)
        {
            TotalScoreTracker.Init();
            //return;
        }

        List<List<Score>> levelScores = TotalScoreTracker.TotalScores.ToList().Select((x, i) => new { Index = i, Value = x })
            .GroupBy(x => x.Index / 4)
            .Select(x => x.Select(v => v.Value).ToList())
            .ToList();

        Debug.Log(levelScores.Count);

        for (int i = 0; i < 5; i++)
        {
            bool finished = true;

            foreach (var item in levelScores[i])
            {
                if (!item.Finished)
                    finished = false;
            }

            levelButtons[i].GetComponent<Button>().interactable = !finished;
        }

        scoreHolder.text = TotalScoreTracker.TotalScores.Sum(x => x.CurrentScore).ToString("F1");
    }

    public void ResetTotalScore()
    {
        TotalScoreTracker.Init();
        scoreHolder.text = "0.0";

        levelButtons.ForEach((x) => { x.GetComponent<Button>().interactable = true; });
    }
}
