using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CheckQuizScore : MonoBehaviour
{
    [SerializeField] private float baseScore;
    [SerializeField] private TextMeshProUGUI scoreHolder;
    [SerializeField] private MCQuiz mcQuiz;

    private int _score;

    public void CheckScore()
    {
        scoreHolder.text = (_score * baseScore).ToString("F1");
        Debug.Log(_score * baseScore);
    }

    public void CheckAnswer(GameObject button)
    {
        var answer = button.GetComponent<QuizAnswer>();

        Debug.Log($"ANSWER: {answer.isCorrect}");

        if(answer.isCorrect)
            _score += 1;

        mcQuiz.SetNextQuiz();
    }

    public void CheckAnswer(QuizAnswer answer)
    {
        if(answer.isCorrect)
            _score += 1;

        mcQuiz.SetNextQuiz();
    }
    
}
