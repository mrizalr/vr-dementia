using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class JumbledSentenceSolver : PlaceInScoreTracker
{
    [SerializeField] private UnityEvent onSentenceSolved;
    
    public override void OnScoreObjectPlaced(Transform socket)
    {
        if(socket.parent.GetComponent<ScoreObject>().Id == socket.GetComponentInChildren<ScoreObject>().Id)
        {
            scoreTracker.TrackedObjects.Add(socket.parent.GetComponent<ScoreObject>());
        }
        
        if(!_socketDict.ContainsKey(socket))
        {
            _socketDict.Add(socket, socket.parent.GetComponent<ScoreObject>());
            _objectPlaced++;
        }
        
        if (_objectPlaced >= objectPlacedLimit)
        {
            onSentenceSolved?.Invoke();
        }
        
    }

    public override void OnScoreObjectRemoved(Transform socket)
    {
        if(scoreTracker.TrackedObjects.Contains(_socketDict[socket]))
        {
            scoreTracker.TrackedObjects.Remove(_socketDict[socket]);
        }
        
        if(_socketDict.ContainsKey(socket))
        {
            _socketDict.Remove(socket);
            _objectPlaced--;
        }
    }

    public void ShowScore()
    {
        scoreHolder.text = scoreTracker.TrackedObjects.Count == comparators.Count ? "0.5" : "0";
    }

    public void CountScore()
    {
        scoreHolder.text = scoreTracker.GetScore(scoreBase).ToString("F1");
    }
}
