using System.Collections;
using System.Collections.Generic;
using MEC;
using SandboxVRSeason2.Framework;
using UnityEngine;
using UnityEngine.Events;

public class JumbledSentenceAssign : MonoBehaviour
{
    [SerializeField] private List<SB_Item> items;
    [SerializeField] private List<SB_Socket> sockets;
    [SerializeField] private UnityEvent onAssigned;
    
    private SequenceRandomizer _randomizer;

    // Start is called before the first frame update
    void Start()
    {
        _randomizer = GetComponent<SequenceRandomizer>();
    }

    public void StartAssignment()
    {
        for (var i = 0; i < items.Count; i++)
        {
            items[i].GetComponent<ScoreObject>().SetId(i);
        }
        
        var random = _randomizer.GenerateRandom(sockets.Count);

        for (var i = 0; i < sockets.Count; i++)
        {
            sockets[i].ForceGrabItem(items[random[i]]);
        }

        Timing.RunCoroutine(WaitForSocketPopulated());
    }

    private IEnumerator<float> WaitForSocketPopulated()
    {
        foreach (var t in sockets)
        {
            while (t.GetComponentInChildren<SB_Item>() == null)
            {
                yield return Timing.WaitForOneFrame;
            }
        }

        onAssigned?.Invoke();
    }
}
