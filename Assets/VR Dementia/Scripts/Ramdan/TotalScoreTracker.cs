using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Score
{
    public bool Finished;
    public float CurrentScore;
}

public static class TotalScoreTracker
{
    public static Score[] TotalScores => _scores;

    private static Score[] _scores;
    private static bool _finished;

    public static void Init()
    {
        _scores = new Score[20];
    }

}
