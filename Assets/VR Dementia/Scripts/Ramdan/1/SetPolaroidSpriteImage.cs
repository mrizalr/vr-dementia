using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetPolaroidSpriteImage : MonoBehaviour
{
    [SerializeField] private Image originalImage;
    [SerializeField] private Material spriteMaterial;
    
    public void CopyImage()
    {
        Debug.Log("Set Image");
        spriteMaterial.mainTexture = originalImage.sprite.texture;
    }
}
