using System.Collections;
using System.Collections.Generic;
using MEC;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(SequenceRandomizer))]
public class PlaceInObjectScoreAssign : MonoBehaviour
{
    [SerializeField] private List<ScoreObject> scoreObjects = new List<ScoreObject>();
    [SerializeField] private float delayInSeconds = 1;
    [SerializeField] private UnityEvent onScoreReady;
    
    private SequenceRandomizer _randomizer;
    private List<int> _randomSequence = new List<int>();

    public void StartAssignment()
    {
        Timing.RunCoroutine(RandomizeScores());
    }

    private IEnumerator<float> RandomizeScores()
    {
        yield return Timing.WaitForSeconds(delayInSeconds);
        _randomizer = GetComponent<SequenceRandomizer>();
        _randomSequence = _randomizer.LastGeneratedSequence;
        
        for (int i = 0; i < scoreObjects.Count; i++)
        {
            scoreObjects[_randomSequence[i]].SetId(i);
        }
        onScoreReady?.Invoke();
    }
}
