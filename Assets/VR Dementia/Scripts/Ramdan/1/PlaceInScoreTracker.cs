using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class PlaceInScoreTracker : MonoBehaviour
{
    [SerializeField] protected UnityEvent OnAllObjectsPlaced;

    [SerializeField] protected List<ScoreObject> comparators = new List<ScoreObject>();
    [SerializeField] protected ScoreTracker scoreTracker;
    [SerializeField] protected int objectPlacedLimit = 3;
    [SerializeField] protected float scoreBase;
    [SerializeField] protected TextMeshProUGUI scoreHolder;
    [SerializeField] protected bool forceAscendingOrderScoring = true;

    protected Dictionary<Transform, ScoreObject> _socketDict = new Dictionary<Transform, ScoreObject>();
    
    protected int _objectPlaced;
    protected float _score;

    public void SetComparators()
    {
        for (var i = 0; i < comparators.Count; i++)
        {
            comparators[i].SetId(i);
        }
    }

    public virtual void OnScoreObjectPlaced(Transform socket)
    {
        if(socket.parent.GetComponent<ScoreObject>().Id == socket.GetComponentInChildren<ScoreObject>().Id)
            scoreTracker.TrackedObjects.Add(socket.GetComponentInChildren<ScoreObject>());

        _socketDict.Add(socket, socket.GetComponentInChildren<ScoreObject>());
        _objectPlaced++;
        if (_objectPlaced >= objectPlacedLimit)
        {
            if (scoreHolder != null)
            {
                scoreHolder.text = scoreTracker.GetScore(scoreBase, forceAscendingOrderScoring).ToString("F1");
            }

            OnAllObjectsPlaced?.Invoke();
        }
    }

    public virtual void OnScoreObjectRemoved(Transform socket)
    {
        if(scoreTracker.TrackedObjects.Contains(_socketDict[socket]))
            scoreTracker.TrackedObjects.Remove(_socketDict[socket]);
        _socketDict.Remove(socket);

        if (_objectPlaced > 0)
            _objectPlaced--;
    }
}
