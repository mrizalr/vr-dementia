using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreTracker : MonoBehaviour
{
    public List<ScoreObject> TrackedObjects => trackedObjects;
    
    [SerializeField] private List<ScoreObject> trackedObjects = new List<ScoreObject>();

    public float GetScore(float baseScore = 1, bool forceAscendingOrder = false)
    {
        int score = 0;

        for (int i = 0; i < trackedObjects.Count; i++)
        {
            if(!forceAscendingOrder) 
            {
                score++;
                continue;
            }

            if (i == trackedObjects[i].Id)
                    score++;
        };

        return score * baseScore;
    }
}
