using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreObject : MonoBehaviour
{
    public int Id => id;

    [SerializeField] private int id;

    public virtual void SetId(int id)
    {
        this.id = id;
    }
}
