using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class WaterCanOrientationCheck : MonoBehaviour
{
    [SerializeField] private Transform targetTransform;
    [SerializeField] private AxisEnum axis;
    [SerializeField] private float minAngle, maxAngle;
    [SerializeField] private UnityEvent onAngleInsideLimit, onAngleOutsideLimit;

    private bool _isInsideLimit;
    
    // Update is called once per frame
    void Update()
    {
        float currentAngle = 0;

        switch (axis)
        {
            case AxisEnum.X:
                currentAngle = targetTransform.rotation.eulerAngles.x;
                break;
            case AxisEnum.Y:
                currentAngle = targetTransform.rotation.eulerAngles.y;
                break;
            case AxisEnum.Z:
                currentAngle = targetTransform.rotation.eulerAngles.z;
                break;
        }

        if (currentAngle >= minAngle && currentAngle <= maxAngle)
        {
            if (_isInsideLimit) return;
            
            onAngleInsideLimit?.Invoke();
            _isInsideLimit = true;
        }
        else
        {
            if (!_isInsideLimit) return;
            
            onAngleOutsideLimit?.Invoke();
            _isInsideLimit = false;
        }
    }
}
