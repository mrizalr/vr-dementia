using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class TelephoneTracker : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI numberHolder;
    [SerializeField] private TextMeshProUGUI scoreHolder;
    [SerializeField] private UnityEvent onAnswerChecked;

    private bool _handleRemoved = true;
    
    private readonly int[] _numbers = {0, 0, 0, 0, 0, 0};
    private int _currentScore;
    private int _numberIndex = 0;

    private readonly int[] _answer = { 9, 9, 8, 6, 5, 7 };
    
    public void AddScore(int score) 
    {
        _currentScore += score;
    }

    private void CheckScore()
    {
        scoreHolder.text = (_currentScore * 0.75f).ToString(_currentScore % 2 == 0 ? "F1" : "F2");
    }

    public void AddNumber(int number)
    {
        if (_numberIndex >= _answer.Length)
        {
            CheckAnswer();
            return;
        }
        
        _numbers[_numberIndex] = number;
        _numberIndex += 1;
        
        PrintText();
    }

    public void RemoveNumber()
    {
        if (_numberIndex <= 0)
            return;

        _numberIndex -= 1;
        _numbers[_numberIndex] = 0;

        PrintText();
    }

    private void CheckAnswer()
    {
        if (_numbers.Sum() == 0)
        {
            return;
        }
        
        if(!_handleRemoved)
            return;

        for (int i = 0; i < _answer.Length; i++)
        {
            if (_numbers[i] != _answer[i])
            {
                onAnswerChecked?.Invoke();
                CheckScore();
                //CheckAnswer();
                return;
            }
        }

        AddScore(1);
        onAnswerChecked?.Invoke();
        CheckScore();
    }

    public void HandleRemoved(bool isRemoved)
    {
        _handleRemoved = isRemoved;
        
        CheckAnswer();
    }

    private void PrintText()
    {
        numberHolder.text = $"{_numbers[0]}{_numbers[1]}{_numbers[2]}-{_numbers[3]}{_numbers[4]}{_numbers[5]}";
    }
}
