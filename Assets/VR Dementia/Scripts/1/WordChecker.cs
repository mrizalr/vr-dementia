using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class WordChecker : MonoBehaviour
{
    private WordRandomizer wordRandomizer;
    private WordGenerator wordGenerator;

    private Image btnImage;

    private void Start()
    {
        wordRandomizer = GetComponent<WordRandomizer>();
        wordGenerator = GetComponentInParent<WordGenerator>();
        btnImage = GetComponent<Image>();

        GetComponent<SB_UIButton>().onDown.AddListener(CheckWord);
    }

    private void CheckWord()
    {
        var buttonIndex = transform.GetSiblingIndex();
        var data = wordGenerator.wordData.ToList().FirstOrDefault(e => e.index == buttonIndex);
        if (IsPatternNotValid(data))
        {
            wordGenerator.ClearSelectedWordPool();
            return;
        }

        wordGenerator.AddSelectedWordPool(buttonIndex);

        if (data.isLastWord)
        {
            foreach (var index in wordGenerator.keywordIndex)
            {
                var indexes = index.Split('-');
                var concatPoolIdx = wordGenerator.GetSelectedWordPool().Concat(wordGenerator.GetCorrectWordPool());

                if (indexes.All(e => concatPoolIdx.Contains(int.Parse(e))))
                {
                    wordGenerator.AddCorrectWordIndexPool();
                    wordGenerator.keywordIndex.Remove(index);
                    wordGenerator.onWordCleared?.Invoke();
                    break;
                }
            }
        }
    }

    private bool IsPatternNotValid(WordGenerator.WordData data)
    {
        var dataWordNeededIsEmpty = data.wordNeededIndex == "" || data.wordNeededIndex == null;
        var selectedWordPoolIsEmpty = wordGenerator.GetSelectedWordPool().Count == 0;
        if (dataWordNeededIsEmpty && !selectedWordPoolIsEmpty)
            return true;

        if ((data.wordNeededIndex != "" && data.wordNeededIndex != null) && !selectedWordPoolIsEmpty)
        {
            var wordsNeeded = data.wordNeededIndex.Split('-');
            var isExistInSelectedWord = wordsNeeded.Any(e => wordGenerator.GetSelectedWordPool().Contains(int.Parse(e)));
            var isExistInCorrectWord = wordsNeeded.Any(e => wordGenerator.GetCorrectWordPool().Contains(int.Parse(e)));

            return !isExistInCorrectWord && !isExistInSelectedWord;
        }


        return false;
    }
}
