using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FigureQuiz : MonoBehaviour
{
    [System.Serializable]
    public struct Answer
    {
        public string answerText;
        public string answerTextEn;
        public bool isCorrect;
    }

    [System.Serializable]
    public struct FigureData
    {
        public string question;
        public string questionEn;
        public bool useImage;
        public Sprite image;
        public Answer[] answers;
    }

    public FigureData[] figureDatas;
}
