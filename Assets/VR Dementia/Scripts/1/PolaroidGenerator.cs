using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PolaroidGenerator : MonoBehaviour
{
    [SerializeField] private GameObject polaroidPrefab, polaroidSB_ITEM;
    [SerializeField] private Transform polaroidPos, polaroidParent, polaroidPlaces;
    [SerializeField] private Sprite[] imageSprites;



    private int times = 3;
    [SerializeField] private float waitTime;

    private bool generating;
    private SequenceRandomizer sequenceRandomizer;

    private List<int> spriteRandomIndex;

    [Header("Check Result")] 
    public bool check = false;
    public GameObject polaroidSB_SOCKET;

    private void Start()
    {
        sequenceRandomizer = GetComponent<SequenceRandomizer>();
        spriteRandomIndex = sequenceRandomizer.GenerateRandom(imageSprites.Length);
    }

    public void GeneratePolaroid()
    {
        if (generating) return;

        StartCoroutine(IEGenerate());
        generating = true;
    }

    private IEnumerator IEGenerate()
    {
        var randomSequence = sequenceRandomizer.GenerateRandom(3);

        for (int i = 0; i < times; i++)
        {
            var polaroid = Instantiate(polaroidPrefab);
            polaroid.transform.position = polaroidPos.position;
            polaroid.transform.parent = polaroidParent;

            var polaroidAnim = polaroid.GetComponent<PolaroidPrintAnimation>();
            foreach (Transform place in polaroidPlaces)
            {
                polaroidAnim.AddPolaroidPlaces(place);
            }
            polaroidAnim.SetPlaceIndex(randomSequence[i]);

            polaroid.GetComponent<SetPolaroidImage>().SetImage(imageSprites[spriteRandomIndex[i]]);
            polaroidSB_ITEM.transform.GetChild(randomSequence[i]).GetComponentInChildren<SetPolaroidImage>().SetImage(imageSprites[spriteRandomIndex[i]]);

            if(check)
            {
                polaroidSB_ITEM.transform.GetChild(randomSequence[i]).GetComponent<ScoreObject>().SetId(i);
                polaroidSB_SOCKET.transform.GetChild(i).GetComponent<ScoreObject>().SetId(i);
            }

            yield return new WaitForSeconds(waitTime);
        }

        polaroidParent.gameObject.SetActive(false);
        polaroidSB_ITEM.SetActive(true);


    }
}
