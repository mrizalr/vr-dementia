using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuizGenerator : MonoBehaviour
{
    private int currentQuizIndex = 0;
    private List<int> randomSequence;

    private GameObject[] quizes;
    private FigureQuiz quizData;
    private SequenceRandomizer sequenceRandomizer;
    [SerializeField] private GameObject quizPrefab;

    private void Start()
    {
        quizData = GetComponent<FigureQuiz>();
        quizes = new GameObject[quizData.figureDatas.Length];

        sequenceRandomizer = GetComponent<SequenceRandomizer>();
        randomSequence = sequenceRandomizer.GenerateRandom(quizData.figureDatas.Length);

        GenerateQuiz();
    }

    public void NextQuestion()
    {
        quizes[currentQuizIndex++].SetActive(false);

        if (currentQuizIndex == quizData.figureDatas.Length) return;
        quizes[currentQuizIndex].SetActive(true);
    }

    private void GenerateQuiz()
    {
        for (int i = 0; i < quizData.figureDatas.Length; i++)
        {
            var q = Instantiate(quizPrefab, transform);
            q.SetActive(false);
            q.GetComponent<Quiz>().Init(randomSequence[i]);

            quizes[i] = q;
        }

        quizes[currentQuizIndex].SetActive(true);
    }
}
