using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SandboxVRSeason2.Framework;
using HurricaneVR.Framework.Core.Bags;
using System;
using UnityEngine.Events;

public class QuizAnswerChecker : MonoBehaviour
{
    private SB_Socket socket;
    private HVRTriggerGrabbableBag bag;

    private QuizGenerator quizGenerator;

    public UnityEvent onCorrectAnswer;

    private void Start()
    {
        quizGenerator = GetComponentInParent<QuizGenerator>();

        socket = GetComponent<SB_Socket>();
        socket.onOccupied.AddListener(OnSocketOccupied);
    }

    private void OnSocketOccupied()
    {
        if (bag == null) bag = socket.GetComponent<HVRTriggerGrabbableBag>();

        var answer = bag.ValidGrabbables[0];
        var answerData = answer.GetComponent<QuizAnswer>();

        if (answerData.isCorrect) OnCorrectAnswer();
    }

    private void OnCorrectAnswer()
    {
        onCorrectAnswer?.Invoke();
        quizGenerator.NextQuestion();
    }
}
