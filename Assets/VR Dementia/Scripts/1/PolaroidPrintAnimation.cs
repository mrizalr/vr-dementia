using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PolaroidPrintAnimation : MonoBehaviour
{
    [SerializeField] private float endPrintingXPosition;
    [SerializeField] private List<Transform> placePosition = new List<Transform>();

    private int placesIndex;

    void Start()
    {
        StartCoroutine(PrintingAnimation());
    }

    public void SetPlaceIndex(int placesIndex)
    {
        this.placesIndex = placesIndex;
    }

    public void AddPolaroidPlaces(Transform place)
    {
        placePosition.Add(place);
    }

    private IEnumerator PrintingAnimation()
    {
        yield return new WaitForSeconds(1f);
        transform.DOLocalMoveX(endPrintingXPosition, 3f);
        yield return new WaitForSeconds(3f);
        StartCoroutine(Place());
    }

    private IEnumerator Place()
    {
        yield return null;
        transform.DOLocalRotate(placePosition[placesIndex].eulerAngles, .3f).SetEase(Ease.InSine);
        transform.DOLocalMove(placePosition[placesIndex].localPosition, .5f).SetEase(Ease.InSine);
    }
}
