using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using HurricaneVR.Framework.Core;

public class Quiz : MonoBehaviour
{
    [SerializeField] private GameObject imageQuestionObj, numberQuestionObj, imageAnswerObj, numberAnswerObj;
    private Image figureImage;
    private TextMeshProUGUI numberTMP;
    private QuizAnswer[] imageAnswer, numberAnswer;

    private SequenceRandomizer sequenceRandomizer;
    private FigureQuiz figureQuiz;

    public void Init(int quizIdx)
    {
        sequenceRandomizer = GetComponentInParent<SequenceRandomizer>();
        figureQuiz = GetComponentInParent<FigureQuiz>();

        figureImage = imageQuestionObj.GetComponent<Image>();
        numberTMP = numberQuestionObj.GetComponent<TextMeshProUGUI>();
        imageAnswer = imageAnswerObj.GetComponentsInChildren<QuizAnswer>();
        numberAnswer = numberAnswerObj.GetComponentsInChildren<QuizAnswer>();

        GenerateQuestion(quizIdx);
    }

    private void OnEnable()
    {
        var grabbable = GetComponentsInChildren<HVRGrabbable>();
        foreach (var g in grabbable)
        {
            g.enabled = true;
        }
    }

    private void OnDisable()
    {
        var colliders = GetComponentsInChildren<HVRGrabbable>();
        foreach (var col in colliders)
        {
            col.enabled = false;
        }
    }

    private void GenerateQuestion(int index)
    {
        var quizData = figureQuiz.figureDatas[index];

        if (quizData.useImage)
        {
            imageQuestionObj.SetActive(true);
            imageAnswerObj.SetActive(true);
            numberQuestionObj.SetActive(false);
            numberAnswerObj.SetActive(false);

            figureImage.sprite = quizData.image;
        }
        else
        {
            imageQuestionObj.SetActive(false);
            imageAnswerObj.SetActive(false);
            numberQuestionObj.SetActive(true);
            numberAnswerObj.SetActive(true);

            numberTMP.text = quizData.question;
        }

        RandomizeAnswer(quizData);
    }

    private void RandomizeAnswer(FigureQuiz.FigureData data)
    {
        var randomAnswerSequence = sequenceRandomizer.GenerateRandom(4);
        var usedAnswer = data.useImage ? imageAnswer : numberAnswer;

        for (int i = 0; i < usedAnswer.Length; i++)
        {
            var answer = data.answers[randomAnswerSequence[i]].answerText;
            var answerEn = data.answers[randomAnswerSequence[i]].answerTextEn;
            var isCorrect = data.answers[randomAnswerSequence[i]].isCorrect;

            usedAnswer[i].Init(answer, answerEn, isCorrect);
        }
    }

    // public void Next()
    // {
    //     if (quizIdx == figureQuiz.figureDatas.Length - 1) return;
    //     GenerateQuestion(questionSequence[++quizIdx]);
    // }


}
