using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class WordRandomizer : MonoBehaviour
{
    private TextMeshProUGUI TMP;
    [SerializeField] private string reservedWord;

    public void Init(string reservedWord)
    {
        this.reservedWord = reservedWord;
    }

    private void Start()
    {
        TMP = GetComponentInChildren<TextMeshProUGUI>();
        RandomWord();
    }

    private void RandomWord()
    {
        if (reservedWord != "")
        {
            TMP.text = reservedWord;
            return;
        }

        var firstLetter = 'A';
        var rdm = Random.Range((int)firstLetter, (int)firstLetter + 26);

        TMP.text = char.ConvertFromUtf32(rdm);
    }
}
