using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LampHandler : MonoBehaviour
{
    [SerializeField] private GameObject[] lamps;

    [SerializeField] private GameObject lampsSB_ITEM;
    [SerializeField] private GameObject lampsAnimation;

    private SequenceRandomizer sequenceRandomizer;
    private bool isLampOn;

    [Header("Check Result")]
    public bool check = false;
    public GameObject lampsSB_SOCKET;

    private void Start()
    {
        sequenceRandomizer = GetComponent<SequenceRandomizer>();
    }

    public void TurnOnLamp()
    {
        if (isLampOn) return;
        StartCoroutine(IETurnOnLamp());
        isLampOn = true;
    }

    private IEnumerator IETurnOnLamp()
    {
        var randomSequence = sequenceRandomizer.GenerateRandom(3);
        for (int i = 0; i < lamps.Length; i++)
        {
            var randomIndex = randomSequence[i];
            yield return new WaitForSeconds(1f);
            lamps[randomIndex].transform.GetChild(0).gameObject.SetActive(true);
            yield return new WaitForSeconds(2f);
            lamps[randomIndex].transform.GetChild(0).gameObject.SetActive(false);

            if(check)
            {
                lampsSB_ITEM.transform.GetChild(randomIndex).GetComponent<ScoreObject>().SetId(i);
                lampsSB_SOCKET.transform.GetChild(i).GetComponent<ScoreObject>().SetId(i);
            }
        }
        lampsAnimation.SetActive(false);
        lampsSB_ITEM.SetActive(true);
    }
}
