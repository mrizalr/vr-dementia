using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.Localization.Settings;

public class WordGenerator : MonoBehaviour
{
    [System.Serializable]
    public struct WordData
    {
        public int index;
        public string reservedWord;
        public string reservedWordEn;
        public string wordNeededIndex;
        public bool isLastWord;
    }

    public WordData[] wordData;

    [SerializeField] private List<int> correctWordIndexPool = new List<int>();
    [SerializeField] private List<int> selectedWordIndexesPool = new List<int>();

    public List<string> keywordIndex;

    [SerializeField] private Color selectColor;
    [SerializeField] private Sprite correctImage;

    public UnityEvent onWordCleared;

    public void AddSelectedWordPool(int index)
    {
        transform.GetChild(index).GetComponent<Image>().color = selectColor;
        transform.GetChild(index).GetComponent<SB_UIButton>().enabled = false;
        selectedWordIndexesPool.Add(index);
    }

    public void ClearSelectedWordPool()
    {
        foreach (var idx in selectedWordIndexesPool)
        {
            transform.GetChild(idx).GetComponent<Image>().color = Color.white;
            transform.GetChild(idx).GetComponent<SB_UIButton>().enabled = true;
        }
        selectedWordIndexesPool = new List<int>();
    }

    public void AddCorrectWordIndexPool()
    {
        foreach (int idx in selectedWordIndexesPool)
        {
            transform.GetChild(idx).GetComponent<Image>().sprite = correctImage;
            transform.GetChild(idx).GetComponent<Image>().color = Color.white;
            transform.GetChild(idx).GetComponentInChildren<TMPro.TextMeshProUGUI>().color = new Color32(37, 160, 176, 255);
            correctWordIndexPool.Add(idx);
        }
        selectedWordIndexesPool = new List<int>();
    }

    public List<int> GetSelectedWordPool()
    {
        return selectedWordIndexesPool;
    }

    public List<int> GetCorrectWordPool()
    {
        return correctWordIndexPool;
    }

    private void Awake()
    {
        foreach (var w in wordData)
        {
            var wordBubble = transform.GetChild(w.index);
            var randomizer = wordBubble.GetComponent<WordRandomizer>();
            var word = LocalizationSettings.SelectedLocale.Identifier.Code == "id" ? w.reservedWord : w.reservedWordEn;
            randomizer.Init(word);
        }
    }
}
