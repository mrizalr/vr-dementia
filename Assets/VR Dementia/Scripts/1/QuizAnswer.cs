using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class QuizAnswer : MonoBehaviour
{
    public GeneratedItemLocalization answerText;
    public bool isCorrect;
    private void Awake()
    {
        answerText = GetComponentInChildren<GeneratedItemLocalization>();
    }

    public void Init(string answerText, string answerTextEn, bool isCorrect)
    {
        this.answerText.SetupString(answerText, answerTextEn);
        this.isCorrect = isCorrect;
    }
}
