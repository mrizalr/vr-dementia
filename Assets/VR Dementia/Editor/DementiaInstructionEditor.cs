using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEditor;
using UnityEditor.Events;

public class DementiaInstructionEditor : MonoBehaviour
{
    public GameObject uiInstructionPrefab;
    public GameObject uiCompletePrefab;
    public GameObject dotIndicatorPrefab;
    public DementiaInstructionData instructionData;
    public List<GameObject> instructionsSpawned;

    [Button] public void InitializeInstruction()
    {

        for(int i = 0; i < instructionData.instruction.Count; i++)
        {
            GameObject instruction = PrefabUtility.InstantiatePrefab(uiInstructionPrefab, transform) as GameObject;
            instruction.transform.SetSiblingIndex(i);
            instruction.SetActive(false);
            instructionsSpawned.Add(instruction);
            instruction.name = "Instruction (" + i + ")";
            DementiaInstructionHolder holder = instruction.GetComponent<DementiaInstructionHolder>();
            holder.titleCodeText.text = instructionData.titleCode;
            holder.titleText.text = instructionData.titleText;
            holder.instructionText.text = instructionData.instruction[i].instructionText;
            holder.instructionImage.sprite = instructionData.instruction[i].instructionImage;
            for(int j = 0; j < instructionData.instruction.Count; j++)
            {
                GameObject dot = Instantiate(dotIndicatorPrefab, holder.panelDotIndicator);
                if(j != i ) dot.GetComponent<Image>().color = new Color(1,1,1,0.5f);
            }
        }

        GameObject complete = PrefabUtility.InstantiatePrefab(uiCompletePrefab, transform) as GameObject;
        complete.name = "Complete";
        complete.SetActive(false);

        for(int i = 0; i < instructionsSpawned.Count; i++) 
        {
            DementiaInstructionHolder holder = instructionsSpawned[i].GetComponent<DementiaInstructionHolder>();

            if(i == 0)
            {
                holder.gameObject.SetActive(true);
                holder.instructionPrev.interactable = false;

                UnityAction<bool> deactivateCurrent = new UnityAction<bool>(instructionsSpawned[i].SetActive);
                UnityAction<bool> activateNext = new UnityAction<bool>(instructionsSpawned[i+1].SetActive);
                UnityEventTools.AddBoolPersistentListener(holder.instructionNext.onClick, deactivateCurrent, false);
                UnityEventTools.AddBoolPersistentListener(holder.instructionNext.onClick, activateNext, true);
            }
            else if(i == instructionData.instruction.Count - 1)
            {
                holder.instructionNext.interactable = false;

                UnityAction<bool> deactivateCurrent = new UnityAction<bool>(instructionsSpawned[i].SetActive);
                UnityAction<bool> activatePrev = new UnityAction<bool>(instructionsSpawned[i-1].SetActive);
                UnityEventTools.AddBoolPersistentListener(holder.instructionPrev.onClick, deactivateCurrent, false);
                UnityEventTools.AddBoolPersistentListener(holder.instructionPrev.onClick, activatePrev, true);
            }
            else
            {
                UnityAction<bool> deactivateCurrent = new UnityAction<bool>(instructionsSpawned[i].SetActive);
                UnityAction<bool> activateNext = new UnityAction<bool>(instructionsSpawned[i+1].SetActive);
                UnityAction<bool> activatePrev = new UnityAction<bool>(instructionsSpawned[i-1].SetActive);

                UnityEventTools.AddBoolPersistentListener(holder.instructionNext.onClick, deactivateCurrent, false);
                UnityEventTools.AddBoolPersistentListener(holder.instructionNext.onClick, activateNext, true);
                UnityEventTools.AddBoolPersistentListener(holder.instructionPrev.onClick, deactivateCurrent, false);
                UnityEventTools.AddBoolPersistentListener(holder.instructionPrev.onClick, activatePrev, true);
            }
        }

        instructionsSpawned.Clear();
    }
}
